import pkg from 'gulp';
const {src, dest, watch, series} = pkg;
// import * as sass from 'sass';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
const sass = gulpSass(dartSass);
import ts from "gulp-typescript";
const tsProject = ts.createProject('tsconfig.json');

function buildStyles() {
    return src('overrides/assets/scss/**/*.scss')   // set the source Sass directory
    .pipe(sass())                                   // to pipe into the sass compiler
    .pipe(dest('overrides/assets/stylesheets'))     // to pipe into the destination function
}

function buildJavascript() {
    return src('overrides/assets/typescripts/**/*.ts')  // set the source Typescript directory
    .pipe(tsProject())                                  // to pipe into Typescript compiler
    .pipe(dest("overrides/assets/javascripts"))         // to pipe into the destionation function
}

function watchChanges() {
    watch(['overrides/assets/scss/**/*.scss'], buildStyles)
}

// export default series(buildStyles, buildJavascript, watchChanges)
export default series(buildStyles, watchChanges)