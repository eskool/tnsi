import gm_tableaux_dev;
size(8cm);
 
string[] x1={"-3","2"},
           y1={"-5","4"};
real[]   h1={0,1};
 
string[] x2={"-inf","0","+inf"},
           y2={"+inf","0","+inf"};
real[]   h2={2,0,1};
 
picture tab1=tabvar(x1,y1,h1),
           tab2=tabvar(var="t",fonct="g",x2,y2,h2);
unitsize(tab1,20);
unitsize(tab2,20);
add(tab1.fit(),(0,0),W);
add(tab2.fit(),(0,0),E);
