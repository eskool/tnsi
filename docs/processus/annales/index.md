# TNSI : Annales sur OS & Processus

## Exercice 1 : Métropole 2021, Sujet 1, Partie A Processus

<env>(Exercice 2 du sujet)</env>

Cet exercice porte sur la gestion des processus par les systèmes d'exploitation et sur les opérateurs booléens.

**Partie A**  
Cette partie est un questionnaire à choix multiples (QCM).  
Pour chacune des questions, une seule des quatre réponses est exacte. Le candidat indiquera sur sa copie le numéro de la question et la lettre correspondant à la réponse exacte.  
Aucune justification n'est demandée. Une réponse fausse ou une absence de réponse n'enlève aucun point.  
1. Parmi les commandes ci-dessous, laquelle permet d'afficher les processus en cours
d'exécution ?  
a. dir  
b. ps  
c. man  
d. ls  
2. Quelle abréviation désigne l'identifiant d'un processus dans un système d'exploitation de type UNIX ?  
a. PIX  
b. SIG  
c. PID  
d. SID  
3. Comment s'appelle la gestion du partage du processeur entre différents processus ?  
a. L'interblocage  
b. L'ordonnancement  
c. La planification  
d. La priorisation  
4. Quelle commande permet d'interrompre un processus dans un système d'exploitation de type UNIX ?  
a. stop  
b. interrupt  
c. end  
d. kill  

**Partie B**  
1. Un processeur choisit à chaque cycle d'exécution le processus qui doit être exécuté. Le tableau ci-dessous donne pour trois processus P1, P2, P3 :  

* la durée d'exécution (en nombre de cycles)  
* l'instant d'arrivée sur le processeur (exprimé en nombre de cycles à partir de 0)  
* le numéro de priorité  
Le numéro de priorité est d'autant plus petit que la priorité est grande. On suppose qu'à chaque instant, c'est le processus qui a le plus petit numéro de priorité qui est exécuté, ce qui peut provoquer la suspension d'un autre processus, lequel reprendra lorsqu'il sera le plus prioritaire.  

<center>

| Processus | Durée d'exécution | Instant d'arrivée | Numéro de priorité |
|:-:|:-:|:-:|:-:|
| P1 | 3 | 3 | 1 |
| P2 | 3 | 2 | 2 |
| P3 | 4 | 0 | 3 |

</center>

Reproduire le tableau ci-dessous sur la copie et indiquer dans chacune des cases le processus exécuté à chaque cycle.  
![Processus Exécuté par Cycle](./img/2021/metropole-sujet1-partieB-1.png){.center}
2. On suppose maintenant que les trois processus précédents s'exécutent et utilisent une ou plusieurs ressources parmi R1, R2 et R3.  
Parmi les scénarios suivants, lequel provoque un interblocage ? Justifier.  

!!! col __33
    ```console
       Scénario 1
    P1 acquiert R1
    P2 acquiert R2
    P3 attend R1
    P2 libère R2
    P2 attend R1
    P1 libère R1
    ```

!!! col __33
    ```console
       Scénario 2
    P1 acquiert R1
    P2 acquiert R3
    P3 acquiert R2
    P1 attend R2
    P2 libère R3
    P3 attend R1
    ```

!!! col __33
    ```console
       Scénario 3
    P1 acquiert R1
    P2 acquiert R2
    P3 attend R2
    P1 attend R2
    P2 libère R2
    P3 acquiert R2
    ```

La Partie C traite de Cryptographie (non recopiée ici)

## Exercice 2 : Métropole 2021, Sujet 2

<env>(Exercice 4 du sujet)</env>

Cet exercice porte sur les systèmes d'exploitation : gestion des processus et des ressources.  
Les parties A et B peuvent être traitées indépendamment.  

**Partie A**  
Dans un bureau d'architectes, on dispose de certaines ressources qui ne peuvent être utilisées simultanément par plus d'un processus, comme l'imprimante, la table traçante, le modem.  
Chaque programme, lorsqu'il s'exécute, demande l'allocation des ressources qui lui sont nécessaires. Lorsqu'il a fini de s'exécuter, il libère ses ressources.

!!! col __33
    ```console
    Programme 1
    demander (table traçante)
    demander (modem)
    exécution
    libérer (modem)
    libérer (table traçante)
    ```

!!! col __33
    ```console
    Programme 2
    demander (modem)
    demander (imprimante)
    exécution
    libérer (imprimante)
    libérer (modem)
    ```

!!! col __33
    ```console
    Programme 3
    demander (imprimante)
    demander (table traçante)
    exécution
    libérer (table traçante)
    libérer (imprimante)
    ```

On appelle p1, p2 et p3 les processus associés respectivement aux programmes 1, 2 et 3.  
1. Les processus s'exécutent de manière concurrente.  
Justifier qu'une situation d'interblocage peut se produire.  
2. Modifier l'ordre des instructions du programme 3 pour qu'une telle situation ne puisse pas se produire. Aucune justification n'est attendue.  
3. Supposons que le processus p1 demande la table traçante alors qu'elle est en cours d'utilisation par le processus p3. Parmi les états suivants, quel sera l'état du processus p1 tant que la table traçante n'est pas disponible :  
a) élu b) bloqué c) prêt d) terminé

**Partie B**  
Avec une ligne de commande dans un terminal sous Linux, on obtient l'affichage suivant :  

![UID, PID, C, STIME, TTY, TIME, CMD](./img/2021/metropole-sujet2.png)

La documentation Linux donne la signification des différents champs :  

* UID : identifiant utilisateur effectif ;
* PID : identifiant de processus ;
* PPID : PID du processus parent ;
* C : partie entière du pourcentage d'utilisation du processeur par rapport au temps de vie des processus ;
* STIME : l'heure de lancement du processus ;
* TTY : terminal de contrôle
* TIME : temps d'exécution
* CMD : nom de la commande du processus  

<clear></clear>

1. Parmi les quatre commandes suivantes, laquelle a permis cet affichage ?  
    a. ls -l  
    b. ps –ef  
    c. cd ..  
    d. chmod 741 processus.txt  
2. Quel est l'identifiant du processus parent à l'origine de tous les processus concernant le navigateur Web (chromium-browser) ?  
3. Quel est l'identifiant du processus dont le temps d'exécution est le plus long ?  

## Exercice 3 : Métropole 2021, Candidat Libre Sujet 2

<env>(Exercice 2 du sujet)</env>

Cet exercice porte sur la gestion des processus et des ressources par un système d'exploitation.

1. Les états possibles d'un processus sont : prêt, élu, terminé et bloqué.  
    a. Expliquer à quoi correspond l'état élu.  
    b. Proposer un schéma illustrant les passages entre les différents états.  
2. On suppose que quatre processus C1, C2, C3 et C4 sont créés sur un ordinateur, et qu'aucun autre processus n'est lancé sur celui-ci, ni préalablement ni pendant l'exécution des quatre processus. L'ordonnanceur, pour exécuter les différents processus prêts, les place dans une structure de données de type file. Un processus prêt est enfilé et un processus élu est défilé.  
    a. Parmi les propositions suivantes, recopier celle qui décrit le fonctionnement des entrées/sorties dans une file :  
    i. Premier entré, dernier sorti  
    ii. Premier entré, premier sorti  
    iii. Dernier entré, premier sorti  
    b. On suppose que les quatre processus arrivent dans la file et y sont placés dans l'ordre C1, C2, C3 et C4.  
    * Les temps d'exécution totaux de C1, C2, C3 et C4 sont respectivement 100 ms, 150 ms, 80 ms et 60 ms.
    * Après 40 ms d'exécution, le processus C1 demande une opération d'écriture disque, opération qui dure 200 ms. Pendant cette opération d'écriture, le processus C1 passe à l'état bloqué.
    * Après 20 ms d'exécution, le processus C3 demande une opération d'écriture disque, opération qui dure 10 ms. Pendant cette opération d'écriture, le processus C3 passe à l'état bloqué.
    Sur la frise chronologique donnée ci-dessous, les états du processus C2 sont donnés. Compléter la frise avec les états des processus C1, C3 et C4.  
    ![Annexe relocalisée](./img/2021/metropole-cl-sujet2-annexe.png)  
3. On trouvera ci- dessous deux programmes rédigés en pseudo-code.  
Verrouiller un fichier signifie que le programme demande un accès exclusif au fichier et l'obtient si le fichier est disponible.  
    <center>
    
    !!! col __50 center
        ```pseudo
        Programme 1
        Verrouiller fichier_1
        Calculs sur fichier_1
        Verrouiller fichier_2
        Calculs sur fichier_1
        Calculs sur fichier_2
        Calculs sur fichier_1
        Déverrouiller fichier_2
        Déverrouiller fichier_1
        ```
    
    !!! col __50 center clear
        ```pseudo
        Programme 2
        Verrouiller fichier_2
        Verrouiller fichier_1
        Calculs sur fichier_1
        Calculs sur fichier_2
        Déverrouiller fichier_1
        Déverrouiller fichier_2
        ```
    </center>
    a. En supposant que les processus correspondant à ces programmes s'exécutent simultanément (exécution concurrente), expliquer le problème qui peut être rencontré.  
    b. Proposer une modification du programme 2 permettant d'éviter ce problème.

## Exercice 4 : Polynesie 2021, Sujet 1

<env>(Exercice 4 du sujet)</env>

**Partie B : Système d'exploitation**
Un système d'exploitation doit assurer la gestion des processus et des ressources.  
1. Dans ce contexte, expliquer et illustrer par un exemple ce qu'est une situation d'interblocage (deadlock).  
2. Citer des mécanismes permettant d'éviter ces situations.  

## Exercice 5 : Métropole 2021, Candidat Libre Sujet 1, Partie A Processus

<env>(Exercice 3 du sujet)</env>

La partie A de cet exercice porte sur la gestion des processus par un système d'exploitation.

**Partie A : Processus**
La commande UNIX `ps` présente un cliché instantané des processus en cours d'exécution.  
Avec l'option `−eo pid,ppid,stat,command`, cette commande affiche dans l'ordre l'identifiant du processus `PID` (process identifier), le `PPID` (parent process identifier), l'état `STAT` et le nom de la commande à l'origine du processus.  

Les valeurs du champ `STAT` indique l'état des processus :  
`R` : processus en cours d'exécution  
`S` : processus endormi  

Sur un ordinateur, on exécute la commande `ps −eo pid,ppid,stat,command` et on obtient un affichage dont on donne ci-dessous un extrait.

```bash
$ ps -eo pid,ppid,stat,command
PID  PPID STAT COMMAND
1    0    Ss  /sbin/init
.... .... ..  ...
1912 1908 Ss  Bash
2014 1912 Ss  Bash
1920 1747 Sl  Gedit
2013 1912 Ss  Bash
2091 1593 Sl  /usr/lib/firefox/firefox
5437 1912 Sl  python programme1.py
5440 2013 R   python programme2.py
5450 1912 R+  ps -eo pid,ppid,stat,command
```

À l'aide de cet affichage, répondre aux questions ci-dessous.  
1. Quel est le nom de la première commande exécutée par le système d'exploitation lors du démarrage ?  
2. Quels sont les identifiants des processus actifs sur cet ordinateur au moment de l'appel de la commande `ps` ? Justifier la réponse.
3. Depuis quelle application a-t-on exécuté la commande `ps` ?  
Donner les autres commandes qui ont été exécutées à partir de cette application.
4. Expliquer l'ordre dans lequel les deux commandes `python programme1.py` et `python programme2.py` ont été exécutées.
5. Peut-on prédire que l'une des deux commandes `python programme1.py` et `python programme2.py` finira avant l'autre ?  

(La Partie B est indépendante de la partie A, et concerne le Routage: elle ne sera pas citée ici)