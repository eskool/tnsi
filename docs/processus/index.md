# TNSI : OS - Processus & Ressources

| Contenus | Capacités Attendues | Commentaires |
|:-:|:-:|:-:|
| Gestion des processus <br/>et des ressources<br/> par un système d’exploitation. | Décrire la création d’un processus,<br/>l’ordonnancement de plusieurs<br/>processus par le système.<br/>Mettre en évidence le risque<br/> de l’interblocage (deadlock). | À l’aide d’outils standard, il<br/>s’agit d’observer les processus<br/>actifs ou en attente sur une<br/>machine.<br/>Une présentation débranchée<br/>de l’interblocage peut être<br/>proposée. |

!!! info "Problématique"
    Comment un Système d'Exploitation multitâche fait-il pour exécuter de nombreux programmes simultanément, alors que la machine ne possède qu'un seul et unique processeur, avec un nombre de coeurs très limités?

## Introduction

Dans les années $1970$ les ordinateurs personnels n'étaient pas capables d'exécuter plusieurs tâches à la fois : on lancait un programme et on y restait jusqu'à ce que celui-ci plante ou se termine. Les <bred>OS - Operating System</bred> :gb: / <bred>SE - Systèmes d'Exploitation</bred> :fr: récents comme par ex. Linux, OSX ou Windows, permettent d'exécuter plusieurs <bred>tâches</bred> :fr: / <bred>tasks</bred> :gb: simultanément - ou du moins donnent l'illusion que celles-ci s'exécutent en même temps (**pseudo-parallélisme**). À un instant donné, il n'y a pas un mais plusieurs **programmes** qui sont **en cours d'exécution** sur un ordinateur : on les nomme des **processus** (lourds).

Un utilisateur manipule des processus tous les jours sans le savoir :

* quand il (double-)clique sur l'icône d'un programme, il provoque la naissance d'un ou plusieurs processus liés au programme lancé
* quand un programme ne répond plus, il peut visualiser les tâches actives (en Ligne de Commande sur Linux et sur MacOS, ou en lançant le gestionnaire de tâches sur Windows) pour tuer le processus posant problème

Par exemple, sur un même PC, en même temps, on peut trouver les processus suivants :

* le navigateur **Firefox** télécharge un fichier, pendant que
* un flux vidéo est en lecture sur **VLC**
* Le traitement de texte **LibreOffice Writer** est en train d'être utilisé, lui-même décomposé en **sous-tâches** / **sous-processus** pouvant être lancées indépendamment les unes des autres. Ces sous-tâches sont souvent appelées des **fils d'exécution** / **processus légers** / **threads**.  Par exemple :
    * une impression
    * une recherche textuelle
    * la création d'un sommaire

## Notion de Processus

### Définition

!!! def "Processus (lourd)"
    Dans une machine donnée, un <bred>Processus</bred> :fr: / <bred>Process</bred> :gb: est **un programme en train d'être exécuté**. On dit aussi qu'un processus (dédié) est <bred>une instance d'exécution d'un programme</bred>. Un processus s'appelle quelquefois un <red>processus lourd</red>, ou <red>tâche lourde</red>.

### Programme vs Processus

Un **programme** se distingue de son exécution (**processus**) :

* Un programme (par ex. un tableur comme Libre Office Calc) est un jeu d'instructions, chargé en mémoire de masse, **susceptibles d'être exécutées**. Ce n'est dans ce cas pas (encore) un processus.
* Un programme **exécuté** devient un **processus**. Le lancement d'un programme entraîne des lectures/écritures de registres et d'une partie de la mémoire. Un processus dispose en outre de **ressources** propres (espaces mémoires, fichiers, accès au réseau, etc..). D'ailleurs un même programme peut être exécuté plusieurs fois sur une même machine au même moment, en occupant des espaces mémoire différents.

### Exécution d'un Programme

A son lancement, les instructions qui composent un programme sont chargées en mémoire de travail (RAM) et exécutées. Le programme devient ainsi un processus. 

Imaginons qu'on lance une deuxième fois le même programme, sans avoir fermé le premier en cours d'exécution (par ex. pour éditer deux fichiers distincts dans un traitement de texte). Le même programme est recopié en mémoire, mais dans un autre endroit, puis il est de nouveau exécuté : on obtient ainsi une deuxième instance. On a alors en mémoire deux instances d'un même programme qui s'exécutent simultanément, mais chacune dans un processus différent (classiquement appelés `P1` et `P2`)

## Hérédité des Processus

!!! def "Processus Parents & Enfants"
    Un processus, qui sera dans ce cas appelé <red>processus parent</red>, peut créer un ou plusieurs processus, dits <red>processus enfants</red>

## Références et Notes

[^1]: [Guillaume Connan, Processus & Ressources](https://gitlab.com/lyceeND/tale/-/blob/master/2020_21/5-Archi_Reseau/8_processus_ressources.md)
[^2]: [Olivier LECLUSE, Gestion des Processus](https://www.lecluse.fr/nsi/NSI_T/archi/process/)
[^3]: [Processus & Threads, Linux Attitude](https://linux-attitude.fr/post/processus-et-threads)
[^4]: [La Gestion des Processus, CNAM, Dept Info](http://deptinfo.cnam.fr/Enseignement/CycleA/AMSI/cours_systemes/12_processus/proces.htm)