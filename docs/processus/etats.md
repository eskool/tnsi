# TNSI : OS - États d'un Processus

## Introduction

Tous les OS *modernes* Linux, macOS, Windows, Android, iOS... sont capables de gérer l'exécution de plusieurs processus "*en même temps*". Mais cette simultanéité n'est qu'une illusion : il s'agit plutôt d'un *chacun son tour*, *petit à petit*, que d'un simultané[^4].

En réalité: 

<center>

<enc>Le processeur ne peut pas exécuter plus d'instructions simultanées qu'il n'a de coeurs</enc>

</center>

Pourtant, une machine avec $4$ coeurs peut exécuter simultanément $23$ programmes: pour réussir cet exploit, l'OS joue un rôle de chef d'orchestre:

* l'OS exécute chaque programme à tour de rôle, en répartissant le **temps de connexion / d'exécution au processeur** (On parle de **pseudo-parallélisme**)
* l'OS partage les **ressources** dont chaque processus a besoin en termes de **mémoire**, **entrées-sorties**, et 
* l'OS s'assure que les processus ne se gênent pas les uns les autres.

Pour pouvoir faire tout cela, le Système d'Exploitation est amené à définir des **états** distincts pour les processus : <rb>Nouveau</rb>, <rb>Prêt</rb>, <rb>Élu</rb>, <rb>Bloqué</rb>, <rb>Terminé</rb>, etc..

## Des États principaux

### État Nouveau

L'<rb>État Nouveau</rb> correspond à un un programme qui a été sélectionné pour être démarré. Ses instructions on été recopiées en mémoire par l'OS et un nouveau processus y a été attaché, mais jamais encore exécuté. Son contexte d'exécution et son PCB doivent être préparés et il doit être inséré dans la liste des processus **Prêts**.

### État Prêt

Le processus est dans l'<rb>État Prêt</rb> :fr: / <rb>Ready (to run)</rb> :gb: lorsque:

* le processus a été créé, et
* il dispose de toutes les ressources (réseau, lecture disque, USB, bluetooth, action utilisateur - clic, mot de passe, etc..) pour effectuer ses opérations. 

Il reste néanmoins en attente de pouvoir accéder au processeur (en attente de quantum de temps), pour passer à l'**état élu**.

### État Élu

Le Processus est dans l'<rb>État Élu</rb> / <rb>en Cours d'Exécution</rb> :fr: / <rb>Running</rb> :gb: lorsqu'il obtient l'accès au processeur, et qu'on lui a attribué un ou plusieurs quantum de temps. 

* Une fois dans l'état Élu, il exécute toutes ses instructions jusqu'à écoulement du temps imparti (s'il ne lui manque aucune ressource). 
* <rb>Blocage</rb> par manque de ressources : Un processus Élu peut aussi demander à accéder à une ressource pas forcément disponible instantanément (par ex. lire une donnée sur un disque dur). Le processus élu ne pourra alors pas poursuivre son exécution tant qu'il n'obtient pas cette ressource : Il passe alors de l'état Élu à l'**état Bloqué**, par manque de ressources. 
* <rb>Préemption</rb> : Cette transition correspond à une opération de réquisition (**Préemption**) du processus Élu par le processeur **alors que celui-ci dispose de toutes les ressources nécessaires à la poursuite de son exécution**. Il existe deux types d'ordonnancement différents : <rb>préemptif</rb>, ou <rb>non préemptif</rb>. Les deux existent :  

    !!! def "Ordonnancement Préemptif vs Non Préemptif"
        Selon que l'opération de préemption (réquisition) du processeur est autorisée ou non, l'**ordonnancement** est dit <rb>préemptif</rb> ou <rb>non préemptif</rb> :

        * si l'ordonnancement est <rb>préemptif</rb>, la transition de l'état élu vers l'état prêt est autorisée : un processus quitte le processeur si il a terminé son exécution , si il se bloque ou si le processeur est réquisitionné.
        * si l'ordonnancement est <rb>non préemptif</rb>, la transition de l'état élu vers l'état prêt est interdite : un processus quitte le processeur si il a terminé son exécution ou si il se bloque.

* Bien que les OS permettent de gérer plusieurs processus avec une illusion de simultanéité, **un seul processus peut se trouver dans un état élu** : le microprocesseur ne peut *s'occuper* que d'un seul processus à la fois.


### État Bloqué

Le processus est dans l'état <rb>État Bloqué</rb> / <rb>en Attente</rb> :fr: / <rb>Blocked</rb> :gb: lorsqu'il est en attente :

* d'une ou plusieurs ressources (par exemple, en attente d'une **opération d'I/O** à terminer: lecture de disque, etc..), ou bien,
* d'un **événement** (par exemple, attente de l'appui d'une touche, ou d'un clic de souris, ou que soit écoulé un certain délai de temps),  

De plus, le processus ne peut rien faire pendant ce temps (avant d'avoir eu accès aux ressources, ou avant que l'événement ne se produise). Il n'est donc pas éligible pour obtenir un quantum de temps CPU. Un autre processus prend alors sa place, et passer dans l'état élu. Lorsque la ressource demandée par le processus bloqué se libère, celui-ci repasse par l'état prêt, et se remet à la queue pour attendre son nouveau tour.

### État Terminé

Dans l'état <rb>Terminé</rb> / <rb>Arrêté</rb> :fr: / <rb>sTopped</rb> :gb:, le processus a terminé son exécution jusqu'à sa dernière instruction, ou bien il a reçu un signal de terminaison (SIGTERM, SIGKILL, ...) de la part de l'OS. Il libère la mémoire et la totalité des ressources qu'il a détenues (fichiers, connexion réseau, etc..).

!!! warning "Fin d'un Processus"
    Un processus ne pourra terminer (passer à l'état Terminé) **que depuis l'état élu**.  

### Résumé Graphique

```mermaid
flowchart LR;
    subgraph En Cours d'Exécution
    E{Élu}
    end
    subgraph Attend des ressources
    B{Bloqué}
    end
    subgraph Attend le processeur
    P{Prêt}
    end
    N[Nouveau] -->|Création| P;
    P -->|Élection| E;
    E -->|Préemption| P;
    E -->|Blocage| B;
    B -->|Déblocage| P;
    E -->|Destruction| F[Terminé];
```

<center>
Ordonnancement (Préemptif)
</center>

## Des États Secondaires

Pour augmenter virtuellement la mémoire dont dispose la machine, certains OS *déchargent* les **processus inactifs** de la mémoire (on dit **Permuté**), typiquement ceux bloqués durant de nombreux quantums de temps. Ils stockent alors toutes ces informations (utiles au futur redémarrage) sur le disque (ou autre mémoire de masse), y compris le PCB du processus. Le processus ne pourra pas être instantanément réexécuté lorsqu'il sera débloqué : il devra préalablement être rechargé en mémoire. Ces opérations *à long terme*, mettent en valeur deux nouveaux états :

* <rb>Permuté Bloqué</rb> : Le processus est déchargé de la mémoire et il est, de plus, en attente d'une ressource
* <rb>Permuté Prêt</rb> : Le processus a été déchargé de la mémoire, mais n'est plus en attente d'une ressource. Il doit être rechargé en mémoire pour passer dans l'état Prêt et être éligible à une exécution.

[^1]: [Guillaume Connan, Processus & Ressources](https://gitlab.com/lyceeND/tale/-/blob/master/2020_21/5-Archi_Reseau/8_processus_ressources.md)
[^2]: [Olivier LECLUSE, Gestion des Processus](https://www.lecluse.fr/nsi/NSI_T/archi/process/)
[^3]: [Processus & Threads, Linux Attitude](https://linux-attitude.fr/post/processus-et-threads)
[^4]: [Architectures Matérielles, Processus, Pixees](https://pixees.fr/informatiquelycee/n_site/nsi_term_archi_proc.html)
[^5]: [Y. LEGOUZOUGEC](http://y.legouzouguec.free.fr/cours/ns/table.html)