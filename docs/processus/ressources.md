# TNSI : OS - Ressources & Contexte d'Exécution

## Ressources d'un Processus

Pour fonctionner, un processus a besoin / dispose de ressources **indépendantes** :

* un **Espace d'adressage mémoire Privé** :fr: / **Address Space** :gb: (qu'il ne partage avec AUCUN autre processus): la mémoire allouée par le Système d'Exploitation pour l'exécution de ce programme est organisée en 4 parties appelées **segments**, et détaillées plus amplement dans le paragraphe suivant, qui incluent :
    * le **Segment de Pile / Stack**
    * le **Segment de Tas / Heap**
    * le **Segment de Données** (statiques) exécutable copié en mémoire
    * le **Segment de Code** exécutable copié en mémoire
* Des **Fichiers ouverts**, identifiés par des **Descripteurs de Fichiers**[^11]: ce sont des nombres entiers qui identifient de manière unique les fichiers ouverts par un processus dans l'OS. Les $3$ fichiers par défaut affectés à chaque processus sont : `stdin (0)`, `stdout (1)`, `stderr (2)`. Les Descripteurs de fichiers sont maintenus par le noyau, individuellement pour chaque processus. Les descripteurs de fichiers sont soumis aux droits/permissions : Lorsqu'un fichier est ouvert, l'OS conserve une trace des droits/permissions des processus sur ce fichier.

<center>

| Nom | Descripteur<br/>de Fichier | Description | Abbréviation |
| :-: | :-: | :-: | :-: |
| Standard Input | $0$ | Le flux de données standard<br/>pour l'**entrée**, par exemple,<br/>dans un pipeline de commande.<br/>Dans un Terminal, c'est le clavier | `stdin` |
| Standard Output | $1$ | Le flux de données standard<br/>pour la **sortie**, par exemple,<br/>lorsqu'une commande affiche du texte.<br/>Dans un Terminal, c'est l'écran utilisateur | `stdout` |
| Standard Error | $2$ | Le flux de données standard<br/>pour la **sortie** d'une erreur en cours.<br/>Dans un Terminal, c'est l'écran utilisateur | `stderr` |

</center>

* un **Environnement** : une sorte de **Contexte d'Exécution** dans lequel le processus a été créé, dont notamment les valeurs stockées dans tous les **Registres** du processeur
* et un ensemble d'autres **ressources gérées par le noyau** :fr: / **kernel** :gb: et utilisées par le processus : par ex. des périphériques, une connexion réseau, etc.. De manière interne, le noyau a besoin de tenir les comptes de certains éléments pour chaque processus:
    * l'**état du processus**, 
    * sa **priorité** (pour indiquer une plus grande importance), 
    * des **statistiques**
* Pour chaque processus, au minimum, un **thread** (dit *main*)

<center>

```console linenums="0"
+--------------+
|   Mémoire    |
+--------------+
|   Fichiers   |
+--------------+
|   Registres  |
+--------------+
|     État     |
|      du      |
|    Noyau     |
+--------------+
```

</center>

## Organisation de l'Espace mémoire d'un Processus

<center>

```console linenums="0"
                       +--------------+
                       |    Noyau     |
                       +--------------+
                       |  Librairies  |
                       |  Partagées   |
                       +--------------+
                       |     ...      |
                       +--------------+
   Segment de Pile --> |     Pile     |
                       |      |       |
                       |      V       |
                       |              |
                       |              |
                       |              |
                       |      Λ       |
                       |      |       |
    Segment du Tas --> |     Tas      |
                       +--------------+
                       |              |
                       |   Données    |
                       |  Statiques   |
Segment de Données --> |              |
                       +--------------+
                       |              |
                       | Instructions |
                       |      du      |
                       |  Programme   |
   Segment de Code --> |              |
                       +--------------+
                       |     ...      |
```

</center>

où[^9]:

* Le **Segment de Code** : contient les instructions du programme. L'OS doit donner des droits de lecture et d'exécution au code du programme, mais, usuellement, pas de droit de modification
* Le **Segment de Données** : contient les données dont l'*adresse en mémoire* et la *valeur* sont connues au moment de l'initialisation de l'espace mémoire du programme. On parle alors de données **statiques**, par opposition aux données dont l'espace mémoire est alloué **dynamiquement** (Pile et Tas). La taille du segment de données statiques est **fixe**: il n'est donc pas possible d'allouer de nouvelles cases mémoires dans cet espace lors de l'exécution du processus. L'OS donne des droits de lecture et de modification à cette zone mémoire pouvant contenir des variables globales, mais, usuellement, pas de droit d'exécution. Ces différences dans les droits affectés par l'OS expliquent pourquoi le Segment de Code et le Segment de Données doivent être dans deux zones distinctes.
* Le **Segment de Pile** :fr: / **Stack** :gb: : Ce segment, de même que le suivant (le Tas), est alloué **dynamiquement** (càd durant l'exécution) par un programme. Chaque fois qu'une fonction est appelée, une nouvelle Pile est créée. **Par convention**, les Piles sont usuellement descendantes en mémoire (la Pile commence à une adresse supérieure, puis elle descend progressivement). La Pile est utilisée au moment de l'appel de fonctions d'un programme (**Pile d'appel**) pour stocker :
    * les **paramètres** des fonctions, mais également 
    * les **variables locales** des fonctions, et leur portée  
    * l'**adresse de retour** de la fonction[^10], lorsque son exécution est terminée
La gestion en Pile de fonctions de ce segment de données est fondamental pour :
    * l'exécution de fonctions récursives (où chaque appel de fonction a besoin d'un espace de mémoire propre pour être exécuté), mais également
    * la libération de la mémoire au moment où la fonction se termine
* Le **Segment du Tas** :fr: / **Heap** :gb: : est une zone, de taille variable, qui va permettre l’allocation de mémoire de façon dynamique par un programme, en fonction de ses besoins. Il peut s'agir de:
    * données dont la **durée de vie** n'est pas liée à l'exécution des fonctions (par exemple les **variables globales**), ou simplement 
    * données dont le **type** impose qu'elles soient allouées dans cette zone mémoire, par exemple parce que leur taille peut évoluer dynamiquement (comme les tableaux/listes en Python)  
    **Par convention**, les Tas sont usuellement ascendants en mémoire (le Tas commence à une adresse inférieure, puis il augmente progressivement)

!!! info "Analogie avec la vie réelle"
    Par analogie, la fabrication d’un gâteau peut être vue comme un processus : on a besoin d’un cuisinier (processeur), d’ustensiles de cuisine et d’ingrédients (ressources) et d’une recette (programme)

## Références & Notes

[^1]: [Guillaume Connan, Processus & Ressources](https://gitlab.com/lyceeND/tale/-/blob/master/2020_21/5-Archi_Reseau/8_processus_ressources.md)
[^2]: [Olivier LECLUSE, Gestion des Processus](https://www.lecluse.fr/nsi/NSI_T/archi/process/)
[^3]: [Processus & Threads, Linux Attitude](https://linux-attitude.fr/post/processus-et-threads)
[^4]: PCB - Process Block Control : [Wikipedia :fr:](https://fr.wikipedia.org/wiki/Process_control_block), [Wikipedia :gb:](https://en.wikipedia.org/wiki/Process_control_block)
[^5]: Memory Map : [Wikipedia :gb:](https://en.wikipedia.org/wiki/Memory_map)
[^6]: Mémoire Virtuelle / Virtual Memory : [Wikipedia :fr:](https://fr.wikipedia.org/wiki/M%C3%A9moire_virtuelle), [Wikipedia :gb:](https://en.wikipedia.org/wiki/Virtual_memory)
[^7]: [Architectures Matérielles, Processus, Pixees](https://pixees.fr/informatiquelycee/n_site/nsi_term_archi_proc.html)
[^8]: [Communication entre processus, par segment de mémoire partagée, Univ de Strasbourg](http://eavr.u-strasbg.fr/~christophe/cours/fip2/slides_shmem.pdf)
[^9]: [extrait du livre NSI, Ellipses, 1ère, Balabonski, Conchon, Filiâtre, Nguyen]()
[^10]: [Ian Wienand: Computer Science from the Bottom Up : Elements of a process](https://www.bottomupcs.com/elements_of_a_process.xhtml)
[^11]: [File Descriptors](https://www.computerhope.com/jargon/f/file-descriptor.htm)