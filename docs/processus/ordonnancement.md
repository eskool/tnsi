# TNSI : Processus - Ordonnancement

Quel rôle joue le Système d'Exploitation dans la gestion des Processus?

## Le Rôle du Système d'Exploitation

Le **Système d'Exploitation**, au travers de son **noyau** :fr: / **kernel** :gb:, s'occupe de :

* Décider quels processus doivent être lancés/**créés**. Plus précisément :
    * L'<rb>ordonnanceur</rb> :fr: / <rb>scheduler</rb> :gb: :
        * gère une file de processus Prêts, et 
        * gère une file des processus Bloqués.
    * Le <rb>répartiteur</rb> :fr: / <rb>dispatcher</rb> :gb: alloue, quant à lui, un processeur à l'élu dans le cas d'une architecture multiprocesseur.

* **interrompre** des processus, grâce à des mécanismes appelés **[interruptions](https://fr.wikipedia.org/wiki/Interruption_(informatique))**. Lors d'une interruption, le processeur enregistre tout ou une partie de son état interne, généralement dans la pile système, et exécute ensuite une **routine d'interruption**, généralement en suivant les directives d'une **table** indiquant pour chaque type d'interruption, le sous-programme à exécuter. 
* **supprimer** des processus

Plusieurs processus peuvent se trouver en mémoire centrale en même temps.  

!!! note "Je retiens"
    Il revient au Système d'Exploitation de réserver de la mémoire pour les processus, de planifier leur exécution, de résoudre les problèmes éventuels entre eux (cf. interblocages/deadlocks, famines, etc..) et d'assurer les communications entre eux.

## L'Ordonnanceur / Scheduler

!!! def "Scheduler / Ordonnanceur"
    L' <rb>Ordonnanceur</rb> :fr: / <rb>Scheduler</rb> :gb: est le module (lui-même un processus) du Système d'Exploitation qui se charge d'**organiser l'exécution des processus à tour de rôle**: il les ordonne, en leur octroyant un peu de temps d'accès/utilisation processeur (**quantum de temps**), en tenant compte des **priorités**, des débordements et des changements de contexte.

!!! def "Quantum de Temps"
    Un <rb>Quantum de Temps</rb> est le plus petit temps d'exécution que l'OS peut attribuer à un processus. C'est une valeur qui change peu. Un processus peut se voir allouer de $1$ à $n$ quantum de temps successifs. Typiquement, un quantum de temps dure $20$ à $50$ ms (millisecondes) selon les OS et les algorithmes d'ordonnancement utilisés.

Nous avons vu qu'il existait deux types d'ordonnancement différents : <rb>préemptif</rb>, ou <rb>non préemptif</rb>. Les deux existent :  

!!! def "Ordonnancement Préemptif vs Non Préemptif"
    Selon que l'opération de préemption (réquisition) du processeur est autorisée ou non, l'**ordonnancement** est dit <rb>préemptif</rb> ou <rb>non préemptif</rb> :

    * si l'ordonnancement est <rb>préemptif</rb>, la transition de l'état élu vers l'état prêt est autorisée : un processus quitte le processeur si il a terminé son exécution , si il se bloque ou si le processeur est réquisitionné. Un processus *préemptible* peut être interrompu au profit d'un processus ou d'une interruption.
    * si l'ordonnancement est <rb>non préemptif</rb>, la transition de l'état élu vers l'état prêt est interdite : un processus quitte le processeur si il a terminé son exécution ou si il se bloque. Un processus *préemptible* peut être interrompu uniquement au profit d'une interruption. Le temps qui lui est accordé est plus long, et l'attente plus courte. 

L'ordonnanceur[^7] :  

* gère une **file des processus Prêts**, qui est classée selon une <rb>Politique d'Ordonnancement</rb> qui assure que le processus en tête de file est le prochain processus à élire, au regard de la politique mise en oeuvre.
* gère une **file de processus Bloqués**

Ces classements sont effectués par l' ordonnanceur et sont réactualisés à chaque fois qu'un processus est préempté ou qu'un processus est débloqué (ajout d'un nouvel élément prêt)

![](./img/ordonnanceur-repartiteur.svg){.center style="width:90%; max-width:800px;"}
<center>
Ordonnanceur (préemptif) et Répartiteur, dans un Sytème Multiprocesseur
</center>

## Le Répartiteur / Dispatcher

C'est le répartiteur qui élit le processus en tête de file, c'est-à-dire qui lui alloue un processeur libre : 

* Dans un système **multiprocesseur**, c'est le répartiteur qui doit alors choisir quel processeur allouer. 
* Dans un système **monoprocesseur**, cette question ne se pose évidemment pas et souvent alors, ordonnanceur et répartiteur sont confondus. 

Une fois élu par le répartiteur, le processus s'exécute dans un processeur. 

* S'il quitte le processeur, sur **préemption**, il réintègre la file des processus prêts
* au contraire, s'il quitte le processeur sur un **blocage**, il intègre la file des processus bloqués. 

Dans les deux cas, un nouveau processus est élu (la tête de file des processus prêts). Plus précisément, la file d'attente des processus bloqués est souvent organisée en files multiniveaux, chaque niveau correspondant à une attente d'un événement/ ressource particulier auquel peut être éventuellement associé une priorité. Dans le cas où des événements se produisent simultanément, cette priorité sert alors à déterminer quel événement doit être traité en premier. 


## Références et Notes

### Vidéos YouTube

* [Processus dans un Système d'Exploitation, Renaud Lachaize, maître de conférences à l'Université Grenoble Alpes](https://www.youtube.com/watch?v=bFqud0gcCHM)
* [Utilité de la mémoire virtuelle, Renaud Lachaize](https://www.youtube.com/watch?v=dsvP0xOg7is)
* [Ordonnancement des Processus, Lilia Sfaxi, maître assistante à l'Institut National des Sciences Appliquées et de Technologie (INSAT) en Tunisie, et chercheuse au laboratoire LIPSIC](https://www.youtube.com/watch?v=LgEhegTslDc)
* [Cours Systèmes d'Exploitation, Complet, Lilia Sfaxit](https://www.youtube.com/watch?v=LgEhegTslDc&list=PLl3CtU4THqPbeu--yZXf630WeXW4EvDO0)

### Références

* [Ordonnancement des Processus, Hanifa Boucheneb, École Polytechnique de Montréal](https://cours.polymtl.ca/inf2610/documentation/notes/chap8.pdf)

### Notes

[^1]: [Algorithmes d'Ordonnancement, wikipedia](https://fr.wikipedia.org/wiki/Ordonnancement_dans_les_syst%C3%A8mes_d%27exploitation) :fr:, [Scheduling Algorithms, wikipedia](https://en.wikipedia.org/wiki/Scheduling_(computing)#Scheduling_disciplines) :gb:
[^2]: [Turnaround Time, TAT, Wikipedia](https://en.wikipedia.org/wiki/Turnaround_time) :gb:
[^3]: [Various Times Related to Process, gatvidyalay.com](https://www.gatevidyalay.com/turn-around-time-response-time-waiting-time/)
[^4]: [What is Burst Time, Arrival Time, Exit Time, .., by After Academy](https://afteracademy.com/blog/what-is-burst-arrival-exit-response-waiting-turnaround-time-and-throughput)
[^5]: Wikipedia : [Throughput](https://en.wikipedia.org/wiki/Throughput) :gb:, [Throughput](https://fr.wikipedia.org/wiki/Throughput) :fr:
[^6]: Ordonnancement : [Algorithme de Priorité/Earliest Deadline First (EDF)](https://en.wikipedia.org/wiki/Earliest_deadline_first_scheduling)
[^7]: [Y.LEGOUZOUGUEC, Méthodes de Programmation Système](http://y.legouzouguec.free.fr/cours/ns/chap3/ordonnancement.html)
[^8]: [Multitâche Préemptif, wikipedia](https://fr.wikipedia.org/wiki/Multit%C3%A2che_pr%C3%A9emptif)
[^9]: [Ordonnancement de Processus, Pierre Antoine Champin, LIRIS, CNRS](https://perso.liris.cnrs.fr/pierre-antoine.champin/enseignement/se/ordonnancement.html)

