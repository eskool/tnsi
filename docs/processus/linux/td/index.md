# TNSI : TD Processus sur Linux

La commande `$ ps -eo pid,ppid,command` affiche dans un Terminal :  
(:warning: **ATTENTION**: AUCUN espace autour des virgules :warning:)  

* le PID (Process ID) de chaque processus système( option `-e`)
* et le PPID (Parent PID) correspondant à chaque processus
* la commande correspondant à chaque processus

<clear></clear>
1. Tester cette commande dans votre terminal Linux  
2. Donner ses processus fils, que peut-on dire des autres processus par rapport au processus `0`.