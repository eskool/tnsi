# TNSI - TD Processus en Script Shell (Bash)

## Stocker et compter les processus d'un utilisateur

Ecrire un script bash `proc.sh` qui :

- teste s'il y a un seul parametre en entree
- verifie si ce parametre est un nom d'utilisateur
- cree un fichier de meme nom que l'utilisateur contenant la liste de ses  processus actif
- afffiche le nombre de processus actifs correspondants s'il y en a 
- traite les cas d'erreurs éventuels