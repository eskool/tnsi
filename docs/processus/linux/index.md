# TNSI : OS - Processus & Signaux sur Linux/*Nix

Lorsque Linux démarre, le noyau crée un processus spécial appelé **init : le parent de tous les processus** [^1], créé par `/sbin/init` (peut varier légèrement selon les distributions). Tous les processus du Système sont créés (on dit **forkés**) depuis **init**, ou bien l'un de ses enfants/descendants. Le PID d'**init** vaut `1` et est exécuté en tant que super-utilisateur. Dans ce cours, nous conviendrons que l'utilisateur principal de Linux sera appelé `eleve` (pas `root`).

## Daemons / Démons

!!! def "Daemons / Démons"
    Un <rb>Daemon</rb> :fr: :gb: / <rb>Démon</rb> :fr: est un processus à finalité spécifique (httpd, mysqld, etc..).

!!! pte "Démons vs Processus usuels"
    Un **démon** est certes un processus, néanmoins il diffère des autres processus usuels en ceci :

    * Sa **durée de vie est longue**: il est fréquent que le démon soit démarré lors du démarrage (boot)
 du système, et reste ainsi jusqu'à l'extinction du système (shut-down).
    * un **démon tourne en arrière plan**, et n'a pas de terminal désigné pour recevoir des entrées et/ou envoyer des sorties

## Signaux

!!! def "Signal"
    Un <rb>signal</rb> :fr: :gb: est un message envoyé à un processus par le SE.

!!! pte "Exemples Les plus connus/usités de Signaux sur Linux"
    La plupart des signaux ont pour effet de tuer le processus, mais :

    * certains ont un autre effet (cf ci-dessous)
    * un programme peut personnaliser sa réponse à certains signaux.

    ```console linenums="0"
    $ man 7 signal
    ...
    Signal        x86/ARM     
    ──────────────────────────
    SIGKILL          9        Tue le processus (utile lorsque SIGTERM échoue, ce signal n'est pas personnalisable)
    SIGTERM         15        Demande "poliment" au processus de s'arrêter
    SIGCONT         18        Relance le processus
    SIGSTOP         19        Suspend le processus
    SIGTSTP         20        Reprend le processus là où il se trouvait
    ```
    Une liste complète des signaux est disponible via :  
    ```console linenums="0"
    $ kill -l
    1) SIGHUP	    2) SIGINT       3) SIGQUIT	    4) SIGILL	    5) SIGTRAP
    6) SIGABRT	    7) SIGBUS	    8) SIGFPE	    9) SIGKILL	    10) SIGUSR1
    11) SIGSEGV	    12) SIGUSR2	    13) SIGPIPE	    14) SIGALRM	    15) SIGTERM
    16) SIGSTKFLT	17) SIGCHLD	    18) SIGCONT	    19) SIGSTOP	    20) SIGTSTP
    21) SIGTTIN	    22) SIGTTOU	    23) SIGURG	    24) SIGXCPU	    25) SIGXFSZ
    26) SIGVTALRM	27) SIGPROF	    28) SIGWINCH	29) SIGIO	    30) SIGPWR
    31) SIGSYS	    34) SIGRTMIN	35) SIGRTMIN+1	36) SIGRTMIN+2	37) SIGRTMIN+3
    38) SIGRTMIN+4	39) SIGRTMIN+5	40) SIGRTMIN+6	41) SIGRTMIN+7	42) SIGRTMIN+8
    43) SIGRTMIN+9	44) SIGRTMIN+10	45) SIGRTMIN+11	46) SIGRTMIN+12	47) SIGRTMIN+13
    48) SIGRTMIN+14	49) SIGRTMIN+15	50) SIGRTMAX-14	51) SIGRTMAX-13	52) SIGRTMAX-12
    53) SIGRTMAX-11	54) SIGRTMAX-10	55) SIGRTMAX-9	56) SIGRTMAX-8	57) SIGRTMAX-7
    58) SIGRTMAX-6	59) SIGRTMAX-5	60) SIGRTMAX-4	61) SIGRTMAX-3	62) SIGRTMAX-2
    63) SIGRTMAX-1	64) SIGRTMAX
    ```

<env>Quelques Exemples Classiques de Signaux (à connaître), et leurs Significations</env>[^4]

<center>

| Signal | N°| Signification | Comportement/Action |
|:-:|:-:|:-:|:-:|
| `SIGINT` | $2$ | Interruption depuis le clavier<br/>++ctrl+c++ |  |
| `SIGQUIT` | $3$ | Quitte depuis le clavier<br/>(Interruption Forte)<br/>$Leftrightarrow$ ++ctrl+\\++ | Terminaison<br/>+core dump |
| `SIGILL` | $4$ | Instruction Illégale |  |
| `SIGBUS` | $7$ | Erreur de Bus<br/>(Erreur d'accès mémoire) |  |
| `SIGKILL` | $9$ | Tue le processus courant | Terminaison |
| `SIGALRM` | $14$ | Interruption d'Horloge (`alarm`) | Ignoré |
| `SIGTERM` | $15$ |  |  |
| `SIGCHLD`<br/>ou `SIGCLD` | $17$ | Un des processus enfant<br/> (Child) est mort ou arreté | Ignoré |
| `SIGCONT` | $18$ | Redémarrage d'un processus arrêté | Ignoré |
| `SIGSTOP` | $19$ | Stoppe provisoirement<br/>le processus courant |  |
| `SIGTSTP` | $19$ | Stoppe provisoirement le processus courant<br/>depuis depuis un Terminal<br/>$Leftrightarrow$ ++ctrl+z++ | Suspension |
| `SIGPROF` | $27$ | Le signaler au Prof..<br/>(pardon, :face-palm: humour..)<br/>Fin du Timer de Profilage |  |
| `SIGIO` | $29$ | Les Entrées/Sorties (I/O)<br/>sont désormais possibles |  |
| `SIGPWR`<br/>ou `SIGINFO` | $30$ | Erreur d'Alimentation Électrique<br/>(Power) |  |

</center>

* ++ctrl+c++ envoie un signal SIGINT (2) au(x) processus courant(s)
* ++ctrl+z++ envoie un signal SIGTSTP (20) au processus courant
* La commande `bg` envoie SIGCONT au processus
* La commande `fg` envoie SIGCONT au processus, et fait en sorte d'attendre la fin
* Exemple: on peut voir que la commande `man` a personnalisé sa réponse à SIGINT, car le processus n'est pas tué lorsqu'on tape ++ctrl+c++.

## Quelques Commandes Linux

### `ps`

La commande `ps` (pour *<rb>P</rb>rocessu<rb>S</rb>*) permet d'afficher un **cliché instantané des processus en cours d'exécution** (pour n'importe quel utilisateur).  
Il existe différentes **personnalités** de `ps`, mais c'est la personnalité **linux** qui est un standard recommandé de nos jours. Ces personnalités acceptent usuellement plusieurs types d'options :

1. les options **UNIX** qui peuvent être regroupées et qui doivent être précédées d'**un tiret** `-`
2. les options **BSD** qui peuvent être regroupées et qui **NE doivent PAS être utilisées avec un tiret**
3. les options **GNU** de forme longue qui doivent être précédées **de deux tirets** `--`

Les options de différents types peuvent être mélangées, **mais des conflits peuvent apparaître**.

!!! exp "Personnalité Unix"
    1. `$ ps -ef` affiche **tous les processus du système**  
    (ou `$ ps -e` auquel cas, il restera seulement les colonnes : PID, TTY, TIME, CMD)
    ```console linenums="0"
    UID           PID    PPID  C STIME   TTY          TIME CMD
    root            1       0  0 avril13 ?      00:00:01 /sbin/init
    root            2       0  0 avril13 ?      00:00:00 [kthreadd]
    root            3       2  0 avril13 ?      00:00:00 [rcu_gp]
    eleve      159336   44078  0 avril13 pts/3  00:00:00 bash
    eleve      197099  197090  0 avril13 pts/4  00:00:00 man ps
    eleve      197109  197099  0 avril13 pts/4  00:00:00 less
    eleve      591437  581935  0 02:46   pts/5  00:00:00 ps -ef
    ```
    2. `$ ps -eo pid,ppid,stat,command` affiche tous les processus du système, en personnalisant les colonnes
    (:warning: **ATTENTION**: AUCUN espace autour des virgules :warning:)  
    ```console linenums="0"
    $ ps -eo pid,ppid,stat,command
       PID    PPID   STAT COMMAND
         1       0   Ss   /sbin/init
         2       0   S    [kthreadd]
         3       2   I<   [rcu_gp]
       166       2   SN   [ksmd]
       233       2   I<   [nvme-wq]
       989     950   Ssl+ /usr/lib/Xorg :0 -seat seat0 -auth /run/lightdm/root/:0 -nolisten tcp vt9 -novtswitch
      1578    1491   Ssl  xfce4-session
      1652    1564   S<sl /usr/bin/pulseaudio --daemonize=no --log-target=journal
      1667    1578   Sl   Thunar --daemon
    153224       2   I    [kworker/4:0]
    247465   44078   Ss   bash
    253461   44078   Ss   bash
    253626  253461   S+   man ps
    253639  253626   S+   less
    376601   31479   S+   python zombie.py
    376605  376601   Z+   [python] <defunct>
    376648  376643   S    sleep 1
    388627  247465   R+   ps -e -o pid,ppid,stat,command
    ```
    3. `$ ps -lu eleve` affiche le format <rb>L</rb>ong des processus de l'<rb>U</rb>tilisateur `eleve` :  
    ```console linenums="0"
    $ ps -lu eleve
    F S   UID     PID    PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
    0 S  1000    1660    1571  0  80   0 - 162307 -     ?        00:00:03 Thunar
    4 S  1000    2059       1  0  80   0 - 10685608 -   ?        00:02:08 code-oss
    4 S  1000    2785       1  7  80   0 - 3366572 -    ?        00:19:11 firefox
    0 S  1000   26102    2259 19  80   0 - 216226 -     pts/0    00:15:27 mkdocs
    0 S  1000  118425  118387  0  80   0 -  2156 -      pts/1    00:00:00 bash
    4 R  1000  118469  118425  0  80   0 -  2551 -      pts/1    00:00:00 ps
    ```
    
!!! exp "Personnalité BSD"
    1. `$ ps axu` affiche tous les processus du système
    ```console linenums="0"
    USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
    root           1  0.0  0.0 166660 11944 ?        Ss   avril13   0:01 /sbin/init
    root           2  0.0  0.0      0     0 ?        S    avril13   0:00 [kthreadd]
    root           3  0.0  0.0      0     0 ?        I<   avril13   0:00 [rcu_gp]
    eleve      197090  0.0  0.0   8816  5436 pts/4    Ss   avril13   0:00 bash
    eleve      197099  0.0  0.0   9748  5476 pts/4    S+   avril13   0:00 man ps
    eleve      197109  0.0  0.0   6916  2676 pts/4    S+   avril13   0:00 less
    eleve      638264  0.0  0.0  10244  3292 pts/5    R+   03:09   0:00 ps axu
    ```
    2. `$ ps axo user,pid,ppid,stat,command` affiche tous les processus du système, en personnalisant les colonnes
    ```console linenums="0"
    USER         PID    PPID STAT COMMAND
    root           1       0 Ss   /sbin/init
    root           2       0 S    [kthreadd]
    root           3       2 I<   [rcu_gp]
    eleve        1667    1578 Sl   Thunar --daemon
    eleve        2331    2280 Ss   /usr/bin/bash
    eleve      197090   44078 Ss   bash
    eleve      197099  197090 S+   man ps
    eleve      197109  197099 S+   less
    eleve      625514  581935 R+   ps axo user,pid,ppid,stat,command
    ```

<env>Signification des Colonnes</env>

* La colonne `F` (pour <rb>F</rb>lag / Drapeau) indique un entier lié au processus :
    * `1` : engendré mais pas exécuté
    * `4` : a utilisé des privilèges de super-utilisateur
* Les <rb>états du processus</rb> sont représentés :
    * La colonne `S` (pour <rb>S</rb>tate) affiche l'état <rb>à un caractère</rb> du processus :

    <center>

    | État | Signification |
    |:-:|:-:|
    | `D` | <rb>D</rb>ead<br/>En Veille **non interruptible**<br/>(normalement les Entrées et Sorties)<br/>Attente de Ressources<br/>ET de Signaux |
    | `I` | Fil Inactif du noyau |
    | `R` | <rb>R</rb>unning<br/><rb>R</rb>unnable State<br/>En Cours d'Exécution<br/>(ou pouvant s'exécuter)<br/>dans la File d'exécution |
    | `S` | <rb>S</rb>leeping<br/>En Sommeil/Veille **interruptible**<br/>en Attente d'un événement <br/>ou de Ressources pour finir |
    | `T` | s<rb>T</rb>opped<br/> / Arrêté par <br/>le signal de contrôle de la tâche|
    | `t` | s<rb>t</rb>opped<br/> / Arrêté par <br/>le déboggueur lors du traçage |
    | `X` | Tué: ne devrait jamais être vu |
    | `Z` | <rb>Z</rb>ombie[^2] ou <rb>defunct</rb> / Défunt<br/>$=$ un processus fils terminé,<br/>mais pas (encore) détruit proprement par son parent.<br/> Il dispose donc encore<br/>d'un `PID` - Process ID.<br/> Encore visible dans la Table des Processus.<br/>Sur Linux, ces processus seront détruits par **init**<br/>si le processus parent se termine. |

    </center>

    * La colonne `stat`, pour les formats BSD, affiche l'état <rb>à plusieurs caractères</rb> du processus : Les caractères suivants peuvent être ajoutés en plus du caractère du tableau précédent) (cf. par ex. `man ps`):  

    <center>

    | Caractère | Signification |
    | :-: | :-: |
    | `<` | **Haute priorité** (non poli pour les autres utilisateurs) |
    | `N` | **Nice - Basse priorité** (poli pour les autres utilisateurs) |
    | `L` | avec ses pages verrouillées en mémoire<br/> (pour temps réeL et entrées<br/>et sorties personnalisées) |
    | `s` | meneur de session |
    | `l` | possède plusieurs threads / processus légers<br/>(« multi-thread », utilisant CLONE_THREAD<br/> comme NPTL pthreads le fait) |
    | `+` | dans le groupe de **processus au premier plan** |

    </center>

* `UID` est le *User ID* : un identifiant sous forme d'entier donné par le système pour l'utilisateur
* `PID` est le *Process IDentifier* : un identifiant sous forme d'entier donné par le système pour le processus
* `PPID` est le *Parent Process IDentifier* qui donne l'identifiant du parent qui a engendré le processus.
* `C` est la *Chip*/puce : (partie entière du) pourcentage d'utilisation du Processeur
* `PRI` est la *PRIorité* du Processus
* `NI` est la *NIceness* (value)/valeur de Politesse du Processus. Cette valeur affecte l'ordonnancement du processus: un entier de $19$, la plus polie / la moins prioritaire, à $-20$ la moins polie / la plus prioritaire.
* `TTY`: le tty/terminal de contrôle
* `TIME` : temps CPU cumulé (en secondes)
* `CMD` est le nom de la commande, càd le nom du programme.

<env>Résumé du cycle de vie d'un processus</env>

![États des Processus](../img/etats-processus.png){.center style="border-radius:6px;"}
[:copyright:](https://www.baeldung.com/linux/process-states)

<env>Plus d'infos</env> avec `$ man ps` ou cette référence [^6]  
(plus d'infos sur `man` avec `man man` :face-palm: )

### `pstree`

La commande `pstree` affiche dans le Terminal, une liste structurée des processus, **sous forme d'arbre** (tree):

```console linenums="0"
$ pstree -p
systemd(1)─┬─ModemManager(1041)─┬─{ModemManager}(1042)
           │                    └─{ModemManager}(1045)
           ├─NetworkManager(882)─┬─{NetworkManager}(984)
           │                     └─{NetworkManager}(991)
           ├─accounts-daemon(1038)─┬─{accounts-daemon}(1060)
           │                       └─{accounts-daemon}(1062)
           ├─avahi-daemon(865)───avahi-daemon(907)
           ├─blueman-tray(1926)─┬─{blueman-tray}(1953)
           │                    ├─{blueman-tray}(1954)
           │                    └─{blueman-tray}(1956)
           ├─bluetoothd(1236)
           ├─code-oss(2059)─┬─electron(2062)───electron(2096)─┬─electron(2130)
           │                │                                 ├─{electron}(2131)
           │                │                                 ├─{electron}(2132)
           │                │                                 ├─{electron}(2134)
           │                ├─electron(2063)───electron(2065)
           │                ├─electron(2099)─┬─{electron}(2102)
           │                │                ├─{electron}(2104)
           │                │                ├─{electron}(2106)
           │                │                └─{electron}(190458)
           │                ├─electron(2116)─┬─{electron}(2118)
           │                │                ├─{electron}(2120)
           │                │                ├─{electron}(2122)
           │                │                ├─{electron}(2123)
           │                │                ├─{electron}(2124)

... (suite de l'affichage coupé: liste assez longue)
```

où :

* L'option `p` ajoute l'affichage du **PID** de chaque processus

<env>Plus d'infos</env> avec `$ man pstree`

### `top`

La commande `top` affiche un résumé dynamique en temps réel (mise à jour toutes les secondes environ), des principaux processus/**tâches** créés dans le système:

```console linenums="0"
top - 19:18:42 up  5:36,  1 user,  load average: 1,37, 1,16, 0,94
Tâches: 419 total,   1 en cours, 418 en veille,   0 arrêté,   0 zombie
%Cpu(s):  5,9 ut,  2,0 sy,  0,7 ni, 91,2 id,  0,1 wa,  0,1 hi,  0,0 si,  0,0 st
MiB Mem :  32017,3 total,  21163,1 libr,   5373,5 util,   5480,7 tamp/cache
MiB Éch :  35223,0 total,  35223,0 libr,      0,0 util.  25945,0 dispo Mem 

    PID UTIL.     PR  NI    VIRT    RES    SHR S  %CPU  %MEM    TEMPS+ COM.           
  26102 luke      20   0  868496 139556  11592 S  42,2   0,4  30:44.67 mkdocs         
   2785 luke      20   0   12,7g   1,1g 477224 S  15,0   3,6  23:29.01 firefox        
  82047 luke      20   0 2780140 262392 122516 S   6,3   0,8   2:33.60 Isolated Web Co
   1037 root      20   0   24,5g 325232 117624 S   3,7   1,0   7:52.84 Xorg           
   1064 root     -51   0       0      0      0 S   3,3   0,0   4:16.50 irq/176-nvidia 
   1645 luke       9 -11 1545484  30492  22680 S   3,0   0,1   4:42.95 pulseaudio     
   1635 luke      20   0  936552 101404  62552 S   2,7   0,3   3:14.55 xfwm4          
    870 dbus      20   0   14352   7188   5368 S   1,3   0,0   0:11.38 dbus-daemon    
   1791 root      20   0  393364  12844  10456 S   1,3   0,0   0:11.79 udisksd        
 194563 luke      20   0  636572  60772  45948 S   1,3   0,2   0:03.17 xfce4-terminal 
  24480 luke      20   0 4250824   1,1g 352344 S   1,0   3,7   6:58.56 thunderbird    
 203664 luke      20   0   11308   4672   3508 R   0,7   0,0   0:00.33 top     
```

On peut y lire par exemple :

* **Running**/En cours d'exécution : `2`
* **Sleeping**/En veille :  `418`
* **Stopped/Arrété** :  `0`
* **Zombie**/En veille :  `0`

Les abbréviations du Résumé supérieur signifient [^3]:

<center>

| Lettres | Signification |
|:-:|:-:|
| ut<br/>/us | <rb>UT</rb>ilisateur<br/>/ <rb>US</rb>er.<br/> Charge processeur demandée par des processus utilisateurs<br/>(sans compter les polis/nice) |
| sy<br/> | <rb>SY</rb>stème.<br/>tout temps CPU passé par les instructions bas niveau<br/>, niveau kernel/noyau  |
| ni<br/> | <rb>NI</rb>ce <br/>/ Valeur de Politesse<br/>Charge demandée par des processus polis/*nicés* |
| id<br/> | <rb>ID</rb>LE <br/>/  Pourcentage de ressources processeur libres <br/>(tâches inactives)|
| wa<br/> | <rb>WA</rb>it<br/>/ IO-<rb>WA</rb>it<br/>Charge de processus dédiés à une tâche système ou utilisateur,<br/> mais actuellement en attente de ressource I/O (par ex. réseau ou disque) |
| hi<br/> | <rb>H</rb>ardware <rb>I</rb>nterrupt<br/>Temps passé à fournir des services <br/>pour les interruptions matérielles |
| si<br/> | <rb>S</rb>oftware <rb>I</rb>nterrupt<br/>Temps passé à fournir des services <br/>pour les interruptions logicielles |
| st<br/> | <rb>S</rb>tolen<br/>Temps volé par cette VM par l'Hyperviseur |

</center>

et les colonnes:

<center>

| Colonne | Signification |
|:-:|:-:|
| VIRT | Taille Totale de la Mémoire <rb>VIR</rb>tuelle (en KiO - kibioctets)<br/>utilisée par le processus<br/>y compris le code, les données et les librairies partagées, etc.. |
| RES | Taille de la Mémoire <rb>RÉS</rb>iduelle (en KiO - kibioctets)<br/>utilisée par le processus <br/>Une sous-partie de la Mémoire VIRTuelle, <br/>représentant la mémoire physique non échangée<br/> (non -swapped) utilisée par le processus<br/>Par ex.: des pages privées anonymes,<br/>pages privées menant vers des fichiers<br/>(cela inclut les images de programmes,<br/> et librairies partagées) |
| SHR | Taille de la Mémoire Partagée (<rb>SH</rb>a<rb>R</rb>ed) (en KiO - kibioctets) :<br/> Une sous-partie de la Mémoire RESiduelle utilisée par le processus,<br/>pouvant être utilisée par d'autres processus |
| %CPU | Partie Entière du pourcentage d'utilisation du CPU |
| %MEM | Partie Entière du pourcentage d'utilisation de la Mémoire (RES) |
| COM. | Commande |

</center>

<env>Plus d'infos</env> avec `$ man top`

### `kill`

Il peut arriver qu'on rencontre des difficultés pour fermer un processus graphique en cliquant par exemple sur la croix (en haut à droite), pourtant prévue à cet effet. Pour résoudre ce problème, il existe une commande `kill` **servant à envoyer des signaux à des processus** (par ex. on envoie le signal SIGTERM au processus `code-oss` pour le TERMINER). Cela suppose néanmoins que l'on connaisse son PID.

#### Signaux

#### Syntaxe

`$ kill [-s nomSignal -n numeroSignal] pid | nomProcessus` permet de terminer le processus dont le PID vaut `pid`.  

* **Par défaut**, la commande `kill` envoie **le Signal SIGTERM (15)** au processus indiqué (par son pid, ou son nom), ce qui a pour effet de le **terminer**. Le signal de Terminaison SIGTERM (15) est préférable au Signal pour Tuer SIGKILL (9), car il peut arriver qu'un processus installe un handler/gestionnaire pour le Signal TERM (SIGTERM) qui va gérer des opérations de nettoyage, par étapes ordonnées, avant de finalement Terminer.
* Lorsqu'un processus ne parvient pas à terminer après un SIGTERM (15), alors le Signal SIGKILL (9) peut être envoyé.

!!! exp "Liste de Tous les Signaux existants sur Linux"
    `$ kill -l` affiche la liste (`-l`) de tous les **signaux d'interruption** existants.  

    ```console linenums="0"
    $ kill -l
    1) SIGHUP	    2) SIGINT	    3) SIGQUIT	    4) SIGILL	    5) SIGTRAP
    6) SIGABRT	    7) SIGBUS	    8) SIGFPE	    9) SIGKILL	    10) SIGUSR1
    11) SIGSEGV	    12) SIGUSR2	    13) SIGPIPE	    14) SIGALRM	    15) SIGTERM
    16) SIGSTKFLT	17) SIGCHLD	    18) SIGCONT	    19) SIGSTOP	    20) SIGTSTP
    21) SIGTTIN	    22) SIGTTOU	    23) SIGURG	    24) SIGXCPU	    25) SIGXFSZ
    26) SIGVTALRM	27) SIGPROF	    28) SIGWINCH	29) SIGIO	    30) SIGPWR
    31) SIGSYS	    34) SIGRTMIN	35) SIGRTMIN+1	36) SIGRTMIN+2	37) SIGRTMIN+3
    38) SIGRTMIN+4	39) SIGRTMIN+5	40) SIGRTMIN+6	41) SIGRTMIN+7	42) SIGRTMIN+8
    43) SIGRTMIN+9	44) SIGRTMIN+10	45) SIGRTMIN+11	46) SIGRTMIN+12	47) SIGRTMIN+13
    48) SIGRTMIN+14	49) SIGRTMIN+15	50) SIGRTMAX-14	51) SIGRTMAX-13	52) SIGRTMAX-12
    53) SIGRTMAX-11	54) SIGRTMAX-10	55) SIGRTMAX-9	56) SIGRTMAX-8	57) SIGRTMAX-7
    58) SIGRTMAX-6	59) SIGRTMAX-5	60) SIGRTMAX-4	61) SIGRTMAX-3	62) SIGRTMAX-2
    63) SIGRTMAX-1	64) SIGRTMAX
    ```

!!! exp "Envoi de Signaux via la commande `kill`"
    Disons que l'on souhaite terminer le processus de pid=2059 :

    * Signal **SIGTERM (15)** :
        * `$ kill 2059` termine le processus de PID `2059` (SIGTERM est le signal par défaut)
        * `$ kill -s SIGTERM 2059` termine le processus de PID `2059`  
        * `$ kill -n 15 2059` termine le processus de PID `2059`  
        * `$ kill -15 2059` termine le processus de PID `2059`  
    * Signal **SIGKILL (9)** :
        * `$ kill -s SIGKILL 2059` termine le processus de PID `2059`  
        * `$ kill -n 9 2059` termine le processus de PID `2059`  
        * `$ kill -9 2059` termine le processus de PID `2059`  

#### Mais comment déterminer le numéro de PID à tuer ?

La syntaxe précédente suppose néanmoins que l'on connaisse le numéro de PID : mais lorsque plusieurs lignes de résultats indiquent un nom de processus, on pourrait condre : comment déterminer le PID ? Peut-on exhiber une généalogie entre processus?

```console linenums="0"
$ ps --forest -lU eleve
...
4 S  1000    2095       1  0  80   0 - 10684795 -   ?        00:00:31 code-oss
0 S  1000    2098    2095  0  80   0 - 116532 -     ?        00:00:00  \_ electron
1 S  1000    2132    2098  3  80   0 - 281760 -     ?        00:02:08  |   \_ electron
1 S  1000    2137    2132  0  80   0 - 138619 -     ?        00:00:00  |       \_ electron
4 S  1000    2099    2095  0  80   0 - 116532 -     ?        00:00:00  \_ electron
1 S  1000    2101    2099  0  80   0 - 116532 -     ?        00:00:00  |   \_ electron
0 S  1000    2135    2095  0  80   0 - 131329 -     ?        00:00:00  \_ electron
0 S  1000    2154    2095  6  80   0 - 12255253 -   ?        00:03:57  \_ electron
0 S  1000    2204    2095  0  80   0 - 9582800 -    ?        00:00:15  \_ electron
0 S  1000    2217    2095  0  80   0 - 10657422 -   ?        00:00:03  \_ electron
0 S  1000    2238    2217  0  80   0 - 9575706 -    ?        00:00:04      \_ electron
0 S  1000    2289    2238  0  80   0 -  2238 -      pts/0    00:00:00      |   \_ bash
0 S  1000    3885    2289 14  80   0 - 216013 -     pts/0    00:07:32      |       \_ mkdocs
0 S  1000    2271    2217  0  80   0 - 9602367 -    ?        00:00:02      \_ electron
...
```

ou bien :

```console linenums="0"
$ pstree -p
systemd(1)─┬─ModemManager(884)─┬─{ModemManager}(898)
           │                   └─{ModemManager}(923)
           ├─NetworkManager(780)─┬─{NetworkManager}(807)
           │                     └─{NetworkManager}(819)
           ├─accounts-daemon(853)─┬─{accounts-daemon}(1093)
           │                      └─{accounts-daemon}(1095)
           ├─anacron(42758)
           ├─avahi-daemon(777)───avahi-daemon(791)
           ├─blueman-tray(1986)─┬─{blueman-tray}(2012)
           │                    ├─{blueman-tray}(2013)
           │                    └─{blueman-tray}(2015)
           ├─bluetoothd(1279)
           ├─code-oss(2095)─┬─electron(2098)───electron(3040)─┬─electron(3042)
           │                │                                 ├─{electron}(3043)
           │                │                                 ├─{electron}(3044)
           │                │                                 ├─{electron}(3045)
           │                │                                 ├─{electron}(3048)
           │                │                                 ├─{electron}(3052)
           │                ├─electron(2099)───electron(3012)
           │                ├─electron(3060)─┬─{electron}(3063)
           │                │                ├─{electron}(3064)
```

On découvre une "généalogie" des processus pour `code-oss` : `2095` est le parent de tous les autres. C'est lui qu'il faut interrompre:

```console linenums="0"
$ ps -lC code-oss
F S   UID     PID    PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
4 S  1000    2095       1  0  80   0 - 10684795 -   ?        00:00:33 code-oss
$ kill 2095
$ ps -lC code-oss
F S   UID     PID    PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
```

où:

* L'option `C` montre le processus lié à une *commande* (le nom sous CMD, pas la commande CLI)

<env>Plus d'infos</env> avec `$ man kill`

### Les Processus dans le Shell (bash)

#### Le PID du shell courant

```console linenums="0"
$ echo $$
33885
```

#### Le PID d'un processus enfant dans le Shell

```console linenums="0"
$ pwd &
[1] 49080
/home/eleve
[1]+  Fini                    pwd
```

### Le dossier `/proc`

#### `cat /proc/1/status`

`# cat /proc/1/status nomProcessus`, ou `$ sudo cat /proc/1/status nomProcessus`, affiche des informations avancées sur l'état du processus *nomProcessus*, et 

#### `cat /proc/1/status`

`# cat /proc/1/maps`, ou `$ sudo cat /proc/1/maps`, affiche des informations avancées sur la cartographie de la mémoire virtuelle (bibliothèques dynamiques, etc..)

## Références & Notes

* https://www.lecluse.fr/nsi/NSI_T/archi/process/
* [Redhat Linux, Process States](https://access.redhat.com/sites/default/files/attachments/processstates_20120831.pdf)

### Notes

[^1]: [Linux Process and Signals](https://www.bogotobogo.com/Linux/linux_process_and_signals.php)
[^2]: [Processus Zombie, wikipedia](https://fr.wikipedia.org/wiki/Processus_zombie)
[^3]: [Comprendre la Commande Top](https://medium.com/@guillaumefenollar/lire-comprendre-et-analyser-la-commande-top-966feefe500e)
[^4]: [Initiation à Linux, Pierre Antoine Champin LIRIS, CNRS](https://perso.liris.cnrs.fr/pierre-antoine.champin/enseignement/linux/s4.html)
[^5]: [Understanding the Linux Kernel, Daniel P. Bovet, Marco Cesati, O'Reilly](https://doc.lagout.org/operating%20system%20/linux/Understanding%20Linux%20Kernel.pdf)
[^6]: [Syntaxe ps pour Archlinux](https://man.archlinux.org/man/ps.1.fr)
[^7]: [Les Signaux, cours INRIA](http://pauillac.inria.fr/~remy/poly/system/camlunix/sign.html)