# TNSI - TD Sockets Réseaux et Multithreading en Python

Ce TD :

* étudie la librairie `socket` de Python qui permet de gérer les sockets réseaux en Python. Et 
* s'intéresse à transformer les codes du serveur et du client en **multithreading** grâce à la classe Thread  de la librairie **threading** de Python.

Voici deux fonctions Python :

```python
# date-serveur.py
import socket
import datetime
BUFSIZE = 1024

def serveur(port):
  sock = socket.socket(type=socket.SOCK_DGRAM)
  sock.bind(("0.0.0.0", port))
  
  while True:
    data , addr = sock.recvfrom(BUFSIZE)
    data = datetime.datetime.now()
    data = "%s\n" % data
    sock.sendto(data.encode(), addr)

if __name__ == '__main__':
  serveur(5555)
```

et 

```python
# date-client.py
import socket
import datetime
import sys
BUFSIZE = 1024

def client(host, port):
  sock = socket.socket(type=socket.SOCK_DGRAM)
  addr = (host, port)
  sock.sendto(b"", addr)
  data = sock.recv(BUFSIZE)
  print(data.decode(), end="")


if __name__ == '__main__':
  if len(sys.argv) ==  2:
    host = sys.argv[1]
  else:
    host = '127.0.0.1'
  client(host, 5555)
```


1. Expliquer les rôles respectifs de ces deux programmes en consultant notamment la documentation de `socket`: https://docs.python.org/3/library/socket.html

    a. Quelle est la taille du Buffer choisi pour les échanges ?
    b. Quelles sont les 2 types principaux de socket et à quoi correspondent-ils ?
    c. A quoi correspond la boucle `while True`{.python} du serveur ?
    d. Que fait la méthode `recvfrom` de socket ?
    e. Qu'envoie finalement le serveur lorsqu'il reçoit une connexion ?
    f. Expliquer brièvement le code du client. Le client envoie des données vides et son adresse et reçoit en retour la date du serveur qu'il affiche avant de la décoder car tous les échanges se font en bytes

2.  

    a. En utilisant la classe `Thread`, comment rendre ce code multi-thread côté serveur ?  
    b. Ajoutez le lancement de plusieurs Threads clients nommés A, B, C, D dans le `__main___`{.python} du client