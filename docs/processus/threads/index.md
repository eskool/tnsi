# TNSI : OS - Processus Légers / Threads

## Introduction

Un processus peut lui-même être décomposé en **sous-processus** / **threads** ou **tâches** / **tasks**. Les threads
permettent de ne plus être liés à une exécution totalement sérielle des instructions. Il est dès lors possible d’avoir, par exemple, un thread responsable d’exécuter un calcul lourd pendant qu’un autre gère les interactions avec l’utilisateur. Un serveur FTP, par exemple, peut plus facilement gérer plusieurs connexions simultanées, chaque connexion étant gérée par un thread.

## Threads

### Définition

!!! def "Fil d'exécution / Thread"
    * Un processus est composé de un ou plusieurs <red>Threads</red> :gb: :fr: / <red>Processus léger</red> / <red>Fil d'exécution</red> :fr:,
    * Un **Processus léger** / **thread** est semblable à un processus: il représente l'exécution d'un ensemble d'instructions du langage machine d'un processeur, mais il partage un même espace mémoire avec d'autres threads.
    * Il existe un **thread principal** (**main**)
    * Un thread est attaché à un processus lourd parent

### Groupes de Threads

!!! pte "Groupes de Threads"
    * Les threads partageant un même espace mémoire sont regroupés en des **groupes** : cela permet par exemple de faciliter le parallélisme sur des machines multi-processeurs.
    * En contrepartie, les données étant partagées dans le même espace mémoire, cela peut créer des conflits et impose une plus grande surveillance pour la synchronisation. (cf exercice ...)

![Processus Multithreads](../img/processus-multithread.svg){.center}

### Principales Utilisations

!!! pte
    Classiquement, les threads sont utilisés :

    * pour développer des logiciels avec des **Interfaces Graphiques**, qui peuvent se focaliser sur la détection des événements extérieurs (souris, clavier, etc..) sans bloquer l'interface
    * pour gérer les accès concurrents à des bases de données

## Ressources d'un Thread

!!! def "Ressources d'un Thread"
    Pour fonctionner, un thread dispose de :
    
    * **Ressources Indépendantes**:
        * Une **Pile** :fr: / **Stack** :gb: : Un pointeur dans la mémoire qui sert à stocker les variables locales, et l'information suffisante pour que le processeur comprenne quel sera la prochaine instruction à exécuter
        * **Registres**
    * **Ressources Partagées**:
        * **L'Espace d'adressage mémoire** du processus parent
        * **Les Fichiers ouverts** par le processus parent
        * Ressources gérées par le **noyau** correspondant au processus parent :
            * **État** du processus
            * Une **Priorité** : Information que le système d’exécution utilisera pour déterminer quel thread sera le prochain à obtenir du temps de processeur.

![Mémoire partagée des Threads](./img/memoire-threads.png){.center}


## Threads vs Processus

Voici une table de comparaison[^1] :

|  | Processus | Thread |
|:-:|:-:|:-:|
| Définition | Un **Processus** est une instance<br/>d'un programme en cours d’exécution | Un **Thread** est une petite partie d’un processus. |
| Communication<br/>entre eux | La communication entre deux processus<br/>est uniquement <b>externe</b>,<br/>coûteuse et limitée. | La communication entre deux threads<br/>se fait via la mémoire partagée,<br/>elle est moins coûteuse que<br/>celle entre processus. |
| Multitâche | Le multitâche basé sur les processus<br/>permet à un ordinateur d’exécuter<br/>deux ou plusieurs programmes simultanément.<br/>(Pseudo-Parallélisme) | Le multitâche basé sur les threads<br/>permet à un programme unique d’exécuter<br/>deux threads ou plus simultanément. |
| Espace d'adressage | Chaque processus a son <br/>espace d’adressage distinct. | Tous les threads d’un processus<br/>partagent le même espace d’adressage<br/>(que celui du processus). |
| Tâche | Les processus sont des tâches lourdes | Les threads sont des tâches légères. |
| Changement<br/>de Contexte | Moins Rapide | Rapide |
| Exemple | Vous travaillez sur un éditeur de texte,<br/>il fait référence à l’exécution d’un processus. | Vous imprimez un fichier<br/>à partir d’un éditeur de texte<br/>tout en travaillant dessus,<br/>ce qui ressemble à l’exécution <br/>d’un thread dans le processus. |

## Notes & Références

### Références

* https://linux-attitude.fr/post/processus-et-threads
* [Multitâche API PThread : Norme IEEE POSIX 1003.1c]()

### Notes

[^1]: [Threads vs Processus (en java..)](https://waytolearnx.com/2018/09/difference-entre-thread-et-processus-en-java.html)
[^2]: [Processus vs Threads, Linux Attitude](https://linux-attitude.fr/post/processus-et-threads)
[^3]: [Processus vs Threads, Microsoft](https://docs.microsoft.com/fr-fr/windows/win32/procthread/when-to-use-multitasking)
[^4]: [Introduction à la Programmmation Parallèle, Guillermo ANDRADE, INRIA Bretagne](http://devlog.cnrs.fr/_media/groupe-de-travail/jdev-2011/programme-planning-intervenants/programme/introduction-au-calcul-parall%C3%A8le.pdf)
[^5]: [Threads, wikipedia](https://fr.wikipedia.org/wiki/Thread_(informatique))
[^5]: [Communication Inter-Processus, wikipedia](https://fr.wikipedia.org/wiki/Communication_inter-processus)
[^6]: [Les Processus Légers (Threads), Patrick DEIBER, CNAM](http://perso.numericable.fr/~pdeiberale69/patrick.deiber/pub/processus-leger.pdf)
[^7]: [Processus Légers / Threads, Programmation Concurrente, Matthieu Moy, Grégoire Pichon, Univ Lyon 1](https://asr-lyon1.gitlabpages.inria.fr/prog-concurrente/cm-threads/cm-threads-slides.pdf)