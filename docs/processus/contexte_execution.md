# 1NSI : OS - Contexte d'Exécution d'un Processus & Switching

## De quoi parle-ton?

Un OS multitâche exécute chaque processus dans un environnement mémoire **cloisonné** et **protégé**.
Un processus ne peut lire et écrire des données en mémoire que dans la zone que l'OS lui a dédié. L'OS garantit qu'aucun autre processus en cours d'exécution ne pourra lire, ni modifier ses données.

Rappelons qu'un processeur dispose de plusieurs registres (RI, AC, CC, ..), un Compteur Ordinal (/PC - Program Counter / Instruction Pointer) et une UAL pour l'exécution des instructions. Lorsqu'un processus est exécuté, ce sont ses instructions qui occupent les registres et la mémoire interne du processeur.

Pour simuler la sensation de simultanéité, l'OS est amené à régulièrement interrompre l'exécution d'un processus `P1`, pour exécuter un autre processus `P2`, puis interrompre `P2` pour exécuter un troisième processus `P3`, etc.. À un moment donné, il faut revenir à l'exécution de `P1`, ce qui suppose que l'on reprenne exactement à l'endroit où il s'était arrêté: Les registres de `P1` sont ré-initialisés dans l'état précis où ils se trouvaient lors de son interruption. C'est pourquoi lors d'une interruption d'un processus, l'OS doit sauvegarder en mémoire l'état précis de tous ses éléments volatiles, pour lui permettre de reprendre précisément dans cet état.

## Contexte d'Exécution

!!! def "Contexte d'Exécution / Execution Context"
    Un <rb>Contexte d'Exécution</rb> / <rb>Execution Context</rb> :gb: désigne l'ensemble de tous les éléments volatiles liés à l'exécution d'un processus. Ils seront sauvegardés en l'état lors de l'interruption d'un processus, et restitués en l'état lors de sa réactivation.

!!! info
    * Les <rb>Processus</rb> sont des <rb>processus lourds</rb>. 
        * Ils disposent d'un Espace d'Adressage (mémoire) dédié : Ils ont **gros contexte**
    * Les <rb>Threads</rb> sont des <rb>processus légers</rb> :
        * Ils partagent leur mémoire entre threads d'un même processus : communication et coordination faciles
        * Moins de ressources lors de la Création : **petit Contexte**

## Commutation de Contexte / Changement de Contexte / Context Switching

!!! def "Commutation de Contexte / Changement de Contexte / Context Switching"
    On appelle <rb>Commutation de Contexte</rb> / <rb>Changement de Contexte</rb> :fr: / <rb>Context Switching</rb> :gb: l'opération de remplacement d'un contexte d'exécution par un autre, et de recopie en mémoire de chaque contexte écrasé.

!!! info
    * Les processus (lourds) ont un changement de contexte plus lent
    * Les threads / processus légers ont un changement de contexte plus rapide

## :fa-book: Le PCB - Process Control Block

Bien que pouvant varier selon les implémentations, voici, pour le plaisir de la culture :fa-book:, quelques uns des éléments que l'OS doit conserver à l'interruption d'un processus et restituer lors de sa réactivation, dans une structure de données du noyau, appelée le <rb>PCB - Process Control Block</rb> :gb: / <rb>B</rb>loc de <rb>C</rb>ontrôle des données contextuelles d'un <rb>P</rb>rocessus[^4].

* Le **PID - Process ID** : un numéro d'identification du processus (entier $\geq 1$)
* Le **PPID - Parent Process ID** : un numéro d'identification du processus **parent** (du processus) (entier $\geq 1$)
* L' **UID - User ID** : un numéro d'identification de **Utilisateur**/User du processus (entier $\geq 0$. `0` pour root sur Linux)
* (Registres de) l'**état** courant du processus : élu, prêt, bloqué, etc..
* Le **Compteur Ordinal** / **PC - Program Counter** ou IP - Instruction Pointer) du processus: un pointeur vers l'adresse de la prochaine instruction du processus
* Les autres valeurs de registres : priorité, politesse, temps CPU accumulé depuis l'élection/blocage du processus, données d'identification de l'événement menant à la suspension d'un processus suspendu (pour détecter lorsqu'il faut reprendre), etc..
* (Le pointeur de) la Pile d'Instructions qui indique la position du prochain emplacement disponible

![Mémoire Virtuelle](./img/memoire-virtuelle.svg){style="float:right; width:30%;"}

* L'**espace d'adressage** du processus géré par un mécanisme de **[Mémoire Virtuelle](https://fr.wikipedia.org/wiki/M%C3%A9moire_virtuelle)** [^5] [^6] par processus: l'emplacement mémoire du code, et des données du processus. L'OS utilise un procédé de **traduction d'adresses**, mélangeant des composants matériels (MMU - Memory Management Unit) et logiciels, qui permet de traduire **les adresses mémoires** utilisées par un programme (dites **adresses virtuelles**) en des **adresses physiques** (la RAM). L'utilisation de la mémoire virtuelle permet de:
    * d'utiliser de la [mémoire de masse](https://fr.wikipedia.org/wiki/M%C3%A9moire_de_masse) comme extension de la [mémoire vive](https://fr.wikipedia.org/wiki/M%C3%A9moire_vive)
    * d'augmenter le [taux de multiprogrammation](https://fr.wikipedia.org/wiki/Taux_de_multiprogrammation) (nombre de processus simultanés présents dans la mémoire à un instant donné)
    * de mettre en place des mécanismes de protection de la mémoire (verrous)
    * de partager la mémoire entre processus

![Descripteurs de Fichiers](./img/descripteurs-fichiers.svg){style="float:right;"}

* la Liste des **Descripteurs de fichiers** : pointeurs vers les ressources utilisées, fichiers, entrées/sorties, etc..
* la Liste de gestion des **Signaux**
* etc..

## Références & Notes

[^1]: [Guillaume Connan, Processus & Ressources](https://gitlab.com/lyceeND/tale/-/blob/master/2020_21/5-Archi_Reseau/8_processus_ressources.md)
[^2]: [Olivier LECLUSE, Gestion des Processus](https://www.lecluse.fr/nsi/NSI_T/archi/process/)
[^3]: [Processus & Threads, Linux Attitude](https://linux-attitude.fr/post/processus-et-threads)
[^4]: PCB - Process Block Control : [Wikipedia :fr:](https://fr.wikipedia.org/wiki/Process_control_block), [Wikipedia :gb:](https://en.wikipedia.org/wiki/Process_control_block)
[^5]: Memory Map : [Wikipedia :gb:](https://en.wikipedia.org/wiki/Memory_map)
[^6]: Mémoire Virtuelle / Virtual Memory : [Wikipedia :fr:](https://fr.wikipedia.org/wiki/M%C3%A9moire_virtuelle), [Wikipedia :gb:](https://en.wikipedia.org/wiki/Virtual_memory)
[^7]: [Architectures Matérielles, Processus, Pixees](https://pixees.fr/informatiquelycee/n_site/nsi_term_archi_proc.html)
[^8]: [Communication entre processus, par segment de mémoire partagée, Univ de Strasbourg](http://eavr.u-strasbg.fr/~christophe/cours/fip2/slides_shmem.pdf)
[^10]: [Ian Wienand: Computer Science from the Bottom Up : Elements of a process](https://www.bottomupcs.com/elements_of_a_process.xhtml)
[^11]: [File Descriptors](https://www.computerhope.com/jargon/f/file-descriptor.htm)