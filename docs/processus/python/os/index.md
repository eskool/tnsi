# Cours Python - module `os`

## PID, PPID, UID, EUID, EGID, SETUID

Avec le module `os`, on peut obtenir les PID, PPID, UID, EUGID, GID, EGID, et modifier l'UID.

```python linenums="0"
import os    
os.getpid())        # pid du processus
os.getppid())       # pid du père
os.getuid())        # uid du proprio réel (celui qui l'exécute)
os.geteuid())       # uid du proprio effectif (celui qui l'a créé)
os.getgid()         # n° du groupe réel
os.getegid()        # n° du groupe effectif

os.setuid(uid)      # change le propriétaire du processus
```

## Création d'un processus fils

En python, il y a plusieurs façons de créer un processus fils, par exemple :

* avec le module `os` : en faisant `newpid = os.fork()`, ou 
* avec la classe `Process` du module `multiprocessing` : en faisant `p = Process(target=f, args=('bob',))` ou `f` est une fonction et `‘bob’`, un paramètre d’entrée de cette fonction. Dans tous les cas, la fonction donne naissance à un processus fils par duplication du processus père. **Le processus fils partage le même espace mémoire adressable avec le processus père.**

* `fork()` renvoie un nombre **négatif** lorsqu'il y a eu un problème lors de la création.
* `fork()` renvoie `0` quand il est dans le fils,
* `fork()` renvoie le **pid du fils** quand il est dans le père (entier >0).

Le fils peut accéder aux données du père, mais s'il tente de les modifier, il y a duplication des données du père. Le fils travaille alors avec une copie des données et ne peut plus accéder au données du père.

```python
import os
import time

def pereFils():
      print("je suis le père")  
    
      newpid = os.fork()

      if newpid == -1:
          print("Erreur de création")
      elif newpid == 0: 
          print("Dans le fils")
          time.sleep(2)
      else: 
          print("Dans le père")
          time.sleep(2)
    
if __name__=="__main__":
    pereFils()
```