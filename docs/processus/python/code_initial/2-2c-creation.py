import os
import time

def pereFils():
      #Variables du père, qui seront dupliquées dans le fils
      s='{0} {1}'.format('PID du père ', os.getpid())
      a=3
      #Création du fils
      newpid = os.fork()

      if newpid == -1:
          print("Erreur de création")
      elif newpid == 0: # dans le fils
          #Variables modifiées par le fils donc dans son propre espace mémoire
          a=5
          s='{0} {1}'.format('PID du fils ',os.getpid())
          time.sleep(2)
      else: #newpid>0 -> dans le père
          time.sleep(2)
      for i in range(a):
          print(s)
          time.sleep(1)

if __name__=="__main__":
    pereFils()