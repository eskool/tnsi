from datetime import datetime
import time
import multiprocessing
from multiprocessing import Semaphore, Process


def proc1(sem1, sem2):

    sem1.acquire()
    try:
        print("Le processus: %r, a pris un jeton dans sem1 et utilise la resource 1, le nombre de jetons disponible dans sem1 est %r."
              % (multiprocessing.current_process().name, sem1.get_value()))
        time.sleep(2)
    finally:
        print("Le processus: %r, libére un jeton dans sem1 et n'utilise plus la resource 1."
              % (multiprocessing.current_process().name))
       
        sem1.release()
    print("Le processus: %r, demande un jeton dans sem2 pour accèder à la ressource 2, le nombre de jetons disponible dans sem2 est: %r"
              % (multiprocessing.current_process().name, sem2.get_value()))
    #time.sleep(1)
    sem2.acquire()
    try:
        
        print("Le processus: %r, a pris un jeton dans sem2 et utilise la resource 2, le nombre de jetons disponible dans sem2 est %r."
              % (multiprocessing.current_process().name, sem2.get_value()))
 
        time.sleep(3)
    finally:
        print("Le processus: %r, libére un jeton dans sem2 et n'utilise plus la resource 2."
              % (multiprocessing.current_process().name))
       
        sem2.release()
        
def proc2(sem1,sem2):

    sem2.acquire()
    try:
        
        print("Le processus: %r, a pris un jeton dans sem2 et utilise la resource 2, le nombre de jetons disponible dans sem2 est %r."
              % (multiprocessing.current_process().name, sem2.get_value()))
        time.sleep(5)
    finally:
        print("Le processus: %r, libére un jeton dans sem2 et n'utilise plus la resource 2."
              % (multiprocessing.current_process().name))
       
        sem2.release()
    print("Le processus: %r, demande un jeton pour accèder à la ressource 1, le nombre de jetons dans sem1 disponible est: %r"
              % (multiprocessing.current_process().name, sem1.get_value()))

    sem1.acquire()
    try:
        
        print("Le processus: %r, a pris un jeton dans sem1 et utilise la resource 1, le nombre de jetons disponible dans sem1 est %r."
              % (multiprocessing.current_process().name, sem1.get_value()))
 
        time.sleep(5)
    finally:
        print("Le processus: %r, libére un jeton dans sem1 et n'utilise plus la resource 1."
              % (multiprocessing.current_process().name))
       
        sem1.release()



if __name__ == '__main__':
    sem1 = Semaphore(1) 
    sem2 = Semaphore(1)  
    print("le nombre de jetons disponible dans le sémaphore 1 est: %r" % sem1.get_value())
    print("le nombre de jetons disponible dans le sémaphore 2 est: %r" % sem2.get_value())

    Process(target = proc1, args = (sem1, sem2)).start()
    Process(target = proc2, args = (sem1, sem2)).start()