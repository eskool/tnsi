import os
import time

def pereFils():
      print("je suis le père")  
      #Création du fils
      newpid = os.fork()

      if newpid == -1:
          print("Erreur de création")
      elif newpid == 0: # dans le fils
          print("Dans le fils")
          time.sleep(2)
      else: #newpid>0 -> dans le père
          print("Dans le père")
          time.sleep(2)
    
if __name__=="__main__":
    pereFils()