# Python program to explain os.waitid() method  
  
# importing os module   
import os  
  
# Create a child process 
# using os.fork() method  
pid = os.fork() 
  
  
# a Non-zero process id (pid) 
# indicates the parent process  
if pid>0 : 
      
    # Wait for the completion of 
    # child process using 
    # os.waitid() method 
  
    # Specify idtype 
    idtype = os.P_PID 
  
    # Specify id 
    id = pid 
      
    # Specify option 
    option = os.WEXITED 
  
    status = os.waitid(idtype, id, option) 
  
    print("\n\nDans le parent--") 
      
    # Print status 
    print("Status du fils::") 
    print(status) 
  
  
else : 
    print("Dans le premier enfant-") 
    print("Process ID:", os.getpid()) 
    print("Hello ! Geeks") 
    print("Exiting..") 