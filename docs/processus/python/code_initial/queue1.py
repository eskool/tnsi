#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 19:07:44 2020

@author: administrateur
"""

import os
from multiprocessing import Process, Lock, Queue
import queue
import time

def foo(q):
    print("P démarre")
    #time.sleep(10)
    q.put('hello')
def foo1(q):
    print("P1 démarre et attend P grâce au join")
    print("P1 affiche le résultat passé par P: ",q.get())

if __name__ == '__main__':
    q = queue.Queue()
    p = Process(target=foo, args=(q,))
    p1 = Process(target=foo1, args=(q,))
    p.start()
    p1.start()
    p.join()