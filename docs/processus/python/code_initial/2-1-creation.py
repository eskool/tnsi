from multiprocessing import Process
import os
import time

def f(nom):
    print('hello', nom)
    print('PID : ',os.getpid())
    print('PID du père : ',os.getppid())
    time.sleep(0.1)

if __name__ == '__main__':
    p = Process(target=f, args=('bob',))
    p.start()
