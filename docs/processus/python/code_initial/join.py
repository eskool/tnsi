from multiprocessing import Process
import time

def f(i):

    time.sleep(0.02)
    print('hello world1 ', i)
    time.sleep(0.02)
    print('hello world2 ', i)
    
    time.sleep(0.3)
    print('hello world3 ', i)

if __name__ == '__main__':
    for num in range(10):
        p = Process(target=f, args=(num,))
        p.start()
        p.join()
        