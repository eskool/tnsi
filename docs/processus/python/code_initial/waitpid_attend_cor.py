
# Python program to explain os.waitid() method  
  
# importing os module   
import os
import time
  
# Create a child process 
# using os.fork() method
print("pid du père1 %d"%(os.getpid()))
pid = os.fork() 
  
  
# a Non-zero process id (pid) 
# indicates the parent process  
if pid>0 : 
    print("pid du père2 %d"%(os.getpid()))
    # Create one more child process  
    pid2 = os.fork() 
      
    if pid2>0: 
        print("pid du père3 %d"%(os.getpid()))
        # Wait for the completion of 
        # any child processes using 
        # os.waitid() method 
  
        # Specify idtype 
        idtype = os.P_ALL 
  
        # Specify id 
        # As idtype is os.P_ALL 
        # method will wait for  
        # any children and specified id  
        # is ignored.              
        id = pid 
  
        # Specify option 
        option = os.WSTOPPED | os.WEXITED 
      
        status = os.waitid(idtype, id, option) 
  
        # print("EXITSTATUS:", os.WEXITSTATUS(status))
        print("EXITSTATUS:", status)
        print("\nDans le parent-") 
        time.sleep(2)
        # Print status 
        print("Status du fils:") 
        print("STATUT=",status[0])
  
    else : 
        print("\nDans le second enfant-") 
        print("2Process ID:", os.getpid())
        time.sleep(2)
        print("2Hey ! There  ") 
        print("2Exiting") 
  
  
else : 
    print("Dans le premier enfant-") 
    print("1Process ID:", os.getpid())
    time.sleep(2)
    print("1Hello ! Geeks") 
    print("1Exiting") 