import os
import time

def pereFils():
      print("je suis le père")  
      
      newpid = os.fork()

      if newpid == -1:
          print("Erreur de création")
      elif newpid == 0: 
          print("Dans le fils")
          time.sleep(2)
      else: 
          print("Dans le père")
          time.sleep(2)
    
if __name__=="__main__":
    pereFils()