from multiprocessing import Process, Lock
import time

def f(l, i):
    #l.acquire()
    print (l)
    time.sleep(0.02)
    print('hello world1 ', i)
    time.sleep(0.02)
    print('hello world2 ', i)
    
    #l.release()
    
    time.sleep(0.3)
    print('hello world3 ', i)

if __name__ == '__main__':
    lock = Lock()

    for num in range(10):
        Process(target=f, args=(lock, num)).start()