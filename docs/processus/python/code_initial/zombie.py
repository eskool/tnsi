# import os
# import time

# def pereFils():
#     print("je suis le père")
#     newpid = os.fork()

#     if newpid == -1:
#         print("Erreur de création")
#     elif newpid == 0: 
#         print("Dans le fils")
#         time.sleep(2)
#     else: 
#         print("Dans le père")
#         time.sleep(2)
    
# if __name__=="__main__":
#     pereFils()


import os
import time

def pereFils():
    #Variables du père, qui seront dupliquées dans le fils
    #Création du fils
    newpid = os.fork()

    if newpid == -1:
        print("Erreur de création")
    elif newpid == 0: # dans le fils
        #Variables modifiées par le fils donc dans son propre espace mémoire
        a=1
        print('{0} {1}'.format('PID du fils ',os.getpid()))
        time.sleep(0.2)
    else: #newpid>0 -> dans le père
        print('{0} {1}'.format('PID du père ',os.getpid()))
        time.sleep(30)

if __name__=="__main__":
    pereFils()