import os

def identifierProcessus():
    #identification du processus instancié par ce programme
    print("Identité du processus instancié par le programme identifier.py :")
    print('PID : ',os.getpid())
    print('PID du père : ',os.getppid())
    print('Le propriétaire réel est : ', os.getuid())
    print('Le propriétaire effectif est : ', os.geteuid())

if __name__=="__main__":
    identifierProcessus()
