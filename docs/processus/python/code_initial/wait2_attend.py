import os
import time


def pereFils():
    print("je suis le père")  
    #Création du fils
    newpid = os.fork()

    if newpid == -1:
        print("Erreur de création")

    elif newpid == 0: # dans le fils
        print("Dans le fils")
        for i in range(0, 5):
            print("Le fils écrit %d"%(i))
            time.sleep(2)
        print("Le processus fils de PID %d termine" %os.getpid())

    else: #newpid>0 -> dans le père
        print("Le père attend que le fils ait terminé")
        childProcExitInfo = os.wait()
        print(f"Type de Infos Processus : {type(childProcExitInfo)}")
        print("Le processus fils %d a terminé"%(childProcExitInfo[0]))
        print("Le père %d se termine après que le fils ait terminé"%(os.getpid()))
        # time.sleep(30)
      

if __name__=="__main__":
    pereFils()

