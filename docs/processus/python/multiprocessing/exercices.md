# TNSI - TD Multi-Processing en Python

Ce TD étudie la libriairie `multiprocessing` de Python, qui permet la gestion de plusieurs processus.

## Lancement de Processus 

1. Observez et commentez le code suivant.  
2. Précisez en particulier le nombre de Processus lancés, ce qu'ils affichent et les rôles des méthodes `os.getppid` et `os.getpid()`.

```python
import multiprocessing
from multiprocessing import Process
import os

def info(title):
    print(title)
    print('Processus : ', multiprocessing.current_process().name)
    print('parent process:', os.getppid())
    print('process id:', os.getpid())
    print('*' *60)

def f(name):
    info('fonction f')
    print('Bonjour', name)

if __name__ == '__main__':
    info('Bonjour depuis le main !')
    p1 = Process(target=f, name='Paul P',args=('Paul',))
    p2 = Process(target=f, name='Laura P', args=('Laura',))
    p1.start()
    p2.start()
    p1.join()
    p2.join()
```

## Accès aux ressources

1. Explorer la bibliothèque `multiprocessing` de Python : `https://docs.python.org/fr/3/library/multiprocessing.html`
Chercher dans la documentation la fonction permettant d'afficher le nombre de coeurs disponibles. 

2. Lancer quelques processus en parallèle en utilisant un `Pool` du module `multiprocessing`


## Utilisation d'un Verrou

Observez le code suivant :

```python
from multiprocessing import Process, Lock
import time

def f(l, i):
    l.acquire()
    try:
        print('hello world', i)
        time.sleep(1)
        print('Bonjour le Monde', i)
        print('*' * 60)
    finally:
        l.release()


if __name__ == '__main__':
    lock = Lock()
    for num in range(10):
        Process(target=f, args=(lock, num)).start()
```

1.  

    a. Que représente la classe `Lock` ?
    b. A quoi servent les méthodes `acquire` et `release` de la class `Lock` ?
    c. Quel est l'affichage produit par l'exécution de ce programme ?
      Vous semble-t-il cohérent ?

2.  

    a. Refaire une version du même code gardant la même structure mais en enlevant l'utilisation du verrou.
    b. Qu'observez-vous cette fois ?
    c. Expliquez ce comportement


## Traitements parallèles sur des images

1. Observer et commenter le code suivant qui utilise des filtres sur des images comme dans le projet NSI 1ère fiche 45. Notez bien que les tableaux `numpy` manipulés ont comme taille hauteur x largeur x 3 avec la dernière colonne représentant les 3 couleurs R, G et B.  

```python
def decoupe_quadrants(img):
    imgnp = np.array(img) # Transformation de l'image en tableau numpy
    h,w,_ = np.shape(imgnp)
    im_inv = inverse(imgnp)
    assert h % 2 == 0, f"{h} lignes : pas divisible par 2"
    assert w % 2 == 0, f"{w} colonnes : pas divisible par 2"
    un = imgnp[0:h//2 , 0:w//2,:]
    deux = imgnp[0:h//2 , w//2:w,:]
    trois = imgnp[h//2:h , 0:w//2,:]
    quatre = imgnp[h//2:h , w//2:w,:]
    return [un,deux,trois,quatre]

def reconstitue_image(quadrants):
    paq1 = np.concatenate((quadrants[0],quadrants[1]), axis=1)
    paq2 = np.concatenate((quadrants[2],quadrants[3]), axis=1)
    image_reconstituee = np.concatenate((paq1, paq2), axis=0)
    return image_reconstituee

def inverse_boucle(im):
    im2 = im.copy()
    # Double boucle pour parcourir tous les pixels
    for i in range(im.shape[0]):
      for j in range(im.shape[1]):
        im2[i, j] = 255 - im[i, j] 
    return im2

def filtre_parallel(img, filtre):
    pool = Pool(2)
    quadrants = decoupe_quadrants(img)
    return reconstitue_image(np.array(pool.map(filtre, quadrants)))

def inv_boucle_paralle(img):
    return filtre_parallel(img,inverse_boucle)

def inv_directe(img):
    img_inv = np.array(img)
    return inverse_boucle(img_inv)

if __name__ == '__main__':
    for filtre in inv_directe, inv_boucle_paralle:
        img = Image.open("ada.jpg")
        debut = time.time()
        neg = filtre(img)
        nom = "ada-{}.png".format(filtre.__name__)
        Image.fromarray(neg).save(nom)
        temps = time.time() - debut
        print(filtre.__name__, temps)
```

2. Est-ce que le traitement en parallèle fait gagner du temps ? Qu'observez-vous si on augmente la taille du `Pool` ?

3. En fait, on peut aussi effectuer avec numpy des opérations matricielles s'appliquant à tout le tabelau représentant l'image. Pour l'image en négatif, on aurait pu directement faire une opération matricielle unique  :

```python
def inverse(t):
    return 255 - t
```

Quelles sont les performances comparées de cette méthode ?
