# TNSI : OS - Programmation Concurrente vs Parallèle

* En **Programmation Séquentielle**, les opérations/instructions (déléguées à des processus/threads/tâches) utilisant des mêmes ressources (un même CPU, ou un même coeur dans un CPU multicoeur) sont exécutées **séquentiellement**, càd les unes à la suite des autres, sur des intervalles de temps **non entrelacés** (qui ne se chevauchent pas, entre leur début et leur fin). 
* Au contraire, en **Programmation Concurrente** : **les intervalles de temps d'exécution sont (peuvent être) entrelacés**.

## Programmation Concurrente

!!! def "Programmation Concurrente"
     La <bred>Programmation Concurrente</bred>, est un **paradigme de programmation** dans lequel les opérations (déléguées à des processus/threads/tâches) utilisant des mêmes ressources (un même CPU, ou un même coeur dans un CPU multicoeur) sont exécutées de manière **concurente**, càd durant des **intervalles de temps entrelacés** (qui se chevauchent, entre leur début et leur fin). Dans la programmation concurrente, un calcul peut avancer sans devoir attendre la fin de l'exécution des autres :
     
        * Cela peut être une qualité/le comportement souhaité, si les calculs (processus/threads) sont indépendants
        * mais également une source de conflits les calculs (processus/threads) ne sont pas indépendants

### Séquences d'Instruction

Un programme est une <rb>séquence d'instructions</rb>[^1] et le résultat qu'il doit fournir est directement relié à l'ordre particulier dans lequel les instructions sont exécutées. Une manière de considérer l'exécution d'un programme est de se placer au niveau du processeur. Il s'agit de regarder la **séquence d'instructions machines** qui est soumise au processeur. La séquence implique un aspect temporel. Chaque instruction est exécutée à un moment précis. Le résultat est déterminé par la suite d'instructions. La suite d'instructions constitue un profil ou une signature du programme. 

### Variation des Temps d'accès

Un programme correct doit être reproductible (<rb>déterminisme</rb>) et donc posséder une signature ou profil particulier et distinctif. Lorsque deux programmes s'exécutent en même temps, **en parallèle**, nous pouvons considérer que nous avons deux séquences d'instructions parallèles. Néanmoins, il se peut que le rythme de progression des séquences ne soit pas constant dans le temps. A un moment donné, une séquence peut aller plus rapidement et plus tard ralentir. Dans un système complexe, plusieurs causes peuvent expliquer un tel changement de rythme. La mémoire virtuelle peut impliquer un délai variable selon que le contenu demandé est déjà en mémoire ou non. **Le temps d'accès à une information sur disque est variable**.

### Entrelacement & Non Déterminisme

!!! pte "Les Processus/Threads/Tâches sont concurrents"
    Les processus/threads/tâches sont concurrents : leurs exécutions sont entrelacées.
    C'est l'ordonnanceur qui décide de leur ordre de contrôle.  
    Cela permet l'exécution d'un grand nombre de programmes (donc de processus/threads/tâches) sur une machine monoprocesseur.

La variation dans la progression des programmes est sans importance si les deux programmes sont indépendants. C'est-à-dire que les résultats et la progression d'un programme ne sont pas affectés par les instructions exécutées par l'autre programme. Cependant, si les deux programmes sont dépendants, il faut préciser si une instruction d'un programme a eu lieu avant ou après une instruction de l'autre programme. Il faut alors tenir compte de la progression stricte dans le temps des deux programmes. On peut ainsi être amené à combiner les deux séquences en une seule, tenant compte de la progression relative des deux séquences. La séquence produite va consister en un <rb>Entrelacement</rb> des deux séquences originales. Il nous faut considérer tous les cas d'entrelacements possibles. Il existe plusieurs entrelacements possibles, et l'on ne peut pas à priori prévoir (hasard) laquelle va se produire : <rb>Non Déterminisme</rb>

Même sur une machine avec un seul processeur, l'exécution concurrente de processus nous force à considérer tous les entrelacements possibles. En effet, on ne sait pas à quel moment particulier un processus va être interrompu pour permettre la reprise d'un autre processus. Le hasard peut faire en sorte qu'on obtienne n'importe laquelle des séquences possibles. Le besoin de synchronisation est toujours présent. Cependant, la présence d'un seul processus facilite l'implantation des mécanismes de synchronisation. Le noyau s'en occupe et s'assure de ne pas être interrompu ou perturbé dans son travail.

### Synchronisation

* Pour des programmes indépendants, tous ces entrelacements sont acceptables. 
* Cependant, pour des programmes dépendants, seulement quelques uns de ces cas sont acceptables. Les autres doivent être évités, car ils conduisent à des résultats erronés, incohérents ou à la catastrophe (par ex. plantage du SE). Dans la programmation de <rb>programmes concurrents</rb> (et, sous-entendu, dépendants), il faut introduire des mécanismes qui permettent de synchroniser les processus et d'éliminer les séquences catastrophiques. La <rb>Synchronisation</rb> (entre processus) implique que la progression d'un processus est éventuellement contrôlée par un autre. Cela implique l'arrêt éventuel d'un processus.

### Coopération & Sections Critiques

La <rb>coopération</rb> entre processus peut prendre plusieurs formes :  

1°) échange d'information (par exemple, par mémoire partagée ou par message)  
2°) synchronisation (par verrous, sémaphores, moniteurs, etc.. )  
3°) et comme un cas particulier mais fréquent, la <rb>section critique</rb> qui est une portion de code ne devant être exécutée que par un seul processus simultanément (pour éviter les incohérences). Dans la section critique, on dispose d'une mémoire commune (ou autres ressources) mais on doit se synchroniser pour assurer l'intégrité des données.



## Un Exemple d'Entrelacements Possibles

Disons que l'on dispose des Processus et leurs Séquences suivantes :

!!! col __50 center
    ```console
    Processus P1
     +--------+
     |   S1   |
     |   S2   |
     +--------+
    ```

!!! col __50 center clear
    ```console
    Processus P2
     +--------+
     |   S3   |
     |   S4   |
     +--------+
    ```

<center>
Processus Concurrents
</center>

### SANS SYNCHRONISATION

Sans Syncrhonisation, du point de vue temporel, toutes les Séquences (états) Suivantes sont possibles :  

<center>$\rightarrow$ S1 $\rightarrow$ S2 $\rightarrow$ S3 $\rightarrow$ S4</center>
<center>$\rightarrow$ S1 $\rightarrow$ S3 $\rightarrow$ S2 $\rightarrow$ S4</center>
<center>$\rightarrow$ S1 $\rightarrow$ S3 $\rightarrow$ S4 $\rightarrow$ S2</center>
<center>$\rightarrow$ S3 $\rightarrow$ S1 $\rightarrow$ S2 $\rightarrow$ S4</center>
<center>$\rightarrow$ S3 $\rightarrow$ S1 $\rightarrow$ S4 $\rightarrow$ S2</center>
<center>$\rightarrow$ S3 $\rightarrow$ S4 $\rightarrow$ S1 $\rightarrow$ S2</center>
<center>etc..</center>

### AVEC SYNCHRONISATION

Une Synchronisation revient à réduire les séquences possibles, car seulement certains ordres sont considérés acceptables/souhaitables.

![Synchronisation avec Attente, Sémaphore a=0, b=0](./img/processus-concurrents-avec-attente.png){.center style="width:80%; max-width:600px;"}
<center>
Avec Synchronisation (Attente, avec deux Sémaphores a=0, b=0)  
Ici, une seule séquence possible : $\rightarrow$ S1 $\rightarrow$ S3 $\rightarrow$ S4 $\rightarrow$ S2  
En effet :  
:one: P1 exécute S1, puis signale sa fin d'exécution avec `signal(a)`  
:two: P2 attendait ce signal, via `wait(a)`, et alors
:three: le processus S3, puis S4 s'exécutent (P2 ne peut être interrompu avant la fin de S4, car P1 attend le `signal(b)`)  
:four: P2 signale alors la fin de S4 avec `signal(b)`  
:five: Puis P1, qui attendait ce signal avec `wait(b)`, peut reprendre, et exécuter S2
</center>


![Synchronisation avec Attente avec Section Critique, Sémaphore ex=0](./img/processus-concurrents-avec-attente-section-critique.png){.center style="width:80%; max-width:600px;"}
<center>
Avec Synchronisation (Attente pour entrer dans une Section Critique, avec Sémaphore ex=0)
</center>

## Un Exemple Réel 

Le programme concurrent P suivant est composé de deux processus, chacun contient des instructions qui manipulent une variable partagée `x`.

!!! col __50
    ```pseudo
    # Programme P
    Entier x = 2      // variable partagée
    # Processus A
    a1: x = x+1
    a2: x = x+2
    a3: FIN
    ```

!!! col __50 clear
    ```pseudo
    # Programme P
    # Processus B
    b1: x = x*3
    b2: FIN
    ```

Quel est le comportement de P ? Quelle est la valeur finale de `x` après l’exécution de P ?  
Une exécution consiste en un entrelacement des actions de chaque processus : chaque processus exécute une ou plusieurs instructions, puis c’est à un autre d’avancer etc.  
Les instructions sont donc exécutées les unes à la suite des autres et non pas en même temps.. Cette sémantique repose sur une notion importante : les instructions **atomiques**, qui ne peuvent pas être interrompues (une fois commencéee, une instruction atomique est exécutée complètement) :

!!! exp
    On peut supposer que :
    
    * la **lecture** du contenu d’une variable est une opération atomique
    * une **affectation** de variable est une opération atomique (on parle de registres atomiques). 
    * Mais qu’en est-il d’une instruction comme `x=x+1` ou `x=x*3` ? Ici il y a, à la fois lecture de `x`, un calcul d’une valeur puis une affectation.. On peut donc supposer qu’une telle instruction se décompose en (au moins) deux étapes :
        * la lecture de `x` et 
        * son affectation dans une variable locale au processus, qui sera utilisée pour le calcul, puis 
        * l’affectation du résultat dans `x`

L'instruction `x = x + 1` peut donc se représenter par :

```pseudo
temp = x
x = temp + 1
```

Un tel choix est important sur le comportement global du programme. A titre d’exemple, on donne à la figure 3 le diagramme d'état du programme précédent lorsque chaque instruction `a1`, `a2` et `b1` est considérée comme étant atomique. Et à la figure 4, on donne le diagramme d'état correspondant à la sémantique utilisant des `temp`, c’est-à-dire au programme P' de la figure 2.

```pseudo
# Programme P`
Entier x = 2      # variable partagée

# Processus A
a1: temp = x
a1`: x = temp + 1
a2: temp = x
a2': x = temp + 2
a2: FIN

# Processus B
b1: temp = x
b1’: x = temp * 3
b2: FIN
```

<center>
Programme P'
</center>

<center>

```mermaid
flowchart 
    A0["(a1,b1,2)"] --> A10["(a2,b1,3)"]
    A0["(a1,b1,2)"] --> A11["(a1,b2,6)"]
    A10 --> A20["(a3,b1,5)"]
    A10["(a2,b1,3)"] --> A21["(a2,b2,9)"]
    A11["(a1,b2,6)"] --> A22["(a2,b2,7)"]
    A20 --> A30["(a3,b2,15)"]
    A21 --> A31["(a3,b2,11)"]
    A22 --> A32["(a3,b2,9)"]
```

Diagramme d'états de P
</center>

On voit donc que :

* dans le premier cas (pour P, sans variable `temp`), les valeurs finales possibles pour `x` sont `9`, `11` ou `15`. 
* dans le deuxième cas (pour P' avec `temp`) les valeurs finales possibles sont : 5, 6, 8, 9, 11 et 15.


## Programmation Concurrente

### Définition

!!! def "Programmation Concurrente"
     La <bred>Programmation Concurrente</bred>, est un **paradigme de programmation** dans lequel les opérations (déléguées à des processus/threads/tâches) utilisant des mêmes ressources (un même CPU, ou un même coeur dans un CPU multicoeur) sont exécutées de manière **concurente**, càd durant des intervalles de temps **entrelacés** (qui se chevauchent, entre leur début et leur fin). Dans la programmation concurrente, un calcul peut avancer sans devoir attendre la fin de l'exécution des autres.

### Les Processus/Threads sont Concurrents

!!! pte "Les Processus/Threads/Tâches sont concurrents"
    Les processus/threads/tâches sont concurrents : leurs exécutions sont entrelacées.
    C'est l'ordonnanceur qui décide de leur ordre de contrôle.  
    Cela permet l'exécution d'un grand nombre de programmes (donc de processus/threads/tâches) sur une machine monoprocesseur.

### Vie Réelle & Concurrence

La programmation concurrente est indispensable lorsque l'on souhaite écrire des programmes interagissant avec **le monde réel (qui est concurrent)**, ou tirant parti de multiples unités centrales (couplées, comme dans un système multiprocesseurs, ou distribuées, éventuellement en grille ou en grappe). 

### Langages de Programmation Concurrents

La programmation concurrente est plus complexe et difficile que la programmation impérative, fonctionnelle ou encore déclarative. En fait, à chacun de ces modèles de programmation, on peut associer une version concurrente, par extension de la sémantique du langage de programmation associé. Par exemple:

* **Prolog** a été étendu en **Concurrent Prolog**, 
* **Haskell** avec **Concurrent Haskell**, 
* **Java** et **Ada** sont des langages à objets avec des primitives (natives) pour la concurrence, 
* etc.

## Parallélisation & Programmation Parallèle

### Parallélisation

!!! def "Parallélisation"
    La <red>Parallélisation</red> est un ensemble de techniques **matérielles** et **logicielles** permettant l'exécution simultanée de séquences d'instructions indépendantes, sur des CPU (processeurs) différents.

### Pourquoi Paralléliser ?

Traitement de problèmes scientifiques : météo, climat, biologie, géophysique, chimie ...

* Applications : bases de données, web, ...
* Parce qu'on n'a plus le choix !
* La multiplication du nombre d'unités de calcul ne réduit pas spontanément le
temps d'exécution des programmes ...

![Processeurs et Parallélisme](./img/processeurs-et-parallelisme.png){.center}

### Programmation Parallèle

!!! def "Programmation Parallèle"
    La <red>Programmation Parallèle</red> désigne l’**exécution simultanée de tâches/processus sur des CPU différents (ou des coeurs différents dans un CPU multicoeur)**.

Il s’agira alors de pouvoir synchroniser des processus entre eux, ceci principalement au travers d’une
mémoire partagée ou de liaisons réseau.

## Programmation Concurrente vs Programmation Parallèle

Le terme programmation concurrente ne doit pas être confondu avec celui de programmation parallèle (ou programmation répartie).

## Références & Notes

### Références

* [Cours Programmation Concurrente, N. Hameurlain, Univ Pau](https://hameur.perso.univ-pau.fr/Cours/Para/Conc.pdf)
* [Cours Programmation Concurrente & Parallèle, Ousmane THIARE, Univ Gaston Berger de Saint Louis](http://www.ousmanethiare.com/images/cours/pcp.pdf)
* [Introduction à la Programmation Parallèle Multicoeus](http://devlog.cnrs.fr/_media/groupe-de-travail/jdev-2011/programme-planning-intervenants/programme/introduction-au-calcul-parall%C3%A8le.pdf)
* [Architectures & Paradigmes de Programmation Parallèle, Violaine Louvet, CNRS Lyon](http://plasmasfroids.cnrs.fr/IMG/pdf/ECOMOD2010_Louvet1.pdf)

### Notes

[^1]: [Gabriel Girard, Cours Processus Concurrents et Parallélisme, Univ Sherbrooke, :ca:](https://info.usherbrooke.ca/GabrielGirard/cours/ift-630-processus-concurrents-et-parallelisme/documents/programmation-concurente.pdf/@@download/file/Programmation%20concurente.PDF)