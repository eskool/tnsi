# TNSI : Recherche dans un Texte<br/>Activité 1 : Méthode `find()` de Python

| Contenus| Capacités<br/>Attendues | Commentaires |
|:-:|:-:|:-:|
| Recherche textuelle. | Étudier l'algorithme de Boyer-Moore<br/> pour la recherche d'un<br/>motif dans un texte.| L'intérêt du prétraitement du motif<br/>est mis en avant.<br/>L'étude du coût, difficile, ne peut<br/>être exigée. |

<img src="./boyer_moore/gif_naive.gif" />

Le but de cette leçon est d'étudier/inventer des algorithmes de recherche de la première occurrence d’un **motif** de longueur p dans un **texte** de longueur n.  
On parle de **recherche textuelle**.

## Fonctions natives de Python

1. Consulter la [documentation de Python sur la méthode `find`](https://docs.python.org/fr/3/) des chaînes de caractères.

2. Quel est le rôle de cette méthode `find()` ?

3. Tester cette méthode sur les exemples suivants, (par exemple dans une console Python) :  

    a. `'numérique et sciences informatiques'.find('que')`  
    b. `'numérique et sciences informatiques'.find('nsi')`

!!! remarque
    En Python, une autre méthode presque identique (`str.index`) lève une erreur lorsque le motif cherché ne se trouve pas dans la chaîne.

## Exemple d'Utilisation de `find()`

### Télécharger un libre dans le Domaine Public au format `.txt`

Télécharger en local (sur votre ordinateur) un livre classique de la littérature française, dont les droits sont tombés dans le **domaine public**, au format `Plain Text UTF-8` (concrètement cela veut dire au format `.txt` encodé en `utf-8`), sur le site:

*  http://www.gutenberg.org/browse/languages/fr

A titre d'exemple, voici un lien menant vers le **Don Juan de Molière, ou le Festin de pierre**, à sauvegarder en local sous le nom `donJuan.txt`[^1] :

* https://www.gutenberg.org/cache/epub/5130/pg5130.txt

### Lire et Faire des recherches dans le fichier

Une fois ce fichier téléchargé en local, on pourra alors charger en mémoire ce roman par ces quelques lignes pour chercher ensuite si le motif 'Julien trembla' apparaît quelque part dans le roman.

```python linenums="1"
fichier = open('donJuan.txt', 'r', encoding="utf-8")
donjuan = fichier.read()
fichier.close()
```

1. Déterminer la position (l'indice) du motif `vérité` dans le texte stocké dans la variable `donjuan`.
2. Créer une fonction `nbOccurences(texte:str, motif:str)->int` qui compte le nombre d'Occurences du `motif` dans le `texte`.

    ??? corrige
        ```python linenums="1"
        def nbOccurences(texte:str, motif:str)->int:
            compteur, i = 0, 0
            while True:
                occurrence = texte.find(motif, i)
                # occurrence est l'index du sous-texte à partir de l'index i
                if occurrence == -1:
                    return compteur
                else:
                    compteur += 1
                    i = occurrence + 1
        ```

[^1]: Vous pouvez choisir un autre livre si vous le souhaitez, mais il faudra adapter la totalité de la suite..