# Aller plus loin : Les Expressions Régulières - Regex

Les expressions régulières sont des syntaxes permettant de faire des recherches dans un texte, qui suivent un certain motif / modèle / pattern.

## Caractères Littéraux

Un caractère au sens usuel est appelé un **caractère littéral**

## Méta-caractères

Un méta-caractère est un caractère représentant tout caractère d'un *certain genre*, suivant un certain <red>motif</red> :fr: /pattern :gb:

Les méta-caractères se répartissent classiquement en 3 groupes distincts

### Les (méta-)caractères uniques

Un <red>méta-caractère unique</red> représente un unique caractère **suivant un certain motif/pattern** :

| Méta-caractère | Signification |
|:-:|:-:|
| `\d` | Tout chiffre de `0` à `9` |
| `\w` | Tout caractère *classique* d'un mot :<br>`A-Za-z0-9` |
| `\s` | Tout caractère représentant *un espace* :<br/>un espace, ou une tabulation |
| `.` | Tout caractère unique, quel qu'il soit |


### Les Quantificateurs

Les <red>Quantificateurs</red> :fr: / <red>Quantifiers</red> :gb: permettent de représenter un certain nombre de fois un (méta-) caractère unique.

| Méta-caractère | Signification |
|:-:|:-:|
| `*` | `0` ou plusieurs fois |
| `+` | `1` ou plusieurs fois |
| `?` | `0` ou `1` <br/>(à placer après) |
| `{min, max}` | un nombre `n` de fois avec $min \leq n \leq max$ |
| `{n}` | un nombre `n` de fois, exactement |

!!! warning
    * Le quantificateur `.*` est **glouton :fr: / greedy :gb: par défaut** : il sélectionne le plus grand match possible (au sens de l'inclusion) qui satisfait la regex.
        * Exemple : Dans le texte `1blablacara4est4foismeilleur` la regex `1.*4` va matcher `1blablacara4est4` càd le dernier 4 (et non pas `1blablacara4`, càd non pas le premier 4)  
    * On peut modifier ce comportement en ajoutant un caractère `?`, appelé un modificateur  :
        * Exemple : Dans le texte `1blablacara4est4foismeilleur` la regex `1.*?4` va maintenant matcher (seulement) `1blablacara4` càd le premier 4 (et non plus `1blablacara4est4` càd le dernier 4, comme précédemment)

### Les méta-caractères de Position

| Méta-caractère | Signification |
|:-:|:-:|
| `^` | qui commence par .. |
| `$` | qui termine par .. |
| `\b` | à frontière de mot<br/>(word <b>b</b>oundary) |

## Classes de Caractères

Les classes de caractères permettent de représenter le OU LOGIQUE (un caractère OU bien un autre, etc..), ou bien des plages de caractères suivant une certaine logique.  
Les classes de caractères sont représentées entre des `[  ]`

Par exemple :

| Exemple de classe<br/>de caractère | Signification |
|:-:|:-:|
| `[abc]` | un `a`, ou un `b` ou un `c` |
| `[-.]` | un `-`, ou un `.` |

!!! warning "caractères littéraux dans les classes"
    * un méta-caractère échappé avec l'anti-slash `\` représente le même caractère au sens littéral :
        * `\.` représente le caractère point littéral 
    * un point `.` à l'intérieur d'une classe de caractères ne signifie plus *tout caractère*, mais le caractère littéral du point `.`

!!! pte
    A l'intérieur des classes, néanmoins, les caractères suivants **peuvent** avoir une signification particulère (méta-caractères dans les classes), selon leur position précise dans la classe :

    | méta-caractère<br/>dans une classe | Signification |
    |:-:|:-:|
    | `-` | une plage de caractères |
    | > | `[a-z]` tout caractère de `a` à `z` |
    | > | `[a-c0-5]` tout caractère de `a` à `c`<br/>ou, tout chiffre de `0` à `5` |
    | `^` | en première position d'une classe, le `^` représente<br/> une négation logique :<<br/>tout caractère autre que ...<br/>Au milieu d'une classe, le `^` représente le caractère littéral `^` |
    | > | `[^0-5]` tout caractère autre qu'un chiffre de `0` à `5` |
    | > | `[0-5^]` tout chiffre entre `0` et `5` ou un caractère `^` littéral  |

## Alternance = OU LOGIQUE sur des groupes de caractères

L'alternance permet de choisir un groupe de lettres ou bien ou autre groupe de lettres

| Alternance | Signification |
|:-:|:-:|
| `(net | com)` | le groupe `net`, ou bien le groupe `com` |

## Groupes et Références Arrière

Les parenthèses permettent de capturer des **(sous-)groupes** d'un résultat d'une regex.
Prenons des exemples :

### Groupes

#### group[0]

On considère ici :

* un texte contenant le numéro : `314-666-5678`
* la regex : `\d{3}-\d{3}-\d{4}` qui sélectionne bien ce numéro

Le *match* global, s'appelle `group[0]`
Ici : `group[0]` correspond donc à `314-666-5678`

#### group[1]

On considère ici :

* un texte contenant le (même) numéro : `314-666-5678`
* la regex `\d{3}-(\d{3})-\d{4}` avec des parenthèses cette fois-ci

`group[0]` représente encore le match global : `314-666-5678`. De plus :  
`group[1]` représente ici `666` : En effet, l'utilisation de parenthèses dans cette regex, signifie que cette (sous-)partie du *match global* doit être capturée séparément, ou additionnellement, dans un autre groupe, appelé `group[1]`

#### group[2]

On considère ici :

* un texte contenant le (même) numéro : `314-666-5678`
* la regex `\d{3}-(\d{3})-(\d{4})` avec des parenthèses cette fois-ci

`group[0]` représente encore le match global : `314-666-5678`. De plus :  
`group[1]` représente encore `666`.  
`group[2]` représente ici `5678` : En effet, l'utilisation de parenthèses dans cette regex, signifie que cette (sous-)partie du *match global* doit encore une fois être capturée séparément, ou additionnellement, dans (encore) un autre groupe, appelé `group[2]`

etc..

### Références Arrière

On peut faire référence à ces groupes, ou bien lors d'un replace, ou dans la regex elle-même, en utilisant des notations particulières :

* `$1` fait référence au groupe 1 lors d'un *replace*, et qu'on veut faire référence au groupe capturé n°1
* `\1` fait référence au groupe 1 depuis l'intérieur de la regex elle-même
* `$2` fait référence au groupe 2 lors d'un *replace*, et qu'on veut faire référence au groupe capturé n°2
* `\2` fait référence au groupe 2 depuis l'intérieur de la regex elle-même
* etc..



