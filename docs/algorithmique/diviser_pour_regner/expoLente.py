def expoLente(x:float,n:int)->int:
  if n == 0: # on calcule x^0
    return 1
  else:
    return x*expoLente(x,n-1)

