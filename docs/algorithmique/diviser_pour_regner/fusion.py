# def interclasser(A1:list,A2:list)->list:
#   Ainter = []
#   n1, n2 = len(A1), len(A2)
#   i1, i2 = 0,0 # indices resp. dans A1 et dans A2
#   while i1 < n1 and i2 < n2:
#     if A1[i1] < A2[i2]:
#       Ainter.append(A1[i1])
#       i1 += 1
#     else:
#       Ainter.append(A2[i2])
#       i2 += 1
#   return Ainter+A1[i1:]+A2[i2:]

def interclasser(A1:list,A2:list)->list :
    if A1 == []:
        return A2
    elif A2 == []:
        return A1
    elif A1[0] >= A2[0]:
        return [A2[0]] + interclasser(A1, A2[1:])
    else:
        return [A1[0]] + interclasser(A1[1:], A2)

def fusion(A:list)->list:
  if len(A) <= 1:
    return A
  m = len(A) // 2
  return interclasser(fusion(A[:m]), fusion(A[m:]))

A=[4,3,8,2,7,1,5]
B=fusion(A)
print("fusion(A) = ",B)


