def expo(a:float,n:int)->float:
  if n == 0:
    return 1
  elif n%2 == 0:
    return expo(a*a,n/2)
  else:
    return a*expo(a*a,(n-1)/2)

a = 2
n = 4
print(f"{a}^{n} = {expo(a,n)}")