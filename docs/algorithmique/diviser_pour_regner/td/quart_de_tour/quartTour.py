from PIL import Image
img = Image.open("img/bateau.jpg")
largeur, hauteur = img.size
n = largeur
img_rot = Image.new("RGB",(largeur,hauteur))
for x in range(n):
  for y in range(n):
    # 1/4 Tour Droite : Solution 1
    # le pixel (x,y) de l'image rotationnée droite provient du pixel (y,n-1-x) de l'image initiale
    (r,g,b) = img.getpixel((y,n-1-x)) # getter de la composante RGB du pixel (x,y)
    img_rot.putpixel((x,y),(r,g,b)) ## setter de la composatnte RGB du pixel (x,y)
    
    # 1/4 Tour Droite : Solution 2
    # On mémorise le pixel (x,y) de l'image initiale
    # On place ce pixel à sa nouvelle position après rotation droite
    # (r,g,b) = img.getpixel((x,y)) # getter de la composante RGB du pixel (x,y)
    # img_rot.putpixel((n-1-y,x),(r,g,b)) ## setter de la composatnte RGB du pixel (x,y)

    # 1/4 Tour Gauche : Solution 1
    # Le pixel (x,y) de l'image rotationnée Gauche, est envoyé après rotation droite, 
    # sur le pixel (n-1-y,x) de l'image initiale.
    # (r,g,b) = img.getpixel((n-1-y,x)) # getter de la composante RGB du pixel (x,y)
    # img_rot.putpixel((x,y),(r,g,b)) ## setter de la composatnte RGB du pixel (x,y)

    # 1/4 Tour Gauche : Solution 2
    # Le pixel (x,y) de l'image initiale doit être placé en (y,n-1-x) après rotation Gauche,
    # de sorte qu'après rotation droite il se retrouve de nouveau en (x,y)
    # (r,g,b) = img.getpixel((x,y)) # getter de la composante RGB du pixel (x,y)
    # img_rot.putpixel((y,n-1-x),(r,g,b)) ## setter de la composatnte RGB du pixel (x,y)

img_rot.save("img/bateauRot.jpg") # sauvegarde l'image
img_rot.show() # Affiche l'image

