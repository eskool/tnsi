from PIL import Image

def echangePixels(image,x0,y0,x1,y1):
  a = img.getpixel((x0,y0))
  b = img.getpixel((x1,y1))
  img.putpixel((x0,y0),b)
  img.putpixel((x1,y1),a)

def echangeQuadrants(image,x0,y0,x1,y1,n):
  for i in range(n):
    for j in range(n):
      echangePixels(image,x0+i,y0+j,x1+i,y1+j)

def tourneQuadrants(image,x0,y0,n):
  if n>=2 :
    m = n // 2
    tourneQuadrants(image,x0,y0,m)              # Tourne A
    tourneQuadrants(image,x0+m,y0,m)            # Tourne B
    tourneQuadrants(image,x0,y0+m,m)            # Tourne C
    tourneQuadrants(image,x0+m,y0+m,m)          # Tourne D
    echangeQuadrants(image,x0,y0,x0+m,y0,m)     # Echange A et B  --> B passe en (x0,y0), A en (x0+m,y0)
    echangeQuadrants(image,x0,y0,x0+m,y0+m,m)   # Echange B et D  --> D passe en (x0,y0), B en (x0+m,y0+m)
    echangeQuadrants(image,x0,y0,x0,y0+m,m)     # Echange D et C  --> C passe en (x0,y0), D en (x0,y0+m)

def tourneImage(image):
  n, p = image.size
  assert n == p
  tourneQuadrants(image,0,0,n)

# Programme Principal
img = Image.open("img/bateau.jpg")
tourneImage(img)
img.show() # Affiche l'image
img.save("img/bateauRotDivPourRegner.jpg")

