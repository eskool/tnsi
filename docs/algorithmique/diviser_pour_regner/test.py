def interclasser(A1:list,A2:list)->list:
  Ainter = []
  n1, n2 = len(A1), len(A2)
  i1, i2 = 0,0 # indices resp. dans A1 et dans A2
  while i1 < n1 and i2 < n2:
    if A1[i1] < A2[i2]:
      Ainter.append(A1[i1])
      i1 += 1
    else:
      Ainter.append(A2[i2])
      i2 += 1
  return Ainter+A1[i1:]+A2[i2:]

def fusion(A:list)->list:
  if len(A)<=1:
    return A
  m = len(A) // 2
  return interclasser(fusion(A[:m]), fusion(A[m:]))

A1 = [2,3,4,8,9,10,11,14,15]
A2 = [1,5,7]
A = [6,8,2,16,24,5,30,17,9,1,10]
Afinal = interclasser(A1,A2)
print("Interclassement = ",Afinal)
print("A NON TRIE=",A)
print("Tri Fusion (A TRIE) =",fusion(A))

