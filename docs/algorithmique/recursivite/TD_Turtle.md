<div class="titre">TD Turtle Récursivité</div>

<br/>

# Activité préliminaire débranchée : 

Dessiner sans lever le stylo et sans passer deux fois sur le même trait la figure suivante 

<center>
<img src="carres.png" style="width:30%;">
</center>

# Fonction Récursive
Proposer une fonction récursive, à 2 paramètres (la longueur du plus grand carré, le nombre de carrés imbriqués) qui réalise le tracé.