import time
import numpy as np
import matplotlib.pyplot as plt

# ALGORITHME GLOUTON RÉCURSIF

def minPieces(S:tuple,X:int)->int:
    global mini
    if X==0:
        return 0
    else:
        mini+=1
        for i in range(len(S)):
            if S[i]<=X:
                plusGrosse = S[i]
            # L.append(plusGrosse)
        minPieces(S,X-plusGrosse)
        return mini

# def getPlusGrosse(S:tuple,X:int)->int:
#     for i in range(len(S)):
#         if S[i]<=X:
#             plusGrosse = S[i]
#     return plusGrosse

L=[]
S=(1,2,5,10,13,20,50,100,200)
X=[i for i in range(5000)]
Y = []

for x in X:
    mini=0
    start=time.process_time()
    m = minPieces(S,x)
    stop=time.process_time()
    t = stop - start
    Y.append(t)

# print("X = ",X)
# print("Y = ",Y)

plt.title("Algorithme Glouton Récursif")
plt.xlabel('montant X à rendre')
plt.ylabel('Temps')
plt.plot(X, Y, marker="+",markersize=7,markeredgecolor="r")

plt.show() # affiche la figure a l'ecran

