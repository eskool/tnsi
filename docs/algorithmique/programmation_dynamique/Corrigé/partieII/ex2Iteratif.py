import time

# ALGORITHME GLOUTON ITÉRATIF

def minPieces(S:tuple,X:int)->int:
    if X==0:
        return 0
    else:
        mini=0
        while X>0:
            for i in range(len(S)):
                if S[i]<=X:
                    plusGrosse = S[i]
            X-=plusGrosse
            mini+=1
        return mini

# def getPlusGrosse(S:tuple,X:int)->int:
#     for i in range(len(S)):
#         if S[i]<=X:
#             plusGrosse = S[i]
#     return plusGrosse

S=(1,2,5,10,13,20,50,100,200)
X=33

count = 0
start=time.process_time()
m = minPieces(S,X)
stop=time.process_time()
t = stop - start

print("Nombre min de pièces, m=",m,"en t= ",t,"sec")