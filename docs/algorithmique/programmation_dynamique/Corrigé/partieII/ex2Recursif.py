import time

# ALGORITHME GLOUTON RÉCURSIF

def minPieces(S:tuple,X:int)->int:
    global mini
    if X==0:
        return 0
    else:
        mini+=1
        for i in range(len(S)):
            if S[i]<=X:
                plusGrosse = S[i]
        minPieces(S,X-plusGrosse)
        return mini

# def getPlusGrosse(S:tuple,X:int)->int:
#     for i in range(len(S)):
#         if S[i]<=X:
#             plusGrosse = S[i]
#     return plusGrosse

S=(1,2,5,10,13,20,50,100,200)
X=33

mini=0
start=time.process_time()
m = minPieces(S,X)
stop=time.process_time()
t = stop - start

print("Nombre min de pièces, m=",m,"en t= ",t,"sec")
