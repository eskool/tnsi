import time

# ALGORITHME GLOUTON RÉCURSIF

def minPieces(S:tuple,X:int,L:list)->int:
    global mini
    if X==0:
        return 0
    else:
        mini+=1
        for i in range(len(S)):
            if S[i]<=X:
                plusGrosse = S[i]
        L.append(plusGrosse)
        minPieces(S,X-plusGrosse,L)
        return mini,L

# def getPlusGrosse(S:tuple,X:int)->int:
#     for i in range(len(S)):
#         if S[i]<=X:
#             plusGrosse = S[i]
#     return plusGrosse

L=[]
S=(1,2,5,10,13,20,50,100,200)
X=67

mini=0
start=time.process_time()
m, L = minPieces(S,X,L)
stop=time.process_time()
t = stop - start

print("Nombre min de pièces, m=",m,"en t= ",t,"sec")
print("Répartiion, Pour X= ",X," L = ",L)