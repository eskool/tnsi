import numpy as np
import matplotlib.pyplot as plt

x = np.array([1, 3, 4, 6])
y = np.array([2, 3, 5, 3])
plt.plot(x, y, marker="+",markersize=7,markeredgecolor="r")
plt.show() # affiche la figure a l'ecran