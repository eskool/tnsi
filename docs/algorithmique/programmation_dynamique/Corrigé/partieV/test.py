import time

def minPieces(S,X):
    cache = [0]*(X+1)
    garde = [0]*(X+1)
    for x in range(1,X+1):
        mini = X
        for i in range(len(S)):
            if (S[i]<=x) and (1+cache[x-S[i]]<mini):
                mini = 1+cache[x-S[i]]
                coin = i
        cache[x] = mini
        garde[x] = coin
    x,L= X,[]
    # while x>0:
    #     L.append(S[garde[x]])
    #     x-=S[garde[x]]
    L = garde
    return cache[X],garde

S=(1,2,5,10,13,20,50,100,200)
X=11

start=time.process_time()
m,L = minPieces(S,X)
stop=time.process_time()
t = stop - start

print("Pour X = ",X)
print("minPieces = ",m)
print("Répartition des pièces = ",L)
print("Temps = ",t)


