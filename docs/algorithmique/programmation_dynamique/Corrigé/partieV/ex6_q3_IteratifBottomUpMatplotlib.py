import time
import numpy as np
import matplotlib.pyplot as plt

# ALGORITHME DYNAMIQUE ITÉRATIF BOTTOM UP

def minPieces(S,X):
    cache = [0]*(X+1)
    for x in range(1,X+1):
        mini = X
        for i in range(len(S)):
            if (S[i]<=x) and (1+cache[x-S[i]]<mini):
                mini = 1+cache[x-S[i]]
        cache[x] = mini
    return cache[X]

S=(1,2,5,10,13,20,50,100,200)
X=[i for i in range(50)]
Y = []


for x in X:
    cache=[0]*(x+1)
    start=time.process_time()
    m = minPieces(S,x)
    stop=time.process_time()
    t = stop - start
    Y.append(t)

# print("X = ",X)
# print("Y = ",Y)

plt.title("Algorithme Dynamique Itératif Bottom Up")
plt.xlabel('montant X à rendre')
plt.ylabel('Temps')
plt.plot(X, Y, marker="+",markersize=7,markeredgecolor="r")

plt.show() # affiche la figure a l'ecran