import time

# ALGORITHME DYNAMIQUE ITÉRATIF BOTTOM UP

def minPieces(S,X):
    cache = [0]*(X+1)
    for x in range(1,X+1):
        mini = X
        for i in range(len(S)):
            if (S[i]<=x) and (1+cache[x-S[i]]<mini):
                mini = 1+cache[x-S[i]]
        cache[x] = mini
    return cache[X]

S=(1,2,5,10,13,20,50,100,200)
X=11

start=time.process_time()
m = minPieces(S,X)
stop=time.process_time()
t = stop - start

print("Pour X=",X,", nombre minimal Pieces=",m,"en t=",t)

