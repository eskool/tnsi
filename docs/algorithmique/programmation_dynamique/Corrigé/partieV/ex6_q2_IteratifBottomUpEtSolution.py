import time

# ALGORITHME DYNAMIQUE ITÉRATIF BOTTOM UP, AVEC VALEURS DE LA SOLUTION

def minPieces(S,X):
    cache = [0]*(X+1)
    premierePiece = [0]*(X+1)
    for x in range(1,X+1):
        mini = X
        for i in range(len(S)):
            if (S[i]<=x) and (1+cache[x-S[i]]<mini):
                mini = 1+cache[x-S[i]]
                indicePiece = i
        cache[x] = mini
        # (sur cette ligne) indicePiece = indice (dans S) de la 1ère pièce à rendre sur x
        premierePiece[x] = S[indicePiece]  # 1ère pièce à rendre sur x
    x,L= X,[]
    while x>0:
        L.append(premierePiece[x])
        x-=premierePiece[x]

    return cache[X],L

S=(1,2,5,10,13,20,50,100,200)
X=66

start=time.process_time()
m,L = minPieces(S,X)
stop=time.process_time()
t = stop - start

print("Pour X = ",X)
print("minPieces = ",m)
print("Répartition des pièces = ",L)
print("Temps = ",t)


