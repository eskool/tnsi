import time

# ALGORITHME DYNAMIQUE RÉCURSIF TOP DOWN

def minPieces(S:tuple,X:int,cache:list)->int:
    if X==0:
        cache[0]=0
        return 0
    elif cache[X]!=0:
        return cache[X]
    else:
        # print("X=",X,"cached")
        mini = X+1
        for i in range(len(S)):
            if S[i]<=X and (1+minPieces(S,X-S[i],cache))<mini:
                mini = 1 + minPieces(S,X-S[i],cache)
                cache[X] = mini
        return mini

S=(1,2,5,10,13,20,50,100,200)
X=11

cache=[0]*(X+1)

start=time.process_time()
m = minPieces(S,X,cache)
stop=time.process_time()
t = stop - start

print("Pour X=",X,", nombre minimal Pieces=",m,"en t=",t)
print("cache=",cache)