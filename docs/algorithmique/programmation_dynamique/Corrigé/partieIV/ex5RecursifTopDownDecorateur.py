import time
import sys
sys.setrecursionlimit(5000)

# ALGORITHME RÉCURSIF TOP DOWN, AVEC DÉCORATEUR

def avecCache(func):
    cache = {}
    def wrapper(*args):
        if args not in cache:
            cache[args] = func(*args)
        return cache[args]
    return wrapper

@avecCache
def minPieces(S:tuple,X:int)->int:
    if X==0:
        return 0
    else:
        mini = X+1
        for i in range(len(S)):
            if S[i]<=X and (1+minPieces(S,X-S[i]))<mini:
                mini = 1 + minPieces(S,X-S[i])
        return mini

S=(1,2,5,10,13,20,50,100,200)
X=17

# cache=[None]*(X+1)

start=time.process_time()
m = minPieces(S,X)
stop=time.process_time()
t = stop - start

print("Pour X=",X,", nombre minimal Pieces=",m,"en t=",t)
# print("cache=",cache)