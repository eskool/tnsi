def monDecorateur(func):
    def wrapper(*args):
        # *args sont les arguments de la fonction 'func'
        # Actions qui modifient la fonction 'func'
        if args[0]%2 == 1:
            return func(args[0]*2,args[1])
        else:
            return func(*args)
    return wrapper

@monDecorateur
def somme(a,b):
  print("a+b=",a+b)

somme(4,6)