import time
import numpy as np
import matplotlib.pyplot as plt

# ALGORITHME DYNAMIQUE RÉCURSIF TOP DOWN

def minPieces(S:tuple,X:int,cache:list)->int:
    if X==0:
        cache[0]=0
        return 0
    elif cache[X]!=0:
        return cache[X]
    else:
        # print("X=",X,"cached")
        mini = X+1
        for i in range(len(S)):
            if S[i]<=X and (1+minPieces(S,X-S[i],cache))<mini:
                mini = 1 + minPieces(S,X-S[i],cache)
                cache[X] = mini
        return mini

S=(1,2,5,10,13,20,50,100,200)
X=[i for i in range(50)]
Y = []


for x in X:
    cache=[0]*(x+1)
    start=time.process_time()
    m = minPieces(S,x,cache)
    stop=time.process_time()
    t = stop - start
    Y.append(t)

# print("X = ",X)
# print("Y = ",Y)

plt.title("Algorithme Dynamique Récursif Top Down")
plt.xlabel('montant X à rendre')
plt.ylabel('Temps')
plt.plot(X, Y, marker="+",markersize=7,markeredgecolor="r")

plt.show() # affiche la figure a l'ecran