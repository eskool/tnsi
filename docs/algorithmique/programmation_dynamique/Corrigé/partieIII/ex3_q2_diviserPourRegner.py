import time

# ALGORITHME RÉCURSIF DIVISER POUR RÉGNER

def minPieces(S:tuple,X:int)->int:
    global count
    count += 1
    if X==0:
        return 0
    else:
        mini = X
        for i in range(len(S)):
            if S[i]<=X and (1 + minPieces(S,X-S[i]))<mini:
                mini = 1 + minPieces(S,X-S[i])
        return mini

S=(1,2,5,10,13,20,50,100,200)
X=23

count = 0
start=time.process_time()
m = minPieces(S,X)
stop=time.process_time()
t = stop - start

print("Nombre min de pièces, m=",m,"en t= ",t,"sec")
print("NBappels = ",count)