# TNSI : Définitions sur les Graphes Quelconques

## Graphes

!!! def
    * Un <bred>Graphe</bred> $G = (V, E)$ est la donnée de:
      * un ensemble fini $V$ de **Points**, ou <bred>Sommets</bred> ( $V =$  <bred><b>V</b>ertices</bred> :gb:), ou <bred>Noeuds</bred> :fr: ou <bred>Nodes</bred> :gb:
      * un ensemble $E$ de <bred>Liens</bred>, entre ces points ( $E=$ <bred><b>E</b>dges</bred> :gb:)
    * Ces liens font correspondre un <bred>sommet de départ</bred> $s$ à un <bred>sommet d'arrivée</bred> $v$

## Graphes Non orientés vs Graphes Orientés

!!! def "Graphes Non Orientés"
    <div style="float:right;">
    <center>

    ```dot
    graph G {
      size="1.5";
      bgcolor=none;
      node [shape=circle, style=bold, fontsize=38];
      4 -- 0
      4 -- 1
      0 -- 2
      0 -- 3
      1 -- 2
      1 -- 3
    }
    ```

    <figcaption>Un Graphe Non Orienté</figcaption>

    </center>

    </div>

    Lorsque les **liens sont symétriques**, c'est-à-dire lorsque l'existence d'un lien entre $s$ et $v$ est équivalent à l'existence d'un lien entre $v$ et $s$ :

    <center>
    
    $s \longrightarrow v \Leftrightarrow v \longrightarrow s$
    
    </center>
    
    Alors on dit que:

    * <bred>le graphe est Non Orienté</bred>, et 
    * les liens entre les sommets sont appelés des <bred>Arêtes</bred>, et 
    * les arêtes sont représentées par des **traits**
    
Exemple : Arbre 1 : Réseau Internet

!!! def "Graphes Orientés"

    <div style="float:right;">

    <center>

    ```dot
    digraph G {
      size="1.5";
      bgcolor=none;
      node [shape=circle, style=bold, fontsize=38];
      5
      4 -> 0
      4 -> 1
      0 -> 2
      5 -> 1
      5 -> 3
      3 -> 0
      1 -> 2
      1 -> 3
      0 -> 6
      {rank=same;0;1;5;6;}
      {rank=same;2;3;}
    }
    ```

    <figcaption>Un Graphe Orienté</figcaption>

    </center>

    </div>

    Lorsque les **liens NE sont PAS symétriques**, càd lorsqu'il existe au moins une relation non symétrique,
    Alors on dit que :
    
    * <bred>le graphe est Orienté</bred>, et 
    * les liens entre les sommets sont appelés des <bred>Arcs</bred>, et 
    * les Arcs sont représentés par des **flèches**
      
Exemple : Arbre 2 : Réseau Social

<env>**Remarque**</env> 

* Une arête non orientée est équivalente à une double relation d'arcs orientés, donc :

<div id="conteneur">

<div>

```dot
graph {
  size="2";
  rankdir=LR
  node [shape=circle]
  s1 -- s2
}
```

</div>

<div style="font-size:3.7em;">

$$\quad \Leftrightarrow \quad$$

</div>

<div>

```dot
digraph {
  size="2";
  rankdir=LR
  node [shape=circle]
  s1 -> s2
  s2 -> s1
}
```

</div>

</div>

* Cela signifie qu'un graphe non orienté est un graphe orienté "*symétrique*", donc un graphe non orienté peut être vu comme un cas particulier de graphe orienté (contrairement à l'intuition ?)

### BONUS : Dessiner des Graphes Grâce au Langage DOT

<div id="conteneur">

<div style="flex:6;">

```python linenums="0"
graph G { // Graphe Non Orienté
  node [shape=circle]
  c, d [style=bold, color=blue, fontcolor=blue]
  a -- b -- c
  b -- d [label="5"]
  c -- d [label="2", style=bold, color=red, fontcolor=red]
  {rank=same;c;d;}
}
```

</div>

<div style="font-size:1.7em;">
$$\Rightarrow$$
</div>

<div style="flex:2;">

<center>

```dot
graph G { // Graphe Non Orienté
  size="2";
  node [fontsize=38, shape=circle]
  edge [fontsize=38]
  c, d [style=bold, color=blue, fontcolor=blue]
  a -- b -- c
  b -- d [label="5"]
  c -- d [label="2", style=bold, color=red, fontcolor=red]
  {rank=same;c;d;}
}
```

<figcaption>Graphe Non Orienté</figcaption>

</center>

</div>

</div>

<div id="conteneur">

<div style="flex:6;">

```python linenums="0"
digraph G {  // Graphe Orienté
  node [shape=circle]
  c, d [style=bold, color=blue, fontcolor=blue]
  a -> b -> c
  b -> d [label="5"]
  c -> d [label="2", style=bold, color=red, fontcolor=red]
  {rank=same;b;c;}
}
```

</div>

<div style="font-size:1.7em;">
$$\Rightarrow$$
</div>

<div style="flex:2;">

<center>

```dot
digraph G {  // Graphe Orienté
  size="2";
  node [fontsize=38, shape=circle]
  edge [fontsize=38]
  c, d [style=bold, color=blue, fontcolor=blue]
  a -> b -> c
  b -> d [label="5"]
  c -> d [label="2", style=bold, color=red, fontcolor=red]
  {rank=same;b;c;}
}
```

<figcaption>Graphe Orienté</figcaption>

</center>

</div>

</div>


## Graphes Simples vs Multigraphes

!!! def "Graphe Simple"
    <div style="float:right;">
    <center>
    ```dot
    graph G {
      size="1.5";
      bgcolor=none;
      node [shape=circle, fontsize=38, style=bold];
      0, 2, 1, 4
      4 -- 0
      4 -- 1
      0 -- 2
      0 -- 3
      1 -- 2
      1 -- 3
    }
    ```
    <figcaption>Un Graphe Simple</figcaption>
    </center>
    </div>
    Un <bred>Graphe Simple</bred> est un graphe qui:

    * n'admet pas d'<bred>arêtes/arcs multiples</bred>, ou <bred>multi-arêtes/multi-arcs</bred> (plusieurs arêtes/arcs entre deux sommets fixés)
    * n'admet pas de <bred>boucle</bred> (arête/arc entre un sommet et lui-même)

!!! def "Multigraphe"
    <div style="float:right;">
    <center>
    ```dot
    graph G {
      size="2";
      bgcolor=none;
      node [shape=circle, style=bold, fontsize=38];
      edge [penwidth=2]
      4, 0, 2, 3, 2
      4 -- 0
      4 -- 0
      4 -- 1
      1 -- 3
      3 -- 5
      3 -- 5
      3 -- 5
      1 -- 1
      1 -- 2
      0 -- 3
      0 -- 2
      2 -- 2
      //{rank=same;0;1;}
      //{rank=same;2;3;5;}
    }
    ```
    <figcaption>Un Multigraphe</figcaption>
    </center>
    </div>
    Un <bred>multigraphe</bred> est le contraire d'un graphe simple:

    * il peut admettre des arêtes/arcs multiples entre deux sommets fixés
    * il peut admettre des boucles (arêtes/arcs d'un sommet vers lui-même)

:warning: <env>**Convention**</env> :warning: Dans la suite de ce cours, sauf mention contraire (par défaut):

* **Tous les graphes Non Orientés considérés seront des graphes Simples**.
* Les **graphes Orientés** pourront quelquefois avoir des boucles, ainsi que plusieurs arcs entre plusieurs sommets

### p-graphes (CULTURE : HORS-PROGRAMME)

!!! def
    Un <bred>p-graphe</bred> est un graphe dont **le nombre maximum d'arêtes/arcs**, entre deux sommets adjacents quelconques, vaut `p`.

!!! exp
    <div id="conteneur">
    <div>
    <center>
    ```dot
    graph G {
      size="2";
      bgcolor=none;
      node [shape=circle, style=bold, fontsize=38];
      4, 0, 2, 3, 2
      4 -- 0
      4 -- 1
      1 -- 3
      3 -- 5
      1 -- 2
      0 -- 3
      0 -- 2
      //{rank=same;0;1;}
      //{rank=same;2;3;5;}
    }
    ```
    <figcaption>Un 1-graphe<br/>= Un Graphe Simple</figcaption>
    </center>
    </div>
    <div>
    <center>
    ```dot
    graph G {
      size="2";
      bgcolor=none;
      node [shape=circle, style=bold, fontsize=38];
      4, 0, 2, 3, 2
      4 -- 0
      4 -- 0
      4 -- 1
      1 -- 3
      1 -- 3
      3 -- 5
      1 -- 2
      0 -- 3
      0 -- 2
      //{rank=same;0;1;}
      //{rank=same;2;3;5;}
    }
    ```
    <figcaption>Un 2-graphe</figcaption>
    </center>
    </div>
    <div>
    <center>
    ```dot
    graph G {
      size="2";
      bgcolor=none;
      node [shape=circle, style=bold, fontsize=38];
      4, 0, 2, 3, 2
      4 -- 0
      4 -- 1
      1 -- 3
      3 -- 5
      1 -- 1
      1 -- 2
      0 -- 3
      0 -- 2
      2 -- 2
      //{rank=same;0;1;}
      //{rank=same;2;3;5;}
    }
    ```
    <figcaption>Un 2-graphe</figcaption>
    </center>
    </div>
    <div>
    <center>
    ```dot
    graph G {
      size="2";
      bgcolor=none;
      node [shape=circle, style=bold, fontsize=38];
      4, 0, 2, 3, 2
      4 -- 0
      4 -- 0
      4 -- 1
      1 -- 3
      1 -- 3
      3 -- 5
      3 -- 5
      3 -- 5
      1 -- 1
      1 -- 2
      0 -- 3
      0 -- 2
      2 -- 2
      //{rank=same;0;1;}
      //{rank=same;2;3;5;}
    }
    ```
    <figcaption>Un 3-graphe</figcaption>
    </center>
    </div>
    </div>

## Graphes Pondérés

!!! def "Graphe Pondéré"
    <div style="float:right;">
    <center>
    ```dot
    digraph G {
      size="2";
      bgcolor=none;
      node [shape=circle, style=bold, fontsize=38];
      edge [fontsize=38]
      5
      4 -> 0 [label="5"]
      4 -> 1 [label="3"]
      0 -> 2 [label="6"]
      5 -> 1 [label="2"]
      5 -> 3 [label="7"]
      3 -> 0 [label="8"]
      1 -> 2 [label="6"]
      1 -> 3 [label="2"]
      0 -> 6 [label="4"]
      {rank=same;0;1;5;6;}
      {rank=same;2;3;}
    }
    ```
    <figcaption>Un Graphe Pondéré<br/>Orienté</figcaption>
    </center>
    </div>
    <div style="float:right;margin-right:1em;">
    <center>
    ```dot
    graph G {
      size="2";
      bgcolor=none;
      node [shape=circle, fontsize=26, style=bold, fontsize=38];
      edge [style=bold, fontsize=38]
      4 -- 0 [label="5"]
      4 -- 1 [label="2"]
      0 -- 2 [label="3"]
      0 -- 3 [label="4"]
      1 -- 2 [label="8"]
      1 -- 3 [label="7"]
    }
    ```
    <figcaption>Un Graphe Pondéré<br/>Non Orienté</figcaption>
    </center>
    </div>
    Des <bred>poids</bred> / <bred>coefficients</bred> / <bred>valuations</bred> peuvent être associés aux **arêtes/arcs** d'un graphe (orienté ou non).  
    Par exemple pour modéliser la distance, le temps, la probablité ou les coûts nécessaires pour passer d'un état à un autre.  
    Un tel graphe est appelé un <bred>graphe pondéré</bred>.
    Exemple : Arbre 3 : Le Réseau Routier ci-dessus.

## Ordre d'un Graphe

!!! def "Ordre d'un Graphe"
    L'<bred>ordre</bred> d'un graphe est **son nombre total de sommets/Noeuds**.  
    En général, il est noté $n=ord(G)=|V|$

## Taille d'un Graphe

!!! def "Taille d'un Graphe"
    La <bred>taille</bred> d'un graphe est **son nombre total d'arêtes/arcs**.  
    En général, elle est notée $a=taille(G)=|E|$

!!! ex "Taille Maximale d'un Graphe Simple Non Orienté"
    On considère un graphe simple $G$ non orienté, d'ordre $n$.
    Quel est la taille maximale du graphe $G$?
    Autrement dit, combien d'arêtes au maximum peut-on tracer avec $n$ sommets (une seule arête au maximum entre deux sommets)
    <div id="conteneur">
    <div style="float:left;margin-right:1em;">
    <center>
    ```dot
    graph G {
      size="1.2";
      bgcolor=none;
      node [shape=circle, style=bold, fontsize=38];
      edge [style=bold]
      1 -- 2
      {rank=same;1;2;}
    }
    ```
    <figcaption>
    Pour $n=2$ sommets, <br/>$a=1$ arête au max.
    </figcaption>
    </center>
    </div>
    <div style="float:left;margin-right:1em;">
    <center>
    ```dot
    graph G {
      layout=circo
      size="1.5";
      bgcolor=none;
      node [shape=circle, style=bold, fontsize=38];
      edge [style=bold]
      1 -- 2
      2 -- 3
      3 -- 1
      {rank=same;2;3;}
    }
    ```
    <figcaption>
    Pour $n=3$ sommets, <br/>$a=3$ arêtes au max.
    </figcaption>
    </center>
    </div>
    <div>
    <center>
    ```dot
    graph G {
      layout=circo
      size="2";
      bgcolor=none;
      node [shape=circle, style=bold, fontsize=38];
      edge [style=bold]
      1, 2, 4
      1 -- 2
      1 -- 4
      2 -- 4
      2 -- 3
      4 -- 3
      3 -- 1
      {rank=same;1;2;}
      {rank=same;3;4;}
    }
    ```
    <figcaption>
    Pour $n=4$ sommets, <br/>$a=6$ arêtes au max.
    </figcaption>
    </center>
    </div>
    </div>
