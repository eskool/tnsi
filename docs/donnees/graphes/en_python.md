# TNSI : Représentations des Graphes en Python

On peut représenter les graphes de plusieurs manières:

* Matrices d'adjacences
* Listes d'adjacences:
  * listes des voisins (graphes non orientés)
  * listes des successeurs, ou des prédécesseurs (graphes orientés)

## Matrice d'Adjacence

!!! def
    Une <bred>matrice</bred> est un tableau de nombres. elle peut être représentée en Python facilement :

    * par une **liste de listes** en Python (par défaut)
    * un **dictionnaire**
    En notant $A$ une certaine matrice, Alors `A[i][j]` désigne l'élément situé à la ligne `i` et à la colonne `j`

### Matrice d'Adjacence d'un graphe NON pondéré

!!! def "Matrice d'Adjacence d'un graphe NON pondéré"
    Un graphe (orienté, ou pas) peut être représenté par une <bred>matrice d'adjacence</bred> :

    * tout lien depuis le sommet `i` vers le sommet `j`, est représenté par `A[i][j] = 1`
    * Une absence de lien du sommet `i` vers le sommet `j`, est représenté par `A[i][j] = 0`

!!! exp
    <div id="conteneur">
    <div>
    <center>
    ```dot
    digraph G {
      size="2"
      node [fontsize=20, shape=circle]
      rankdir=LR
      0 -> {0, 1}
      1 -> {1, 2, 3}
      2:e -> 2:s
      3 -> 2
      {rank=same; 1, 2}
    }
    ```
    <figcaption><b>Graphe 1</b></figcaption>
    </center>
    </div>
    <div>
    <center>
    ```dot
    graph G {
      size="2";
      node [fontsize=20, shape=circle];
      rankdir=LR
      0 -- {1, 2}
      1 -- { 2, 3, 4}
      2 -- 3
      3 -- 4
      {rank=same; 0, 2}
      {rank=same; 1, 3}
      {rank=same; 4}
    }
    ```
    <figcaption><b>Graphe 2</b></figcaption>
    </center>
    </div>
    </div>
    <div id="conteneur">
    <div>
    <center>
    $M_1=\begin{pmatrix}
    1 & 1 & 0 & 0\\
    0 & 1 & 1 & 1\\
    0 & 0 & 1 & 0\\
    0 & 0 & 1 & 0\\
    \end{pmatrix}
    $
    <figcaption><b>Matrice d'adjacence Graphe 1<br/>Matrice NON Symétrique</b></figcaption>
    </center>
    </div>
    <div>
    <center>
    $M_2=\begin{pmatrix}
    0 & 1 & 1 & 0 & 0\\
    1 & 0 & 1 & 1 & 1\\
    1 & 1 & 0 & 1 & 0\\
    0 & 1 & 1 & 0 & 1\\
    0 & 1 & 0 & 1 & 0\\
    \end{pmatrix}
    $
    <figcaption><b>Matrice d'adjacence Graphe 2<br/>Matrice Symétrique</b></figcaption>
    </center>
    </div>
    </div>
    <div id="conteneur">
    <div style="margin-left:0.5em;margin-right:0.5em;width:45%;">
    ```python
    # Matrice Adjacence en Liste de Listes :
    M1 = [[1, 1, 0, 0],
          [0, 1, 1, 1],
          [0, 0, 1, 0],
          [0, 0, 1, 0]]
    ```
    </div>
    <div style="margin-left:0.5em;margin-right:0.5em;width:45%;">
    ```python
    # Matrice Adjacence en Liste de Listes :
    M2 = [[0, 1, 1, 0, 0],
          [1, 0, 1, 1, 1],
          [1, 1, 0, 1, 0],
          [0, 1, 1, 0, 1],
          [0, 1, 0, 1, 0]]
    ```
    </div>
    </div>
    <div id="conteneur">
    <div style="margin-left:0.5em;margin-right:0.5em;width:45%;">
    ```python
    # Matrice Adjacence en Dictionnaire (Graphes Étiquetés) :
    M1 = {0 : [1, 1, 0, 0],
          1 : [0, 1, 1, 1],
          2 : [0, 0, 1, 0],
          3 : [0, 0, 1, 0]}
    ```
    </div>
    <div style="margin-left:0.5em;margin-right:0.5em;width:45%;">
    ```python
    # Matrice Adjacence en Dictionnaire (Graphes Étiquetés) :
    M2 = {0 : [0, 1, 1, 0, 0],
          1 : [1, 0, 1, 1, 1],
          2 : [1, 1, 0, 1, 0],
          3 : [0, 1, 1, 0, 1],
          4 : [0, 1, 0, 1, 0]}
    ```
    </div>
    </div>

!!! def "Matrice Symétrique"
    Notons $A=(a_{ij})$ la matrice d'adjacence.  
    On dit que la matrice d'adjacence est <bred>symétrique</bred> $\Leftrightarrow$ $a_{ij}=a_{ji}$ pour tous les $i,j$

### Matrice d'Adjacence d'un graphe Pondéré

!!! def "Matrice d'Adjacence d'un graphe pondéré"
    Un graphe **pondéré** (orienté, ou pas) peut être représenté par une <bred>matrice d'adjacence</bred> :

    * tout lien depuis le sommet `i` vers le sommet `j`, est représenté par <env>$A[i][j] = a_{ij}$</env> où $a_{ij}$ désigne le poids du lien du sommet `i` vers le sommet `j`
    * Une absence de lien du sommet `i` vers le sommet `j`, est représenté par `A[i][j] = 0`

!!! exp
    <div id="conteneur">
    <div>
    <center>
    ```dot
    digraph G {
      size="2.2"
      bgcolor=none
      node [fontsize=24, shape=circle];
      edge [fontsize=24];
      rankdir=LR
      0 -> 0 [label="3"]
      0 -> 1 [label="2"]
      1 -> 1 [label="4"]
      1 -> 2 [label="0.5"]
      1 -> 3 [label="0.2"]
      2:e -> 2:s [label="0.6"]
      3 -> 2 [label="5"]
      {rank=same; 1, 2}
    }
    ```
    <figcaption><b>Graphe 3 Orienté</b></figcaption>
    </center>
    </div>
    <div>
    <center>
    ```dot
    graph G {
      size="2.4";
      splines=line;
      node [fontsize=24, shape=circle];
      edge [fontsize=24];
      rankdir=LR
      0 -- 1 [label="4"]
      0 -- 2 [label="5"]
      1 -- 2 [label="0.1"]
      1 -- 3 [label="0.3"]
      1 -- 4 [label="0.2"]
      2 -- 3 [label="0.8"]
      3 -- 4 [label="0.9"]

      {rank=same; 0, 2}
      {rank=same; 1, 3}
      {rank=same; 4}
    }
    ```
    <figcaption><b>Graphe 4 Non Orienté</b></figcaption>
    </center>
    </div>
    </div>
    <div id="conteneur">
    <div>
    <center>
    $M_3=\begin{pmatrix}
    3 & 2 & 0 & 0\\
    0 & 4 & 0.5 & 0.2\\
    0 & 0 & 0.6 & 0\\
    0 & 0 & 5 & 0\\
    \end{pmatrix}
    $
    <figcaption><b>Matrice d'adjacence Graphe 3<br/>Matrice NON Symétrique</b></figcaption>
    </center>
    </div>
    <div>
    <center>
    $M_4=\begin{pmatrix}
    0 & 4 & 5 & 0 & 0\\
    4 & 0 & 0.1 & 0.3 & 0.2\\
    5 & 0.1 & 0 & 0.8 & 0\\
    0 & 0.3 & 0.8 & 0 & 0.9\\
    0 & 0.2 & 0 & 0.9 & 0\\
    \end{pmatrix}
    $
    <figcaption><b>Matrice d'adjacence Graphe 4<br/>Matrice Symétrique</b></figcaption>
    </center>
    </div>
    </div>
    <div id="conteneur">
    <div style="margin-left:0.5em;margin-right:0.5em;width:45%;">
    ```python
    # Matrice Adjacence en Liste de Listes :
    M3 = [[3, 2, 0, 0],
          [0, 4, 0.5, 0.2],
          [0, 0, 0.6, 0],
          [0, 0, 5, 0]]
    ```
    </div>
    <div style="margin-left:0.5em;margin-right:0.5em;width:45%;">
    ```python
    # Matrice Adjacence en Liste de Listes :
    M4 = [[0, 4, 5, 0, 0],
          [4, 0, 0.1, 0.3, 0.2],
          [5, 0.1, 0, 0.8, 0],
          [0, 0.3, 0.8, 0, 0.9],
          [0, 0.2, 0, 0.9, 0]]
    ```
    </div>
    </div>
    <div id="conteneur">
    <div style="margin-left:0.5em;margin-right:0.5em;width:45%;">
    ```python
    # Matrice Adjacence en Dictionnaire (graphes Étiquetés) :
    M3 = {0 : [3, 2, 0, 0],
          1 : [0, 4, 0.5, 0.2],
          2 : [0, 0, 0.6, 0],
          3 : [0, 0, 5, 0]}
    ```
    </div>
    <div style="margin-left:0.5em;margin-right:0.5em;width:45%;">
    ```python
    # Matrice Adjacence en Dictionnaire (graphes Étiquetés) :
    M4 = {0 : [0, 4, 5, 0, 0],
          1 : [4, 0, 0.1, 0.3, 0.2],
          2 : [5, 0.1, 0, 0.8, 0],
          3 : [0, 0.3, 0.8, 0, 0.9],
          4 : [0, 0.2, 0, 0.9, 0]}
    ```
    </div>
    </div>

### Symétrie de la matrice d'Adjacence

!!! def "Matrice Symétrique"
    Notons $A=(a_{ij})$ la matrice d'adjacence.  
    On dit que la matrice d'adjacence est <bred>symétrique</bred> $\Leftrightarrow$ $a_{ij}=a_{ji}$ pour tous les $i,j$  
    Cela revient à ce que les coefficients $a_{ij}$ soient symétriques par rapport à la **diagonale principale**

!!! pte "Matrice d'Adjacence Symétrique? ou pas?"
    * Un graphe **non orienté** admet une matrice d'adjacence **symétrique**
    * Un graphe **orienté** admet, en général, une matrice d'adjacence **non symétrique**

## Liste d'Adjacence

* Pour représenter un graphe, on peut également, pour chacun de ses sommets, donner la liste des sommets auxquels il est relié.
  * Lorsque le graphe est **non orienté**, la **liste d'adjacence** est une <bred>liste de voisins</bred>
  * Lorsque le graphe est **orienté**, la **liste d'adjacence** peut être représentée par :
      * la <bred>liste de ses successeurs</bred>, ou bien
      * la <bred>liste de ses prédécesseurs</bred>, lorsque les problèmes étudiés s'y prêtent mieux (ça arrive)

* Implémentation:
    * Pour un graphe d'ordre $n$, on numérotera les sommets de $0$ à $n-1$
    * **Graphes non étiquetés :** Les listes de voisins et/ou de successeurs se représentent usuellement par des **listes de listes** en Python.
    * **Graphes étiquetés :** Les listes de voisins et/ou de successeurs se représentent usuellement par des **dictionnaires** en Python.

!!! exp
    <div id="conteneur">
    <div>
    <center>
    ```dot
    digraph G {
      size="2"
      bgcolor=none
      node [fontsize=20, shape=circle]
      rankdir=LR
      0 -> {0, 1}
      1 -> {1, 2, 3}
      2:e -> 2:s
      3 -> 2
      {rank=same; 1, 2}
    }
    ```
    <figcaption><b>Graphe 1 Orienté</b></figcaption>
    </center>
    </div>
    <div>
    <center>
    ```dot
    graph G {
      size="2";
      //bgcolor=none
      node [fontsize=20, shape=circle];
      rankdir=LR
      0 -- {1, 2}
      1 -- { 2, 3, 4}
      2 -- 3
      3 -- 4
      {rank=same; 0, 2}
      {rank=same; 1, 3}
      {rank=same; 4}
    }
    ```
    <figcaption><b>Graphe 2 Non Orienté</b></figcaption>
    </center>
    </div>
    </div>
    <div id="conteneur">
    <div style="margin-left:0.5em;margin-right:0.5em;width:45%;">
    ```python
    # Liste de Successeurs en Liste de Listes :
    S1 = [[0,1],
          [1, 2, 3],
          [2],
          [2]]
    ```
    </div>
    <div style="margin-left:0.5em;margin-right:0.5em;width:45%;">
    ```python
    # Liste de Voisins en Liste de Listes :
    V2 = [[1, 2],
          [0, 2, 3, 4],
          [0, 1, 3],
          [1, 2, 4],
          [1, 3]]
    ```
    </div>
    </div>
    <div id="conteneur">
    <div style="margin-left:0.5em;margin-right:0.5em;width:45%;">
    ```python
    # Liste de Successeurs en Dictionnaire (Graphes Étiquetés) :
    S1 = {0 : [0, 1],
          1 : [1, 2, 3],
          2 : [2],
          3 : [2]}
    ```
    </div>
    <div style="margin-left:0.5em;margin-right:0.5em;width:45%;">
    ```python
    # Liste de Voisins en Dictionnaire (Graphes Étiquetés) :
    V2 = {0 : [1, 2],
          1 : [0, 2, 3, 4],
          2 : [0, 1, 3],
          3 : [1, 2, 4],
          4 : [1, 3]}
    ```
    </div>
    </div>
    <div id="conteneur">
    <div>
    <center>
    ```dot
    digraph G {
      size="2.5"
      bgcolor=none
      node [fontsize=20, shape=circle]
      edge [fontsize=16]
      rankdir=LR
      0 -> 0 [label="3"]
      0 -> 1 [label="2"]
      1 -> 1 [label="4"]
      1 -> 2 [label="0.5"]
      1 -> 3 [label="0.2"]
      2:e -> 2:s [label="0.6"]
      3 -> 2 [label="5"]
      {rank=same; 1, 2}
    }
    ```
    <figcaption><b>Graphe 3 Orienté Pondéré</b></figcaption>
    </center>
    </div>
    <div>
    <center>
    ```dot
    graph G {
      size="3";
      //bgcolor=none
      splines=line;
      node [fontsize=20, shape=circle];
      edge [fontsize=16]
      rankdir=LR
      0 -- 1 [label="4"]
      0 -- 2 [label="5"]
      1 -- 2 [label="0.1"]
      1 -- 3 [label="0.3"]
      1 -- 4 [label="0.2"]
      2 -- 3 [label="0.8"]
      3 -- 4 [label="0.9"]
      {rank=same; 0, 2}
      {rank=same; 1, 3}
      {rank=same; 4}
    }
    ```
    <figcaption><b>Graphe 4 Non Orienté Pondéré</b></figcaption>
    </center>
    </div>
    </div>
    <div id="conteneur">
    <div style="margin-left:0.5em;margin-right:0.5em;width:45%;">
    ```python
    # Liste de Successeurs Pondéré en Liste de Listes :
    S3 = [[[0,3],[1,2]],
          [[1,4], [2,0.5], [3,0.2]],
          [2,0.6],
          [2,5]]
    ```
    </div>
    <div style="margin-left:0.5em;margin-right:0.5em;width:45%;">
    ```python
    # Liste de Voisins Pondéré en Liste de Listes :
    V4 = [[[1,4], [2,5]],
          [[0,4], [2,0.1], [3,0.3], [4,0.2]],
          [[0,5], [1,0.1], [3,0.8]],
          [[1,0.3], [2,0.8], [4,0.9]],
          [[1,0.2], [3,0.9]]]
    ```
    </div>
    </div>
    <div id="conteneur">
    <div style="margin-left:0.5em;margin-right:0.5em;width:45%;">
    ```python
    # Liste de Successeurs Pondéré en Dictionnaire (Graphes Étiquetés) :
    S3 = {0 : [[0,3],[1,2]],
          1 : [[1,4], [2,0.5], [3,0.2]],
          2 : [2,0.6],
          3 : [2,5]}
    ```
    </div>
    <div style="margin-left:0.5em;margin-right:0.5em;width:45%;">
    ```python
    # Liste de Voisins Pondéré en Dictionnaire (G.Étiquetés):
    V4 = {0 : [[1,4], [2,5]],
          1 : [[0,4], [2,0.1], [3,0.3], [4,0.2]],
          2 : [[0,5], [1,0.1], [3,0.8]],
          3 : [[1,0.3], [2,0.8], [4,0.9]],
          4 : [[1,0.2], [3,0.9]]}
    ```
    </div>
    </div>

