# TNSI : Vocabulaire des Graphes Orientés

## Arcs

!!! def "Arcs"
    Les liens entre sommets s'appellent des <bred>Arcs</bred> :fr: ou <bred>Edges</bred> :gb:

## Chemins Orientés

### Définition

!!! def "Chemins Orientés"
    <div id="conteneur">
    <div>
    Un <bred>Chemin (Orienté)</bred> est une suite (finie) consécutive d'arcs (entrants-sortants) sur un graphe orienté.
    </div>
    <div>
    <center>
    ```dot
    digraph G {
      size="2";
      rankdir=LR
      bgcolor=none;
      node [shape=circle, fontsize=38];
      edge [penwidth=2]
      2, 1, 4, 3 [color=red, style=bold]
      0 -> 5
      0 -> 2
      1 -> 5
      4 -> 2 [color=red, style=bold]
      1 -> 3 [color=red, style=bold]
      3 -> 4
      3 -> 0
      2 -> 1 [color=red, style=bold]
      6 -> 3
      {rank = same; 0; 2; 3;}
      {rank = same; 5; 1; 4;}
    }
    ```
    <figcaption>Un Chemin (Orienté) <br/>de Longueur 3</figcaption>
    </center>
    </div>
    </div>

### Longueur d'un Chemin Orienté

!!! def "Longueur d'un Chemin Orienté"
    La <bred>Longueur du chemin (orienté)</bred> est le nombre d'**arcs** du chemin.

!!! exp
    <div id="conteneur">
    <div style="flex:2;">
    ```dot
    digraph G {
      rankdir=LR
      size="2";
      node [shape=circle, fontsize=38]
      4 -> 2 -> 1 -> 3
    }
    ```
    </div>
    <div style="flex:4;">
    $\quad$ noté $\quad 4 \rightarrow 2 \rightarrow 1 \rightarrow 3 \quad$ est un chemin de longueur $3$
    </div>
    </div>

## Cycles Orientés ou Circuits

!!! def "Circuits"
    <div style="float:right;">
    ```dot
    digraph G {
      size="1.5";
      rankdir=LR
      bgcolor=none;
      node [shape=circle, fontsize=38];
      0, 2, 1, 3 
      1, 3, 4, 2 [color=red, style=bold]
      0 -> 5
      0 -> 2
      1 -> 5
      4 -> 2 [color=red, style=bold]
      1 -> 3 [color=red, style=bold]
      3 -> 4 [color=red, style=bold]
      3 -> 0
      2 -> 1 [color=red, style=bold]
      6 -> 3
      {rank = same; 0; 2; 3;}
      {rank = same; 5; 1; 4;}
    }
    ```
    <center>
    <figcaption>Un autre Circuit <br/>(Cycle Orienté) <br/>de Longueur 4</figcaption>
    </center>
    </div>
    <div style="float:right;margin-right:1.5em;">
    ```dot
    digraph G {
      size="1.5";
      rankdir=LR
      bgcolor=none;
      node [shape=circle, fontsize=38];
      0, 2, 1, 3 [color=red, style=bold]
      0 -> 5
      0 -> 2 [color=red, style=bold]
      1 -> 5
      4 -> 2
      1 -> 3 [color=red, style=bold]
      3 -> 4
      3 -> 0 [color=red, style=bold]
      2 -> 1 [color=red, style=bold]
      6 -> 3
      {rank = same; 0; 2; 3;}
      {rank = same; 5; 1; 4;}
    }
    ```
    <center>
    <figcaption>Un Circuit<br/> (Cycle Orienté) <br/>de Longueur 4</figcaption>
    </center>
    </div>

    * Un <bred>Circuit</bred> ou <bred>Cycle (Orienté)</bred> est un chemin (orienté) menant d'un sommet à lui-même
    * La <bred>Longueur du circuit</bred> est la longueur du chemin sous-jacent  
    Exemple : $0 \rightarrow 2 \rightarrow 1 \rightarrow 3 \rightarrow 0$ est un *circuit* de longueur $4$
    * Un graphe orienté sans aucun circuit est un <bred>graphe orienté acyclique</bred> :fr:,
    ou <bred>Directed Acyclic Graph (DAG)</bred> :gb:

!!! exp "Exemples d'utilisation des Graphes Orientés Acycliques - DAG"
    Les DAG sont utilisés pour :

    * les `cryptomonnaies` et la `blockchain`
    * le logiciel de versionning `Git`
    * les chemins des Labyrinthes parfaits (càd sans sycles)

## Simple / Faible Connexité

!!! def "Simple/Faible Connexité"
    <div id="conteneur">
    <div style="flex:4;">
    Un graphe orienté est dit <bred>simplement connexe</bred>, ou <bred>faiblement connexe</bred>, ou <bred>connexe</bred>, si en remplaçant chaque arc orienté par une arête non orientée, on obtient ainsi un graphe non orienté (dit *associé*) qui soit connexe.
    </div>
    <div style="flex:2;">
    <center>
    ```dot
    digraph G {
      size="2";
      rankdir=LR
      bgcolor=none;
      node [shape=circle, color=red, style=bold, fontsize=38];
      edge [color=red, style=bold]
      6, 0, 2, 1, 3
      0, 1, 3, 4, 2
      8
      9
      0 -> 2
      1 -> 5
      4 -> 2
      1 -> 3
      4 -> 3
      3 -> 0
      2 -> 1
      5 -> 4
      6 -> 3
      2 -> 6
      5 -> 7
      7 -> 4
      4 -> 8
      7 -> 8
      9 -> 1
      {rank = same; 0; 2; 3;}
      {rank = same; 5; 1; 4;}
      {rank = same; 8; 7;9;}
    }
    ```
    <figcaption>Graphe Orienté<br/><em>Simplement Connexe</em></figcaption>
    </center>
    </div>
    </div>
    <div id="conteneur" style="display:flex;">
    <div style="flex:4;"></div>
    <div style="flex:2;">
    <center>
    ```dot
    graph G {
      size="2";
      rankdir=LR
      bgcolor=none;
      node [shape=circle, color=red, style=bold, fontsize=38];
      edge [color=red, style=bold]
      6, 0, 2, 1, 3
      0, 1, 3, 4, 2
      8
      9
      0 -- 2
      1 -- 5
      4 -- 2
      1 -- 3
      4 -- 3
      3 -- 0
      2 -- 1
      5 -- 4
      6 -- 3
      2 -- 6
      5 -- 7
      7 -- 4
      4 -- 8
      7 -- 8
      9 -- 1
      {rank = same; 0; 2; 3;}
      {rank = same; 5; 1; 4;}
      {rank = same; 8; 7;9;}
    }
    ```
    <figcaption>Graphe Non Orienté<br/><em>Connexe</em> associé </figcaption>
    </center>
    </div>
    </div>


## Connexité Forte

### Connexité Forte

!!! def "Connexité Forte"
    <div id="conteneur">
    <div style="flex:4;">
    Un graphe orienté dans lequel deux sommets quelconques sont toujours connectés par un chemin orienté (de l'un vers l'autre), est appelé un <bred>graphe fortement connexe</bred>
    </div>
    <div style="flex:2;">
    ```dot
    digraph G {
      size="2";
      rankdir=LR
      bgcolor=none;
      node [shape=circle, color=red, style=bold, fontsize=38];
      edge [color=red, style=bold]
      6, 0, 2, 1, 3
      0, 1, 3, 4, 2
      8 [color=blue, style=bold]
      9 [color=cyan2, style=bold]
      0 -> 2
      1 -> 5
      4 -> 2
      1 -> 3
      4 -> 3
      3 -> 0
      2 -> 1
      5 -> 4
      6 -> 3
      2 -> 6
      5 -> 7
      7 -> 4
      4 -> 8 [color=blue]
      7 -> 8 [color=blue]
      9 -> 1 [color=cyan2]
      {rank = same; 0; 2; 3;}
      {rank = same; 5; 1; 4;}
      {rank = same; 8; 7;9;}
    }
    ```
    <center>
    <figcaption>Graphe <bred>NON</bred><br/>Fortement Connexe</figcaption>
    </center>
    </div>
    </div>
    <div id="conteneur">
    <div style="flex:4;"></div>
    <div style="flex:2;">
    <center>
    ```dot
    digraph G {
      size="2";
      rankdir=LR
      bgcolor=none;
      node [shape=circle, color=red, style=bold, fontsize=38];
      edge [color=red, style=bold]
      6, 0, 2, 1, 3
      0, 1, 3, 4, 2
      0 -> 2
      1 -> 5
      4 -> 2
      1 -> 3
      4 -> 3
      3 -> 0
      2 -> 1
      5 -> 4
      6 -> 3
      2 -> 6
      5 -> 7
      7 -> 4
      {rank = same; 0; 2; 3;}
      {rank = same; 5; 1; 4;}
    }
    ```
    <figcaption>Graphe<br/>Fortement Connexe</figcaption>
    </center>
    </div>
    </div>

!!! pte
    Un graphe orienté est fortement connexe  
    $\Leftrightarrow$ Quels que soient les sommets `s` et `v` de G :

    * Il existe toujours un chemin orienté de `s` vers `v`
    * Il existe toujours un chemin orienté de `v` vers `s`

    $\Leftrightarrow$ Quels que soient les sommets `s` et `v` de G, il existe un circuit passant par `s` et `v`

<env>**Exemple**</env> le **graphe d'un réseau social** est connexe, mais pas le graphe  du **réseau routier mondial** n'est PAS connexe

### Composante Fortement Connexe

!!! def "Composante Fortement Connexe"
    <div id="conteneur">
    <div style="flex:4;">
    Lorsque le graphe orienté est composé **de plusieurs "*morceaux*"** (en fait **de plusieurs sous-graphes "*induits*"** par un sous-ensemble de sommets, qui peuvent -ou pas- être reliés entre eux par des arêtes du graphe) **dont chacun est fortement connexe**, chaque morceau est appelé une <bred>composante connexe forte</bred> ou une <bred>composante fortement connexe</bred>.
    </div>
    <div style="flex:2;">
    ```dot
    digraph G {
      size="2";
      rankdir=LR
      bgcolor=none;
      node [shape=circle, fontsize=38];
      7, 8, 9 [color="#FF00FF", style=bold]
      7 -> 8 -> 9 -> 7 [color="#FF00FF", style=bold]
      9 -> 10
      10 [color=cyan2, style=bold]

      0, 2, 1, 3 
      0, 1, 3, 4, 2 [color=red, style=bold]
      0 -> 5
      0 -> 2 [color=red, style=bold]
      1 -> 5
      4 -> 2 [color=red, style=bold]
      1 -> 3 [color=red, style=bold]
      3 -> 4 [color=red, style=bold]
      3 -> 0 [color=red, style=bold]
      2 -> 1 [color=red, style=bold]
      6 -> 3
      5 [color=purple, style=bold]
      6 [color=blue, style=bold]
      {rank = same; 0; 2; 3;}
      {rank = same; 5; 1; 4;}
    }
    ```
    <center>
    <figcaption>Chaque couleur<br/>représente une<br/>Composante <br/>Fortement Connexe</figcaption>
    </center>
    </div>
    </div>

!!! pte
    * Chaque composante fortement connexe est fortement connexe
    * Deux sommets quelconques d'une composante fortement connexe sont toujours connectés (reliables par un chemin orienté)
    * Tout circuit est fortement connexe

## Illustrations

<div id="conteneur">
<div>
<center>
```dot
digraph G {
    size="6.9"
    node [shape=circle, fontsize=38, style=bold]
    0 -> 1
    0 -> 2
    0 -> 3
    4 [color=deepskyblue]
    0 -> 4 -> 10
    subgraph clusterCompoConnexe0 {
      node [color=blue]
      edge [color=blue]
      2, 3 [color=blue]
      9 -> 8
      2 -> 3
      2 -> 9
      8 -> 3
      8 -> 2
      3 -> 9
      {rank = same; 2; 3;}
      {rank = same; 8; 9;}
      label="Composante Forte"
      fontcolor=blue
      fontsize=38
    }
    10 [color=gold2]
    11 [color=firebrick2]
    10 -> 11
    8 -> 11

    subgraph clusterCircuit {
      1, 5, 6 [color=red]
      1 -> 5 [color=red]
      5 -> 6 [color=red]
      6 -> 1 [color=red]
      label="circuit"
      fontcolor=red
      fontsize=38
      {rank = same; 5; 6;}
    }
    7 [color=chartreuse, style=bold]
    6 -> 7

    subgraph clusterCompoConnexe1 {
      node [color="#FF00FF"]
      edge [color="#FF00FF"]
      15 -> 16 -> 17 -> 19 -> 15
      16 -> 18 -> 19
      label="Composante Forte"
      fontcolor="#FF00FF"
      fontsize=38
      {rank = same; 15; 16; 17;}
    }

    20 [color=darkgreen, style=bold]
    21 [color=darkorange, style=bold]
    22 [color=cyan, style=bold]
    23 [color="#FF1493", style=bold]
    24 [color="#11FF11", style=bold]
    20 -> 21 -> 22
    20 -> 23
    21 -> 23
    23 -> 24
}
```
<figcaption>
Graphe G Orienté <bpurple>Non Pondéré,</bpurple><br/>
Non Connexe, Non Fortement Connexe,<br/>
et <bblue>Un Chemin 2 -> 3 -> 9 -> 8 -> 11 de longueur 4</bblue>
</figcaption>
</center>
</div>
</div>

<div id="conteneur">
<div>
<center>
```dot
digraph G {
    size="6.8"
    node [shape=circle, fontsize=38, style=bold]
    edge [fontsize=38]
    0 -> 1 [label="4"]
    0 -> 2 [label="5"]
    0 -> 3 [label="0.8"]
    4 [color=deepskyblue]
    0 -> 4 [label="7"]
    4 -> 10 [label="6"]
    subgraph clusterCompoConnexe0 {
      node [color=blue]
      edge [color=blue]
      2, 3 [color=blue]
      9 -> 8 [label="4"]
      2 -> 3 [label="5"]
      2 -> 9 [label="7"]
      8 -> 3 [label="8"]
      8 -> 2 [label="3"]
      3 -> 9 [label="5"]
      {rank = same; 2; 3;}
      {rank = same; 8; 9;}
      label="Composante Forte"
      fontcolor=blue
      fontsize=38
    }
    10 [color=gold2]
    11 [color=firebrick2]
    10 -> 11 [label="8"]
    8 -> 11 [label="2"]

    subgraph clusterCircuit {
      edge [color=red]
      1, 5, 6 [color=red]
      1 -> 5 [label="3"]
      5 -> 6 [label="1"]
      6 -> 1 [label="2"]
      label="circuit"
      fontcolor=red
      fontsize=38
      {rank = same; 5; 6;}
    }
    7 [color=chartreuse, style=bold]
    6 -> 7 [label="2"]

    subgraph clusterCompoConnexe1 {
      node [color="#FF00FF"]
      edge [color="#FF00FF"]
      15 -> 16 [label="4"]
      16 -> 17 [label="2"]
      17 -> 19 [label="1"]
      19 -> 15 [label="3"]
      16 -> 18 [label="5"] 
      18 -> 19 [label="7"]
      label="Composante Forte"
      fontcolor="#FF00FF"
      fontsize=38
      {rank = same; 15; 16; 17;}
    }

    20 [color=darkgreen, style=bold]
    21 [color=darkorange, style=bold]
    22 [color=cyan, style=bold]
    23 [color="#FF1493", style=bold]
    24 [color="#11FF11", style=bold]
    20 -> 21 [label="3"]
    21 -> 22 [label="5"]
    20 -> 23 [label="4"]
    21 -> 23 [label="8"]
    23 -> 24 [label="9"]
}
```

<figcaption>
Graphe G Orienté <bpurple>Pondéré</bpurple><br/>Non Connexe, Non Fortement Connexe<br/>
et <bblue>Un Chemin 0 -> 1 -> 5 -> 6 -> 7, de poids total

$=4+3+1+2=10$

</bblue>
</figcaption>
<br/>
</center>
</div>
</div>
