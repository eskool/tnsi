# TNSI : Parcours en Largeur ou BFS - Breadth First Search :gb:

## Introduction

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/NrQGxfFMYzs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Introduction (+ Lien avec la notion de plus courts chemins dans un Graphe) (12 min 54)

</center>

## Parcours en Largeur avec une File

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/N0PUdc-k74g" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Parcours en Largeur avec File (28 min 58)

</center>

## Résumé

Un **Parcours en Largeur** :fr: ou **BFS - Breadth First Search** :gb: :

* peut être implémenté avec des **Files**
* Détermine si le graphe est **Connexe, ou pas**
* Détermine si le graphe admet un **Cycle, ou pas**
* fonctionne pour des **Graphes Orientés, ou Non Orientés**
* ne fonctionne QUE pour des graphes NON pondérés, à priori, mais:
* admet une généralisation dans un **Graphe Pondéré**:
    * **à poids $>0$, Orienté ou Non** : qui est alors appelé un <bred>Algorithme de Dijkstra</bred> (cf TD) (**Edsger Dijkstra, 1959**)
    * **pour des poids quelconques ($>0$ ou $<0$), Orienté** : grâce à l'Algorithme de <bred>Floyd-Warshall</bred> (article en 1962), encore appelé Algorithme de <bred>Roy-Floyd-Warshall</bred> (car énoncé par Bernard Roy en 1959)
* Construit un **Arbre Couvrant** (pas forcément minimal)
* Construit un **Arbre des plus courts chemins** entre un sommet de départ et tous les autres sommets (atteignables), **dans un graphe non pondéré**
* Construit un **chemin** entre un sommet de départ et tous les autres sommets (atteignables), **dans un graphe non pondéré**




