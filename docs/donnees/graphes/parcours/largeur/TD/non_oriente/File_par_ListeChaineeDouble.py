# URL : https://github.com/glassus/nsi/blob/master/Terminale/Theme_1_Structures_de_donnees/1.1_Listes_Piles_Files/01_Listes_Piles_Files.ipynb

# Implémentation avec Liste Chaînée Double

class Cellule :
    def __init__(self, valeur, suivante):
        self.valeur = valeur
        self.suivante = suivante

    def get_valeur(self) -> int:
        return self.valeur

    def get_suivante(self):
        return self.suivante

    def set_suivante(self, cellule) -> None: # 'cellule' est définie comme la suivante de self (de la cellule courante)
        if cellule is None or (type(cellule.valeur) == type(self.valeur)):
            self.suivante = cellule
        else:
            raise TypeError

class File:
    """Structure de File"""
    def __init__(self, data=[]):
        if data == []:
            self.tete = None
            self.queue = None
        else:
            self.tete = None
            self.queue = None
            data.reverse()
            for x in data:
                self.tete = Cellule(x, self.tete)
                if self.queue is None:
                    self.queue = self.tete

    def est_vide(self):
        return self.tete is None
    
    def vider(self):
        self.tete = None
        self.queue = None
  
    def enfile(self, x)->None: # en O(1)
        """On enfile/enqueue/queue à droite (ordre normal)"""
        # Implémentation  dans l'Ordre Normal (enfile à Droite)
        cell = Cellule(x, None)
        if self.est_vide():
          self.tete = cell
        else:
          self.queue.suivante = cell
        self.queue = cell
  
    def defile(self): # en O(1)
        """On defile/dequeue/tete à gauche (ordre normal)"""
        # implémenté dans l'Ordre Normal (défile à gauche)
        if self.est_vide():
            # on pourrait lever une erreur ici, question de choix
            return None
        else:
            v = self.tete.valeur # On récupère la valeur à renvoyer
          # print("Defile", v)
            self.tete = self.tete.suivante  # On supprime la 1ère cellule (la tête, à gauche)
            if self.tete is None:
              self.queue = None
            return v

    def get_tete(self):
        if self.tete is None:
            return None
        else:
            return self.tete.valeur

    def copy(self):
      fTemp = File()
      t = self.tete
      while self.tete is not None:
          fTemp.enfile(self.tete.valeur)
          self.tete = self.tete.suivante
      self.tete = t
      return fTemp

    def retirer_queue(self): # en O(n)
        """renvoie la liste courante, privée du dernier élément (queue)"""
        # print("Retirer Queue")
        cell = self.tete
        if (cell is None) or (cell.get_suivante() is None): # en O(1)
            self.tete = None
            self.queue = None
            return None if cell is None else cell.valeur
        else: # en O(n)
            while (cell is not None) and (cell.get_suivante().get_suivante() is not None):
                cell = cell.get_suivante()
            v = self.queue.valeur
            self.queue = cell
            cell.set_suivante(None)
            return v

    def renverse(self):
          fileTemp = self.copy()
          self.vider()
          while not fileTemp.est_vide():
              self.enfile(fileTemp.retirer_queue())

    def concatene(self, f2):
        if self is None:
            return f2
        else:
            temp = f2.copy()
            while temp.tete is not None:
                self.enfile(temp.tete.valeur)
                temp.defile()
        f2.vider()

    def __repr__(self):
        s = ""
        c = self.tete
        while c != None :
            s += str(c.valeur)+"|"
            c = c.suivante
        return "|•|"+s+"⟂|"
  
    # def __str__(self):
    #     s = "|•|"
    #     c = self.tete
    #     while c != None :
    #         s += str(c.valeur)+"|"
    #         c = c.suivante
    #     return s
  
# f = File()  # f=None
# print("f = ", f)
# f.enfile(3)   # f= 3
# print("f = ", f)
# f.enfile(5)  # f= 3 5 par convention
# print("f = ", f)
# f.est_vide()  #  False
# print("f est vide = ", f.est_vide())
# f.enfile(1)  # f= 3 5 1
# print("f = ", f)
# f.defile()  # f= 5 1    valeur renvoyée : 3
# print("f = ", f)
# f.defile()  # f= 1      valeur renvoyée : 5
# print("f = ", f)
# f.enfile(9)  # f= 1 9
# print("f = ", f)
# f.defile()  # f= 9       valeur renvoyée : 1
# print("f = ", f)
# f.defile()  # f= None      valeur renvoyée : None
# print("f = ", f)
# f.est_vide() # True
# print("f est vide = ", f.est_vide())

# f = File([1,2,3])
f = File()
print("f=",f)
print("tete de f =",f.get_tete())
# f.enfile(1)
# f.enfile(2)
# f.enfile(3)
# print("f =", f)

# f1 = File()
# f1.enfile(14)
# f1.enfile(15)
# f1.enfile(16)
# print("f1 init =", f1)

# f.concatene(f1)
# print("concatene f =", f)
# print("f1 =", f1)

# f.renverse()
# print("f renverse =", f)

# f1 = f.copy()
# print("f1 copie de f:")
# print("f =", f)
# print("f1 =", f1)

# # print("après enfile :")
# # f.enfile(4)
# # f.enfile(5)
# # f.enfile(6)

# # f1.enfile(7)
# # f1.enfile(8)
# # f1.enfile(5)
# # f1.enfile(6)
# # print("f =", f)
# # print("f1 =", f1)

# print("defile")
# f1.defile()
# print("f =", f)
# print("f1 defile =", f1)
# f1.defile()
# print("f =", f)
# print("f1 defile=", f1)

