def toto():
  global INF
  INF = 100
  print("from TOTO")

def tata():
  # global INF
  INF = 200
  print("from TATA")
  print("INF = ", INF)

toto()
print(INF)
tata()