from File_par_ListeChaineeDouble import File

class Graphe:
  def __init__(self, mat=[], liste_successeurs=[], dico_successeurs={}):
    if mat != [] and liste_successeurs == [] and dico_successeurs == {}:
      print("INIT avec Matrice d'Adjacence")
      self.m = mat
      # self.check_symetrie_matrice()
      self.mat_vers_liste_successeurs()
      self.mat_vers_dico_successeurs()
    elif mat == [] and liste_successeurs != [] and dico_successeurs == {}:
      print("INIT avec Liste de successeurs")
      self.liste_successeurs = liste_successeurs
      self.liste_vers_matrice()
      # self.check_symetrie_matrice()
      self.mat_vers_dico_successeurs()
    elif mat == [] and liste_successeurs == [] and dico_successeurs != {}:
      print("INIT avec le Dico des successeurs")
      self.dico_successeurs = dico_successeurs
      self.dico_vers_matrice()
      # self.check_symetrie_matrice()
      self.mat_vers_liste_successeurs()
    else:
      print("ATTENTION, on ne peut instancier un graphe avec plusieurs paramètres.")
      print("Choisissez l'un des trois paramètres suivants:")
      print("* ou bien : g= Graphe(mat=[...])")
      print("* ou bien : g= Graphe(liste_successeurs=[...])")
      print("* ou bien : g= Graphe(dico_successeurs=[...])")

  ## MATRICE D'ADJACENCE

  def afficher_matrice(self)->None:
    print("Matrice d'Adjacence, M =")
    n = len(self.m)
    for s in range(n):
      print(self.m[s])

  def admet_mat_symetrique(self)->bool:
    n = self.ordre()
    for s in range(n):
      for v in range(s+1):
        if self.m[s][v] != self.m[v][s]:
          return False
    return True

  def check_symetrie_matrice(self):
    if not self.admet_mat_symetrique():
        raise "Erreur : la matrice d'adjacence d'un Graphe Non Orienté DOIT être Symétrique"

  ## SOMMETS

  def ordre(self)->int:
    """renvoie l'ordre du graphe : son nombre de sommets"""
    return len(self.m)

  def sommets(self)->list:
    """Renvoie la liste des n sommets : de 0 à (n-1)"""
    return [s for s in range(len(self.m))]

  def successeurs(self, s:int)->list:
    """Renvoie la liste de tous les voisins de s"""
    return [v for v in range(len(self.m)) if self.m[s][v] != 0]

  def predecesseurs(self, s:int)->list:
    """Renvoie la liste de tous les voisins de s"""
    return [v for v in range(len(self.m)) if self.m[v][s] != 0]

  def ajouter_sommet(self):
    """Il faut ajouter une dernière ligne de 0, 
    et aussi une dernière colonne de 0"""
    n = self.ordre()
    mat = [[] for s in range(n)]
    for s in range(n):
      mat[s] = self.m[s]
      mat[s].append(0)
    mat.append([0 for s in range(n+1)])
    self.m = mat
    self.mat_vers_liste_successeurs()
    self.mat_vers_dico_successeurs()

  def supprimer_sommet(self, s:int)->None:
    """Supprime le sommet 's'"""
    print("Supprime le sommet", s)
    n = self.ordre()
    assert s in range(n), "Sommet inexistant"
    mat = [[] for s in range(n)]
    # Enlève la colonne 'v=s' et définit comme vide la ligne 's'
    for x in range(n):
      if x != s:
        mat[x] = self.m[x][:s]+self.m[x][s+1:]
    mat = mat[:s]+mat[s+1:] # supprime la ligne s qui est une liste vide
    self.m = mat
    self.mat_vers_liste_successeurs()
    self.mat_vers_dico_successeurs()

  ## ARCS ORIENTÉS

  def admet_arc(self, s:int, v:int)->bool:
    return self.m[s][v] == 1

  def arcs(self)->list:
    """Renvoie la liste des n sommets : de 0 à (n-1)"""
    n = len(self.m)
    return [(s,v) for s in range(n) for v in range(n) if self.m[s][v] == 1]

  def ajouter_arc(self, s:int, v:int):
    self.m[s][v] = 1
    self.mat_vers_liste_successeurs()
    self.mat_vers_dico_successeurs()

  def supprimer_arc(self, s:int, v:int):
    self.m[s][v] = 0
    self.mat_vers_liste_successeurs()
    self.mat_vers_dico_successeurs()

  def poids(self, s:int, v:int)->int:
    return self.m[s][v]

  def degre_plus(self, s:int)->int:
    n = self.ordre()
    return len([v for v in self.m[s] if v == 1])

  def degre_moins(self, s:int)->int:
    n = self.ordre()
    return len([v for v in [self.m[x][s] for x in range(n)] if v == 1])

  def degre(self, s:int)->int:
    n = self.ordre()
    return self.degre_plus(s) + self.degre_moins(s)

  ## LISTE DE SUCCESSEURS

  def afficher_liste_successeurs(self)->None:
    print("Liste Successeurs, L =")
    n = self.ordre()
    for s in range(n):
      print("Successeurs de",s," : ",self.liste_successeurs[s])

  def liste_vers_matrice(self)->list:
    liste_successeurs = self.liste_successeurs
    n = len(liste_successeurs)
    self.m = [[0]*n for s in range(n)]
    for s in range(n):
      for v in liste_successeurs[s]:
        self.m[s][v] = 1

  def mat_vers_liste_successeurs(self):
    n=self.ordre()
    lv = [[] for s in range(n)]
    for s in range(n):
      for v in range(n):
        if self.m[s][v] == 1:
          lv[s].append(v)
    self.liste_successeurs = lv

  ## DICTIONNAIRE DE SUCCESSEURS

  def afficher_dico_successeurs(self):
    print("AFFICHE DICO SUCCESSEURS")
    n = len(self.dico_successeurs)
    for s in range(n):
      print(s," : ",self.dico_successeurs[s])

  def dico_vers_matrice(self):
    n = len(self.dico_successeurs)
    self.m = [[0]*n for s in range(n)]
    for s in range(n):
      for v in range(n):
        if v in self.dico_successeurs[s]:
          self.m[s][v] = 1

  def mat_vers_dico_successeurs(self):
    n = len(self.m)
    self.dico_successeurs = {s : [] for s in range(n)}
    for s in range(n):
      for v in range(n):
        if self.m[s][v] == 1:
          self.dico_successeurs[s].append(v)


  ## PARCOURS EN PROFONDEUR RÉCURSIF

  def initialise_parcours(self):
    global BLANC, GRIS, NOIR
    BLANC, GRIS, NOIR = 0, 1, 2
    global INF
    n = self.ordre()
    INF = self.ordre()+1
    self.Pere = [None]*n
    self.admet_cycle = False
    self.cycles = []
    self.Couleur = [BLANC]*n
    self.Dist = [INF]*n
    self.parcourus = []
    self.temps = 0
    self.debut = [0]*n
    self.fin = [0]*n

  def profondeur_recursif(self, depart, cycle_souhaite=False):
    self.initialise_parcours()
    self.parcourus.append(depart)
    self.parcours_profondeur_recursif(depart, cycle_souhaite)

  def parcours_profondeur_recursif(self, depart, cycle_souhaite=False):
    #BLANC, GRIS, NOIR = 0, 1, 2
    # print(self.Couleur[depart])
    self.Couleur[depart] = GRIS
    self.temps += 1
    self.debut[depart] = self.temps
    for v in self.successeurs(depart):
      if self.Couleur[v] == BLANC:
        self.Pere[v] = depart
        self.parcourus.append(v)
        self.parcours_profondeur_recursif(v, cycle_souhaite)
      else:
        if cycle_souhaite:
          if self.Couleur[v] == GRIS: # cycle
            self.admet_cycle = True
            s = depart
            cycle = []
            while s != v:
              cycle.append(s)
              s = self.Pere[s]
            cycle.append(s)
            cycle.reverse()
            # Si on veut ajouter le sommet de départ à la fin
            # cycle.append(s)
            self.cycles.append(cycle)
    self.Couleur[depart] = NOIR
    self.temps += 1
    self.fin[depart] = self.temps

  def cycles_composante_forte(self, s:int)->bool:
    self.profondeur_recursif(s, cycle_souhaite = True)
    return self.cycles

  ## CONNEXITÉ FORTE

  def est_fortement_connexe(self):
    n = self.ordre()
    for s in self.sommets():
      self.initialise_parcours()
      # self.profondeur_recursif(self.sommets()[s])
      self.profondeur_recursif(s)
      if len(self.parcourus) != n:
        print(s, "est dans une autre composante forte")
        return False
    return True

  def matrice_nulle(self, n:int)->list:
    return [[0 for i in range(n)] for j in range(n)]

  def composante_fortement_connexe(self, s:int):
    self.initialise_parcours()
    self.profondeur_recursif(s)
    # ATTENTION, CECI NE FONCTIONNE PAS : parcourus_apd_s = composante_forte = self.parcourus
    parcourus_apd_s = self.parcourus
    composante_forte = [u for u in parcourus_apd_s]
    for u in parcourus_apd_s:
      # print("Parcoure àpd",u)
      self.initialise_parcours()
      self.profondeur_recursif(u)
      if set(self.parcourus) != set(parcourus_apd_s): # pas de chemin de retour de `u` vers `s`
        # print("pas de chemin de retour àpd ",u)
        composante_forte.remove(u)
      # print("composante forte =", composante_forte)
    return composante_forte

  def meme_composante_forte(self, s:int, v:int)->bool:
    return v in self.composante_fortement_connexe(s)

  def premier_sommet_non_parcouru(self, listeSommetsParcourus:list)->int:
    n = self.ordre()
    for s in range(n):
      if s not in listeSommetsParcourus:
        return s
    return None

  def composantes_fortement_connexes(self):
    composantes = []
    sommets_parcourus = []
    depart = self.sommets()[0]
    while depart is not None:
      self.initialise_parcours()
      newComposante = self.composante_fortement_connexe(depart)
      sommets_parcourus += newComposante
      composantes.append(newComposante)
      depart = self.premier_sommet_non_parcouru(sommets_parcourus)
    return composantes

  def arc_dans_composante_forte(self, s:int, v:int, depart=0):
    composante_forte = self.composante_fortement_connexe(depart)
    self.initialise_parcours()
    self.profondeur_recursif(depart)
    # self.profondeur_recursif(s)
    for u in composante_forte:
      if (s in composante_forte and v in composante_forte) and (self.admet_arc(s,v) or self.admet_arc(v,s)):
        return True
    return False

  # ARBRE COUVRANT

  def arc_dans_arbre_couvrant(self, s:int, v:int, depart=0):
    self.initialise_parcours()
    self.profondeur_recursif(depart)
    # print("PARCOURUS ARETE DANS ARBRE COUVRANT=", self.parcourus)
    for u in self.parcourus:
      # print("ARC =",(self.Pere[u], u))
      if (self.Pere[u], u) == (s, v):
        return True
    return False

  # def cycles_composante_forte(self, s:int)->bool:
  #   self.profondeur_recursif(s, cycle_souhaite = True)
  #   return self.cycles

  ## CHEMINS ORIENTÉS

  def chemin_depuis_racine_vers(self, s:int)->list:
    chemin = []
    u = s
    while u is not None:
      chemin.append(u)
      u = self.Pere[u]
    return chemin[::-1]

  def get_plus_proche_sommet_commun(self, s:int, v:int)->int:
    chemin_s = self.chemin_depuis_racine_vers(s)[::-1]
    chemin_v = self.chemin_depuis_racine_vers(v)[::-1]
    for u in chemin_s:
      if u in chemin_v:
        return u
    return None

  def chemin_depuis_commun_vers(self, s:int, sommet_commun=0)->list:
    chemin = []
    u = s
    while u != sommet_commun:
      chemin.append(u)
      u = self.Pere[u]
    chemin.append(u)
    # print("chemin origine jusqua",sommet_commun,"=",chemin)
    return chemin[::-1]

  def chemin_entre(self, s:int, v:int)->list:
    # NE PAS  REFAIRE UN PARCOURS, SINON, CONFLIT AVEC LE PARCOURS COURANT
    departOLD = self.parcourus[0]
    if not self.meme_composante_forte(s,v):
      return []
    else:
      self.profondeur_recursif(departOLD)
      meme_commun = self.get_plus_proche_sommet_commun(s, v)
      self.profondeur_recursif(s)
      chemin1 = self.chemin_depuis_racine_vers(meme_commun)[:-1]
      self.profondeur_recursif(departOLD)
      chemin2 = self.chemin_depuis_commun_vers(v, meme_commun)
      return chemin1+chemin2

  ## PLUS COURT CHEMIN

  def plus_court_chemin_entre(self, s:int, v:int)->list:
    print("self.parcourus=",self.parcourus)
    if self.parcourus != []: # il existe déjà un parcours en largeur autre que 's'
      departOLD = self.parcourus[0]
    else:
      departOLD = s
    if v not in self.parcourus: # 'v' n'est pas atteignable depuis 's'
      return []
    else:
      self.largeur_iteratif(s)
      ppc = self.chemin_depuis_racine_vers(v)
      self.largeur_iteratif(departOLD)
      return ppc

  ##################################
  # PARCOURS LARGEUR
  ##################################

  def largeur_iteratif(self, depart=0, cycle_souhaite=True):
    voisins = File()
    voisins.enfile(depart)
    self.initialise_parcours()
    self.parcourus.append(depart)
    self.Couleur[depart] = GRIS
    self.Dist[depart] = 0
    while not voisins.est_vide():
      explore = voisins.get_tete()
      for v in self.successeurs(explore):
        if self.Couleur[v] == BLANC:
          self.Couleur[v] = GRIS
          self.parcourus.append(v)
          self.Dist[v] = self.Dist[explore] + 1
          self.Pere[v] = explore
          voisins.enfile(v)
        elif self.Couleur[v] == GRIS and cycle_souhaite:
          sommet_commun = self.get_plus_proche_sommet_commun(explore, v)
          # chemin1 = self.chaine_vers_racine_jusqua(explore, sommet_commun)
          # chemin2 = self.chaine_vers_racine_jusqua(v, sommet_commun)
          chemin1 = self.chemin_depuis_commun_vers(explore, sommet_commun)
          chemin2 = self.chemin_depuis_commun_vers(v, sommet_commun)
          chemin2.pop()
          chemin2.reverse()
          cycle = chemin1+chemin2
          self.cycles.append(cycle)
      voisins.defile()
      self.Couleur[explore] = NOIR



  ########################################################################
  ## RENDU DOT ET IMAGE
  ########################################################################

  def toDot(self, filename="test.dot"):
      """Crée un fichier graph .dot, par défaut test.dot
      """
      assert ".dot" == filename[-4:], "filename must contain '.dot' extension"
      # print("dot Filename = ",filename)
      with open(filename,"w") as fichier:
          fichier.write("digraph G {\n node [shape=circle]\n")
          for s in self.sommets():
              sommet = str(s)+";\n"
              fichier.write(sommet)
          for (s,v) in self.arcs():
            arc = str(s)+" -> "+str(v)+" [style=bold];\n"
            fichier.write(arc)
          fichier.write("}")

  def toDotArbreCouvrant(self, filename="testArbreCouvrant.dot", depart=0):
      """Crée un fichier graph .dot, par défaut testArbreCouvrant.dot
      """
      assert ".dot" == filename[-4:], "filename must contain '.dot' extension"
      # print("dot Filename = ",filename)
      with open(filename,"w") as fichier:
          fichier.write("digraph G {\n node [shape=circle]\n")
          for s in self.sommets():
              sommet = str(s)
              if s in self.parcourus:
                sommet += " [style=bold, color=red]"
              sommet += ";\n"
              fichier.write(sommet)
          # for (s,v,p) in self.arcs():
          for (s,v) in self.arcs():
            if self.arc_dans_arbre_couvrant(s,v,depart=depart):
              arc = str(s)+" -> "+str(v)+f" [style=bold, color=red];\n"
            else:
              arc = str(s)+" -> "+str(v)+f" [style=bold];\n"
            fichier.write(arc)
          # fichier.write("{rank=same; 1,2}")
          fichier.write("}")

  def showArbreCouvrant(self, depart=0, name="testArbreCouvrant"):
    self.initialise_parcours()
    self.profondeur_recursif(depart)
    self.toDotArbreCouvrant(f"{name}.dot", depart)
    self.renderPicture(f"{name}.dot")

  def toDotComposanteForte(self, filename="testComposanteConnexeForte.dot", depart=0):
      """Crée un fichier graph .dot, par défaut testArbreCouvrant.dot
      """
      assert ".dot" == filename[-4:], "filename must contain '.dot' extension"
      # print("dot Filename = ",filename)
      with open(filename,"w") as fichier:
          fichier.write("digraph G {\n node [shape=circle]\n")
          for s in self.sommets():
              sommet = str(s)
              if s in self.composante_fortement_connexe(depart):
                sommet += " [style=bold, color=red]"
              sommet += ";\n"
              fichier.write(sommet)
          for (s,v) in self.arcs():
            if self.arc_dans_composante_forte(s,v,depart=depart):
              arc = str(s)+" -> "+str(v)+f" [style=bold, color=red];\n"
            else:
              arc = str(s)+" -> "+str(v)+f" [style=bold];\n"
            fichier.write(arc)
          # fichier.write("{rank=same; 1,2}")
          fichier.write("}")

  def showComposanteForte(self, depart=0, name="testComposanteConnexeForte"):
    self.initialise_parcours()
    self.profondeur_recursif(depart)
    self.toDotComposanteForte(f"{name}.dot", depart)
    self.renderPicture(f"{name}.dot")

  def toDotPlusCourtChemin(self,s:int, v:int, filename="testPlusCourtChemin.dot"):
      """Crée un fichier graph .dot, par défaut test.dot
      """
      assert ".dot" == filename[-4:], "filename must contain '.dot' extension"
      pcChemin = self.plus_court_chemin_entre(s,v)
      with open(filename,"w") as fichier:
          fichier.write("digraph G {\n node [shape=circle]\n")
          for s in self.sommets():
              sommet = str(s)+";\n"
              fichier.write(sommet)
          for (s,v) in self.arcs():
              if (s in pcChemin) and (v in pcChemin) and abs(pcChemin.index(s)-pcChemin.index(v))==1:
                  arete = str(s)+" -> "+str(v)+" [color=red,style=bold];\n"
              else:
                  arete = str(s)+" -> "+str(v)+"\n"
              fichier.write(arete)
          fichier.write("}")

  def showPlusCourtChemin(self, depart=0, arrivee=1, name="testPlusCourtchemin"):
    self.largeur_iteratif(depart)
    self.toDotPlusCourtChemin(depart, arrivee, f"{name}.dot")
    self.renderPicture(f"{name}.dot")

  def extraire_nom_sans_extension(self, filename="test.dot"):
    """Enlève l'extension '.dot' au nom de fichier"""
    assert type(filename) is str, "Erreur : le nom de fichier DOIT être une chaîne STR"
    assert ".dot" == filename[-4:], "Erreur : le nom de fichier DOIT finir par une extension '.dot'"
    return filename[:-4]

  def renderPicture(self, filename="test.dot"):
      name = self.extraire_nom_sans_extension(filename)
      # print("name = ",name)
      import os
      os.system(f"dot -Tpng {name}.dot -o {name}.png")
      from PyQt5 import QtGui, QtWidgets
      app = QtWidgets.QApplication([])
      window = QtWidgets.QWidget()
      HLayout = QtWidgets.QHBoxLayout()
      pixmap = QtGui.QPixmap(name)
      label = QtWidgets.QLabel()
      label.setPixmap(pixmap)
      HLayout.addWidget(label)
      window.setLayout(HLayout)
      window.setWindowTitle("Mon Beau Graphe!")
      window.show()
      app.exec_()

  def show(self, name="test"):
    self.toDot(f"{name}.dot")
    self.renderPicture(f"{name}.dot")


if __name__ == "__main__":
  # m1 = [[0,2,3,0], 
  #       [0,0,0,1], 
  #       [0,4,0,5], 
  #       [0,0,1,0]]
  # g1 = Graphe(m1)
  # g1.afficher_matrice()
  # g1.show()
  # print("sommets = ", g1.sommets())
  # print("arcs = ", g1.arcs())
  # g1.afficher_liste_successeurs()
  # g1.afficher_dico_successeurs()

  # lv1 = [[[1,2],[2,3]],
  #        [[3,1]],
  #        [[1,4],[3,5]],
  #        [[2,1]]]
  # # g1 = Graphe(liste_successeurs=lv1)
  # g1.afficher_matrice()
  # g1.afficher_liste_successeurs()
  # g1.afficher_dico_successeurs()

  # d1 = {0 : {1:2,2:3},
  #       1 : {0:2,2:4,3:1},
  #       2 : {0:3,1:4,3:5},
  #       3 : {1:1,2:5},
  #       }
  # g1 = Graphe(dico_successeurs=d1)
  # g1.afficher_matrice()
  # g1.afficher_dico_successeurs()
  # g1.afficher_liste_successeurs()
  # for s in range(g1.ordre()):
  #   print("degré de",s,"=",g1.degre(s))
  # g1.show()

  # m0 = [[0,0,2,0,0],
  #       [3,0,0,5,0],
  #       [0,1,0,0,0],
  #       [4,0,1,0,5],
  #       [0,6,0,0,0]]

  m0 = [[0,0,1,0,0],
        [1,0,0,1,0],
        [0,1,0,0,0],
        [1,0,1,0,1],
        [0,1,0,0,0]]

  g0 = Graphe(m0)
  g0.ajouter_sommet()
  g0.ajouter_sommet()
  g0.ajouter_sommet()
  g0.ajouter_sommet()
  g0.ajouter_sommet()
  g0.ajouter_sommet()
  g0.ajouter_sommet()
  g0.ajouter_sommet()
  g0.ajouter_sommet()
  g0.supprimer_arc(3,4)
  g0.supprimer_arc(1,3)
  g0.ajouter_arc(2,6)
  g0.ajouter_arc(2,5)
  g0.ajouter_arc(7,9)
  g0.ajouter_arc(8,10)
  g0.ajouter_arc(8,9)
  g0.ajouter_arc(9,10)
  g0.ajouter_arc(1,11)
  g0.ajouter_arc(6,11)
  # g0.ajouter_arc(11,3)
  g0.ajouter_arc(11,12)
  g0.ajouter_arc(11,13)
  g0.ajouter_arc(13,4)
  g0.ajouter_arc(12,3)
  # g2.ajouter_arc(0,5,2)
  # g2.ajouter_arc(5,3,4)
  # n = g2.ordre()
  # g2.afficher_matrice()
  depart = 2
  s = 3
  v = 11
  g0.profondeur_recursif(depart, cycle_souhaite=True)
  print("parcourus =", g0.parcourus)
  print("cycles =", g0.cycles)
  print("Couleurs =", g0.Couleur)
  # print("Distances =", g0.Dist)
  print("chemin depuis racine vers",s, "=",g0.chemin_depuis_racine_vers(s))
  meme_commun = g0.get_plus_proche_sommet_commun(s,v)
  print("entre s=",s, "et v=",v, "Meme commun  =", g0.get_plus_proche_sommet_commun(s, v))
  print("chemin depuis commun ",meme_commun, "vers=",s, " Sous-chaine =", g0.chemin_depuis_commun_vers(s, meme_commun))
  print("chemin depuis commun ",meme_commun, "vers=",v, " Sous-chaine =", g0.chemin_depuis_commun_vers(v, meme_commun))
  print("chemin entre s=",s, "et v=",v, "=", g0.chemin_entre(s, v))
  print("plus court chemin entre s=",s, "et v=",v, "=", g0.plus_court_chemin_entre(s, v))
  # g0.afficher_matrice()
  # g0.show(depart)
  # g0.showComposanteForte(depart)
  g0.showPlusCourtChemin(s,v)
  # g0.showArbreCouvrant(depart)
  # g0.showComposanteForte(depart)
  # g0.showArbreCouvrant(depart)
  # print("parcourus = ", g2.parcourus)
  # print("cycles = ", g2.cycle)
  # for v in range(n):
  #   print("self.Pere[",v,"]=",g2.Pere[v])
  # print("fortement connexe", g2.est_fortement_connexe())
  # print("composante fortement connexe contenant", depart,"=", g2.composante_fortement_connexe(depart))
  # print("composantes fortement connexes", g2.composantes_fortement_connexes())
  # print("ARC DANS ARBRE COUVRANT =",g2.arc_dans_arbre_couvrant(0,5, depart))
  # print("CYCLES = ",g2.cycles_composante_forte(depart))
  # s=2
  # v=5
  # print(s, "et",v, "même composante forte: ", g2.meme_composante_forte(s,v))
  # print(s, "->",v," dans compo forte contenant", depart, "?", g2.arc_dans_composante_forte(s,v,s))
  # g2.showArbreCouvrant(depart)
  # g2.show()

  # g1.ajouter_sommet()
  # g1.afficher_matrice()
  # g1.ajouter_arc(2,4)
  # g1.afficher_matrice()
  # g1.supprimer_arc(2,4)
  # g1.afficher_matrice()
  # rep = g1.admet_mat_symetrique()
  # print("Est Symétrique = ", rep)

  # g1.toDot()
  # g1.renderPicture("test.dot")


