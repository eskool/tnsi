from Pile_par_ListeChainee import Pile

class Graphe: # NON ORIENTÉ PONDÉRÉ
  def __init__(self, mat=[], liste_voisins=[], dico_voisins={}):
    if mat != [] and liste_voisins == [] and dico_voisins == {}:
      print("INIT avec Matrice d'Adjacence")
      self.m = mat
      self.check_symetrie_matrice()
      self.mat_vers_liste_voisins()
      self.mat_vers_dico_voisins()
    elif mat == [] and liste_voisins != [] and dico_voisins == {}:
      print("INIT avec Liste de Voisins")
      self.liste_voisins = liste_voisins
      self.liste_vers_matrice()
      # self.liste_voisins_vers_voisins()
      self.check_symetrie_matrice()
      self.mat_vers_dico_voisins()
    elif mat == [] and liste_voisins == [] and dico_voisins != {}:
      print("INIT avec le Dico des Voisins")
      self.dico_voisins = dico_voisins
      self.dico_vers_matrice()
      self.check_symetrie_matrice()
      self.mat_vers_liste_voisins()
    else:
      print("ATTENTION, on ne peut instancier un graphe avec plusieurs paramètres.")
      print("Choisissez l'un des trois paramètres suivants:")
      print("* ou bien : g= Graphe(mat=[...])")
      print("* ou bien : g= Graphe(liste_voisins=[...])")
      print("* ou bien : g= Graphe(dico_voisins=[...])")

  ## MATRICE D'ADJACENCE

  def afficher_matrice(self)->None:
    print("Matrice d'Adjacence, M =")
    for s in range(len(self.m)):
      print(self.m[s])

  def admet_mat_symetrique(self)->bool:
    n = self.ordre()
    for s in range(n):
      for v in range(s+1):
        if self.m[s][v] != self.m[v][s]:
          print(f"m[{s}][{v}]={self.m[s][v]}")
          print(f"m[{v}][{s}]={self.m[v][s]}")
          print("s=",s,"v=",v)
          return False
    return True

  def check_symetrie_matrice(self):
    if not self.admet_mat_symetrique():
        raise "Erreur : la matrice d'adjacence d'un Graphe Non Orienté DOIT être Symétrique"

  ## SOMMETS

  def ordre(self)->int:
    """renvoie l'ordre du graphe : son nombre de sommets"""
    return len(self.m)

  def sommets(self)->list:
    """Renvoie la liste des n sommets : de 0 à (n-1)"""
    return [s for s in range(len(self.m))]

  def voisins(self, s:int)->list:
    """Renvoie la liste de tous les voisins de s"""
    return [v for v in range(len(self.m)) if self.m[s][v] != 0]

  def ajouter_sommet(self):
    """Il faut ajouter une dernière ligne de 0, 
    et aussi une dernière colonne de 0"""
    n = self.ordre()
    mat = [[] for s in range(n)]
    for s in range(n):
      mat[s] = self.m[s]
      mat[s].append(0)
    mat.append([0 for s in range(n+1)])
    self.m = mat
    self.mat_vers_liste_voisins()
    self.mat_vers_dico_voisins()

  def supprimer_sommet(self, s:int)->None:
    """Supprime le sommet 's'"""
    print("Supprime le sommet", s)
    n = self.ordre()
    assert s in range(n), "Sommet inexistant"
    mat = [[] for s in range(n)]
    # Enlève la colonne 'v=s' et définit comme vide la ligne 's'
    for x in range(n):
      if x != s:
        mat[x] = self.m[x][:s]+self.m[x][s+1:]
    mat = mat[:s]+mat[s+1:] # supprime la ligne i qui est une liste vide
    self.m = mat
    self.mat_vers_liste_voisins()
    self.mat_vers_dico_voisins()

  ## ARÊTES

  def admet_arete(self, s:int, v:int)->bool:
    return self.m[s][v] != 0

  def aretes(self)->list:
    """Renvoie la liste des n sommets : de 0 à (n-1)"""
    n = len(self.m)
    return [(s,v,self.m[s][v]) for s in range(n) for v in range(n) if self.m[s][v] != 0 and s<v]

  def ajouter_arete(self, s:int, v:int, p:int):
    self.m[s][v] = p
    self.m[v][s] = p
    self.mat_vers_liste_voisins()
    self.mat_vers_dico_voisins()

  def supprimer_arete(self, s:int, v:int):
    self.m[s][v] = 0
    self.m[v][s] = 0
    self.mat_vers_liste_voisins()
    self.mat_vers_dico_voisins()

  def poids(self, s:int, v:int)->int:
    return self.m[s][v]

  def max_poids(self):
    maxi = 0
    n = self.ordre()
    for s in range(n):
      for v in range(s+1):
        if self.m[s][v] > maxi:
          maxi = self.m[s][v]
    return maxi

  def degre(self, s:int)->int:
    return len([v for v in self.m[s] if v != 0])

  ## LISTE DE VOISINS

  def afficher_liste_voisins(self)->None:
    print("Liste Voisins, L =")
    n = self.ordre()
    for s in range(n):
      print("Voisins de",s," : ",self.liste_voisins[s])

  # def liste_voisins_vers_voisins(self):
  #   """Peuple la liste 'self.voisins' qui ne contient QUE les voisins des sommets (PAS les poids)"""
  #   n = self.ordre()
  #   self.voisins = [None]*n
  #   for s in range(n):
  #     self.voisins[s] = [x[0] for x in self.liste_voisins[s]]

  # def liste_voisins_vers_liste_voisins_poids(self):
  #   n = self.ordre()
  #   self.liste_voisins_poids = [None]*n
  #   for s in range(n):
  #     self.liste_voisins_poids[s] = [x[1] for x in self.liste_voisins[s]]

  def liste_vers_matrice(self)->list:
    liste_voisins = self.liste_voisins
    n = len(liste_voisins)
    self.m = [[0]*n for s in range(n)]
    for s in range(n):
      for v, p in liste_voisins[s]:
        self.m[s][v] = p
        self.m[v][s] = p

  def mat_vers_liste_voisins(self):
    n=self.ordre()
    lv = [[] for s in range(n)]
    for s in range(n):
      for v in range(n):
        if self.m[s][v] != 0:
          lv[s].append([v,self.m[s][v]])
    self.liste_voisins = lv
    # self.liste_voisins_vers_voisins()
    
  ## DICTIONNAIRE DE VOISINS

  def afficher_dico_voisins(self):
    print("AFFICHE DICO VOISINS")
    n = len(self.dico_voisins)
    for s in self.dico_voisins.keys():
      print(s," : ",self.dico_voisins[s])

  def dico_vers_matrice(self):
    n = len(self.dico_voisins)
    self.m = [[0]*n for s in range(n)]
    for s in self.dico_voisins.keys():
      for v,p in self.dico_voisins[s].items():
        self.m[s][v] = p

  def mat_vers_dico_voisins(self):
    n = len(self.m)
    self.dico_voisins = {s : {} for s in range(n)}
    for s in range(n):
      for v in range(n):
        if self.m[s][v] != 0:
          self.dico_voisins[s][v] = self.m[s][v]

  ## PARCOURS EN PROFONDEUR ITÉRATIF

  def initialise_parcours(self):
    BLANC = 0
    n = self.ordre()
    self.Pere = [None]*n
    self.admet_cycle = False
    self.cycles = []
    self.Couleur = [BLANC]*n
    self.parcourus = []
    self.temps = 0
    self.debut = [0]*n
    self.fin = [0]*n

  def profondeur_iteratif(self, depart, cycle_souhaite=False):
    BLANC, NOIR = 0, 1
    voisins = Pile()
    voisins.empile(depart)
    self.initialise_parcours()
    while not voisins.est_vide(): # il reste des sommets
      explore = voisins.depile()
      if self.Couleur[explore] == BLANC: # jamais vu le sommet 'explore'
        self.parcourus.append(explore)
        self.Couleur[explore] = NOIR
        # self.temps += 1
        # self.debut[explore] = self.temps
        voisinsExplore = self.voisins(explore)
        voisinsExplore.reverse()
        for v in voisinsExplore:
          if self.Couleur[v] == BLANC: # on empile les voisins non visités
            voisins.empile(v)
            self.Pere[v] = explore
      else: # explore déjà visité
        if self.Pere[explore] != explore:
          self.admet_cycle = True
          if cycle_souhaite:
            u = self.Pere[self.Pere[explore]]
            cycle = [explore, self.Pere[explore], u]
            while not self.admet_arete(explore,u) and self.Pere[u] is not None:
              u = self.Pere[u]
              cycle.append(u)
            cycle.reverse()
            # cycle.append(cycle[0])
            # self.admet_cycle = cycle
            self.cycles.append(cycle)
            # return self.cycles

  def cycles_composante(self, s:int)->bool:
    self.profondeur_iteratif(s, cycle_souhaite = True)
    return self.cycles

  ## CONNEXITÉ

  def est_connexe(self):
    self.initialise_parcours()
    self.profondeur_iteratif(self.sommets()[0])
    return len(self.parcourus) == self.ordre()

  def composante_connexe(self, s:int):
    self.initialise_parcours()
    self.profondeur_iteratif(s)
    return self.parcourus

  def meme_composante(self, s:int, v:int)->bool:
    return v in self.composante_connexe(s)

  def premier_sommet_non_parcouru(self, listeSommetsParcourus:list)->int:
    n = self.ordre()
    for s in range(n):
      if s not in listeSommetsParcourus:
        return s
    return None

  def composantes_connexes(self):
    composantes = []
    sommets_parcourus = []
    depart = self.sommets()[0]
    while depart is not None:
      self.initialise_parcours()
      newComposante = self.composante_connexe(depart)
      sommets_parcourus += newComposante
      composantes.append(newComposante)
      depart = self.premier_sommet_non_parcouru(sommets_parcourus)
    return composantes

  # ARBRE COUVRANT

  def arete_dans_arbre_couvrant(self, s:int, v:int, depart=0):
    self.initialise_parcours()
    self.profondeur_iteratif(depart)
    # print("PARCOURUS ARETE DANS ARBRE COUVRANT=", self.parcourus)
    for u in self.parcourus:
      if set({u, self.Pere[u]}) == set({s, v}):
        return True
    return False

  ## CHAINES

  def chaine_vers_racine(self, s:int)->list:
    chemin = []
    u = s
    while u is not None:
      chemin.append(u)
      u = self.Pere[u]
    return chemin

  def get_plus_proche_sommet_commun(self, s:int, v:int)->int:
    chemin_s = self.chaine_vers_racine(s)
    chemin_v = self.chaine_vers_racine(v)
    for u in chemin_s:
      if u in chemin_v:
        return u
    return None

  def chaine_vers_racine_jusqua(self, s:int, sommet_commun=0)->list:
    chemin = []
    u = s
    while u != sommet_commun:
      chemin.append(u)
      u = self.Pere[u]
    chemin.append(u)
    # print("chemin origine jusqua",sommet_commun,"=",chemin)
    return chemin

  def chaine_entre(self, s:int, v:int)->list:
    # NE PAS  REFAIRE UN PARCOURS, SINON, CONFLIT AVEC LE PARCOURS COURANT
    if v not in self.parcourus:
      return []
    else:
      meme_commun = self.get_plus_proche_sommet_commun(s, v)
      chemin1 = self.chaine_vers_racine_jusqua(s, meme_commun)
      chemin2 = self.chaine_vers_racine_jusqua(v, meme_commun)[:-1]
      chemin2.reverse()
      return chemin1+chemin2

########################################################################

  def toDot(self, filename="test.dot"):
      """Crée un fichier graph .dot, par défaut test.dot
      """
      assert ".dot" == filename[-4:], "filename must contain '.dot' extension"
      # print("dot Filename = ",filename)
      with open(filename,"w") as fichier:
          fichier.write("graph G {\n node [shape=circle]\n")
          for s in self.sommets():
              sommet = str(s)+";\n"
              fichier.write(sommet)
          for (s,v,p) in self.aretes():
            arete = str(s)+" -- "+str(v)+f" [style=bold, label={p}];\n"
            fichier.write(arete)
          # fichier.write("{rank=same; 1,2}")
          fichier.write("}")

  def toDotArbreCouvrant(self, filename="testArbreCouvrant.dot", depart=0):
      """Crée un fichier graph .dot, par défaut testArbreCouvrant.dot
      """
      assert ".dot" == filename[-4:], "filename must contain '.dot' extension"
      # print("dot Filename = ",filename)
      with open(filename,"w") as fichier:
          fichier.write("graph G {\n node [shape=circle]\n")
          for s in self.sommets():
              sommet = str(s)
              if s in self.parcourus:
                sommet += " [style=bold, color=red]"
              sommet += ";\n"
              fichier.write(sommet)
          for (s,v,p) in self.aretes():
            if self.arete_dans_arbre_couvrant(s,v,depart=depart):
              arete = str(s)+" -- "+str(v)+f" [style=bold, label={p}, color=red, fontcolor=red];\n"
            else:
              arete = str(s)+" -- "+str(v)+f" [style=bold, label={p}];\n"
            fichier.write(arete)
          # fichier.write("{rank=same; 1,2}")
          fichier.write("}")

  def showArbreCouvrant(self, depart=0, name="testArbreCouvrant"):
    self.initialise_parcours()
    self.profondeur_iteratif(depart)
    self.toDotArbreCouvrant(f"{name}.dot", depart)
    self.renderPicture(f"{name}.dot")

  def extraire_nom_sans_extension(self, filename="test.dot"):
    """Enlève l'extension '.dot' au nom de fichier"""
    assert type(filename) is str, "Erreur : le nom de fichier DOIT être une chaîne STR"
    assert ".dot" == filename[-4:], "Erreur : le nom de fichier DOIT finir par une extension '.dot'"
    return filename[:-4]

  def renderPicture(self, filename="test.dot"):
      name = self.extraire_nom_sans_extension(filename)
      # print("name = ",name)
      import os
      os.system(f"dot -Tpng {name}.dot -o {name}.png")
      from PyQt5 import QtGui, QtWidgets
      app = QtWidgets.QApplication([])
      window = QtWidgets.QWidget()
      HLayout = QtWidgets.QHBoxLayout()
      pixmap = QtGui.QPixmap(name)
      label = QtWidgets.QLabel()
      label.setPixmap(pixmap)
      HLayout.addWidget(label)
      window.setLayout(HLayout)
      window.setWindowTitle("Mon Beau Graphe!")
      window.show()
      app.exec_()

  def show(self, name="test"):
    self.toDot(f"{name}.dot")
    self.renderPicture(f"{name}.dot")

if __name__ == "__main__":
  # m1 = [[0,2,3,0],
  #       [2,0,4,1],
  #       [3,4,0,5],
  #       [0,1,5,0]]
  # g1 = Graphe(m1)
  # g1.afficher_matrice()
  # print("sommets = ", g1.sommets())
  # print("aretes = ", g1.aretes())
  # g1.afficher_liste_voisins()
  # g1.afficher_dico_voisins()

  # lv1 = [[[1,2],[2,3]],
  #        [[0,2],[2,4],[3,1]],
  #        [[0,3],[1,4],[3,5]],
  #        [[1,1],[2,5]]]
  # g1 = Graphe(liste_voisins=lv1)
  # g1.afficher_matrice()
  # g1.afficher_liste_voisins()
  # g1.afficher_dico_voisins()
  # print("PROFONDEUR = ", g1.profondeur(1))
  # g1.show()

  # d1 = {0 : {1:2,2:3},
  #       1 : {0:2,2:4,3:1},
  #       2 : {0:3,1:4,3:5},
  #       3 : {1:1,2:5},
  #       }
  # g1 = Graphe(dico_voisins=d1)
  # g1.afficher_matrice()
  # g1.afficher_dico_voisins()
  # g1.afficher_liste_voisins()
  # for s in range(g1.ordre()):
  #   print("degré de",s,"=",g1.degre(s))
  # g1.show()

  # m2 = [[0,5,2,4,0],
  #       [5,0,1,3,1],
  #       [2,1,0,1,2],
  #       [4,3,1,0,1],
  #       [0,1,2,1,0]]
  # g2 = Graphe(m2)
  # g2.afficher_matrice()
  # print("PROFONDEUR = ", g2.profondeur(1))
  # g2.show()

  lv0 = [[[1,1],[2,1],[3,1]],
         [[0,1],[2,1],[4,1]],
         [[0,1],[1,1]],
         [[0,1]],
         [[1,1],[5,1]],
         [[4,1]]]
  g0 = Graphe(liste_voisins=lv0)
  g0.afficher_matrice()
  g0.ajouter_sommet()
  g0.ajouter_arete(2,6,3)
  g0.supprimer_arete(0,2)
  g0.ajouter_arete(2,5,1)
  g0.ajouter_sommet()
  g0.ajouter_sommet()
  g0.ajouter_sommet()
  g0.ajouter_sommet()
  g0.ajouter_arete(7,9,2)
  g0.ajouter_arete(1,3,4)
  g0.ajouter_arete(8,10,1)
  g0.ajouter_arete(8,9,1)
  g0.ajouter_arete(9,10,2)
  # g0.supprimer_arete(4,5)
  print("connexe", g0.est_connexe())
  # print("composante connexe 0", g0.composante_connexe(0))
  # print("composante connexe 9", g0.composante_connexe(9))
  depart = 2
  s = 3
  v = 5
  g0.profondeur_iteratif(2, cycle_souhaite=True)
  print("parcourus =", g0.parcourus)
  print("cycles =", g0.cycles)
  print("chaine vers racine depuis",s, "=",g0.chaine_vers_racine(s))
  print("chaine vers racine depuis",v, "=",g0.chaine_vers_racine(v))
  meme_commun = g0.get_plus_proche_sommet_commun(s,v)
  print("entre s=",s, "et v=",v, "Meme commun  =", g0.get_plus_proche_sommet_commun(s, v))
  print("chaine vers racine depuis s=",s, "jusqu'à commun=",meme_commun, " Sous-chaine =", g0.chaine_vers_racine_jusqua(s, meme_commun))
  print("chaine entre s=",s, "et v=",v, "=", g0.chaine_entre(s, v))

  # print("premier non parcouru : ", g0.premier_sommet_non_parcouru(g0.parcourus))
  # print("composantes = ", g0.composantes_connexes())
  # print(f"cycle composante 0 = ", g0.cycle_composante(6))
  # print(f"cycle composante 7 = ", g0.cycle_composante(7))
  # g0.afficher_matrice()
  # print("arete_dans_arbre_couvrant =",g0.arete_dans_arbre_couvrant(8,9,7))
  # g0.admet_cycle()
  # for v in g0.sommets():
  #   print(f"Pere[{v}]={g0.Pere[v]}")
  # for v in g0.sommets():
  #   print(f"Debut[{v}]={g0.debut[v]}")
  #   print(f"Fin[{v}]={g0.fin[v]}")
  # g0.show()
  # s=10
  # v=7
  # print(s, "et",v, "même composante : ", g0.meme_composante(s,v))
  g0.showArbreCouvrant(2)


  # g1.ajouter_sommet()
  # g1.afficher_matrice()
  # g1.ajouter_arete(2,4)
  # g1.afficher_matrice()
  # g1.supprimer_arete(2,4)
  # g1.afficher_matrice()
  # rep = g1.admet_mat_symetrique()
  # print("Est Symétrique = ", rep)

  # g1.toDot()
  # g1.renderPicture("test.dot")
