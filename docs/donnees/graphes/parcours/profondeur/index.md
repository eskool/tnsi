# TNSI : Parcours en Profondeur <br/>ou DFS - Depth First Search :gb:

## Introduction

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/kcedjJOjDpg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Introduction (15 min 22)

<br/>

</center>

## Implémentation Itérative avec une Pile

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/-wPLAsAuenA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Implémentation Itérative avec une Pile (4 min 54)

<br/>

</center>

## Implémentation Récursive du Parcours en Profondeur

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/nfRsSozzAKY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Implémentation Récursive du Parcours en Profondeur (35 min 16)

</center>

## Résumé

:one: Un **Parcours en Profondeur** :fr: ou **DFS - Depth First Search** :gb: peut être implémenté par un Algorithme : 

* **Itératif**, avec une **Pile**
* **Récursif**, avec une **Pile d'appels** récursifs

:two: Un **Parcours en Profondeur (DFS)** permet de:

* Construire un **Arbre Couvrant (en général non minimal)**
* Construire un **chemin** entre un sommet de départ (racine) et tous les autres sommets (atteignables)
* Déterminer si le graphe est **Connexe, ou pas**
* Déterminer si le graphe admet un **Cycle, ou pas**

:three: un **Parcours en Profondeur** fonctionne aussi bien pour des **Graphes Orientés, que Non Orientés**

