# TNSI : Introduction aux Parcours de Graphes

!!! def "Parcours de Graphes"
    <bred>Parcourir un graphe</bred>, c'est parcourir l'ensemble de tous ses sommets dans un ordre à priori quelconque.
    Il existe plusieurs manières classiques de parcourir des graphes : du moins de parcourir tous les <bred>sommets atteignables</bred> dans un graphe, depuis un sommet de départ.  
    Classiquement, on distingue des:

    * **Parcours en Profondeur** :fr:, ou **DFS - Depth First Search** :gb:
    * **Parcours en Largeur** :fr:, ou **BFS - Breadth First Search** :gb:

