# TNSI : Quelques Graphes Classiques

## Graphes Complets $K_n$

!!! def "Graphe Complet"
    * Un <bred>graphe Complet (Non Orienté)</bred> est un graphe simple dans lequel deux sommets quelconques sont forcément adjacents (reliés par une arête et une seulement).
    * Un <bred>graphe Complet (Orienté)</bred> est un graphe simple dans lequel deux sommets $s$ et $v$ quelconques sont forcément reliés par exacteemnt deux arcs orientés (un dans chaque sens) : $s \rightarrow v$ et $v \rightarrow s$

    *Le* graphe complet à $n$ sommets est noté <enc>$K_n$</enc>

<env>**Exemples de Graphes Complets (par défaut : Non Orientés)**</env>

<div id="conteneur">
<div>
<center>
```dot
graph G {
  size="1.2";
  node [shape=circle, style=bold, fontsize=38];
  edge [style=bold]
  1 -- 2
  {rank=same;1;2;}
}
```
<figcaption>
$$K_2$$
</figcaption>
</center>
</div>

<div>
<center>
```dot
graph G {
  layout=circo
  size="1.2";
  node [shape=circle, penwidth=3.0, fontsize=38];
  edge [penwidth=3.0]
  1 -- 2
  2 -- 3
  3 -- 1
  {rank=same;2;3;}
}
```
<figcaption>
$$K_3$$
</figcaption>
</center>
</div>

<div>
<center>
```dot
graph G {
  layout=circo
  size="2";
  node [shape=circle, penwidth=3.0, fontsize=38];
  edge [penwidth=3.0]
  1, 2, 4
  1 -- 2
  1 -- 4
  2 -- 4
  2 -- 3
  4 -- 3
  3 -- 1
  {rank=same;1;2;}
  {rank=same;3;4;}
}
```
<figcaption>
$$K_4$$
</figcaption>
</center>
</div>

<div>
<center>
```dot
graph G {
  layout=circo
  size="2";
  node [shape=circle, penwidth=3.0, fontsize=38];
  edge [penwidth=3.0]
  1 -- {2, 3, 4, 5}
  2 -- {4, 5}
  2 -- 3 [minlen=5]
  3 -- {4, 5}
  4 -- 5 [minlen=1.5]
}
```
<figcaption>
$$K_5$$
</figcaption>
</center>
</div>

<div>
<center>
```dot
graph {
  layout=circo;
  size="2.4";
  node [shape=circle, penwidth=3.0, fontsize=38]
  edge [shape=circle, penwidth=3.0]
  1 -- 2;
  2 -- 3;
  3 -- 4;
  4 -- 5;
  5 -- 6;
  1 -- 6;
  1 -- 3;
  1 -- 4;
  1 -- 5;
  2 -- 4;
  2 -- 5;
  2 -- 6;
  3 -- 5;
  3 -- 6;
  4 -- 6;
}
```
<figcaption>
$$K_6$$
</figcaption>
</center>
</div>
</div>

## Graphes Eulériens

### Chaînes/Chemins Eulérien(ne)s

!!! def "Chaîne/Chemin Eulérien(ne)"
    <div id="conteneur">
    <div>
    Une <bred>Chaîne/Chemin Eulérien(ne)</bred> ou <bred>Parcours Eulérien</bred> est une chaîne/chemin qui **passe une fois et une seule par chaque <blue>arête</blue> du graphe**
    </div>
    <div>
    <center>
    [<img src="../img/euler.jpg"/>](https://fr.wikipedia.org/wiki/Leonhard_Euler)
    <figcaption>Leonhard Euler (1707-1883)</figcaption>
    </center>
    </div>
    </div>


!!! pte
    Une chaîne/chemin eulérienne est une chaîne/chemin simple maximale (passant par toutes les arêtes du graphe)

<env>**ATTENTION**</env> Selon les graphes envisagés: Ce n'est PAS toujours possible!

!!! ex
    ```dot
    graph G {
      size="1.2";
      node [shape=circle, style=bold, fontsize=38];
      edge [style=bold]
      1 -- 2
      {rank=same;1;2;}
    }
    ```

    <figcaption>
    $$K_2$$
    </figcaption>

!!! ex
    Pour chacun des graphes suivants, trouver ***lorsque c'est possible*** une **chaîne eulérienne** / un **parcours eulérien**, càd tracer le graphe sans lever le stylo, en ne passant qu'une et une seule fois par chaque arête :
    <div id="conteneur">
    <div>
    <center>
    ```dot
    graph G {
      layout=circo
      size="2.2";
      node [shape=circle, penwidth=3.0, fontsize=38];
      edge [penwidth=3.0]
      1 -- 2
      2 -- 3
      3 -- 4
      4 -- 1
      4 -- 5
      {rank=same;2;3;}
    }
    ```
    <figcaption>
    Cerf-volant
    </figcaption>
    </center>
    </div>
    <div>
    <center>
    ```dot
    graph G {
      layout=circo
      size="2";
      bgcolor=none;
      node [shape=circle, penwidth=3.0, fontsize=38];
      edge [penwidth=3.0]
      1, 2, 4
      1 -- 2
      1 -- 4
      2 -- 4
      2 -- 3
      4 -- 3
      3 -- 1
      {rank=same;1;2;}
      {rank=same;3;4;}
    }
    ```
    <figcaption>
    $K_4$
    </figcaption>
    </center>
    </div>
    <div>
    <center>
    ```dot
    graph G {
      layout=twopi
      root=5
      size="1.5";
      bgcolor=none;
      node [shape=circle, penwidth=2.0, fontsize=30];
      edge [penwidth=2.0]
      1 -- 2 -- 3 -- 4 -- 1
      1 -- 5
      3 -- 5
      2 -- 5
      4 -- 5
      {rank=same;1;2;}
      {rank=same;3;4;}
    }
    ```
    <figcaption>
    Enveloppe<br/>Pliée
    </figcaption>
    </center>
    </div>
    <div>
    === "Enveloppe Ouverte"
        <center>
        ```dot
        graph G {
          layout=neato
          size="2";
          node [shape=circle, penwidth=2.0, fontsize=24];
          edge [penwidth=2.0, minlen=5]
          1, 2, 4, 5, 6
          1 -- 2
          1 -- 4
          2 -- 3
          3 -- 4
          1 -- 5
          2 -- 5
          3 -- 5
          4 -- 5
          6 -- 1
          6 -- 2
          {rank=same;1;2;}
          {rank=same;3;4;}
        }
        ```
        <figcaption>
        Enveloppe<br/>Ouverte
        </figcaption>
        </center>
    === "Corrigé"
        <img src="../img/enveloppe.gif">
    </div>
    <div>
    <center>
    ```dot
    graph G {
      layout=circo
      size="2";
      bgcolor=none;
      node [shape=circle, penwidth=3.0, fontsize=30];
      edge [penwidth=3.0]
      1 -- {2, 3, 4, 5}
      2 -- {4, 5}
      2 -- 3 [minlen=5]
      3 -- {4, 5}
      4 -- 5 [minlen=1.5]
    }
    ```
    <figcaption>
    $K_5$
    </figcaption>
    </center>
    </div>
    </div>


### Cycles/Circuits Eulériens & Graphes Eulériens

!!! def "Circuit Eulérien & Graphe Eulérien"
    * Un <bred>Cycle/Circuit Eulérien</bred>, ou <bred>Tournée Eulérienne</bred>, est un cycle qui parcoure toutes les arêtes/arcs du graphe une et une seule fois (en revenant au sommet initial)
    * Un <bred>graphe Eulérien</bred> est un graphe contenant un cycle Eulérien
    Remarque: On appelle **graphe semi-eulérien** les graphes admettant :
        * un parcours eulérien, 
        * mais pas de cycle eulérien

!!! pte
    Un **cycle Eulérien** est un cycle simple maximal (qui passe par toutes les arêtes/arcs)

!!! ex
    Dans l'exercice précédent, quels sont les graphes Eulériens?

### Caractérisation des Graphes Eulériens

!!! thm "d'Euler, ou d'Euler-Hierholzer : Caractérisation des Graphes Eulériens, 1736"
    Un graphe connexe **non orienté** admet un **circuit Eulérien**  
    $\Leftrightarrow$ tous ses sommets sont **de degré pair**

<env>**Remarque**</env> Il existe une version orientée de ce théorème d'Euler (juste pour info) :
Un graphe (fortement, ou pas/simplement) connexe **orienté** admet un cycle Eulérien orienté 
$\Leftrightarrow$ chacun de ses sommets est l'extrémité initiale et finale du même nombre d'arcs

!!! ex
    Pour quelles valeurs $n$ peut-on dire que le graphe complet $K_n$ est Eulérien?

###  Histoire / Jeu : Les 7 ponts de Königsberg

La ville de **Königsberg** :de: (aujourd'hui **Kaliningrad** :ru:) est construite autour de deux îles situées sur le Pregel et reliées entre elles par un pont. Six autres ponts relient les rives de la rivière à l'une ou l'autre des deux îles, comme représentés sur le plan ci-dessus.  
**Le problème consiste à déterminer s'il existe, ou non, une promenade dans les rues de Königsberg permettant, à partir d'un point de départ au choix, de passer une et une seule fois par chaque pont, et de revenir à son point de départ, étant entendu qu'on ne peut traverser le Pregel qu'en passant sur les ponts.**

<center>

<div>

<img src="../img/pontsKonigsberg.png">

</div>

</center>

1. D'après vous : Cette promenade est-elle réalisable? OUI? ou NON?
2. Montrer que l'on peut modéliser la situation par un graphe connexe non orienté. Lequel?
3. Ce graphe est-il Eulérien?
=== "4. Question"
    Concluez : Est-il possible de partir d'un point de départ, de parcourir tous les ponts une et une seule fois exactement, et de revenir au point de départ ? 
    Justifiez : Si oui, détaillez la solution. Si non, expliquez pourquoi.
=== "Corrigé"
    <div style="display:inline-block; width:20em;margin-right:2em;">
    <img src="../img/pontsKonigsbergModelisation.svg">
    </div>
    <div style="display:inline-block;margin-right:1em;">
    <center>
    ```dot
    graph G {
      layout=neato
      rankdir=LR
      size="1.5";
      bgcolor=none;
      node [shape=circle, penwidth=3.0, fontsize=26];
      edge [penwidth=3.0]
      1 -- 4
      1 -- {2, 3}
      1 -- 2
      1 -- 3
      2 -- 4
      3 -- 4
      {rank=same;2;3;}
    }
    ```
    <figcaption>
    Modélisation sous forme de graphe
    </figcaption>
    </center>
    </div>  
    On a $deg(2)=3$ (ou $deg(3)=3$, ou $deg(4)=3$), donc en particulier est impair.
    Le <bred>Théorème d'Euler</bred> prouve que le graphe n'est PAS Eulérien, donc il n'existe pas de cycle eulérien :
    On <bred>NE PEUT PAS</bred> partir d'un point de départ, parcourir tous les ponts une et une seule fois exactement, et revenir au point de départ.
<clear></clear>
=== "5. Question"
    Une caractérisation un peu moins forte existe pour les parcours eulériens :
    !!! thm "d'Euler pour les parcours Eulériens"
        Un graphe connexe admet un **parcours Eulérien**  
        $\Leftrightarrow$ ses sommets sont tous de degré pair **sauf au plus deux**.
  
    Est-il possible de partir d'un point de départ, de parcourir tous les ponts une et une seule fois exactement, quitte à finir sur un autre point d'arrivée (différent de celui de départ) ?
    Justifiez : Si oui, détaillez la solution. Si non, expliquez pourquoi.
=== "Corrigé"
    Il existe $3$ sommets ayant un degré impair ($s=2$, $s=3$ et $s=4$), donc cela en fait 1 de trop!
    Le <bred>Théorème d'Euler</bred> prouve que le graphe n'est PAS Eulérien, donc il n'existe pas de cycle eulérien :
    On <bred>NE PEUT PAS</bred> partir d'un point de départ, parcourir tous les ponts une et une seule fois exactement, même en arrivant à un point d'arrivée autre que celui de départ...

## Graphes Hamiltoniens

### Chaînes/Chemins Hamiltonien(ne)s

!!! def
    <div style="float:right;">
    <center>
    [<img src="../img/hamilton.jpg" style="width:10em;"/><figcaption>
    William Rowan Hamilton (1805-1865) :gb:
    </figcaption>](https://fr.wikipedia.org/wiki/William_Rowan_Hamilton)
    </center>
    </div>
    Une <bred>Chaîne/Chemin Hamiltonien(ne)</bred> ou <bred>Parcours Hamiltonien</bred> est une chaîne/chemin qui **passe une fois et une seule par chaque <blue>sommet</blue> du graphe**

!!! pte
    Une chaîne/chemin hamiltonien(ne) est une chaîne/chemin élémentaire maximal(e) (passant par tous les sommets du graphe)

<env>**ATTENTION**</env> Selon les graphes envisagés: Ce n'est PAS toujours possible!

!!! ex
    Pour chacun des graphes suivants, trouver ***lorsque c'est possible*** une **chaîne hamiltonien** / un **parcours hamiltonien**, càd tracer le graphe sans lever le stylo, en ne passant qu'une et une seule fois par chaque sommet :

    <div id="conteneur">
    <div>
    <center>
    ```dot
    graph G {
      layout=circo
      size="1.5";
      node [shape=circle, penwidth=3.0, fontsize=38];
      edge [penwidth=3.0]
      1 -- 2
      1 -- 3
      1 -- 4
      {rank=same;2;3;}
    }
    ```
    <figcaption>(Hand) Spinner</figcaption>
    </center>
    </div>
    <div>
    <center>
    ```dot
    graph G {
      layout=circo
      size="1.5";
      node [shape=circle, penwidth=3.0, fontsize=38];
      edge [penwidth=3.0]
      1 -- 2
      1 -- 3
      1 -- 4
      2 -- 3
      {rank=same;2;3;}
    }
    ```
    <figcaption>Spatule</figcaption>
    </center>
    </div>
    <div>
    <center>
    ```dot
    graph G {
      layout=circo
      size="1.5";
      node [shape=circle, penwidth=3.0, fontsize=38];
      edge [penwidth=3.0]
      1 -- 2
      1 -- 3
      1 -- 4
      2 -- 3
      4 -- 3
      {rank=same;2;3;}
    }
    ```
    <figcaption>Origami</figcaption>
    </center>
    </div>
    <div>
    <center>
    ```dot
    graph G {
      layout=circo
      size="2.5";
      node [shape=circle, penwidth=3.0, fontsize=38];
      edge [penwidth=3.0]
      1 -- 2
      2 -- 3
      3 -- 4
      4 -- 1
      4 -- 5
      {rank=same;2;3;}
    }
    ```
    <figcaption>
    Cerf-volant
    </figcaption>
    </center>
    </div>
    <div>
    <center>
    ```dot
    graph G {
      layout=twopi
      root=5
      size="1.5";
      node [shape=circle, penwidth=2.0, fontsize=26];
      edge [penwidth=2.0]
      1 -- 2
      3 -- 4
      1 -- 5
      3 -- 5
      2 -- 5
      4 -- 5
      {rank=same;1;2;}
      {rank=same;3;4;}
    }
    ```
    <figcaption>
    Papillon
    </figcaption>
    </center>
    </div>
    <div>
    <center>
    ```dot
    graph G {
      layout=twopi
      root=5
      size="1.5";
      node [shape=circle, penwidth=2.0, fontsize=26];
      edge [penwidth=2.0]
      1 -- 2 -- 3 -- 4 -- 1
      1 -- 5
      3 -- 5
      2 -- 5
      4 -- 5
      {rank=same;1;2;}
      {rank=same;3;4;}
    }
    ```
    <figcaption>
    Enveloppe<br/>Pliée
    </figcaption>
    </center>
    </div>
    <div>
    === "Figure"
        <center>
        ```dot
        graph G {
          layout=neato
          size="2.2";
          node [shape=circle, penwidth=2.0, fontsize=24];
          edge [penwidth=2.0]
          1, 2, 4, 5, 6
          1 -- 2
          1 -- 4
          2 -- 3
          3 -- 4
          1 -- 5
          2 -- 5
          3 -- 5
          4 -- 5
          6 -- 1
          6 -- 2
          {rank=same;1;2;}
          {rank=same;3;4;}
        }
        ```
        <figcaption>
        Enveloppe<br/>Ouverte
        </figcaption>
        </center>
    === "Corrigé"
        <rep>
        <img src="../img/enveloppe.gif">
        </rep>
    </div>
    <div>
    <center>
    ```dot
    graph G {
      //layout=circo
      rankdir=LR
      layout=fdp
      root=4
      size="2.5";
      bgcolor=none;
      node [shape=circle, penwidth=3.0, fontsize=38];
      edge [penwidth=3.0]
      1 -- 7
      3 -- 9
      1 -- 2 -- 3
      4 -- 5 -- 6
      7 -- 8 -- 9
      1 -- 4 -- 7
      3 -- 6 -- 9
    }
    ```
    <figcaption>
    Ruche
    </figcaption>
    </center>
    </div>
    </div>

### Cycles/Circuits Hamiltoniens & Graphes Hamiltoniens

!!! def "Cycle/Circuit Hamiltonien ou Tournée Hamiltonienne"
    Un <bred>Cycle/Circuit Hamiltonien</bred>, ou <bred>Tournée Hamiltonienne</bred>, est un cycle qui parcoure toutes les sommets du graphe une et une seule fois (en revenant au sommet initial)

!! def "Graphe Hamiltonien"
    Un <bred>graphe Hamiltonien</bred> est un graphe contenant un cycle Hamiltonien
    Remarque: On appelle **graphe semi-hamiltonien** les graphes admettant :
        * un parcours hamiltonien, 
        * mais pas de cycle hamiltonien

!!! pte
    Un **cycle Hamiltonien** est un cycle élémentaire maximal (qui passe par tous les sommets)
    Un **cycle Hamiltonien** est une chaîne/chemin hamiltonien qui est un cycle.

!!! ex
    Dans l'exercice précédent, quels sont les graphes Hamiltoniens?

### Caractérisation des Graphes Hamiltoniens

Il n'existe pas, comme pour les graphes Eulériens, de caractérisation (condition nécessaire et suffisante) connue pour les graphes Hamiltoniens : c'est un problème difficile. On connaît néanmoins plusieurs résultats donnant des conditions suffisantes pour que le graphe soit Hamiltonien : 

!!! thm "Théorème de Ore (1960) (CULTURE: HORS-PROGRAMME)"
    Soit $G$ un graphe simple à $n$ sommets, avec $n\ge3$  
    Si $deg(u)+deg(v)\ge n$ pour toute paire $(u, v)$ de sommets **non adjacents**,  
    Alors le graphe $G$ est Hamiltonien

et une conséquence (un cas particulier)

!!! thm "**Théorème de Dirac (1952):** (CULTURE: HORS-PROGRAMME)"
    Soit $G$ un graphe simple à $n$ sommets, avec $n\ge3$  
    Si $\displaystyle deg(s)\ge \frac n2$ pour chaque sommet $s$,  
    Alors le graphe $G$ est Hamiltonien

### Histoire & Jeux : Le problème du Cavalier aux Échecs

<div style="float:right;width:30%;">

<img src="../img/cavalierHamiltonien.gif">

<center>

<figcaption>

Jeu du cavalier, Plateau d'Echecs

</figcaption>

</center>

</div>

Au $IX$e siècle, le poète indien Rudrata a été l'un des tous premiers, dans son [traité poétique (Kavyalankara)](https://archive.org/details/Kavyalankara_of_Rudrata_with_Hindi_Vyakhya_-_Satyadev_Choudhary_1965/page/n1/mode/2up), à parler des échecs en Inde..., et le premier à énoncer le **problème des cavaliers** aux échecs, ou **cavalier d'Euler**, qui peut se voir comme un problème de recherche de cycles/chemins hamiltoniens sur un échiquier à $64$ cases : 
Un cavalier doit parcourir chaque case une et seule fois en n'utilisant que les mouvements de cette pièce, (et revenir sur la même case, si on souhaite un cycle). Une première solution à ce problème sera proposée par le joueur et théoricien arabe des échecs [Al-Adli](https://fr.wikipedia.org/wiki/Al-Adli) dès $840$ ap. J.C.

<clear></clear>

### Histoire & Jeux : Le Dodécaèdre de Hamilton

<div style="float:right;margin-right:1em;width:30%;">

<img src="../img/dodecaedre3D.png">

<center>

<figcaption>

Icosian Game, Dodécaèdre $$3D$$

</figcaption>

</center>

<img src="../img/icosianGame.svg">

<center>

<figcaption>

Icosian Game, aplatissement du dodécahèdre en $$2D$$

</figcaption>

</center>

</div>

Plus tardivement, en $1857$, ***William Rowan Hamilton*** inventa un un jeu mathématique, appelé ***icosian game***, dont le but est de parcourir tous les sommets d'un dodécaèdre en $3D$, une et une seule fois, en partant et en revenant au même sommet.

<clear></clear>

<newpage></newpage>

