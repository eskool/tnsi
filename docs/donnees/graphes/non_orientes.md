# TNSI : Vocabulaire des Graphes Non Orientés

## Arêtes

!!! def "Arêtes"
    * Les liens entre sommets s'appellent des <bred>Arêtes</bred> :fr: ou <bred>Edges</bred> :gb:
    * On notera $E=\{\text{couples }\{s,v\} \text{ non ordonnés, pour tous les sommets} \,\, s \,\, \text{et} \,\, v \,\, \text{du Graphe}\}$ l'ensemble des arêtes d'un graphe
    * Notation : l'arête $\{s,v\}$ sera quelquefois notée `s -- v`
    * L'arête `s -- v` est dite <bred>incidente</bred> au sommet `s` (et à `v`)

<env>**Remarque**</env>

* La notation mathématique pour un ensemble $\{s,v\}$ de deux éléments, **ne tient pas compte de l'ordre** de `s` et `v`, ainsi par ex.  $\{1,2\} = \{2,1\}$
* en Python, cela peut être implémenté avec le type `set`

<def data="Boucles">

Une <bred>boucle</bred> :fr: ou <bred>loop</bred> :gb: est une **arête d'un sommet vers lui-même**

</def>

## Sommets Adjacents

!!! def "Sommets Adjacents"
    Deux sommets `s` et `v` sont <bred>adjacents</bred> s'il existe une arête de `s` vers `v` (donc aussi de `v` vers `s`)

## Degré d'un Sommet

!!! def "Degré d'un Sommet"
    Le <bred>Degré d'un Sommet</bred> `s`, noté $deg(s)$ ou $d(s)$, est le **nombre d'arêtes incidentes à ce Sommet** (=nombre d'arêtes Sortantes / Entrantes).  
    :warning: <env>**ATTENTION</env>:warning: Une boucle compte pour $2$**

!!! exp "Degré dans un graphe Simple"
    <div style="float:right;margin-right:1em;">
    <center>
    ```dot
    graph G {
      size="2";
      bgcolor=none;
      node [shape=circle, fontsize=30, style=bold];
      edge [style=bold, fontsize=30]
      4 -- 0
      4 -- 1
      0 -- 2
      0 -- 3
      1 -- 2
      1 -- 3
    }
    ```
    <figcaption>Un Graphe Simple</figcaption>
    </center>
    </div>

    * Le degré de $0$ vaut $3$: $deg(0) = d(0) = 3$
    * Le degré de $1$ vaut $3$: $deg(1) = d(1) = 3$
    * Le degré de $2$ vaut $2$: $deg(2) = d(2) = 2$
    * Le degré de $3$ vaut $2$: $deg(3) = d(3) = 2$
    * Le degré de $4$ vaut $2$: $deg(4) = d(4) = 2$

!!! exp "Degré dans un Multigraphe"
    <div style="float:right;margin-right:1em;">
    <center>
    ```dot
    graph G {
      size="2";
      bgcolor=none;
      node [shape=circle, fontsize=30, style=bold];
      edge [style=bold, fontsize=30]
      4 -- 0
      4 -- 0
      4 -- 1
      0 -- 2
      0 -- 2
      0 -- 2
      2 -- 2
      0 -- 3
      1 -- 2
      1 -- 3
      1 -- 1
    }
    ```
    <figcaption>Un Graphe Non Pondéré<br/>Non Orienté<br/>Multigraphe (avec boucles)</figcaption>
    </center>
    </div>

    * Le degré de $0$ vaut $6$: $d(0) = 6$
    * Le degré de $1$ vaut $5$: $d(1) = 5$
    * Le degré de $2$ vaut $6$: $d(2) = 6$
    * Le degré de $3$ vaut $2$: $d(3) = 2$
    * Le degré de $4$ vaut $3$: $d(4) = 3$

!!! ex "Degré Maximal d'un Graphe Simple"
    On considère un graphe simple $G$, d'ordre $n$.
    Quel est le degré maximal d'un sommet $s$?

!!! ex "Lemme des Poignées de main"
    1. Montrer que la somme des degrés de tous les sommets est un nombre pair : Plus précisément, La somme des degrés de tous les sommets vaut le double du nombre total d'arêtes : 
    <center>
    <enc>$\displaystyle \sum_{s\in V} d(s) = 2a = 2\times |E|$</enc>
    </center>
      Autrement dit: si $n=ordre(G)=|V|$ désigne son nombre de sommets:
    <center>
    <enc>$\displaystyle \sum_{i=1}^{n} d(s_i)=2a$</enc>
    </center>
    2. Le nombre de sommets de degré impair est pair

## Chaînes

### Définition

!!! def "Chaînes"
    <div style="float:right;">
    <center>
    ```dot
    graph G {
      size="2";
      bgcolor=none;
      node [shape=circle, fontsize=30];
      0, 2, 1, 4 [color=red, style=bold]
      4 -- 0
      4 -- 1 [color=red, style=bold]
      0 -- 2 [color=red, style=bold]
      0 -- 3
      1 -- 2 [color=red, style=bold]
      1 -- 3
    }
    ```
    <figcaption>Une Chaîne de<br/>Longueur 3</figcaption>
    </center>
    </div>

    Une <bred>Chaîne de `s` à `v`</bred> est une suite (finie) d'arêtes consécutives, reliant `s` à `v`,  sur un graphe non orienté.  

!!! exp "Chaîne de `0` à `4`"
    <div id="conteneur">
    <div style="float:left;flex:2;">
    ```dot
    graph G {
      rankdir=LR
      size="2.5";
      node [shape=circle, fontsize=38, penwidth=2]
      edge [penwidth=2]
      0 -- 2 -- 1 -- 4
    }
    ```
    </div>
    <div style="float:left; flex:4;margin-left:1em;">noté <code>0-2-1-4</code> est une chaîne de longueur <code>3</code></div>
    </div>

### Longueur d'une Chaîne

!!! def "Longueur d'une Chaîne"
    La <bred>Longueur d'une chaîne</bred> est :

    * le **nombre** d'<bblue>arêtes</bblue> de la chaîne, pour un **graphe non pondéré**, ou bien,
    * la **somme des poids** des <bblue>arêtes</bblue>, pour un **graphe pondéré**

<env>**Remarque**</env> Il n'y a (en général) évidemment pas unicité de la chaîne d'un même point de départ à un même point d'arrivée :

* $0 - 3 - 1 - 4$ est une autre chaîne de $0$ à $4$, de longueur $3$
* $0 - 4$ est une autre chaîne de $0$ à $4$, de longueur $1$ : c'est une arête !

### Distance entre deux sommets

!!! def "Distance"
    La <bred>distance</bred> entre deux sommets est la plus courte longueur entre eux (parmi toutes les chaînes possibles entre eux)

### Chaînes Simples, Chaînes Élémentaires

!!! def "Chaînes Simples"
    Une <bred>Chaîne simple</bred> est une chaîne :

      * **ne passant pas deux fois par la même <blue>arête</blue>**
      * $\Leftrightarrow$ dont **toutes les arêtes sont distinctes**

!!! def "Chaînes Élémentaires"
    Une <bred>Chaîne élémentaire</bred> est une chaîne :

    * **ne passant pas deux fois par le même <blue>sommet</blue>**
    * $\Leftrightarrow$ dont **tous les sommets sont distincts** 

    :warning: sauf peut-être les deux extrémités:warning: 

## Cycles

### Définition

!!! def "Cycles"
    <div style="float:right;">
    <center>
    ```dot
    graph G {
      size="2";
      node [shape=circle, fontsize=38];
      0, 3, 1, 2 [color=blue, style=bold]
      4 -- 0
      4 -- 1
      0 -- 2 [color=blue, style=bold]
      0 -- 3 [color=blue, style=bold]
      1 -- 2 [color=blue, style=bold]
      1 -- 3 [color=blue, style=bold]
    }
    ```
    <figcaption>Un autre Cycle</figcaption>
    </center>
    </div>
    <div style="float:right;margin-right:1.5em;">
    <center>
    ```dot
    graph G {
      size="2";
      node [shape=circle, fontsize=38];
      0, 1, 3, 4 [color=blue, style=bold]
      4 -- 0 [color=blue, style=bold]
      4 -- 1 [color=blue, style=bold]
      0 -- 2
      0 -- 3 [color=blue, style=bold]
      1 -- 2
      1 -- 3 [color=blue, style=bold]
    }
    ```
    <figcaption>Un Cycle</figcaption>
    </center>
    </div>

    Un <bred>Cycle</bred> est une chaîne menant d'un sommet à lui-même.  
    
!!! exp
    $0-3-1-2-0$ est un *cycle* de longueur $4$

!!! def "Graphe Acyclique"
    Un graphe **sans aucun cycle** est un <bred>graphe acyclique</bred>

### Longueur d'un Cycle

!!! def "Longueur d'un Cycle"
    La <bred>Longueur du cycle</bred> est la longueur de sa chaîne sous-jacente.  

### Cycles Simples, Cycles Élémentaires

!!! def "Cycles Simples"
    Un <bred>Cycle simple</bred> est un cycle :

    * dont la chaîne est simple
    * $\Leftrightarrow$ **ne passant pas deux fois par la même <blue>arête</blue>**, càd
    * $\Leftrightarrow$ dont **toutes les arêtes sont distinctes**

!!! def "Cycles Élémentaires"
    Un <bred>Cycle élémentaire</bred> est un cycle :

    * dont la chaîne est élémentaire
    * $\Leftrightarrow$ **ne passant pas deux fois par le même <blue>sommet</blue>**, càd
    * $\Leftrightarrow$ dont **tous les sommets sont distincts** 

    :warning: sauf les deux extrémités qui sont égales...:warning: 

!!! pte
    * Un cycle élémentaire ne contient pas d'autre cycle
    * Un cycle élémentaire n'admet que des sommets d'ordre $2$

## Graphe Connexe

!!! def "Connexité"
    <div style="float:right;">
    <center>
    ```dot
    graph G {
      size="2"
      rankdir=LR
      node [fontsize=30, shape=circle];
      0 -- 1 -- 4 -- 0
      2 -- 3
    }
    ```
    <figcaption>Graphe NON Connexe,<br/>et un Cycle 0 - 1 - 4 - 0</figcaption>
    </center>
    </div>
    <div style="float:right;margin-right:1em;">
    <center>
    ```dot
    graph G {
      size="2";
      node [shape=circle, fontsize=30];
      0, 2, 1, 4 [color=red, style=bold]
      4 -- 0
      4 -- 1 [color=red, style=bold]
      0 -- 2 [color=red, style=bold]
      0 -- 3
      1 -- 2 [color=red, style=bold]
      1 -- 3
    }
    ```
    <figcaption>Graphe Connexe</figcaption>
    </center>
    </div>
    Un graphe non orienté ***en un seul morceau***, est appelé un <bred>graphe connexe</bred>

!!! pte
    Un graphe non orienté est connexe $\Leftrightarrow$ Il existe toujours une chaîne entre deux sommets quelconques du graphe

<env>**Exemple**</env> le **graphe d'internet** est connexe, mais pas le graphe  du **réseau routier mondial** n'est PAS connexe

### Composantes Connexes d'un Graphe

!!! def "Composante Connexe"
    Lorsque le graphe non orienté est composé **de plusieurs morceaux**, chaque morceau est appelé une <bred>composante connexe</bred> du graphe.

!!! pte
    * Chaque composante connexe est connexe
    * Deux sommets quelconques d'une composante connexe sont toujours connectés (reliables par une chaîne)
    * Toute Chaîne est connexe

## Illustrations

<div id="conteneur">

<div>

<center>

```dot
digraph G {
    size="8"
    node [shape=circle, fontsize=38]
    edge [arrowhead=none]
    subgraph cluster0 {
      0 -> 1
      2, 0, 3, 8, 11, 10 [color=blue, style=bold]
      0 -> 2 [color=blue, style=bold]
      0 -> 3 [color=blue, style=bold]
      2 -> 3
      8 -> 9
      2 -> 9
      8 -> 3 [color=blue, style=bold]
      0 -> 4 -> 10
      10 -> 11 [color=blue, style=bold]
      8 -> 11 [color=blue, style=bold]
      subgraph cluster00 {
        1, 5, 6 [color=red, style=bold]
        1 -> 5 [color=red, style=bold]
        1 -> 6 [color=red, style=bold]
        5 -> 6 [color=red, style=bold]
        label="cycle"
        fontcolor=red
        fontsize=38
        {rank = same; 5; 6;}
      }
    6 -> 7
    2 -> 8
    3 -> 9
    label = "Composante Connexe";
    fontsize=38
    {rank = same; 2; 3;}
    {rank = same; 8; 9;}
    }

    subgraph cluster1 {
    15 -> 16 -> 17
    16 -> 18 -> 19
    label="Composante Connexe"
    fontsize=38
    }

    subgraph cluster2 {
    20 -> 21 -> 22
    20 -> 23
    21 -> 23
    23 -> 24
    label="Composante Connexe"
    fontsize=38
    }
}
```

<figcaption>
Graphe G Non Orienté <bpurple>Non Pondéré</bpurple> Non Connexe<br/>
et <bblue>Une Chaîne de 2 à 10 de longueur 5</bblue>
</figcaption>
<br/>

</center>

</div>

</div>

<div id="conteneur">

<div>

<center>

```dot
digraph G {
  size="8"
  node [shape=circle, fontsize=38]
  edge [arrowhead=none, fontsize=38]
  subgraph cluster0 {
    0 -> 1 [label="4"]
    2, 0, 3, 8, 11, 10 [color=blue, fontcolor=blue, style=bold]
    0 -> 2 [label="5", color=blue, fontcolor=blue, style=bold]
    0 -> 3 [label="0.8"color=blue, fontcolor=blue, style=bold]
    2 -> 3 [label="6"]
    8 -> 9 [label="0.4"]
    2 -> 9 [label="8"]
    8 -> 3 [label="2", color=blue, fontcolor=blue, style=bold]
    0 -> 4 [label="7"]
    4 -> 10 [label="6"]
    10 -> 11 [label="5", color=blue, fontcolor=blue, style=bold]
    8 -> 11 [label="4", color=blue, fontcolor=blue, style=bold]
    subgraph cluster00 {
      1, 5, 6 [color=red, style=bold]
      1 -> 5 [label="2", color=red, fontcolor=red, style=bold]
      1 -> 6 [label="3", color=red, fontcolor=red, style=bold]
      5 -> 6 [label="9", color=red, fontcolor=red, style=bold]
      label="cycle"
      fontcolor=red
      fontsize=38
      {rank = same; 5; 6;}
    }
    6 -> 7 [label="4"]
    2 -> 8 [label="3"]
    3 -> 9 [label="6"]
    label = "Composante Connexe";
    fontsize=38
    {rank = same; 2; 3;}
    {rank = same; 8; 9;}
    }

    subgraph cluster1 {
    15 -> 16 [label="1"]
    16 -> 17 [label="4"]
    16 -> 18 [label="2"]
    18 -> 19 [label="3"]
    label="Composante Connexe"
    fontsize=38
    }

    subgraph cluster2 {
    20 -> 21 [label="3"]
    21 -> 22  [label="4"]
    20 -> 23 [label="5"]
    21 -> 23 [label="6"]
    23 -> 24 [label="7"]
    label="Composante Connexe"
    fontsize=38
    }
}
```

<figcaption>
Graphe G Non Orienté <bpurple>Pondéré</bpurple> Non Connexe<br/>
et <bblue>Une Chaîne de 2 à 10, de poids total =5+0,8+2+4+5=16,8</bblue>
</figcaption>

</center>
</div>

</div>

## Arbres


!!! def "Arbres"
    <div id="conteneur">
    <div>
    Un <bred>Arbre</bred> est un **graphe connexe acyclique**
    </div>
    <div>
    <center>
    ```dot
    graph G {
      size="2.5"
      node [fontsize=38, shape=circle]
      0 -- {1, 2, 3}
      1 -- {4, 5}
      2 -- {6, 7, 8}
    }
    ```
    <figcaption>un Arbre</figcaption>
    </center>
    </div>
    </div>
