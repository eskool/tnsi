class Graphe:
  def __init__(self, mat=[]):
    self.m = mat

  ## MATRICE D'ADJACENCE

  def afficher_matrice(self)->None:
    print("Matrice d'Adjacence, M =")
    for s in range(len(self.m)):
      print(self.m[s])

  def admet_mat_symetrique(self)->bool:
    n = self.ordre()
    for s in range(n):
      for v in range(s+1):
        if self.m[s][v] != self.m[v][s]:
          return False
    return True

  def check_symetrie_matrice(self):
    if not self.admet_mat_symetrique():
        raise "Erreur : la matrice d'adjacence d'un Graphe Non Orienté DOIT être Symétrique"

  ## SOMMETS

  def ordre(self)->int:
    """renvoie l'ordre du graphe : son nombre de sommets"""
    return len(self.m)

  def sommets(self)->list:
    """Renvoie la liste des n sommets : de 0 à (n-1)"""
    return [s for s in range(len(self.m))]

  def successeurs(self, s:int)->list:
    """Renvoie la liste de tous les voisins de s"""
    return [v for v in range(len(self.m)) if self.m[s][v] != 0]

  def predecesseurs(self, s:int)->list:
    """Renvoie la liste de tous les voisins de s"""
    return [v for v in range(len(self.m)) if self.m[v][s] != 0]

  def ajouter_sommet(self):
    """Il faut ajouter une dernière ligne de 0, 
    et aussi une dernière colonne de 0"""
    n = self.ordre()
    mat = [[] for s in range(n)]
    for s in range(n):
      mat[s] = self.m[s]
      mat[s].append(0)
    mat.append([0 for s in range(n+1)])
    self.m = mat

  def supprimer_sommet(self, s:int)->None:
    """Supprime le sommet 's'"""
    print("Supprime le sommet", s)
    n = self.ordre()
    assert s in range(n), "Sommet inexistant"
    mat = [[] for s in range(n)]
    # Enlève la colonne 'v=s' et définit comme vide la ligne 's'
    for x in range(n):
      if x != s:
        mat[x] = self.m[x][:s]+self.m[x][s+1:]
    mat = mat[:s]+mat[s+1:] # supprime la ligne i qui est une liste vide
    self.m = mat

  ## ARCS ORIENTÉS

  def admet_arc(self, s:int, v:int)->bool:
    return self.m[s][v] == 1

  def arcs(self)->list:
    """Renvoie la liste des n sommets : de 0 à (n-1)"""
    n = len(self.m)
    return [(s,v) for s in range(n) for v in range(n) if self.m[s][v] == 1]

  def ajouter_arc(self, s:int, v:int):
    self.m[s][v] = 1

  def supprimer_arc(self, s:int, v:int):
    self.m[s][v] = 0

  def poids(self, s:int, v:int)->int:
    return self.m[s][v]

  def degre_plus(self, s:int)->int:
    n = self.ordre()
    return len([v for v in self.m[s] if v == 1])

  def degre_moins(self, s:int)->int:
    n = self.ordre()
    return len([v for v in [self.m[x][s] for x in range(n)] if v == 1])

  def degre(self, s:int)->int:
    n = self.ordre()
    return self.degre_plus(s) + self.degre_moins(s)

  ########################################################################
  ## RENDU DOT ET IMAGE
  ########################################################################

  def toDot(self, filename="test.dot"):
      """Crée un fichier graph .dot, par défaut test.dot
      """
      assert ".dot" == filename[-4:], "filename must contain '.dot' extension"
      # print("dot Filename = ",filename)
      with open(filename,"w") as fichier:
          fichier.write("digraph G {\n node [shape=circle]\n")
          for s in self.sommets():
              sommet = str(s)+";\n"
              fichier.write(sommet)
          for (s,v) in self.arcs():
            arc = str(s)+" -> "+str(v)+" [style=bold];\n"
            fichier.write(arc)
          fichier.write("}")

  def extraire_nom_sans_extension(self, filename="test.dot"):
    """Enlève l'extension '.dot' au nom de fichier"""
    assert type(filename) is str, "Erreur : le nom de fichier DOIT être une chaîne STR"
    assert ".dot" == filename[-4:], "Erreur : le nom de fichier DOIT finir par une extension '.dot'"
    return filename[:-4]

  def renderPicture(self, filename="test.dot"):
      name = self.extraire_nom_sans_extension(filename)
      # print("name = ",name)
      import os
      os.system(f"dot -Tpng {name}.dot -o {name}.png")
      from PyQt5 import QtGui, QtWidgets
      app = QtWidgets.QApplication([])
      window = QtWidgets.QWidget()
      HLayout = QtWidgets.QHBoxLayout()
      pixmap = QtGui.QPixmap(name)
      label = QtWidgets.QLabel()
      label.setPixmap(pixmap)
      HLayout.addWidget(label)
      window.setLayout(HLayout)
      window.setWindowTitle("Mon Beau Graphe!")
      window.show()
      app.exec_()

if __name__ == "__main__":
  m1 = [[0,0,1,0],
        [1,0,1,1],
        [0,0,0,1],
        [1,0,0,0]]
  g1 = Graphe(m1)
  g1.afficher_matrice()
  g1.ajouter_arc(2,1)
  g1.supprimer_arc(1,3)
  g1.afficher_matrice()
  for s in range(g1.ordre()):
    print("degré ",s,"=", g1.degre(s))

  # m2 = [[0,0,1,0,0],
  #       [1,0,1,1,1],
  #       [0,0,0,1,0],
  #       [0,0,0,0,0],
  #       [0,0,1,1,0]]
  # g2 = Graphe(m2)

  # g1.ajouter_sommet()
  # g1.afficher_matrice()
  # g1.afficher_matrice()
  # g1.supprimer_arc(2,4)
  # g1.afficher_matrice()
  # rep = g1.admet_mat_symetrique()
  # print("Admet Matrice Symétrique = ", rep)

  # g1.toDot()
  # g1.renderPicture("test.dot")


