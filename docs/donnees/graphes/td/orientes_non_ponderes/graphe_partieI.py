class Graphe:
  def __init__(self, mat=[], liste_successeurs=[], dico_successeurs={}):
    if mat != [] and liste_successeurs == [] and dico_successeurs == {}:
      print("INIT avec Matrice d'Adjacence")
      self.m = mat
      # self.check_symetrie_matrice()
      self.mat_vers_liste_successeurs()
      self.mat_vers_dico_successeurs()
    elif mat == [] and liste_successeurs != [] and dico_successeurs == {}:
      print("INIT avec Liste de Successeurs")
      self.liste_successeurs = liste_successeurs
      self.liste_vers_matrice()
      # self.check_symetrie_matrice()
      self.mat_vers_dico_successeurs()
    elif mat == [] and liste_successeurs == [] and dico_successeurs != {}:
      print("INIT avec le Dico des Successeurs")
      self.dico_successeurs = dico_successeurs
      self.dico_vers_matrice()
      # self.check_symetrie_matrice()
      self.mat_vers_liste_successeurs()
    else:
      print("ATTENTION, on ne peut instancier un graphe avec plusieurs paramètres.")
      print("Choisissez l'un des trois paramètres suivants:")
      print("* ou bien : g= Graphe(mat=[...])")
      print("* ou bien : g= Graphe(liste_successeurs=[...])")
      print("* ou bien : g= Graphe(dico_successeurs=[...])")

  ## MATRICE D'ADJACENCE

  def afficher_matrice(self)->None:
    print("Matrice d'Adjacence, M =")
    for s in range(len(self.m)):
      print(self.m[s])

  def admet_mat_symetrique(self)->bool:
    n = self.ordre()
    for s in range(n):
      for v in range(s+1):
        if self.m[s][v] != self.m[v][s]:
          return False
    return True

  def check_symetrie_matrice(self):
    if not self.admet_mat_symetrique():
        raise "Erreur : la matrice d'adjacence d'un Graphe Non Orienté DOIT être Symétrique"

  ## SOMMETS

  def ordre(self)->int:
    """renvoie l'ordre du graphe : son nombre de sommets"""
    return len(self.m)

  def sommets(self)->list:
    """Renvoie la liste des n sommets : de 0 à (n-1)"""
    return [s for s in range(len(self.m))]

  def successeurs(self, s:int)->list:
    """Renvoie la liste de tous les voisins de s"""
    return [v for v in range(len(self.m)) if self.m[s][v] != 0]

  def predecesseurs(self, s:int)->list:
    """Renvoie la liste de tous les voisins de s"""
    return [v for v in range(len(self.m)) if self.m[v][s] != 0]

  def ajouter_sommet(self):
    """Il faut ajouter une dernière ligne de 0, 
    et aussi une dernière colonne de 0"""
    n = self.ordre()
    mat = [[] for s in range(n)]
    for s in range(n):
      mat[s] = self.m[s]
      mat[s].append(0)
    mat.append([0 for s in range(n+1)])
    self.m = mat

  def supprimer_sommet(self, s:int)->None:
    """Supprime le sommet 's'"""
    print("Supprime le sommet", s)
    n = self.ordre()
    assert s in range(n), "Sommet inexistant"
    mat = [[] for s in range(n)]
    # Enlève la colonne 'v=s' et définit comme vide la ligne 's'
    for x in range(n):
      if x != s:
        mat[x] = self.m[x][:s]+self.m[x][s+1:]
    mat = mat[:s]+mat[s+1:] # supprime la ligne s qui est une liste vide
    self.m = mat

  ## ARCS ORIENTÉS

  def admet_arc(self, s:int, v:int)->bool:
    return self.m[s][v] == 1

  def arcs(self)->list:
    """Renvoie la liste des n sommets : de 0 à (n-1)"""
    n = len(self.m)
    return [(s,v) for s in range(n) for v in range(n) if self.m[s][v] == 1]

  def ajouter_arc(self, s:int, v:int):
    self.m[s][v] = 1

  def supprimer_arc(self, s:int, v:int):
    self.m[s][v] = 0

  def poids(self, s:int, v:int)->int:
    return self.m[s][v]

  def degre_plus(self, s:int)->int:
    n = self.ordre()
    return len([v for v in self.m[s] if v == 1])

  def degre_moins(self, s:int)->int:
    n = self.ordre()
    return len([v for v in [self.m[x][s] for x in range(n)] if v == 1])

  def degre(self, s:int)->int:
    n = self.ordre()
    return self.degre_plus(s) + self.degre_moins(s)

  ## LISTE DE SUCCESSEURS

  def afficher_liste_successeurs(self)->None:
    print("Liste Successeurs, L =")
    n = self.ordre()
    for s in range(n):
      print("Successeurs de",s," : ",self.liste_successeurs[s])

  def liste_vers_matrice(self)->list:
    liste_successeurs = self.liste_successeurs
    n = len(liste_successeurs)
    self.m = [[0]*n for s in range(n)]
    for s in range(n):
      for v in liste_successeurs[s]:
        self.m[s][v] = 1

  def mat_vers_liste_successeurs(self):
    n=self.ordre()
    lv = [[] for s in range(n)]
    for s in range(n):
      for v in range(n):
        if self.m[s][v] == 1:
          lv[s].append(v)
    self.liste_successeurs = lv

  ## DICTIONNAIRE DE SUCCESSEURS

  def afficher_dico_successeurs(self):
    print("AFFICHE DICO SUCCESSEURS")
    n = len(self.dico_successeurs)
    for s in range(n):
      print(s," : ",self.dico_successeurs[s])

  def dico_vers_matrice(self):
    n = len(self.dico_successeurs)
    self.m = [[0]*n for s in range(n)]
    for s in range(n):
      for v in range(n):
        if v in self.dico_successeurs[s]:
          self.m[s][v] = 1

  def mat_vers_dico_successeurs(self):
    n = len(self.m)
    self.dico_successeurs = {s : [] for s in range(n)}
    for s in range(n):
      for v in range(n):
        if self.m[s][v] == 1:
          self.dico_successeurs[s].append(v)

  ########################################################################
  ## RENDU DOT ET IMAGE
  ########################################################################

  def toDot(self, filename="test.dot"):
      """Crée un fichier graph .dot, par défaut test.dot
      """
      assert ".dot" == filename[-4:], "filename must contain '.dot' extension"
      # print("dot Filename = ",filename)
      with open(filename,"w") as fichier:
          fichier.write("digraph G {\n node [shape=circle]\n")
          for s in self.sommets():
              sommet = str(s)+";\n"
              fichier.write(sommet)
          for (s,v) in self.arcs():
            arc = str(s)+" -> "+str(v)+" [style=bold];\n"
            fichier.write(arc)
          fichier.write("}")

  def extraire_nom_sans_extension(self, filename="test.dot"):
    """Enlève l'extension '.dot' au nom de fichier"""
    assert type(filename) is str, "Erreur : le nom de fichier DOIT être une chaîne STR"
    assert ".dot" == filename[-4:], "Erreur : le nom de fichier DOIT finir par une extension '.dot'"
    return filename[:-4]

  def renderPicture(self, filename="test.dot"):
      name = self.extraire_nom_sans_extension(filename)
      # print("name = ",name)
      import os
      os.system(f"dot -Tpng {name}.dot -o {name}.png")
      from PyQt5 import QtGui, QtWidgets
      app = QtWidgets.QApplication([])
      window = QtWidgets.QWidget()
      HLayout = QtWidgets.QHBoxLayout()
      pixmap = QtGui.QPixmap(name)
      label = QtWidgets.QLabel()
      label.setPixmap(pixmap)
      HLayout.addWidget(label)
      window.setLayout(HLayout)
      window.setWindowTitle("Mon Beau Graphe!")
      window.show()
      app.exec_()

if __name__ == "__main__":
  m1 = [[0,0,1,0],
        [1,0,1,1],
        [0,0,0,1],
        [1,0,0,0]]
  g1 = Graphe(m1)
  g1.afficher_matrice()
  g1.afficher_liste_successeurs()
  g1.afficher_dico_successeurs()

  # ls1 = [[2],
  #        [0,2,3],
  #        [3],
  #        [0]]
  # g1 = Graphe(liste_successeurs=ls1)
  # g1.afficher_matrice()
  # g1.afficher_liste_successeurs()
  # g1.afficher_dico_successeurs()

  # d1 = { 0 : [2],
  #        1 : [0,2,3],
  #        2 : [3],
  #        3 : [0]
  #       }
  # g1 = Graphe(dico_successeurs=d1)
  # g1.afficher_dico_successeurs()
  # g1.afficher_matrice()
  # g1.afficher_liste_successeurs()
  # g1.afficher_dico_successeurs()
  # print(g1.arcs())
  # print("degre de",1,"=",g1.degre(1))

  # g2.afficher_liste_successeurs()
  # m2 = [[0,0,1,0,0],
  #       [1,0,1,1,1],
  #       [0,0,0,1,0],
  #       [0,0,0,0,0],
  #       [0,0,1,1,0]]
  # # g2 = Graphe(m2)

  # g1.toDot()
  # g1.renderPicture("test.dot")


