class Graphe:
  def __init__(self, mat=[], liste_voisins=[], dico_voisins={}):
    if mat != [] and liste_voisins == [] and dico_voisins == {}:
      print("INIT avec Matrice d'Adjacence")
      self.m = mat
      self.check_symetrie_matrice()
      self.mat_vers_liste_voisins()
      self.mat_vers_dico_voisins()
    elif mat == [] and liste_voisins != [] and dico_voisins == {}:
      print("INIT avec Liste de Voisins")
      self.liste_voisins = liste_voisins
      self.liste_vers_matrice()
      self.check_symetrie_matrice()
      self.mat_vers_dico_voisins()
    elif mat == [] and liste_voisins == [] and dico_voisins != {}:
      print("INIT avec le Dico des Voisins")
      self.dico_voisins = dico_voisins
      self.dico_vers_matrice()
      self.check_symetrie_matrice()
      self.mat_vers_liste_voisins()
    else:
      print("ATTENTION, on ne peut instancier un graphe avec plusieurs paramètres.")
      print("Choisissez l'un des trois paramètres suivants:")
      print("* ou bien : g= Graphe(mat=[...])")
      print("* ou bien : g= Graphe(liste_voisins=[...])")
      print("* ou bien : g= Graphe(dico_voisins=[...])")

  ## MATRICE D'ADJACENCE

  def afficher_matrice(self)->None:
    print("Matrice d'Adjacence, M =")
    n = len(self.m)
    for s in range(n):
      print(self.m[s])

  def admet_mat_symetrique(self)->bool:
    n = self.ordre()
    for s in range(n):
      for v in range(s+1):
        if self.m[s][v] != self.m[v][s]:
          return False
    return True

  def check_symetrie_matrice(self):
    if not self.admet_mat_symetrique():
        raise "Erreur : la matrice d'adjacence d'un Graphe Non Orienté DOIT être Symétrique"

  ## SOMMETS

  def ordre(self)->int:
    """renvoie l'ordre du graphe : son nombre de sommets"""
    return len(self.m)

  def sommets(self)->list:
    """Renvoie la liste des n sommets : de 0 à (n-1)"""
    return [s for s in range(len(self.m))]

  def voisins(self, s:int)->list:
    """Renvoie la liste de tous les voisins de s"""
    return [v for v in range(len(self.m)) if self.m[s][v] == 1]

  def ajouter_sommet(self):
    """Il faut ajouter une dernière ligne de 0, 
    et aussi une dernière colonne de 0"""
    n = self.ordre()
    mat = [[] for s in range(n)]
    for s in range(n):
      mat[s] = self.m[s]
      mat[s].append(0)
    mat.append([0 for s in range(n+1)])
    self.m = mat
    self.mat_vers_liste_voisins()
    self.mat_vers_dico_voisins()

  def supprimer_sommet(self, s:int)->None:
    """Supprime le sommet 's'"""
    print("Supprime le sommet", s)
    n = self.ordre()
    assert s in range(n), "Sommet inexistant"
    mat = [[] for s in range(n)]
    # Enlève la colonne 'v=s' et définit comme vide la ligne 's'
    for x in range(n):
      if x != s:
        mat[x] = self.m[x][:s]+self.m[x][s+1:]
    mat = mat[:s]+mat[s+1:] # supprime la ligne s qui est une liste vide
    self.m = mat
    self.mat_vers_liste_voisins()
    self.mat_vers_dico_voisins()

  ## ARÊTES

  def admet_arete(self, s:int, v:int)->bool:
    return self.m[s][v] == 1

  def aretes(self)->list:
    """Renvoie la liste des n sommets : de 0 à (n-1)"""
    n = len(self.m)
    return [(s,v) for s in range(n) for v in range(n) if self.m[s][v] == 1 and s<v]

  def ajouter_arete(self, s:int, v:int):
    self.m[s][v] = 1
    self.m[v][s] = 1
    self.mat_vers_liste_voisins()
    self.mat_vers_dico_voisins()

  def supprimer_arete(self, s:int, v:int):
    self.m[s][v] = 0
    self.m[v][s] = 0
    self.mat_vers_liste_voisins()
    self.mat_vers_dico_voisins()

  def poids(self, s:int, v:int)->int:
    """Renvoie le poids de l'arête s -- v"""
    return self.m[s][v]

  def degre(self, s:int)->int:
    return len([v for v in self.m[s] if v == 1])

  ## LISTE DE VOISINS

  def afficher_liste_voisins(self)->None:
    print("Liste Voisins, L =")
    n = self.ordre()
    for s in range(n):
      print("Voisins de",s," : ",self.liste_voisins[s])

  def liste_vers_matrice(self)->list:
    liste_voisins = self.liste_voisins
    n = len(liste_voisins)
    self.m = [[0]*n for s in range(n)]
    for s in range(n):
      for v in liste_voisins[s]:
        self.m[s][v] = 1
        # self.m[v][s] = 1

  def mat_vers_liste_voisins(self):
    n=self.ordre()
    lv = [[] for s in range(n)]
    for s in range(n):
      for v in range(n):
        if self.m[s][v] == 1:
          lv[s].append(v)
    self.liste_voisins = lv

  ## DICTIONNAIRE DE VOISINS

  def afficher_dico_voisins(self):
    print("AFFICHE DICO VOISINS")
    n = len(self.dico_voisins)
    for s in range(n):
      print(s," : ",self.dico_voisins[s])

  def dico_vers_matrice(self):
    n = len(self.dico_voisins)
    self.m = [[0]*n for s in range(n)]
    for s in range(n):
      for v in range(n):
        if v in self.dico_voisins[s]:
          self.m[s][v] = 1

  def mat_vers_dico_voisins(self):
    n = len(self.m)
    self.dico_voisins = {s : [] for s in range(n)}
    for s in range(n):
      for v in range(n):
        if self.m[s][v] == 1:
          self.dico_voisins[s].append(v)

  ########################################################################
  ## RENDU DOT ET IMAGE
  ########################################################################

  def toDot(self, filename="test.dot"):
      """Crée un fichier graph .dot, par défaut test.dot
      """
      assert ".dot" == filename[-4:], "filename must contain '.dot' extension"
      # print("dot Filename = ",filename)
      with open(filename,"w") as fichier:
          fichier.write("graph G {\n node [shape=circle]\n")
          for s in self.sommets():
              sommet = str(s)+";\n"
              fichier.write(sommet)
          for (s,v) in self.aretes():
            arete = str(s)+" -- "+str(v)+" [style=bold];\n"
            fichier.write(arete)
          fichier.write("}")

  def extraire_nom_sans_extension(self, filename="test.dot"):
    """Enlève l'extension '.dot' au nom de fichier"""
    assert type(filename) is str, "Erreur : le nom de fichier DOIT être une chaîne STR"
    assert ".dot" == filename[-4:], "Erreur : le nom de fichier DOIT finir par une extension '.dot'"
    return filename[:-4]

  def renderPicture(self, filename="test.dot"):
      name = self.extraire_nom_sans_extension(filename)
      # print("name = ",name)
      import os
      os.system(f"dot -Tpng {name}.dot -o {name}.png")
      from PyQt5 import QtGui, QtWidgets
      app = QtWidgets.QApplication([])
      window = QtWidgets.QWidget()
      HLayout = QtWidgets.QHBoxLayout()
      pixmap = QtGui.QPixmap(name)
      label = QtWidgets.QLabel()
      label.setPixmap(pixmap)
      HLayout.addWidget(label)
      window.setLayout(HLayout)
      window.setWindowTitle("Mon Beau Graphe!")
      window.show()
      app.exec_()

  def show(self, name="test"):
    self.toDot(f"{name}.dot")
    self.renderPicture(f"{name}.dot")

if __name__ == "__main__":
  m1 = [[0,1,1,0],
        [1,0,1,1],
        [1,1,0,1],
        [0,1,1,0]]
  g1 = Graphe(m1)
  g1.ajouter_sommet()
  g1.afficher_matrice()
  g1.afficher_liste_voisins()
  g1.show()
  # g1.afficher_dico_voisins()

  # lv1 = [[1,2],
  #        [0,2,3],
  #        [0,1,3],
  #        [1,2]]
  # g1 = Graphe(liste_voisins=lv1)
  # g1.afficher_matrice()
  # g1.afficher_liste_voisins()
  # g1.afficher_dico_voisins()

  # d1 = { 0 : [1,2],
  #        1 : [0,2,3],
  #        2 : [0,1,3],
  #        3 : [1,2]
  #       }
  # g1 = Graphe(dico_voisins=d1)
  # g1.afficher_matrice()
  # g1.afficher_liste_voisins()
  # g1.afficher_dico_voisins()
  # print(g1.aretes())

  # g1.toDot("test1.dot")
  # g1.renderPicture("test1.dot")
  # g1.toDot()
  # g1.renderPicture("test2.dot")

  # g2.afficher_liste_voisins()
  # m2 = [[0,1,1,0,0],
  #       [1,0,1,1,1],
  #       [1,1,0,1,0],
  #       [0,1,1,0,1],
  #       [0,1,0,1,0]]
  # g2 = Graphe(m2)
  # g2.show()



