# TNSI : Introduction aux Graphes

<html style="width:100%;">
<titre>TNSI : cours Graphes</titre>

## Introduction : Exemples d'Utilisation des Graphes

<div style="float:left; width:50%;">

<center>

```dot
digraph reseauSocial {
  size="3";
  rankdir=LR;
  //splines=false;
  node [fontsize=20];

  Padme -> Anakin
  Anakin -> Padme
  Anakin -> Rey
  Rey -> Yoda
  Yoda -> Anakin
  Anakin -> Yoda
  Leia -> Yoda
  Leia -> Luke
  Luke -> Leia
  Yoda -> Luke [constraint=false]
  Anakin -> Leia
  Han -> Anakin

  }
```

<figcaption><b>Arbre 1 : </b>Réseau Social, de type Follower</figcaption>

</center>

</div>

<div style="float:left; width:50%;">

<figure>

<center>

<img src="img/reseauSocial.png"/>
<figcaption><b>Arbre 2 : </b>Réseau Social, de type Amitié. <br/>Sur ce graphe, on constate qu'il y a une grosse composante connexe, et quelques petites</figcaption>

</center>

</figure>

</div>

<clear></clear>

<div style="float:left; width:50%;">

<center>

```dot
graph reseauRoutier {
  size="3.5";
  //rankdir=LR
  splines=polyline;
  node [fontsize=20, shape=box];
  Lille -- Brest [label="758"]
  Lille -- Paris [label="226"]
  Lille -- Strasbourg [label="525"]
  Brest -- Paris [label="593"]
  Paris -- Bordeaux [label="584"]
  Paris -- Lyon [label="466"]
  Paris -- Strasbourg [label="491"]
  Brest -- Bordeaux [label="644"]
  Lyon -- Strasbourg [label="487"]
  Lyon -- Toulouse [label="538"]
  Lyon -- Bordeaux [label="556"]
  Lyon -- Marseille [label="314"]
  Bordeaux -- Toulouse [label="244"]
  Toulouse -- Marseille [label="404"]
  Marseille -- Nice [label="199", constraint=false]

  {rank=same; Brest, Paris, Strasbourg}
  {rank=same; Lyon}
  {rank=same; Bordeaux, Toulouse, Marseille, Nice}
  }
```

<figcaption><b>Arbre 3 : </b>Réseau Routier à double Sens (<b>Non Orienté</b>)</figcaption>

</center>

</div>

<div style="float:left; width:50%;">

<figure>

<center>

<img src="img/reseauRoutier.png"/>
<figcaption><b>Arbre 4 : </b>Réseau Routier.<br/>Sur ce graphe, les <b>sommets</b> sont des lieux sur la carte, et les <b>arcs</b> sont des routes les reliant. On pourrait imaginer : 

<ul style="float:left;">
  <li>Des ruelles toutes à double sens : Graphe <b>Non Orienté</b>. Ou bien,</li>
  <li>Ajouter des flèches, pour indiquer les sens uniques. Dans ce cas, le graphe serait <b>Orienté</b></li>
</ul>

</figcaption>

</center>

</figure>

</div>

<clear></clear>

<div style="float:left; width:50%;">

<figure>

<center>

<img src="img/labyrinthe.png"/>
<figcaption><b>Arbre 5 : </b>Labyrinthe.<br/>Voici un labyrinthe qui n'est pas parfait, il y a un îlot. On ne peut pas en sortir en utilisant la technique de la main gauche (ou droite) si on part d'un point entre D et F.

À droite, on voit une modélisation sous forme de graphe, dont on pourra faire des parcours de graphe (en largeur ou en profondeur) pour résoudre des problèmes.</figcaption>

</center>

</figure>

</div>

<div style="float:left; width:50%;">

<figure>

<center>

<img src="img/jeu.jpg"/>


<figcaption><b>Arbre 6 : </b>Théorie des Jeux.<br/>Dans un jeu où on ne retrouve pas deux fois la même position dans la même partie, le graphe des positions est sans cycle, donc un arbre.

Dans cet exemple (Algorithme <em>minimax</em>) :<br/>

1️⃣ On commence par les feuilles, qui modélisent des parties finies, en convenant de coder le résultat final :

<ul style="text-align:left;">

<li><code>+1</code> pour une victoire de X, et</li> 
<li><code>−1</code> pour une défaite de X,</li>
<li><code>0</code> pour un nul.</li>

</ul>

2️⃣ À chaque étage, on remonte à l'étage précédent, en raisonnant comme suit :

<ul style="text-align:left;">
<li>si c'est le tour de X, on prend le maximum des possibilités.</li>
<li>Si c'est le tour de O, on prend le minimum des possibilités.</li>

</ul>

3️⃣ On peut prouver qu'il y a match nul pour deux joueurs qui ont la meilleure stratégie. 

</figcaption>

</center>

</figure>

</div>

<clear></clear>

<newpage></newpage>

<div style="float:left; width:50%;">

<figure>

<center>

<img src="img/grapheReseauInfo.png"/>
<figcaption><b>Arbre 7 : </b>Réseau Internet</figcaption>

</center>

</figure>

</div>

<div style="float:left; width:50%;">

<figure>

<center>

<img src="img/dependances.jpg"/>
<figcaption><b>Arbre 8 : </b>Graphe des Dépendances.<br/>Ce graphe indique les modules requis pour accéder à d'autres dans un cursus universitaire en informatique, ou bien les lectures conseillées de chapitres avant d'aborder d'autres.
<br/>Ce genre de graphe est utilisé, par exemple, en compilation, où un source ne peut être compilé que si ses dépendances sont satisfaites... Il faut évidemment vérifier l'absence de cycle !</figcaption>

</center>

</figure>

</div>

