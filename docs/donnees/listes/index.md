# TNSI : La Structure de Données *Listes*

## Le Type Abstrait de Données <bred>*Liste*</bred>

### Définition

!!! def "Listes"
    Une <bred>Liste</bred> :fr: ou <bred>List</bred> :gb:, est un **Type Abstrait de Données (TAD)** représentant un **conteneur/collection fini** d'<bred>éléments</bred>/objets/données :
    
    * Tous accessibles (contrairement aux **Piles** et aux **Files**), mais de plusieurs manières, selon l'implémentation choisie :

        * soit par **Accès Séquentiel** (l'un derrière l'autre, avec la notion de ***successeur*** ou ***suivant***), au minimum.
        * soit par **Accès Direct** / **Accès Aléatoire** :fr:, ou **Random Access** :gb:, c'est -à-dire **via un indice**, lorsqu'il y en a un
        
    L'**ordre** des éléments a donc une importance

### Primitives

!!! pte
    Une Liste dispose usuellement des opérations/**primitives** suivantes :

    * Primitives **de Base** :
        * `creerListeVide()` / `createEmptyList` : crée une Liste Vide
        * `estVide()` / `isEmpty()` : renvoie si la liste est vide, ou pas
        * `tete()` / `head()` / `shift()` : récupére l'élément en **tête** de la liste, en le supprimant de la liste
        * `ajouteTete()` / `unShift()` : ajoute un élément en **tête** de la liste
        * `retirerQueue()` / `dequeue()` / `pop()` / `tail()` : récupére l'élément situé en **queue** de la Liste
        * `ajouteQueue()` / `enqueue()` / `push()` : ajoute un élément en **queue** de la Liste
        * `longueur()` / `length()` : renvoie la longueur de la liste
    * Primitives **auxilliaires**, fréquemment rencontrées
        * `inserer(L,e,i)`/`Create(L,e,i)` : insère un élément `e` dans la Liste `L` à la position `i`
        * `supprimer(L,i)`/`Delete(L,i)` : supprime l'élément en position `i` de la liste `L`
        * `modifier(L,e,i)`/`Update(L,e,i)` : modifie l'élément à la position `i` dans la Liste `L`, en le nouvel `e`

```dot
digraph liste {
  rankdir=LR
  node [shape=Mrecord, color=grey, fontcolor=red, fontsize=18, width=0.6, fixedsize=true];
  headerValues [label="<header>"];
  ranksep=0

  node [shape=record, color=blue, fontcolor=black, fontsize=14, width=2, fixedsize=true];
  values [label="{<header> 4 | 8 | <footer> 7 }"];

  node [shape=Mrecord, color=grey, fontcolor=red, fontsize=18, width=0.6, fixedsize=true];
  footerValues [label="<footer>"];

  headerValues:header:se -> values:header:sw [label="unShift / ajouteQueue"];
  values:header:nw -> headerValues:header [label="Shift / Queue / Tail"];

  footerValues:footer:sw -> values:footer:c [label="Push / ajouteTête"];
  values:footer:ne -> footerValues:footer [label="Pop / Tête / Head"];
}
```

<env><b>Remarques</b></env>

* La définition du type abstrait `liste` **n'est PAS normalisée**, notamment pour les opérations/**primitives** des listes : il peut donc y avoir quelques variations sur ce sujet. Néanmoins, le principe reste fondamentalement le même.
* Généralement, les éléments seront de même type de données (dans les langages à **typage statique**: Java/C++,), mais la structure de données liste ne l'impose pas (donc certains langages à **typage dynamique** : Python, .. autorisent des éléments de types différents)
* **Positions Tête /Queue** : Selons les contextes et les auteurs, la **Tête** et la **Queue** peuvent être inversées.
* La modélisation **linéaire** d'une Liste se prête bien à la représentation d'éléments qui arrivent et/ou partent dans le temps. Dans ce contexte :
    * **La Tête représente souvent le Premier élément arrivé**
    * **La Queue représente souvent le Dernier élément arrivé** (comme quand on on arrive dernier pour accéder à une ressource, et qu'on se place à la queue)
* La structure abstraite de liste est à la base de nombreuses autres structures, notamment les **Piles** et les **Files**, les **Files de priorité**, etc..

## La Structure de Données *Listes*

### Implémentations des Listes

!!! pte
    Les Listes sont également des **Structures de Données** lorsque les primitives sont implémentées. 

Il existe plusieurs implémentations d'une Liste :

### Avec un Tableau Dynamique

On peut représenter une liste de $n$ éléments par un Tableau Dynamique $L[0..(n-1)]$, lui même implémenté par la donnée de :

* **la taille $n$ du tableau** (un entier) 
On ne détaillera pas ici son stockage en mémoire, mais on pourrait par exemple imaginer, très simplement, de placer l'entier $n$ en début du tableau dynamique, et décaler les $n$ cellules vers la droite (ce qui donnerait un tableau à $n+1$ valeurs)
* **$n$ Cellules Contiguës**, de `L[0]` à `L[n-1]`, contenant chacune l'**adresse mémoire** de l'élément contenu dans la cellule. Chaque Cellule étant de plus référencée par son indice.

```dot
digraph {
  node [shape=plaintext, fontcolor=red, fontsize=18, width=1.5, fixedsize=true];
  headerValeurs [label="Valeurs :"]
  headerIndices [label="Indices :"]
  headerValeurs -> headerIndices [color=none];
  ranksep=0

  node [shape=record, fontcolor=black, fontsize=14, width=4, fixedsize=true];
  valeurs [label="4 | 8 | 3 | 5 | 7 |  |  "];
  indices [label="0 | 1 | 2 | 3 | 4 | 5 | 6", color=none];

  { rank=same; headerValeurs; valeurs }
  { rank=same; headerIndices; indices }
}
```

* Pour **lire** l’élément d’indice $i$, il suffit donc d’aller chercher son adresse dans la $(i+1)$-ème cellule et pour cela d’ajouter $i$ à l’adresse de début de tableau. **Cela se fait en temps constant $O(1)$**. 
* Pour **modifier** l'élément d'indice $i$, il suffit de faire pointer la cellule correspondante sur un autre objet ce qui se fait aussi **en temps constant  $O(1)$**. 
* Pour **insérer** un élément à l'indice $i$ à la liste, il faut modifier $n$ (ce dont nous ne parlerons pas) et ajouter une cellule au tableau (à la fin):
    * Si la cellule qui suit directement les $n$ cellules consécutives est "*libre*", pas de problème, on y met le pointeur qui désigne l’élément ajouté, ce qui se fait **en $O(1)$**.
    * Si ce n’est pas le cas, on doit alors déplacer tout le tableau vers une zone libre plus grande, où on pourra le prolonger, ce qui nécessite la recopie intégrale des $n$ cellules et se fait **en $O(n)$**.  
    <span style="font-size:0.85em;"><env>Remarque</env> Pour que cela arrive le moins souvent possible, on utilise d’habitude le procédé suivant : au départ, on prévoit en général plus de place que les $n$ cases initiales, au cas où on voudrait prolonger la liste. Puis lors de chaque déplacement forcé, on s’arrange pour doubler la place mémoire disponible et la réserver. Cela réduit le nombre de déplacements et le coût d’ajout de $p$ éléments est alors en $O(max(n,p))$. Par exemple, si on part d’une liste vide et on ajoute un à un,  $n$ éléments à la liste, cela fait un nombre maximal de recopies de cases $\displaystyle \approx n+\frac n2+ \frac n4··· = O(n)$, et la complexité del’opération globale a le même ordre de grandeur que s’il n’y avait pas eu de déplacement ! En pratique, on **lisse** le coût des déplacements et on fait l’approximation que les ajouts se font en temps constant.
* pour **supprimer** un élément de la liste, on doit modifier $n$, et déplacer d'un cran vers la gauche tous les éléments situés à droite de l'élément à supprimer. Cela se fait **en $O(n)$** dans le pire des cas.

<env><b>CAS DE PYTHON : ATTENTION, FAUX AMI</b></env> 
Le langage Python est un langage de Programation, mais il en existe plusieurs implémentations :

* L'Implémentation par défaut de **Python** est [<bred>CPython</bred>](https://fr.wikipedia.org/wiki/CPython "CPython est l'implémentation de référence du langage Python. C'est un interpréteur de bytecode écrit en langage C. C'est un logiciel libre. ") qui est un interpréteur de [<bred>bytecode</bred>](https://fr.wikipedia.org/wiki/Bytecode "Un bytecode est un code intermédiaire entre les instructions machines et le code source, qui n'est pas directement exécutable. Le bytecode (également appelé code portable ou p-code) peut être créé à la volée et résider en mémoire (compilation à la volée, ou JIT Just-In-Time en anglais) ou bien résider dans un fichier, généralement binaire qui représente le programme, tout comme un fichier de code objet produit par un compilateur. ") écrit en langage C.

* Pour info, il existe néanmoins d'autres implémentations de Python:
    * [IronPython](https://ironpython.net/) (pour dévelopement avec/pour le framework .NET), 
    * [Jython](https://www.jython.org/) (écrit en Java, exécutables dans la Jython Virtual Machine), 
    * [PyPy](https://www.pypy.org/) (écrit en Python, qui peut donc s'auto-interpréter!)

En pratique: Cela veut dire qu'une même notion du langage Python peut être implémentée différemment, selon l'implémentation de Python choisie.
**A Noter :**

* Les types de données Python peuvent prêter à confusion : Le type de données `list` est implémenté (**en CPython**) par un **Tableau Dynamique**, et **non PAS par une Structure de Données Liste (Chaînée)**, comme le nom `list` pourrait le laisser croire trompeusement.
* Il est donc normal que les résultats trouvés ci-dessus soient en cohérence totale avec la Documentation Officielle de Python concernant la Complexité pour les `list`, que l'on trouvera sur [cette page de la Documentation officielle](https://wiki.python.org/moin/TimeComplexity). 
<span style="font-size:0.85em;">Cf. le [ticket Stackoverflow suivant](https://stackoverrun.com/fr/q/9104972) qui justifie pourquoi, sur la Documentation Officielle, la complexité de l'ajout est **toujours en $O(1)$** dans le *pire des cas **amortis*** (mais PAS en $O(1)$).</span>

### Avec une Liste Chaînée (Simple)

!!! def "Listes Chaînées"
    Une <bred>Liste Chaînée (Simple)</bred>, ou <bred>Liste Simplement Chaînée</bred>, ou tout simplement une <bred>Liste</bred>, est une implémentation d'une liste dans laquelle :

    * Tous les éléments sont appelés des <bred>Cellules</bred> :fr:, ou <bred>Maillons</bred> :fr:, ou <bred>Noeuds</bred> :fr:, ou <bred>Nodes</bred> :gb:, et se composent de deux choses :
        * une **valeur**
        * un **lien**, en fait un **pointeur/référence**, vers l'élément **suivant** / **successeur**
    * On suppose également connue la référence vers la **première Cellule** appelée <bred>Tête</bred> (et seulement cette référence)

Une **Liste Chaînée** est une autre implémentation possible pour une liste. on dit que les Cellules sont **chaînées (entre elles)**

```dot
digraph listeChainee {
        rankdir=LR;
        node [shape=record];
        start [label="{ <data> }", width=0.5]
        a [label="{ <data> 1  | <ref>  }"]
        b [label="{ <data> 2 | <ref>  }"];
        c [label="{ <data> 3 | <ref>  }"];
        d [label="{ <data> 4 | <ref> &#x27c2; }"];
        start:data -> a:data [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false, arrowsize=1.2];
        a:ref:c -> b:data [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false, arrowsize=1.2];
        b:ref:c -> c:data [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
        c:ref:c -> d      [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
}
```

Le symbole <enc>**&#x27c2;**</enc> $\,$ dont le nom officiel est "***taquet vers le haut***", représente la fin de la liste (`queue`), lorsque le *suivant* n'existe pas.
En Python, on utilise souvent `None` pour implémenter ce symbole.

Le code Python-OOP suivant implémente une Classe `Cellule` d'une Liste Chaînée :

```python
class Cellule:
"""Une Cellule d'une Liste Chaînée"""
  def __init__(self,v,s):
    self.valeur = v
    self.suivante = s
```

On peut alors créer une instance de la liste `|•|1|2|3|4|⟂|` précédente grâce à :

```python
uneListe = Cellule(1, Cellule(2, Cellule(3, Cellule(4,None))))
```

Plus précisément, on a créé ici 4 objets de la Classe Cellule, que l'on peut visualiser comme suit :

```dot
digraph listeChainee {
  rankdir=LR
  node [shape=record];
  a [label="{ <ref>  }", width=0.5];
  b [label="<data> Cellule | <ref> 1 | <next>"];
  c [label="<data> Cellule | <ref> 2 | <next>"];
  d [label="<data> Cellule | <ref> 3 | <next>"];
  e [label="<data> Cellule | <ref> 4 | None"];

  a:ref:c -> b:data [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false, arrowsize=1.2];
  b:next:c -> c:data [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  c:next:c -> d:data      [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  d:next:c -> e:data      [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];

}
```

Par la suite on s'autorisera le dessin simplifié suivant :

```dot
digraph listeChainee {
  rankdir=LR
  node [shape=record];
  a [label="{ <ref>  }", width=0.4];
  b [label="{<data> 1 | <next>}"];
  c [label="{<data> 2 | <next>}"];
  d [label="{<data> 3 | <next>}"];
  e [label="{<data> 4 | <next> &#x27c2;}"];

  a:ref:c -> b:data [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false, arrowsize=1.2];
  b:next:c -> c:data [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  c:next:c -> d:data      [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  d:next:c -> e:data      [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];

}
```

### Listes Chaînées Doubles

!!! def "Listes Chaînées Doubles"
    **Définition :** Une <bred>Liste Chaînée Double (comprendre *à Double Extrémité*)</bred> :fr:, ou <bred>Double Ended Queue</bred> :gb:, est une Liste Chaînée contenant des références :

    * vers le **premier** élément (la `tête`) de la liste (de même que les Listes Chaînées Simples)
    * **et aussi**, vers le **dernier** élément (la `queue`) de la liste

### Autres Variantes

<env>**Listes Cycliques**</env> Le tout dernier élément est relié au tout premier

```dot
digraph listeCyclique {
  rankdir=LR
  //splines=polyline;
  node [shape=record];
  a [label="{<data> 1 | <next>}"];
  b [label="{<data> 2 | <next>}"];
  c [label="{<data> 3 | <next> }"];

  a:next:sc -> b:data [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  b:next:c -> c:data      [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  c:next:sc -> a:data:s      [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
}
```

<env>**Listes Doublement Chaînées**</env> Chaque élément est relié au précédent et au suivant

```dot
digraph listeChaineeDouble {
  rankdir=LR
  node [shape=record];
  a [label="{<prev> | <data> 1 | <next>}"];
  b [label="{<prev> | <data> 2 | <next>}"];
  c [label="{<prev> | <data> 3 | <next> }"];

  a:next:nc -> b:prev:nw [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  b:prev:sc -> a:next:s [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  b:next:nc -> c:prev:nw [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
  c:prev:sc -> b:next:s [arrowhead=vee, arrowtail=dot, dir=both, tailclip=false];
}
```

<env>**Listes Cycliques Doublement Chaînées**</env> Combine les deux variantes prédentes