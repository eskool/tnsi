# Implémentation avec Liste Chaînée

class Cellule:
    def __init__(self, valeur=None, suivante=None):
        self.valeur = valeur
        self.suivante = suivante

    def get_valeur(self) -> int:
        return self.valeur

    def get_suivante(self):
        return self.suivante

    def set_suivante(self, cellule) -> None: # 'cellule' est définie comme la suivante de self (de la cellule courante)
        if cellule is None or (type(cellule.valeur) == type(self.valeur)):
            self.suivante = cellule
        else:
            raise TypeError

    def get_queue(self):
        if self.get_suivante() is not None:
            return self.get_suivante().get_queue()
        else:
            return self

    # def concatener(self,c2):
    #     """concatène self avec l2 dans cet ordre"""
    #     # if self is None:
    #     #     return c2
    #     # elif self.get_suivante() is None:
    #     #     return Cellule(self.valeur, c2)
    #     # else:
    #     #     return Cellule(self.valeur, self.get_suivante().concatener(c2))
    #     return Cellule(self.valeur, c2)


    def __getitem__(self, i):  # en O(n)
        if self is None:
            return None
        if i == 0:
            return self.get_valeur()
        else:
            return self.get_suivante().__getitem__(i-1)

    def __repr__(self) -> str:
        # Pour implémentation de Liste Chaînée par classe 'Cellule'
        cell = self
        mylist = f"|•|{cell.valeur}|"
        while cell.get_suivante() is not None: # en O(n)
            cell = cell.get_suivante()
            mylist += str(cell.get_valeur())+"|"
        mylist += "⟂|"
        return mylist
        # Pour implémentation de Liste Chaînée par classe 'Liste'
        # return f"{self.valeur}"

    def __len__(self):
        if self.get_suivante() is None:
            return 1
        else:
            return 1 + len(self.get_suivante())

# l = Cellule(3, Cellule(8, Cellule(5, Cellule(7, None))))
# l = Cellule(3, None)
# l = Cellule(3, Cellule(8, None))
# print("l = ", l)
# print("l.get_valeur() = ", l.get_valeur())
# print("l[0] = ", l[0])
# print("l[1] = ", l[1])
# print("l[2] = ", l[2])
# print("longueur = ", len(l))
# print("queue = ", l.get_queue())

class Liste:
    def __init__(self) -> None:
        self.tete = None
        self.queue = None
    
    def est_vide(self) -> bool:
        return self.tete is None

    def ajouter_tete(self, valeur) -> None: # en O(1)
        """Ajoute 'valeur' en tête de liste"""
        print("Ajoute Tête", valeur)
        if self.tete is None: # Liste Vide
            self.tete = self.queue = Cellule(valeur)
        else: # Liste NON Vide
            self.tete = Cellule(valeur, self.tete)
            if (self.tete is self.queue): # 1ère fois qu'on ajoute un élément en tête
                self.tete.set_suivante(self.queue)

    def get_tete(self) -> None: # en O(1)
        return self.tete

    def retirer_tete(self) -> None: # en O(1)
        """retire le premier élément de la liste"""
        print("Retirer Tête")
        if self.tete is not None: # pour ne pas renvoyer d'erreurs si self. est vide (queue)
            self.tete = self.tete.get_suivante()

    def get_queue(self): # en O(1)
        return self.queue

    def retirer_queue(self): # en O(n)
        """renvoie la liste courante, privée du dernier élément (queue)"""
        print("Retirer Queue")
        cell = self.tete
        if (cell is None) or (cell.get_suivante() is None): # en O(1)
            self.tete = None
            self.queue = None
        else: # en O(n)
            while (cell is not None) and (cell.get_suivante().get_suivante() is not None):
                cell = cell.get_suivante()
            self.queue = cell
            cell.set_suivante(None)

    def ajouter_queue(self, valeur): # en O(1)
        """ajoute 'valeur' en queue de liste"""
        print("Ajoute Queue", valeur)
        cell = Cellule(valeur)
        self.queue.set_suivante(cell)
        self.queue = cell

    def insert(self, i, valeur):
        cell = self.tete
        assert type(i) is int, "L'indice doit être entier"
        assert i>=0, "L'indice d'insertion doit être >= 0"  # comme le insert de Python :accepte lorsque i> len(liste)
        print("Insert", valeur,"en position ",i)
        if (self.tete is None):    # insère un élément en position 0 dans une Liste Vide
            self.ajouter_tete(valeur)       # self.queue est gérée dans ajouter_tete()
        else: # Liste NON Vide
            premiereCelluleSuivante = self[i]
            # self[i-1] désigne donc la cellule derrière laquelle il faut insérer la 'valeur'
            if i==0: # insère en position 0. La queue n'est pas modifiée
                cell = Cellule(valeur)
                cell.set_suivante(self[0])
                self.tete = cell
            else: # insertion AILLEURS qu'en position 0 (au milieu ou à droite) dans Liste NON Vide
                if self[i-1] is None: # insertion en position dépassée à droite
                    # recalcule le bon 'i' en cas de dépassement
                    # pour faire comme le type 'list' de Python ...
                    i = len(self)
                if self[i-1].get_suivante() is None: # insertion en Dernière position
                    # cell = Cellule(valeur)
                    # self[i-1].set_suivante(cell)
                    self.ajouter_queue(valeur)
                else: # insertion PAS en dernière position
                    cell = Cellule(valeur)
                    self[i-1].set_suivante(cell)
                    self[i].set_suivante(premiereCelluleSuivante)

    def renverser(self):
        nouvelle = Liste()
        cell = self.tete
        while cell is not None:
            nouvelle.ajouter_tete(cell.get_valeur())
            cell = cell.get_suivante()
        return nouvelle

    def concatener(self,l2):
        """concatène self avec l2 dans cet ordre"""
        if self.est_vide():
            return l2
        else:
            self.queue.set_suivante(l2.tete)
            self.queue = l2.queue
            return self

    def __getitem__(self, i): # en O(n)
        if self.tete is None: # Liste vide
            return None
        else:
            j = 0
            cell = self.tete
            if (cell is None) or (cell.get_suivante() is None): # Liste à 1 seul élément
                return self.tete if i==0 else None
            else: # liste à plus d'un élément
                while (cell.get_suivante() is not None) and (cell.get_suivante().get_valeur() != self.tete.get_valeur()) :
                    cell = cell.get_suivante()
                    j += 1
                    if j==i:
                        return cell
                # On peut retourner None, ou lever une erreur : Choix Perso
                return self.tete if i==0 else None
                # if i==0:
                #     return self.tete
                # else:
                #     raise IndexError("list index "+str(i)+" out of range")

    def __repr__(self) -> str:  # en O(n)
        if self.tete is None:
            return "|•|⟂|"
        else:
            return f"{self.tete}"

    def __len__(self) -> int: # en O(n)
        cell = self.tete
        if cell is None:
            return 0
        else:
            length = 0
            while cell.get_suivante() is not None:
                length += 1
                cell = cell.get_suivante()
            return length + 1



l1 = Liste()
# l1.ajouter_tete(3)
# l1.ajouter_tete(4)
# l1.ajouter_tete(5)
print("l1 =", l1)

l2 = l1.renverser()
print("l2 =", l2)
# l2.ajouter_tete(6)
# l2.ajouter_tete(7)
# l2.ajouter_tete(8)

# l3  = l1.concatener(l2)
# print("l3 =", l3)

# l = Liste()
# print("l =",l)
# l.insert(0,15)
# print("l =",l)

# l.ajouter_tete(5)
# print("l =",l)
# l.ajouter_tete(6)
# print("l =",l)
# l.insert(0,20)
# print("l =",l)
# l.ajouter_tete(7)
# print("l =",l)
# l.ajouter_tete(8)
# print("l =",l)
# # l.ajouter_queue(9)
# # print("l =",l)

# l.retirer_queue()
# print("l =",l)
# l.retirer_queue()
# print("l =",l)
# l.retirer_queue()
# print("l =",l)
# l.retirer_queue()
# print("l =",l)

# # l.retirer_queue()
# # print("l =",l)

# l.ajouter_tete(18)
# print("l =",l)

# l.insert(0,10)
# print("l =",l)


# l.retirer_queue()
# print("l =",l)
# print("l[1] =",l[1])
# print("l[1].valeur =",l[1].get_valeur())
# l.retirer_queue()
# print("l =",l)
# l.retirer_queue()
# print("l =",l)
# l.ajouter_queue(11)
# print("l =",l)

# print("l[3]=",l[3])
# print("l[5]=",l[5])











