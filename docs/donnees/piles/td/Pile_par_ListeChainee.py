# URL : https://github.com/glassus/nsi/blob/master/Terminale/Theme_1_Structures_de_donnees/1.1_Listes_Piles_Files/01_Listes_Piles_Files.ipynb

# Implémentation avec Liste Chaînée

class Cellule :
    """Structure de Cellule"""
    def __init__(self, valeur, suivante):
        self.valeur = valeur
        self.suivante = suivante

class Pile:
    """Structure de Pile"""
    def __init__(self, data=[]):
        if data == []:
            self.sommet = None
        else:
            self.sommet = None
            for x in data:
                self.sommet = Cellule(x, self.sommet)
                # self.empile(x)
            # print("Pile =",self)

    def est_vide(self):
        return self.sommet is None
    
    def vider(self):
        """Vide la Pile"""
        while not self.est_vide():
            self.depile()

    def empile(self, x)->None: # en O(1)
        """On empile/sommet à l'envers : à gauche"""
        # implémenté à l'envers (on empile/sommet à gauche) pour tirer profit de la classe Cellule
        # print("Empile", x)
        self.sommet = Cellule(x, self.sommet)
    
    def depile(self): # en O(1)
        """On depile/sommet à l'envers : à gauche"""
        # implémenté à l'envers (on dépile/sommet à gauche) pour tirer profit de la classe Cellule
        # print("Depile")
        if self.est_vide():
            # on pourrait lever une erreur ici, question de choix : Question de Choix
            return None
            # raise IndexError("Vous avez essayé de dépiler une pile vide !")
        else:
            v = self.sommet.valeur #on récupère la valeur à renvoyer
            self.sommet = self.sommet.suivante  # on supprime la 1ère cellule (la plus à gauche)
            return v

    def get_sommet(self):
        if self.est_vide() == True:
            return None
        else:
            return self.sommet.valeur

    def copy(self):
        pTemp = Pile()
        s = self.sommet
        while self.sommet is not None:
            pTemp.sommet = Cellule(self.sommet.valeur, self.sommet)
            self.sommet = self.sommet.suivante
        self.sommet = pTemp.sommet = s
        return pTemp

    def renverse(self):
        pileTemp = self.copy()
        self.vider()
        while not pileTemp.est_vide():
            self.empile(pileTemp.depile())

    def concatene(self, p2):
        if self is None:
            return p2
        else:
            pTemp = p2.copy()
            pTemp.renverse()
            while pTemp.sommet is not None:
                self.empile(pTemp.sommet.valeur)
                pTemp.depile()

    def __repr__(self):
        # Affichage à l'envers ('sommet à droite') pour Lecture humaine
        s = ""
        c = self.sommet
        while c is not None :
            s += str(c.valeur)[::-1]+"|"
            c = c.suivante
        return "|•"+s[::-1]+"|"

    # def __str__(self):
    #     s = "|•|"
    #     c = self.sommet
    #     while c != None :
    #         s += str(c.valeur)+"|"
    #         c = c.suivante
    #     return s

if __name__ == "__main__":
    # p = Pile()  # p=None
    # # print("p = ", p)
    # p.empile(1)   # p= 3
    # # print("p = ", p)
    # p.empile(2)  # p= 3 5 par convention
    # # print("p = ", p)
    # p.empile(3)  # p= 3 5 1
    p = Pile([4,5,6])
    print("p = ", p)

    p1 = p.copy()
    print("copy p = ", p)
    print("copy p1 = ", p1)

    p.empile(5)
    print("copy p = ", p)
    print("copy p1 = ", p1)

    p1.empile(10)
    p1.empile(11)
    p1.empile(12)

    print("copy p = ", p)
    print("copy p1 = ", p1)

    p.renverse()
    print("renverse = ", p)

    print("empile p")
    p.empile(6)
    print("copy p = ", p)
    print("copy p1 = ", p1)

    p1.empile(7)
    p1.empile(8)
    print("copy p = ", p)
    print("copy p1 = ", p1)


