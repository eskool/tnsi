# URL : 
# https://github.com/glassus/nsi/blob/master/Terminale/Theme_1_Structures_de_donnees/1.1_Listes_Piles_Files/01_Listes_Piles_Files.ipynb

# Implémentation avec un Tableau (type 'list' de Python)

class Pile:
    def __init__(self, data=[]):
        self.data = data
      
    def est_vide(self):
        # if len(self.data) == 0:
        #     return True
        # else:
        #     return False
        return len(self.data) == 0 

    def vider(self):
        self.data = []

    def empile(self,x)->None: # en O(1)
        """On empile/sommet à droite"""
        self.data.append(x)

    def depile(self): # en O(1)
        """"On dépile/sommet à droite"""
        if self.est_vide() == True :
            return []
            # On pourrait lever une erreur ici : Question de Choix
            # raise IndexError("Vous avez essayé de dépiler une pile vide !")
        else :
            return self.data.pop() 

    def get_sommet(self):
        if self.est_vide() == True:
            return None
        else:
            return self.data[-1]

    def copy(self):
        pTemp = []
        for x in self.data:
            pTemp.append(x)
        return Pile(data=pTemp)

    def renverse(self):
        pileTemp = self.copy()
        self.vider()
        while not pileTemp.est_vide():
            self.empile(pileTemp.depile())

    def concatene(self,p2):
        # if self is None:
        #     return p2
        # # elif p2 is None:
        # #     return self
        # else:
        for x in p2.data:
            self.empile(x)
        p2.vider()

    def __repr__(self):       # Hors-Programme : pour afficher 
        s = "|•|"              # convenablement la pile avec p
        for k in self.data :
            s = s + str(k) + "|"
        return s

    # def __str__(self):       # Hors-Programme : pour afficher 
    #     s = "|⟂|"              # convenablement la pile avec print(p)
    #     for k in self.data :
    #         s = s + str(k) + "|"
    #     return s


p1 = Pile()  # p=None
p2 = Pile([1,2,3])
p1.concatene(p2)
print("p1 = ", p1)
print("p2 = ", p2)
# p0 = p1.copy()
# # print("p0 = ",p0)
# p2 = Pile([4,5,6])
# print("p2 = ",p2)

# p2.depile()
# print("p1 = ",p1)
# print("p2 = ",p2)

# p2 = Pile([4,5,6])  # p=None
# print("p2 = ", p2)

# p1.concatene(p2)
# print("p1 = ", p1)
# print("p2 = ", p2)

# p1.renverse()
# print("p1 renverse = ", p1)

