# TNSI : Cours sur les Piles

## Qu'est-ce qu'une Pile ?

Une <bred>Pile</bred> :fr:, ou <bred>Stack</bred> :gb: est une **collection** d'objets, ayant des opérations basées sur la structure **LIFO (Last In, First Out)** :gb: / **Dernier Arrivé, Premier Sorti** :fr:.
Dans une pile, **les ajouts et suppressions n'ont lieu que sur une même extremité** : le **Sommet** de la Pile.

<center>

```dot
digraph pile {
  rankdir=LR

  node [shape=record, fontcolor=red, fontsize=12, height = 0];
  headerValues [label="Pile contenant | 5, 8 et 4 ", color=none, width=0.5]
  headerIndices [label="", color=none]
  headerValues -> headerIndices [color=none];
  ranksep=0

  node [shape=record, fontcolor=black, fontsize=12, width=0.5, height = 1.5, fixedsize=true];
  values [label="<f0> 4 | <f2> 8 | <f3> 5 ", color=blue, fillcolor=lightblue, style=filled, width=0.5];
  indices [label="<f0> Sommet |  |  |  |  | ", color=white, fontcolor=red, fontsize=12, width=0.7];

  node [shape=plaintext, fontcolor=red, fontsize=12, height = 0.3];
  pop [label="1", fontcolor=none]
  " " [label=" |  ", shape=Mrecord, height=0.75, fontcolor=none; color=none]
  " " -> pop [color=none, tailclip=false,]

  { rank=same; headerValues; values }
  { rank=same; headerIndices; indices }

  indices:f0 -> values:f0 [color=red];
}
```

```dot
digraph pile {
  rankdir=LR

  node [shape=record, fontcolor=red, fontsize=12, height = 0];
  headerValues [label="Empilement / | Push | de la valeur 2 ", color=none, width=0.5]
  headerIndices [label="", color=none]
  headerValues -> headerIndices [color=none];
  ranksep=0

  node [shape=record, fontcolor=black, fontsize=12, width=0.5, height = 2, fixedsize=true];
  values [label="<f0> 2 | <f1> 4 | <f2> 8 | <f3> 5 ", color=blue, fillcolor=lightblue, style=filled, width=0.5];
  indices [label="<f0> Sommet |  |  |  |  | ", color=white, fontcolor=red, fontsize=12, width=0.7];

  node [shape=plaintext, fontcolor=red, fontsize=12, height = 0.3];
  pop [label="1", fontcolor=none]
  " " [shape=Mrecord,color=none]
  " " -> pop [color=none, tailclip=false,]

  { rank=same; headerValues; values }
  { rank=same; headerIndices; indices }

  indices:f0 -> values:f0 [color=red];
}
```

```dot
digraph pile {
  rankdir=LR

  node [shape=record, fontcolor=red, fontsize=12, height = 0];
  headerValues [label="Dépilement / Pop | du Sommet | (de la valeur 2) ", color=none, width=0.5]
  headerIndices [label="", color=none]
  headerValues -> headerIndices [color=none];
  ranksep=0

  node [shape=record, fontcolor=black, fontsize=12, width=0.5, height = 1.5, fixedsize=true];
  values [label="<f0> 4 | <f2> 8 | <f3> 5 ", color=blue, fillcolor=lightblue, style=filled, width=0.5];
  indices [label="<f0> Sommet |  |  |  |  | ", color=white, fontcolor=red, fontsize=12, width=0.7];

  node [shape=plaintext, fontcolor=red, fontsize=12, height = 0.3];
  pop [label="2", fontcolor=blue]
  " . " [label="  ", shape=Mrecord, height=0, fontcolor=none, shape=Mrecord, color=none]
  " " [label=" ", shape=Mrecord, height=0.5, fontcolor=none, shape=Mrecord, color=grey]
  " " -> pop [color=blue, tailclip=false,]

  { rank=same; headerValues; values }
  { rank=same; headerIndices; indices }

  indices:f0 -> values:f0 [color=red];
}
```

</center>

## Définition

!!! def
    Une <bred>Pile</bred> :fr: / **Stack** :gb: / **LIFO** :gb: (**Last In First Out** ou **Dernier Arrivé Premier Sorti** ) est une une **Structure de Données**, donc une collection d'objets implémentant les opérations/**primitives** suivantes:

    * savoir la pile est vide: `est_vide` ou `is_empty`
    * <bred>*empiler*</bred> / <bred>push</bred> un nouvel élément au ***sommet*** de la pile, en temps constant : `empiler` ou `push`
    * <bred>*dépiler*</bred> / <bred>pop</bred> l'élément courant au ***sommet*** de la pile, en temps constant : `depiler` ou `pop`
    Parfois, on peut trouver deux opérations distinctes:
        * une opération pour (seulement) renvoyer le sommet de la pile, sans le supprimer
        * une opération pour le supprimer
    * **Sommet** renvoie la valeur du Sommet

!!! exp
    * Pile d'assiettes: c'est en haut de la pile qu'il faut prendre ou ajouter des assiettes.
    * Pile de Livres: idem. etc..

## Principales Utilisations

### Traitement des appels de fonctions

gestion des adresses de retour, nécessaire dans le cas de fonctions récursives…

### Évaluation d'expressions arithmétiques

les algorithmes utilisent la structure de Pile

## Spécification

!!! pte "Préconditions"
    * dépiler est défini $\Leftrightarrow$ est_vide=Faux
    * Sommet est définie $\Leftrightarrow$ est_vide=Faux

!!! pte "Axiomes"
    Une Pile dispose des axiomes suivants:

    * est_vide(Pile_vide) = Vrai
    * est_vide(empiler(Pile p,Element e)) = Faux
    * dépiler(empiler(Pile p, Element e)) = p
    * Sommet(empiler(Pile p, Element e)) = e


## Implémentations

### Avec un Tableau Dynamique (avec le type `list` de Python)

### Avec le module `collections.deque`