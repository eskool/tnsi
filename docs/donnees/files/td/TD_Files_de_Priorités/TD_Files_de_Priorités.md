<titre>TNSI : TD Files de Priorités</titre>

>ATTENTION : Ce TD utilise la notion de Tas, elle-même liée à la notion d'Arbre Binaire

# Qu'est-ce-qu'une File de Priorités ?

En informatique, une file de priorité est un type abstrait élémentaire sur laquelle on peut effectuer trois opérations :

* insérer un élément ;
* extraire l'élément ayant la plus grande clé ;
* tester si la file de priorité est vide ou pas.

# Implémenter une File de Priorités avec une Liste Chaînée

Une implémentation simple de la file de priorité consiste à utiliser une liste chaînée. Dans ce cas, la complexité de l'insertion serait O(1) mais celle de l'extraction du plus grand élément serait linéaire en la taille de la structure, causée par la recherche de l'élément de clé maximale. Cette solution est donc inefficace.

# Implémenter une File de Priorités avec un Tas

cf page https://fr.wikipedia.org/wiki/File_de_priorit%C3%A9


D'autres implémentations permettent de réaliser toutes les opérations efficacement, c'est-à-dire en O(1) ou O(log n) (éventuellement en complexité amortie). La principale est le tas, lui-même implémenté via un tableau. D'autres améliorations, plus difficiles à implémenter, existent, telles que le tas binomial et le tas de Fibonacci.

En Python, le module 'heapq'2 implémente la file de priorité. En C++, la STL fournit une implémentation nommée priority_queue3. Ce type de file se base au choix sur un des conteneurs présents dans la STL (liste, vecteur, etc.) et manipule des objets d'un type défini par l'utilisateur. Il est nécessaire de fournir une fonction de comparaison qui permettra à la bibliothèque d'ordonner la file. 

