from Pile_par_ListeChainee import Cellule, Pile

class File:
    def __init__(self, data=[]):
        if data == []:
            self.entree = Pile()
            self.sortie = Pile()
        else:
            self.entree = Pile(data)
            self.sortie = Pile()

    def est_vide(self):
        return self.entree.est_vide() and self.sortie.est_vide()

    def vider(self):
        self.entree = Pile()
        self.sortie = Pile()

    def enfile(self,x):
        self.entree.empile(x)

    def defile(self):
        if self.est_vide():
            return None
            #raise IndexError("File vide !")
        if self.sortie.est_vide() == True:
            while not self.entree.est_vide() :
                self.sortie.empile(self.entree.depile())
            # équivalent :
            # self.entree.renverse()
            # self.sortie = self.entree.copy()
            # self.entree.vider()
        return self.sortie.depile()

    def get_tete(self):
        if self.est_vide():
            return None
        else:
            if self.sortie.est_vide() == True:
                while not self.entree.est_vide():
                    self.sortie.empile(self.entree.depile())
        return self.sortie.get_sommet()

    def copy(self):
        fTemp = File()
        fTemp.entree = self.entree.copy()
        fTemp.sortie = self.sortie.copy()
        return fTemp

    def renverse(self):
        p1 = self.entree.copy()
        p2 = self.sortie.copy()
        p1.renverse()
        p2.renverse()
        self.entree = p1
        self.sortie = p2

    def concatene(self, f2):
        if self is None:
            return f2
        else:
            # pour File 1 : on concatène la pile de sortie sur la pile d'entrée
            self.sortie.renverse()
            self.entree.concatene(self.sortie)
            # pour File 2 : on concatène la pile de sortie sur la pile d'entrée
            # f2.entree.renverse()
            f2.entree.concatene(self.sortie)
            # on concatène enfin les deux piles concaténées ensemble, sur la première
            self.entree.concatene(f2.entree)
            # puis on vide la deuxième
            f2.vider()

    def __repr__(self):
        entreeBis = self.entree.copy()
        sortieBis = self.sortie.copy()
        sortieBis.renverse()
        sortieBis.concatene(entreeBis)
        return f"{sortieBis.__repr__()}⟂|"


# f = File()  # f=None
# print("f = ", f)
# f.enfile(3)   # f= 3
# print("f = ", f)
# f.enfile(5)  # f= 3 5 par convention
# print("f = ", f)
# f.est_vide()  #  False
# print("f est vide = ", f.est_vide())
# f.enfile(1)  # f= 3 5 1
# print("f = ", f)
# f.defile()  # f= 5 1    valeur renvoyée : 3
# print("f = ", f)
# f.defile()  # f= 1      valeur renvoyée : 5
# print("f = ", f)
# f.enfile(9)  # f= 1 9
# print("f = ", f)
# f.defile()  # f= 9       valeur renvoyée : 1
# print("f = ", f)
# f.defile()  # f= None      valeur renvoyée : None
# print("f = ", f)
# f.est_vide() # True
# print("f est vide = ", f.est_vide())

f = File([1,2,3])
print("f=",f)
print("tete f=",f.get_tete())
print("f=",f)
# f = File()
# f.enfile(1)
# f.enfile(2)
# f.enfile(3)
# f.enfile(2)
# f.enfile(3)
# print("f =", f)

# print("copie dans f1 de f")
# f1 = f.copy()
# print("f=",f)
# print("f1=",f1)

# print("renverse f")
# f.renverse()
# print("f =", f)

# print("new f1")
# f1 = File([14,15,16])
# # f1.enfile(5)
# # f1.enfile(6)
# print("f=",f)
# print("f1 =", f1)

# print("concatène f1 sur f")
# f.concatene(f1)
# print("f =", f)
# print("f1 =", f1)


