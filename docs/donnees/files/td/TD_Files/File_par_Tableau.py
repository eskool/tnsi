class File:
    def __init__(self, data=[]):
        # self.data = []
        self.data = data
      
    def est_vide(self):
        return len(self.data) == 0 
    
    def vider(self):
        self.data = []

    def enfile(self,x): # en O(1)
        # Ordre normal : On enfile à droite
        self.data.append(x)

    def defile(self): # en O(n)
        # Ordre Normal : on défile à gauche
        if self.est_vide() == True :
            # raise IndexError("Vous avez essayé de défiler une file vide !")
            return None
        else :
            return self.data.pop(0) 

    def get_tete(self):
        if self.data == []:
            return None
        else:
            return self.data[0]

    def copy(self):
        fTemp = []
        for x in self.data:
            fTemp.append(x)
        return File(data=fTemp)

    def renverse(self):
        fileTemp = self.copy()
        self.vider()
        while not fileTemp.est_vide():
            self.enfile(fileTemp.data.pop())

    def concatene(self,f2):
        if self is None:
            return f2
        else:
            for x in f2.data:
                self.enfile(x)
            f2.vider()

    def __repr__(self):       # Hors-Programme : pour afficher 
        s = "|•|"              # convenablement la file avec print(p)
        for k in self.data :
            s = s + str(k) + "|"
        return s+"⟂|"

    # def __str__(self):       # Hors-Programme : pour afficher 
    #     s = "|•|"              # convenablement la file avec print(p)
    #     for k in self.data :
    #         s = s + str(k) + "|"
    #     return s

if __name__ == "__main__":
    f = File([1,2,3])
    print("f AVANT=", f)
    f.renverse()
    print("f APRES=", f)

    # f1 = File()
    # print("tete de f1", f1.get_tete())
    # print("f1=", f1)
    # # f2 = File([4,5,6])
    # # f2 = File()
    # print("f2=", f2)

    # f1.concatene(f2)
    # print("f1 concaténé=", f1)

    # f0 = f1.copy()
    # print("f0=", f0)
    # print("f1=", f1)
    # f1.enfile(7)
    # print("f0=", f0)
    # print("f1=", f1)


    # f = File()  # f=None
    # print("f = ", f)
    # f.enfile(3)   # f= 3
    # print("f = ", f)
    # f.enfile(5)  # f= 3 5 par convention
    # print("f = ", f)
    # f.est_vide()  #  False
    # print("f est vide = ", f.est_vide())
    # f.enfile(1)  # f= 3 5 1
    # print("f = ", f)
    # f.defile()  # f= 5 1    valeur renvoyée : 3
    # print("f = ", f)
    # f.defile()  # f= 1      valeur renvoyée : 5
    # print("f = ", f)
    # f.enfile(9)  # f= 1 9
    # print("f = ", f)
    # f.defile()  # f= 9       valeur renvoyée : 1
    # print("f = ", f)
    # f.defile()  # f= None      valeur renvoyée : None
    # print("f = ", f)
    # f.est_vide() # True
    # print("f est vide = ", f.est_vide())