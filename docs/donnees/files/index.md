# TNSI : Cours sur les Files

## Qu'est-ce qu'une File?

Une <bred>File</bred> :fr:, ou <bred>Queue</bred> :gb: est une collection d'objets, ayant des opérations basées sur la structure **FIFO (First In, First Out)** :gb: / **Premier Arrivé, Premier Sorti** :fr:.  
Dans une File / Queue :

* les **ajouts/arrivées** n'ont lieu que sur une même extremité : la **Queue** de la File.
* les **suppressions/départs** n'ont lieu que sur l'**autre** extremité: la **Tête** de la File.

<env>**Représentation Verticale des Files**</env>

<center>

```dot
digraph pile {
  rankdir=LR
  ranksep=0

  node [shape=record, fontcolor=red, fontsize=12, height = 0];
  headerValues [label="File contenant | 4, 8 et 5 ", color=none, width=0.5]
  headerIndices [label="", color=none, width=0]
  headerValues -> headerIndices [color=none];

  node [shape=record, fontcolor=black, fontsize=12, width=0.4, height = 1.2, fixedsize=true];
  values [label="<f0> 4 | <f2> 8 | <f3> 5 ", color=blue, fillcolor=lightblue, style=filled, width=0.4];
  indices [label="<f0> Tête |  |  | <f0> Queue |  | ", color=white, fontcolor=red, fontsize=12, width=0.7];

  { rank=same; headerValues; values }
  { rank=same; headerIndices; indices }

  indices:f0 -> values:f0 [color=red];
  indices:f3 -> values:f3 [color=red];
}
```

```dot
digraph pile {
  rankdir=LR
  ranksep=0
  node [shape=record, fontcolor=red, fontsize=12, height = 0];
  headerValues [label="Enfilement / | Enqueue / | Ajout de | la valeur 2 ", color=none, width=0.5]
  headerIndices [label="", color=none, width=0]
  headerValues -> headerIndices [color=none];
  ranksep=0

  node [shape=record, fontcolor=black, fontsize=12, width=0, height = 1.4, fixedsize=true];
  values [label="<f0> 4 | <f1> 8 | <f2> 5 | <f3> 2 ", color=blue, fillcolor=lightblue, style=filled, width=0.4];
  indices [label="<f0> Tête |  |  | <f3> Queue |  | ", color=white, fontcolor=red, fontsize=12, width=0.6];

  { rank=same; headerValues; values }
  { rank=same; headerIndices; indices }

  indices:f0 -> values:f0 [color=red];
  indices:f3 -> values:f3 [color=red];
}
```

```dot
digraph pile {
  rankdir=LR
  ranksep=0

  node [shape=record, fontcolor=red, fontsize=12, height = 0];
  headerValues [label="Défilement / Dequeue / | Retrait de la valeur 4 ", color=none, width=0.5]
  headerIndices [label="", color=none]
  headerValues -> headerIndices [color=none];
  ranksep=0


  node [shape=record, fontcolor=black, fontsize=12, width=0.4, height = 1.2, fixedsize=true];
  values [label="<f0> 8 | <f2> 5 | <f3> 2 ", color=blue, fillcolor=lightblue, style=filled, width=0.4];
  indices [label=" |  |  |  | <f0> Queue | ", color=white, fontcolor=red, fontsize=12, width=0.8];

  node [shape=plaintext, fontcolor=red, fontsize=12, width=0.8, height = 0.3];
  pop [label="4 (Tête)", fontcolor=blue]
  " " [label=" ", shape=Mrecord, height=0.4, width=0.4, fontcolor=none, shape=Mrecord, color=grey]
  " " -> pop [color=blue, tailclip=false]

  { rank=same; headerValues; values }
  { rank=same; headerIndices; indices }

  indices:f0 -> values:f3 [color=red];
}
```

</center>

<clear></clear>

<env>**Représentation Horizontale des Files**</env>

<center>

```dot
digraph File {
  rankdir=LR
  ranksep=0
  splines=false;
  node [shape=record, fontcolor=color, fontsize=14, color=blue, style=filled, fillcolor=lightblue, height = 0, width=0];
  edge[color=blue]
  submid [label="File contenant | 4, 8 et 5", color=none, fontcolor=red, fillcolor=none]
  liste [label="{<first> 5 | <f1> 8 | <last> 4}"]
  //pop [label="<f0> ",fontcolor=red, color=none, fillcolor=none]  
}
```

```dot
digraph File {
  rankdir=LR
  ranksep=0

  splines=false;
  node [shape=record, fontcolor=color, fontsize=14, color=blue, style=filled, fillcolor=lightblue, height = 0, width=0];
  edge[color=blue]
  subleft [label="", color=none, fillcolor=none]
  submid [label="Enfilement / Enqueue / | Ajout de la valeur 2", color=none, fontcolor=red, fillcolor=none]
  subleft -> submid  [color=none]
  unshift [label="<f0> Queue", fontcolor=red, color=none, fillcolor=none, width=0]
  unshift -> liste [color=blue];
  liste [label="{<first> 2 | <f1> 5 | <f2> 8 | <last> 4}"]
}
```

```dot
digraph File {
  rankdir=LR
  ranksep=0
  splines=false;
  node [shape=record, fontcolor=color, fontsize=14, color=blue, style=filled, fillcolor=lightblue, height = 0];
  edge[color=blue]
  subright [label="Tête", color=none, fillcolor=none, fontcolor=red]
  submid [label="Défilement / Dequeue / | Retrait de la valeur 4", color=none, fontcolor=red, fillcolor=none]
  submid -> subright [color=none]
  liste -> pop [color=blue];
  liste [label="{<first> 2 | <f1> 5 | <last> 8 }"]
  pop [label="<f0> 4", shape=Mrecord, fontcolor=red, color=grey, fillcolor=none, width=0.2]  
}
```

</center>

!!! info "Remarque"
    * Certains auteurs et/ou implémentations (comme dans ce Cours) utilisent les représentations décrites ci-dessus:
        * représentation verticale: **queue en bas** et **tête en haut**.
        * représentation horizontale: **queue à gauche** et **tête à droite**.
    * :warning: **ATTENTION** :warning: D'autres auteurs et/ou implémentations utilisent le contraire : 
        * représentation verticale: **queue en haut** et **tête en bas** et/ou
        * représentation horizontale: **queue à droite** et **tête à gauche** (comme le TD à venir)
       
<clear></clear>

## Définition

!!! def
    Une <bred>File</bred> :fr: / <bred>Queue</bred> :fr::gb: / **FIFO** :gb: (**First In First Out** ou **Premier Arrivé Premier Sorti** ) est une **Structure de Données**, donc collection d'objets implémentant les opérations/**primitives** suivantes:

    * savoir si la file est vide: `est_vide` ou `is_empty`
    * <bred>*enfiler*</bred> / <bred>*enqueue*</bred> un nouvel élément en ***queue*** de la file, en temps constant : `enfiler` ou `enqueue`
    * <bred>*défiler*</bred> / <bred>*dequeue*</bred> l'élément courant en ***tête*** de la file, en temps constant : `defiler` ou `dequeue`
    Parfois, on peut trouver deux opérations distinctes:
        * une opération pour (seulement) renvoyer la tête de la file, sans la supprimer
        * une opération pour la supprimer
    * **Tête** renvoie la valeur de la Tête

!!! exp
    * Une File / Queue à la Boulangerie: c'est le premier arrivé au comptoir qui est servi d'abord, et sort en premier.
    * Des Files / Queues de la circulation au péage : c'est le premier arrivé au péage, qui paye d'abord, et sort en premier.

## Principales Utilisations

Cette structure de données est utilisée par exemple:

* De manière générale : **mémorisation temporaire** de transactions qui doivent attendre pour être traitées
* **Serveurs d'impression** : `Spoolers` :gb: qui traitent ainsi les requêtes dans l'ordre dans lequel elles arrivent, et les insèrent dans une file d'attente (dite aussi `queue` ou `spool`)
* Certains **moteurs multitâches dans un OS**, qui doivent accorder du temps-machine à chaque tâche, sans en privilégier aucune
* Algorithme de **Parcours en Largeur** dans un Graphe ou un Arbre
* Toutes sortes de **mémoires tampons** : `buffers` :gb:
* Gestion des Stocks : les algorithmes doivent respecter la gestion physique des stocks pour assurer la cohérence physique/valorisation.

## Spécification

!!! pte "Préconditions"
    * défiler est défini $\Leftrightarrow$ est_vide = Faux
    * Tête est définie $\Leftrightarrow$ est_vide = Faux
    * Queue est définie $\Leftrightarrow$ est_vide = Faux

!!! pte "Axiomes"
    Une File dispose des axiomes suivants:

    * est_vide(File_vide) = Vrai
    * est_vide(enfiler(File f,Element e)) = Faux
    * Queue(enfiler(File f, Element e)) = e

## Implémentations (cf TD)

### Avec un Tableau Dynamique (avec le type `list` de Python) (au B.O.)

### Avec Deux Piles (au B.O.)

### Avec une Liste Chaînée

Une liste chaînée dont on n'utilise que les opérations *ajouterQueue* et *retirerTête* constitue une File/Queue. Si la file/queue est implémentée avec un tableau, la structure enregistre deux indices, l'un correspondant à la Queue (dernier arrivé), l'autre à la Tête (le prochain à sortir).

### Avec le module `collections.deque`