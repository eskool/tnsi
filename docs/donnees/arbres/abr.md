# TNSI : Arbres Binaires de Recherche (ABR)

!!! def
    * Un <bred>Arbre Binaire de Recherche (ABR)</bred> :fr:, ou <bred>Binary Search Tree (BST)</bred> :gb:, est un *Arbre Binaire* tel que :
        * les clés du sous-arbre gauche sont **inférieures ou égales** à celle de la racine
        * les clés du sous-arbre droit sont **supérieures ou égales** à celle de la racine
        * les deux sous-arbres sont eux-mêmes des Arbres Binaires de Recherche
    * Les <bred>Clés</bred> d'un Arbre Binaire de Recherche sont ses **étiquettes**

**Remarque :** L'égalité est ici possible pour le sous-arbre gauche (le sous-arbre droit a des clés strictement supérieures), mais on pourrait tout aussi bien faire le contraire.

## Cas de Figure ABR

### Arbres Binaires de Recherche (ABR)

<div id="conteneur">

<div>

```dot
graph ABR {
  size="1.7"
  node [shape=circle, fontsize=30, penwidth=3]
  edge [penwidth=3]
  20 -- {15, 24}
  15 -- 7
  15 -- v1 [color=none]
  24 -- v1 [color=none]
  24 -- 36
  36 -- {28, 38}
  v1 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Arbre Binaire<br/>de Recherche</figcaption>

</div>

<div>

```dot
graph ABR {
  size="1.8"
  node [shape=circle, fontsize=30, penwidth=3]
  edge [penwidth=3]
  5 -- {4, 16}
  4 -- 2 [weight=2]
  4 -- v1 [color=none]
  16 -- 10
  16 -- 20 [weight=2]
  10 -- {7, 14}
  14 -- {12, 15}
  v1 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Arbre Binaire<br/>de Recherche</figcaption>

</div>

<div>

```dot
graph ABR {
  size="1.7"
  node [shape=circle, fontsize=30, penwidth=3]
  edge [penwidth=3]
  24 -- {13, 27}
  13 -- 8 [weight=2]
  13 -- 17
  17 -- {15, 22}
  27 -- v1 [color=none]
  27 -- 32
  32 -- 28
  32 -- v2 [color=none]
  v1, v2 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Arbre Binaire<br/>de Recherche</figcaption>

</div>

<div>

```dot
graph ABR {
  size="2"
  node [shape=circle, fontsize=30, penwidth=3]
  edge [penwidth=3]
  50 -- {16, 70}
  16 -- 8 [weight=2]
  16 -- 25
  25 -- 19
  25 -- v1 [color=none, weight=3]
  8 -- v2 [color=none, weight=3]
  8 -- 15
  15 -- 13
  15 -- v3 [color=none, color=none]
  70 -- 52
  70 -- v4 [color=none, weight=3]
  52 -- v5 [color=none]
  52 -- 68
  68 -- 64
  68 -- v6 [color=none]
  v1, v2, v3, v4, v5, v6 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Arbre Binaire<br/>de Recherche</figcaption>

</div>

<div>

```dot
graph ABR {
  size="2.2"
  node [shape=circle, fontsize=30, penwidth=3]
  edge [penwidth=3]
  20 -- {14, 23}
  14 -- 8
  14 -- v1 [color=none]
  23 -- v1 [color=none]
  23 -- 25
  8 -- {4, 12}
  12 -- 10
  12 -- v3 [color=none]
  10 -- {9, 11}
  v1, v2, v3 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Arbre Binaire<br/>de Recherche</figcaption>

</div>

<div>

```dot
graph ABR {
  size="2.2"
  node [shape=circle, fixedsize=true, width=0.8,fontsize=30, penwidth=3]
  edge [penwidth=3]
  40 -- {35, 60}
  35 -- {30, 37}
  30 -- {20,34}
  20 -- 10
  20 -- v1 [color=none]
  10 -- 5
  10 -- v2 [color=none]
  v1, v2 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Arbre Binaire<br/>de Recherche</figcaption>

</div>

</div>

### Non ABR. Justifiez

<div id="conteneur">

<div>

```dot
graph ABR {
  size="1.3"
  node [shape=circle, fontsize=38, penwidth=3]
  edge [penwidth=3]
  20 -- {15, 38}
  15 -- 7
  15 -- v1 [color=none]
  38 -- 36
  38 -- v2 [color=none, weight=2]
  36 -- {28, 39}
  v1, v2 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Pas un ABR</figcaption>

</div>

<div>

```dot
graph ABR {
  size="1.6"
  node [shape=circle, fontsize=38, penwidth=3]
  edge [penwidth=3]
  5 -- {4, 16}
  4 -- 2 [weight=2]
  2 -- v2 [color=none]
  2 -- 6
  4 -- v1 [color=none]
  16 -- 10
  16 -- 20 [weight=2]
  10 -- {7, 14}
  14 -- {9, 17}
  v1, v2 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Pas un ABR</figcaption>

</div>

<div>

```dot
graph ABR {
  size="1.3"
  node [shape=circle, fontsize=38, penwidth=3]
  edge [penwidth=3]
  24 -- {13, 27}
  13 -- 8 [weight=2]
  13 -- 17
  17 -- {12, 22}
  27 -- v1 [color=none]
  27 -- 32
  32 -- 20
  32 -- v2 [color=none]
  v1, v2 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Pas un ABR</figcaption>

</div>

<div>

```dot
graph ABR {
  size="1.8"
  node [shape=circle, fontsize=38, penwidth=3]
  edge [penwidth=3]
  50 -- {16, 70}
  16 -- 8 [weight=2]
  16 -- 25
  25 -- 19
  25 -- v1 [color=none, weight=3]
  8 -- v2 [color=none, weight=3]
  8 -- 17
  17 -- 13
  17 -- v3 [color=none, color=none]
  70 -- 48
  70 -- v4 [color=none, weight=3]
  48 -- v5 [color=none]
  48 -- 68
  68 -- 64
  68 -- v6 [color=none]
  v1, v2, v3, v4, v5, v6 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Pas un ABR</figcaption>

</div>

<div>

```dot
graph ABR {
  size="2"
  node [shape=circle, fontsize=38, penwidth=3]
  edge [penwidth=3]
  20 -- {14, 23}
  14 -- 8
  14 -- v1 [color=none]
  23 -- v1 [color=none]
  23 -- 25
  8 -- 4 [weight=2]
  8 -- 21
  21 -- 10
  21 -- v3 [color=none]
  10 -- {7, 13}
  25 -- 18
  25 -- v4 [color=none, weight=2]
  v1, v2, v3, v4 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Pas un ABR</figcaption>

</div>

<div>

```dot
graph ABR {
  size="2"
  node [shape=circle, fixedsize=true, width=0.8,fontsize=34, penwidth=3]
  edge [penwidth=3]
  40 -- {35, 60}
  35 -- {30, 41}
  30 -- {20,34}
  34 -- 28
  34 -- 36 [weight=2]
  20 -- 10
  20 -- v1 [color=none]
  10 -- 5 [weight=2]
  10 -- 23
  10 -- v2 [color=none]
  v1, v2 [color=none, fontcolor=none, label=""]
  
}
```

<figcaption>Pas un ABR</figcaption>

</div>

</div>

## Opérations de Base dans un ABR

Dans un ABR quelconque, on souhaite usuellement disposer des $3$ opérations de Base suivantes :

* Recherche d'une Clé
* Insertion d'une Clé
* Suppression d'une Clé

### Recherche dans un ABR quelconque

<center>

```dot
digraph ABR {
  size="6"
  node [shape=circle, style=filled, fillcolor="#CFFDFF", fixedsize=true, width=0.8, fontsize=38, penwidth=0]
  edge [fontsize=38, penwidth=3, dir=none]
  34 -> 22 [xlabel="<34", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  34 -> 22
  34 -> 66 [weight=2]
  22 -> 7 [weight=3]
  22 -> 29
  22 -> 29 [label=">22", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  7 -> v1 [weight=4, color=none]
  7 -> 17
  17 -> 9 [weight=4]
  17 -> 21
  9 -> 8, 16
  29 -> 25
  29 -> 25 [label="<29", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  29 -> 32 [weight=3]
  25 -> 23
  25 -> 28
  25 -> 28 [label=">25", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  28 -> 27
  28 -> 27 [label="<28", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  28 -> v2 [weight=2]
  27 -> 26
  27 -> v3
  32 -> 30
  32 -> v4 [weight=3]
  66 -> 50
  66 -> 71 [weight=3]
  50 -> 37 [weight=2]
  50 -> 56
  37 -> 36 [weight=3]
  37 -> 44
  36 -> 35
  36 -> v5
  56 -> 55
  56 -> v6 [weight=2]
  55 -> 52 [weight=2]
  55 -> v7
  52 -> 51
  52 -> v8
  71 -> 70
  71 -> 81 [weight=4]
  70 -> 68 [weight=2]
  70 -> v9
  68 -> 67
  68 -> 69 [weight=2]
  81 -> 80
  81 -> 97 [weight=5]
  97 -> 94
  97 -> v10
  94 -> 88
  94 -> v11
  27 [shape=doublecircle, color=red, penwidth=2]
  v1, v2, v3, v4, v5, v6, 
  v7, v8, v9, v10, v11 [color=none, fontcolor=none, style=none, label=""]
}
```

<figcaption>ABR <b>quelconque</b><br/>Recherche de <b>27</b></figcaption>

</center>

!!! mth "de la Recherche d'une Clé Cible dans un ABR quelconque"
    Dans un Arbre Binaire de Recherche (ABR) quelconque, l'unique chemin menant vers la Clé Cible (à partir de la racine) est obligatoirement calculable/parcourable de la manière suivante:
    Pour chaque noeud courant de chaque niveau parcouru :

    * Si la **Clé Cible = clé du noeud courant** du niveau, Alors on a bien trouvé la Clé Cible sur le niveau courant 
    * Si la **Clé Cible $<$ clé du noeud courant** du niveau, Alors la Recherche se poursuit dans le **sous-arbre gauche** du noeud
    * Si la **Clé Cible $>$ clé du noeud courant** du niveau, Alors la Recherche se poursuit dans le **sous-arbre droit** du noeud

    Lorsqu'on arrive au dernier niveau de cet unique chemin parcouru (le noeud est une feuille) mais sans avoir trouvé le nombre Cible, cela prouve que la Clé Cible n'existe nulle part dans l'ABR (inutile de vérifier les autres chemins à partir de la racine)

Concernant la Complexité :

!!! pte "Complexité de Recherche d'un nombre dans un ABR quelconque"
    Dans un ABR quelconque, dans le pire des cas, la complexité en temps de la Recherche d'une Clé Cible est en <enc>$O(h)$</enc>, càd proportionnelle **à sa hauteur $h$**

<env>Limitation des ABR quelconques</env>

Malheureusement, l'algorithme de Recherche d'une clé dans un ABR n'est **pas efficace** lorsque **l'ABR est quelconque**, en effet il existe :

* des ABR **non équilibrés**, 
* voire pire (cas extrême) : des ABR **dégénérés (arbres peignes)**, 

Dans chacun de ces deux cas, **la hauteur $h$ est égale à la taille $n$ de l'arbre**.
Donc dans le pire des cas (où $h=n$), la complexité de la recherche d'une clé cible dans un ABR est **linéaire en $n$**, càd est en $O(n)$, donc aussi mauvais que pour les Listes et/ou les Files.
D'ailleurs un ABR Peigne peut être considéré comme une **Liste**, ou une **File**.
Pour éviter cela, il suffit d'**empêcher les ABR d'être des Peignes**, et plus généralement **empêcher les ABR d'être non équilibrés**.
C'est pourquoi dans la suite, on s'intéressera à des **Arbres Binaires de Recherche *Équilibrés***, que nous appelerons des **Arbres AVL**.

<div style="float:left;font-size:3.7em;margin-top:0.8em;">$$\Rightarrow$$</div>

<center>

<div id="conteneur">

<div>

```dot
graph ABR {
  size="2.8"
  node [shape=circle, fixedsize=true, width=0.8,fontsize=38, penwidth=3]
  edge [penwidth=3]
  40 -- {35, v1}
  35 -- {30, v2}
  30 -- {20, v3}
  20 -- v4
  20 -- 22
  22 -- v5
  22 -- 27
  27 -- v6
  27 -- 29
  v1, v2, v3, v4, v5, v6 [color=none, fontcolor=none, label=""]
}
```

<figcaption>ABR <b>non équilibré</b></figcaption>

</div>

<div>

```dot
graph ABR {
  size="2.8"
  node [shape=circle, fixedsize=true, width=0.8,fontsize=30, penwidth=3]
  edge [penwidth=3]
  40 -- {35, v1}
  35 -- {30, v2}
  30 -- {20, v3}
  20 -- 10
  20 -- v4
  10 -- 5
  10 -- v5
  v1, v2, v3, v4, v5 [color=none, fontcolor=none, label=""]
}
```

<figcaption>ABR <b>dégénéré.</b><br/>Arbre Peigne <b>Gauche</b></figcaption>

</div>

<div>

```dot
graph ABR {
  size="2.8"
  node [shape=circle, fixedsize=true, width=0.8,fontsize=30, penwidth=3]
  edge [penwidth=3]
  7 -- {v1, 12}
  12 -- {v2, 15}
  15 -- {v3, 20}
  20 -- v4
  20 -- 25
  25 -- v5
  25 -- 30
  v1, v2, v3, v4, v5 [color=none, fontcolor=none, label=""]
}
```

<figcaption>ABR <b>dégénéré.</b><br/>Arbre Peigne <b>Droite</b></figcaption>

</div>


</div>

</center>

### Insertion dans un ABR quelconque

<center>

```dot
digraph ABR {
  size="6"
  //splines=false
  node [shape=circle, style=filled, fillcolor="#CFFDFF", fixedsize=true, width=0.8, fontsize=38, penwidth=0]
  edge [fontsize=38, penwidth=3, dir=none]
  34 -> 22 [weight=2]
  34 -> 66
  34 -> 66 [xlabel=">34", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  22 -> 7 [weight=3]
  22 -> 29
  7 -> v1 [weight=4, color=none]
  7 -> 17
  17 -> 9 [weight=4]
  17 -> 21
  9 -> 8, 16
  29 -> 25 [weight=2]
  29 -> 32 [weight=3]
  25 -> 23 [weight=3]
  25 -> 28
  28 -> 27
  28 -> v2 [weight=2]
  27 -> 26
  27 -> v3
  32 -> 30
  32 -> v4 [weight=4]
  66 -> 50
  66 -> 50 [label="<66", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  66 -> 71 [weight=3]
  50 -> 37 [weight=3]
  50 -> 56
  50 -> 56 [label=">50", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  37 -> 36 [weight=4]
  37 -> 44
  36 -> 35
  36 -> v5
  56 -> 55
  56 -> 55 [label="<56", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  56 -> v6 [weight=4]
  55 -> 52 
  55 -> 52 [label="<28", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  55 -> v7
  52 -> 51
  52 -> 53 [label=">52", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  71 -> 70
  71 -> 81 [weight=4]
  70 -> 68 [weight=2]
  70 -> v9
  68 -> 67
  68 -> 69 [weight=2]
  81 -> 80
  81 -> 97 [weight=5]
  97 -> 94
  97 -> v10
  94 -> 88
  94 -> v11
  53 [shape=doublecircle, fillcolor=red, color=red, fontcolor=white, penwidth=2]
  v1, v2, v3, v4, v5, v6, 
  v7, v8, v9, v10, v11 [color=none, fontcolor=none, style=none, label=""]
}
```

<figcaption>ABR <b>quelconque</b><br/>Insertion de <b>53</b></figcaption>

</center>

!!! mth
    L'**Insertion** d'un noeud (avec sa clé) se déroule en deux étapes :  
    :one: la Recherche de la position où insérer le noeud dans l'ABR : Dans le pire des cas, en $O(h)$  
    :two: l'Insertion : Dans le pire des cas, en $O(1)$  
    Durant la Recherche de la position où l'insérer :

    * Soit on trouve que **cette clé existe déjà dans l'arbre**, auquel cas :
        * On insère la nouvelle clé (doublon) comme **fils gauche** du noeud contenant la clé (par convention, et en général),
        * le SAG de la clé doublon devient le SAG de la nouvelle clé après son insertion
    * Soit on arrive à une feuille : on ajoute alors le nouveau noeud comme fils de la feuille en comparant sa clé à celle de la feuille :
        * Si **clé nouveau noeud $\le$ clé feuille**, Alors le nouveau noeud sera **à gauche de la feuille**
        * Si **clé nouveau noeud $>$ noeud feuille**, Alors le nouveau noeud sera **à droite de la feuille**

donc :

!!! pte "Complexité Insertion dans un ABR quelconque"
    Dans un ABR quelconque, dans le pire des cas, La complexité de l'**Insertion** est en <enc>$O(h)$</enc>

<div id="conteneur">

<div>

<center>

```dot
graph ABR {
  size="1.4"
  node [shape=circle, fixedsize=true, width=0.8,fontsize=30, penwidth=3]
  edge [penwidth=3]
  10 -- {6, 12}
  6 -- 4
  6 -- v1 [color=none]
  v1 [color=none, fontcolor=none, label=""]
}
```

<figcaption>AVANT insertion de 2 :<br/>ABR <b>équilibré</b></figcaption>

</center>

</div>

<div style="float:left;font-size:3.7em;margin-top:-0.3em;">$$\Rightarrow$$</div>

<div>

<center>

```dot
graph ABR {
  size="1.9"
  node [shape=circle, fixedsize=true, width=0.8,fontsize=30, penwidth=3]
  edge [penwidth=3]
  10 -- {6, 12}
  6 -- 4
  6 -- v1 [color=none]
  4 -- 2
  4 -- v2 [color=none]
  v1, v2 [color=none, fontcolor=none, label=""]
}
```

<figcaption>APRÈS insertion de 2 :<br/>ABR <b>déséquilibré</b></figcaption>

</center>

</div>

</center>

</div>

<env>**Remarque**</env>

* L'**Insertion** d'une clé dans un ABR est compatible avec la structure d'ABR, autrement dit : Après insertion d'une clé, un arbre ABR reste un arbre ABR.
* :warning: **ATTENTION** :warning: Par contre, L'insertion d'une clé **peut casser l'équilibre** dans un ABR

### Suppression dans un ABR quelconque

$3$ cas de Figure sont possibles :

<env>**Suppression d'une Feuille**</env> (Exemple : Suppression de la Feuille $67$ )

<center>

```dot
digraph ABR {
  size="6"
  //splines=false
  node [shape=circle, style=filled, fillcolor="#CFFDFF", fixedsize=true, width=0.8, fontsize=38, penwidth=0]
  edge [fontsize=38, penwidth=3, dir=none]
  34 -> 22 [weight=2]
  34 -> 66
  34 -> 66 [xlabel=">34", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  22 -> 7 [weight=3]
  22 -> 29
  7 -> v1 [weight=4, color=none]
  7 -> 17
  17 -> 9 [weight=4]
  17 -> 21
  9 -> 8, 16
  29 -> 25 [weight=2]
  29 -> 32 [weight=3]
  25 -> 23 [weight=3]
  25 -> 28
  28 -> 27
  28 -> v2 [weight=2]
  27 -> 26
  27 -> v3
  32 -> 30
  32 -> v4 [weight=4]
  66 -> 50 [weight=2]
  66 -> 71
  66 -> 71 [label=">66", dir=forward, color=red, fontcolor=red, arrowhead=vee, weight=3]
  50 -> 37 [weight=4]
  50 -> 56 [weight=2]
  37 -> 36 [weight=4]
  37 -> 44
  36 -> 35
  36 -> v5
  56 -> 55
  56 -> v6 [weight=3]
  55 -> 52 
  55 -> v7
  52 -> 51
  52 -> v12
  71 -> 70
  71 -> 70 [label="<71", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  71 -> 81 [weight=5]
  70 -> 68 [xlabel="<70", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  70 -> 68
  70 -> v9 [weight=1]
  68 -> 67
  68 -> 67 [xlabel="<68", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  68 -> v13 [color=none]
  68 -> 69 [weight=3]
  81 -> 80
  81 -> 97 [weight=6]
  97 -> 94
  97 -> v10
  94 -> 88
  94 -> v11
  67 [shape=doublecircle, fillcolor=red, color=red, fontcolor=white, penwidth=2]
  v1, v2, v3, v4, v5, v6, 
  v7, v8, v9, v10, v11, v12,v13 [color=none, fontcolor=none, style=none, label=""]
}
```

<figcaption>

ABR <b>quelconque</b><br/>BUT : Supprimer <span style="background-color:#FF0000;color:white;font-weight:700;border-radius:50%;padding:0.3em;">67</span><br/>

1. Recherche de <span style="background-color:#FF0000;color:white;font-weight:700;border-radius:50%;padding:0.3em;">67</span> <br/>

2. On supprime la Feuille

</figcaption>

```dot
digraph ABR {
  size="6"
  //splines=false
  node [shape=circle, style=filled, fillcolor="#CFFDFF", fixedsize=true, width=0.8, fontsize=38, penwidth=0]
  edge [fontsize=38, penwidth=3, dir=none]
  34 -> 22 [weight=2]
  34 -> 66
  34 -> 66 [xlabel=">34", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  22 -> 7 [weight=3]
  22 -> 29
  7 -> v1 [weight=4, color=none]
  7 -> 17
  17 -> 9 [weight=4]
  17 -> 21
  9 -> 8, 16
  29 -> 25 [weight=2]
  29 -> 32 [weight=3]
  25 -> 23 [weight=3]
  25 -> 28
  28 -> 27
  28 -> v2 [weight=2]
  27 -> 26
  27 -> v3
  32 -> 30
  32 -> v4 [weight=4]
  66 -> 50 [weight=2]
  66 -> 71
  66 -> 71 [label=">66", dir=forward, color=red, fontcolor=red, arrowhead=vee, weight=3]
  50 -> 37 [weight=4]
  50 -> 56 [weight=2]
  37 -> 36 [weight=4]
  37 -> 44
  36 -> 35
  36 -> v5
  56 -> 55
  56 -> v6 [weight=3]
  55 -> 52 
  55 -> v7
  52 -> 51
  52 -> v12
  71 -> 70
  71 -> 70 [label="<71", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  71 -> 81 [weight=5]
  70 -> 68 [xlabel="<70", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  70 -> 68
  70 -> v9 [weight=1]
  68 -> v13 [color=red, penwidth=6, weight=3]
  68 -> 69 [weight=3]
  81 -> 80
  81 -> 97 [weight=6]
  97 -> 94
  97 -> v10
  94 -> 88
  94 -> v11
  v1, v2, v3, v4, v5, v6, 
  v7, v8, v9, v10, v11, v12,v13 [color=none, fontcolor=none, style=none, label=""]
}
```

<figcaption>ABR <b>quelconque</b><br/>APRÈS Suppression de <span style="background-color:#FF0000;color:white;font-weight:700;border-radius:50%;padding:0.3em;">67</span><br/>La suppression d'une Feuille<br/>ne déséquillibre pas l'ABR<br/>(plus qu'il ne l'était déjà)</figcaption>

</center>

<env>**Suppression d'un noeud à $1$ seul enfant**</env> (Exemple: Suppression de $70$ )

<center>

```dot
digraph ABR {
  size="6"
  //splines=false
  node [shape=circle, style=filled, fillcolor="#CFFDFF", fixedsize=true, width=0.8, fontsize=38, penwidth=0]
  edge [fontsize=38, penwidth=3, dir=none]
  34 -> 22 [weight=2]
  34 -> 66
  34 -> 66 [xlabel=">34", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  22 -> 7 [weight=3]
  22 -> 29
  7 -> v1 [weight=4, color=none]
  7 -> 17
  17 -> 9 [weight=4]
  17 -> 21
  9 -> 8, 16
  29 -> 25 [weight=2]
  29 -> 32 [weight=3]
  25 -> 23 [weight=3]
  25 -> 28
  28 -> 27
  28 -> v2 [weight=2]
  27 -> 26
  27 -> v3
  32 -> 30
  32 -> v4 [weight=4]
  66 -> 50 [weight=2]
  66 -> 71
  66 -> 71 [label=">66", dir=forward, color=red, fontcolor=red, arrowhead=vee, weight=3]
  50 -> 37 [weight=4]
  50 -> 56 [weight=2]
  37 -> 36 [weight=4]
  37 -> 44
  36 -> 35
  36 -> v5
  56 -> 55
  56 -> v6 [weight=3]
  55 -> 52 
  55 -> v7
  52 -> 51
  52 -> v12
  71 -> 70
  71 -> 70 [label="<71", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  71 -> 81 [weight=5]
  70 -> 68 [weight=2]
  70 -> v9 [weight=1]
  68 -> 67 [weight=2]
  68 -> 69 [weight=3]
  81 -> 80
  81 -> 97 [weight=6]
  97 -> 94
  97 -> v10
  94 -> 88
  94 -> v11
  70 [shape=doublecircle, fillcolor=red, color=red, fontcolor=white, penwidth=2]
  68 [shape=doublecircle, fillcolor=deeppink, color=red, fontcolor=white, penwidth=2]
  v1, v2, v3, v4, v5, v6, 
  v7, v8, v9, v10, v11, v12,v13 [color=none, fontcolor=none, style=none, label=""]
}
```

<figcaption>ABR <b>quelconque</b><br/>BUT : Supprimer <span style="background-color:#FF0000;color:white;font-weight:700;border-radius:50%;padding:0.3em;">70</span><br/>

:one: Recherche de <span style="background-color:#FF0000;color:white;font-weight:700;border-radius:50%;padding:0.3em;">70</span> <br/>:two: Déterminer <span style="background-color:#FF00FF;color:white;font-weight:700;border-radius:50%;padding:0.3em;">68</span> = son unique enfant <br/>:three: On supprime le noeud</figcaption>

```dot
digraph ABR {
  size="6"
  //splines=false
  node [shape=circle, style=filled, fillcolor="#CFFDFF", fixedsize=true, width=0.8, fontsize=38, penwidth=0]
  edge [fontsize=38, penwidth=3, dir=none]
  34 -> 22 [weight=2]
  34 -> 66
  34 -> 66 [xlabel=">34", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  22 -> 7 [weight=3]
  22 -> 29
  7 -> v1 [weight=4, color=none]
  7 -> 17
  17 -> 9 [weight=4]
  17 -> 21
  9 -> 8, 16
  29 -> 25 [weight=2]
  29 -> 32 [weight=3]
  25 -> 23 [weight=3]
  25 -> 28
  28 -> 27
  28 -> v2 [weight=2]
  27 -> 26
  27 -> v3
  32 -> 30
  32 -> v4 [weight=4]
  66 -> 50 [weight=2]
  66 -> 71
  66 -> 71 [label=">66", dir=forward, color=red, fontcolor=red, arrowhead=vee, weight=3]
  50 -> 37 [weight=4]
  50 -> 56 [weight=2]
  37 -> 36 [weight=4]
  37 -> 44
  36 -> 35
  36 -> v5
  56 -> 55
  56 -> v6 [weight=3]
  55 -> 52 
  55 -> v7
  52 -> 51
  52 -> v12
  71 -> 68
  71 -> 68 [label="<71", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  71 -> 81 [weight=5]
  //70 -> 68 [weight=2]
  //70 -> v9 [weight=1]
  68 -> 67 [weight=2]
  68 -> 69 [weight=3]
  81 -> 80
  81 -> 97 [weight=6]
  97 -> 94
  97 -> v10
  94 -> 88
  94 -> v11
  68 [shape=doublecircle, fillcolor=red, color=red, fontcolor=white, penwidth=2]
  v1, v2, v3, v4, v5, v6, 
  v7, v8, v9, v10, v11, v12,v13 [color=none, fontcolor=none, style=none, label=""]
}
```

<figcaption>ABR <b>quelconque</b><br/>APRÈS Suppression de <span style="background-color:#FF0000;color:white;font-weight:700;border-radius:50%;padding:0.3em;">70</span><br/>La suppression d'un noeud à 1 seul enfant <br/>peut déséquillibrer l'ABR <br/>(plus qu'il ne l'était déjà)</figcaption>

</center>

<env>**Suppression d'un noeud à $2$ enfants**</env>

<center>

<span style="background-color:#5a9ac5;color:white;font-weight:700;padding:0.2em;border-radius:7px;box-shadow:#444 5px 5px 5px;">Suppression, puis Remplacement par le <span style="background-color:#0FFF00;padding:0.05em 0.2em;border-radius:5px;">max des min</span></span>

</center>

<center>

```dot
digraph ABR {
  size="6"
  //splines=false
  node [shape=circle, style=filled, fillcolor="#CFFDFF", fixedsize=true, width=0.8, fontsize=38, penwidth=0]
  edge [fontsize=38, penwidth=3, dir=none]
  34 -> 22 [weight=2]
  34 -> 66
  34 -> 66 [xlabel=">34", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  22 -> 7 [weight=3]
  22 -> 29
  7 -> v1 [weight=4, color=none]
  7 -> 17
  17 -> 9 [weight=4]
  17 -> 21
  9 -> 8, 16
  29 -> 25 [weight=2]
  29 -> 32 [weight=3]
  25 -> 23 [weight=3]
  25 -> 28
  28 -> 27
  28 -> v2 [weight=2]
  27 -> 26
  27 -> v3
  32 -> 30
  32 -> v4 [weight=4]
  66 -> 50
  66 -> 50 [label="<66", dir=forward, color=red, fontcolor=red, arrowhead=vee, weight=2]
  66 -> 71 [weight=4]
  50 -> 37 [weight=4]
  50 -> 56 [weight=2]
  37 -> 36 [weight=4]
  37 -> 44 [color=deeppink, penwidth=6]
  36 -> 35
  36 -> v5
  56 -> 55
  56 -> v6 [weight=3]
  55 -> 52 
  55 -> v7
  52 -> 51
  52 -> v12
  71 -> 70
  71 -> 81 [weight=5]
  70 -> 68 [weight=2]
  70 -> v9
  68 -> 67 [weight=2]
  68 -> 69 [weight=2]
  81 -> 80
  81 -> 97 [weight=5]
  97 -> 94
  97 -> v10
  94 -> 88
  94 -> v11
  50 [shape=doublecircle, fillcolor=red, color=red, fontcolor=white, penwidth=2]
  44 [shape=doublecircle, fillcolor=deeppink, color=red, fontcolor=white, penwidth=2]
  v1, v2, v3, v4, v5, v6, 
  v7, v8, v9, v10, v11, v12 [color=none, fontcolor=none, style=none, label=""]
}
```

<figcaption>ABR <b>quelconque</b><br/>BUT : Supprimer <span style="background-color:#FF0000;color:white;font-weight:700;border-radius:50%;padding:0.3em;">50</span><br/>

:one: Recherche de <span style="background-color:#FF0000;color:white;font-weight:700;border-radius:50%;padding:0.3em;">50</span><br/>:two: Déterminer <span style="background-color:#FF00FF;color:white;font-weight:700;border-radius:50%;padding:0.3em;">44</span> =  le <b>max des min</b></figcaption>

```dot
digraph ABR {
  size="6"
  //splines=false
  node [shape=circle, style=filled, fillcolor="#CFFDFF", fixedsize=true, width=0.8, fontsize=38, penwidth=0]
  edge [fontsize=38, penwidth=3, dir=none]
  34 -> 22 [weight=2]
  34 -> 66
  34 -> 66 [xlabel=">34", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  22 -> 7 [weight=3]
  22 -> 29
  7 -> v1 [weight=4, color=none]
  7 -> 17
  17 -> 9 [weight=4]
  17 -> 21
  9 -> 8, 16
  29 -> 25 [weight=2]
  29 -> 32 [weight=3]
  25 -> 23 [weight=3]
  25 -> 28
  28 -> 27
  28 -> v2 [weight=2]
  27 -> 26
  27 -> v3
  32 -> 30
  32 -> v4 [weight=4]
  66 -> 44
  66 -> 44 [label="<66", dir=forward, color=red, fontcolor=red, arrowhead=vee, weight=2]
  66 -> 71 [weight=4]
  44 -> 37 [weight=4]
  44 -> 56 [weight=2]
  37 -> 36 [weight=4]
  37 -> v13 [color=deeppink, penwidth=6]
  36 -> 35
  36 -> v5
  56 -> 55
  56 -> v6 [weight=3]
  55 -> 52 
  55 -> v7
  52 -> 51
  52 -> v12
  71 -> 70
  71 -> 81 [weight=5]
  70 -> 68 [weight=2]
  70 -> v9
  68 -> 67 [weight=2]
  68 -> 69 [weight=2]
  81 -> 80
  81 -> 97 [weight=5]
  97 -> 94
  97 -> v10
  94 -> 88
  94 -> v11
  44 [shape=doublecircle, fillcolor=deeppink, color=red, fontcolor=white, penwidth=2]
  v1, v2, v3, v4, v5, v6, 
  v7, v8, v9, v10, v11, v12,v13 [color=none, fontcolor=none, style=none, label=""]
}
```

<figcaption>ABR <b>quelconque</b><br/>APRÈS Suppression de <span style="background-color:#FF0000;color:white;font-weight:700;border-radius:50%;padding:0.3em;">50</span><br/>+ Remplacement par <span style="background-color:#FF00FF;color:white;font-weight:700;border-radius:50%;padding:0.3em;">44</span> =  le <b>max des min</b><br/>La suppression d'un noeud à 2 enfants <br/>peut déséquillibrer l'ABR <br/>(plus qu'il ne l'était déjà)</figcaption>

</center>

<center>

<span style="background-color:#5a9ac5;color:white;font-weight:700;padding:0.2em;border-radius:7px;box-shadow:#444 5px 5px 5px;">Suppression, puis Remplacement par le <span style="background-color:#0FFF00;padding:0.1em 0.2em;border-radius:5px;">min des max</span></span>

</center>

<center>

```dot
digraph ABR {
  size="6"
  //splines=false
  node [shape=circle, style=filled, fillcolor="#CFFDFF", fixedsize=true, width=0.8, fontsize=38, penwidth=0]
  edge [fontsize=38, penwidth=3, dir=none]
  34 -> 22 [weight=2]
  34 -> 66
  34 -> 66 [xlabel=">34", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  22 -> 7 [weight=3]
  22 -> 29
  7 -> v1 [weight=4, color=none]
  7 -> 17
  17 -> 9 [weight=4]
  17 -> 21
  9 -> 8, 16
  29 -> 25 [weight=2]
  29 -> 32 [weight=3]
  25 -> 23 [weight=3]
  25 -> 28
  28 -> 27
  28 -> v2 [weight=2]
  27 -> 26
  27 -> v3
  32 -> 30
  32 -> v4 [weight=4]
  66 -> 50
  66 -> 50 [label="<66", dir=forward, color=red, fontcolor=red, arrowhead=vee, weight=2]
  66 -> 71 [weight=4]
  50 -> 37 [weight=4]
  50 -> 56 [weight=2]
  37 -> 36 [weight=4]
  37 -> 44
  36 -> 35
  36 -> v5
  56 -> 55
  56 -> v6 [weight=3]
  55 -> 52 
  55 -> v7
  52 -> 51 [color=deeppink, penwidth=6]
  52 -> v12
  71 -> 70
  71 -> 81 [weight=5]
  70 -> 68 [weight=2]
  70 -> v9
  68 -> 67 [weight=2]
  68 -> 69 [weight=2]
  81 -> 80
  81 -> 97 [weight=5]
  97 -> 94
  97 -> v10
  94 -> 88
  94 -> v11
  50 [shape=doublecircle, fillcolor=red, color=red, fontcolor=white, penwidth=2]
  51 [shape=doublecircle, fillcolor=deeppink, color=red, fontcolor=white, penwidth=2]
  v1, v2, v3, v4, v5, v6, 
  v7, v8, v9, v10, v11, v12 [color=none, fontcolor=none, style=none, label=""]
}
```

<figcaption>ABR <b>quelconque</b><br/>BUT : Supprimer <span style="background-color:#FF0000;color:white;font-weight:700;border-radius:50%;padding:0.3em;">50</span><br/>

:one: Recherche de <span style="background-color:#FF0000;color:white;font-weight:700;border-radius:50%;padding:0.3em;">50</span><br/>:two: Déterminer <span style="background-color:#FF00FF;color:white;font-weight:700;border-radius:50%;padding:0.3em;">51</span> =  le <b>min des max</b></figcaption>

```dot
digraph ABR {
  size="6"
  //splines=false
  node [shape=circle, style=filled, fillcolor="#CFFDFF", fixedsize=true, width=0.8, fontsize=38, penwidth=0]
  edge [fontsize=38, penwidth=3, dir=none]
  34 -> 22 [weight=2]
  34 -> 66
  34 -> 66 [xlabel=">34", dir=forward, color=red, fontcolor=red, arrowhead=vee]
  22 -> 7 [weight=3]
  22 -> 29
  7 -> v1 [weight=4, color=none]
  7 -> 17
  17 -> 9 [weight=4]
  17 -> 21
  9 -> 8, 16
  29 -> 25 [weight=2]
  29 -> 32 [weight=3]
  25 -> 23 [weight=3]
  25 -> 28
  28 -> 27
  28 -> v2 [weight=2]
  27 -> 26
  27 -> v3
  32 -> 30
  32 -> v4 [weight=4]
  66 -> 51
  66 -> 51 [label="<66", dir=forward, color=red, fontcolor=red, arrowhead=vee, weight=2]
  66 -> 71 [weight=4]
  51 -> 37 [weight=4]
  51 -> 56 [weight=2]
  37 -> 36 [weight=4]
  37 -> 44
  36 -> 35
  36 -> v5
  56 -> 55
  56 -> v6 [weight=3]
  55 -> 52 
  55 -> v7
  52 -> v13 [color=deeppink, penwidth=6]
  52 -> v12
  71 -> 70
  71 -> 81 [weight=5]
  70 -> 68 [weight=2]
  70 -> v9
  68 -> 67 [weight=2]
  68 -> 69 [weight=2]
  81 -> 80
  81 -> 97 [weight=5]
  97 -> 94
  97 -> v10
  94 -> 88
  94 -> v11
  51 [shape=doublecircle, fillcolor=deeppink, color=red, fontcolor=white, penwidth=2]
  v1, v2, v3, v4, v5, v6, 
  v7, v8, v9, v10, v11, v12, v13 [color=none, fontcolor=none, style=none, label=""]
}
```

<figcaption>ABR <b>quelconque</b><br/>APRÈS Suppression de <span style="background-color:#FF0000;color:white;font-weight:700;border-radius:50%;padding:0.3em;">50</span><br/>+ Remplacement par <span style="background-color:#FF00FF;color:white;font-weight:700;border-radius:50%;padding:0.3em;">51</span> =  le <b>min des max</b><br/>La suppression d'un noeud à 2 enfants <br/>peut déséquillibrer l'ABR <br/>(plus qu'il ne l'était déjà)</figcaption>

</center>

!!! mth "Suppression d'un noeud dans un ABR quelconque"
    La Suppression d'un noeud d'un ABR quelconque se fait en deux étapes:  
    :one: On Recherche la clé dans l'ABR : Dans le pire des cas, en $O(h)$   
    :two: On le Supprime : Dans le pire des cas, en $O(1)$. Trois situations sont possibles:

    * Suppression d'une feuille : Il suffit de l'enlever de l'arbre puisqu'elle n'a pas de fils.
    * Suppression d'un noeud avec un enfant : Il faut l'enlever de l'arbre en le remplaçant par son unique fils (gauche ou droit).
    * Suppression d'un noeud $N$ avec deux enfants : Dans ce cas, pour préserver la structure d'arbre ABR après la suppression du noeud $N$, on peut :
        * Remplacer $N$ par le plus grand des nombres plus petits que lui (le **max des min**): càd par le noeud le plus à droite du sous-arbre gauche de $N$.  Ou bien :
        * Remplacer $N$ par le plus petit des nombres plus grands que lui (le **min des max**): càd par le noeud le plus à gauche du sous-arbre droit de $N$.

!!! pte "Complexité de la Suppression dans un ABR quelconque"
    Dans un ABR quelconque, dans le pire des cas, La complexité de la **Suppression** est en <enc>$O(h)$</enc>

<env>**Remarque**</env>

<div id="conteneur">

<div>

<center>

```dot
graph ABR {
  size="1.4"
  node [shape=circle, fixedsize=true, width=0.8,fontsize=30, penwidth=3]
  edge [penwidth=3]
  10 -- {6, 12}
  6 -- 4
  6 -- v1 [color=none]
  v1 [color=none, fontcolor=none, label=""]
}
```

<figcaption>AVANT suppression de 12 :<br/>ABR <b>équilibré</b></figcaption>

</center>

</div>

<div>

<center>

```dot
graph ABR {
  size="1.4"
  node [shape=circle, fixedsize=true, width=0.8,fontsize=30, penwidth=3]
  edge [penwidth=3]
  10 -- 6
  10 -- v1 [color=none]
  6 -- 4
  6 -- v2 [color=none]
  v1, v2 [color=none, fontcolor=none, label=""]
}
```

<figcaption>APRÈS suppression de 12 :<br/>ABR <b>déséquilibré</b></figcaption>

</center>

</div>

</div>

* **En choisissant correctement le remplaçant du noeud supprimé** (par le max des min, ou par le min des max), la **Suppression** d'une clé dans un ABR est compatible avec la structure d'ABR, autrement dit : Après Suppression d'une clé, un arbre ABR reste un arbre ABR.
* :warning: **ATTENTION** :warning: Par contre, La Suppression d'une clé **peut casser l'équilibre** dans un ABR


### Résumé des Opérations dans un ABR quelconque

!!! pte
    **Dans un ABR quelconque**, on peut calculer la complexité **dans le pire des cas**, pour chacune des opérations suivantes :

    * La **Recherche** d'une clé est en <enc>$O(h)$</enc> $\,\,$ où $h$ désigne la hauteur de l'ABR
    * L'**Insertion** d'une clé est en <enc>$O(h)$</enc>
    * La **Suppression** d'une clé est en <enc>$O(h)$</enc>

<env>**Conséquences**</env>
Pour la Recherche, l'Insertion, ou la Suppression :

* Pour les ABR équilibrés (= Arbres AVL, cf le § ci-après) : en $O(\log_2 n)$
* pour les ABR déséquilibrés : en $O(n)$

