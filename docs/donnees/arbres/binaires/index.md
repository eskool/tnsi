# TNSI : Arbres Binaires (AB)

## Définitions

!!! def
    Un <bred>Arbre Binaire (AB)</bred> est un arbre dont chaque noeud a **au plus** deux fils, généralement ordonnés:

    * le <bred>fils gauche</bred> ou <bred>enfant gauche</bred> (éventuellement vide) et
    * le <bred>fils droit</bred> ou <bred>enfant droit</bred> (éventuellement vide)

<center>

```dot
graph arbre {
  size=1.5
  node [shape=circle, fontsize=30, penwidth=2];
  edge [penwidth=2]
  T -- { Y, O}
  Y -- P
  Y -- H [color=none]
  O -- { H, N}
  }
```

**Arbre 1 :** Arbre Binaire Enraciné, étiqueté avec des Lettres

```dot
graph arbre {
  size=2
  node [shape=circle, fontsize=30, penwidth=2];
  edge [penwidth=2]
  N -- { Y, O1}
  Y -- D [weight=2]
  Y -- P
  O1 -- T
  O1 -- H [weight=2]
  O1 [label="O"]
  D -- C [weight=3]
  D -- O2
  O2 [label="O"]
  "" [color=none]
  P -- "" [color=none]
  P -- E
  }
```

**Arbre 2 :** Arbre Binaire Enraciné, étiqueté avec des Lettres

</center>

## Arbres Binaires Équilibrés

### Définition

!!! def "Arbre Binaire Équilibré, Version 1"
    Un Arbre Binaire est <bred>Équilibré</bred> $\Leftrightarrow$ pour chaque noeud, **les hauteurs des sous-arbres gauche (SAG) et sous-arbre droit (SAD) ne diffèrent au plus que de $1$**

Intuitivement, un **arbre binaire équilibré**, ou **arbre binaire à critère d'équilibre**, est un arbre binaire qui maintient une *certaine forme d'équilibre* :

* dans la répartition entre ses fils Gauche et Droit : comprendre entre le côté gauche et le côté droit (de chaque noeud) de l'arbre, et
* dans la profondeur entre chacune de ses branches

!!! info :warning: **ATTENTION** :warning:
    Le terme **équilibré**  peut avoir des significations variables selon les auteurs. Toujours Bien Lire la définition donnée.

### Cas de Figure Arbes Binaires Équilibrés. Justifiez

<div id="conteneur">

<div>

```dot
graph AB {
  size="1.3"
  node [label="", shape=circle, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  b -- {d, e}
}
```

<figcaption>Arbre Binaire <br/>équilibré<br/></figcaption>

</div>

<div>

```dot
graph AB {
  size="1.8"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  c -- h
  c -- i [weight=2]
  b -- d [weight=2]
  b -- e
  d -- {f, g}

}
```

<figcaption>Arbre Binaire <br/>équilibré</figcaption>

</div>

<div>

```dot
graph AB {
  size="1.8"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  c -- v1 [color=none]
  //c -- e [color=none]
  c -- i [weight=2]
  b -- d [weight=2]
  b -- e
  d -- f
  d -- g
  v1 [color=none, fontcolor=none]
}
```

<figcaption>Arbre Binaire <br/>équilibré</figcaption>

</div>

<div>

```dot
graph AB {
  size="1.8"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  c -- h
  c -- i [weight=2]
  b -- d [weight=2]
  b -- e
  d -- f [weight=2]
  d -- g
  d -- v1 [color=none]
  i -- j
  i -- v2 [color=none]
  v1, v2 [color=none, fontcolor=none]
}
```

<figcaption>Arbre Binaire <br/>équilibré</figcaption>

</div>

<div>

```dot
graph AB {
  size="2.5"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  b -- v2 [color=none] 
  c -- {h, i}
  c -- v3 [color=none]
  b -- {d, e}
  e -- v1 [color=none]
  e -- m
  d -- f [weight=3]
  d -- g
  i -- j
  i -- v4 [color=none]
  k [color=none, fontcolor=none]
  i -- k [label=" ", color=none]
  f -- v5 [color=none]
  f -- l
  v1, v2, v3, v4, v5 [color=none, fontcolor=none]
}
```

<figcaption>Arbre Binaire <br/>équilibré</figcaption>

</div>

</div>

### Cas de Figure Arbes Binaires Non Équilibrés. Justifiez

<div id="conteneur">

<div>

```dot
graph AB {
  size="2.2"
  node [label="", shape=circle, penwidth=2]
  edge [penwidth=2]
  a -- {b, v1}
  b -- {c, v2}
  c -- {d, v3}
  d -- {v4, v5}
  v1, v2, v3, v4, v5 [color=none, fontcolor=none]
}
```

<figcaption>Arbre Binaire <br/>Non équilibré<br/><em>"Arbre Peigne"</em></figcaption>

</div>

<div>

```dot
graph AB {
  size="1.8"
  node [label="", shape=circle, penwidth=2]
  //node [shape=circle, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  b -- {d, e}
  d -- {f, g}
}
```

<figcaption>Arbre Binaire <br/>Non équilibré</figcaption>

</div>

<div>

```dot
graph AB {
  size="1.8"
  node [label="", shape=circle, penwidth=2]
  //node [shape=circle, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  c -- {h, i}
  b -- d
  b -- h [color=none]
  d -- {f, g}
  i -- j
  k [color=none, fontcolor=none]
  i -- k [color=none]
}
```

<figcaption>Arbre Binaire <br/>Non équilibré</figcaption>

</div>

<div>

```dot
graph AB {
  size="2.1"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  b -- d
  b -- h
  d -- f [weight=2]
  d -- g
  c -- h [color=none]
  c -- i
  h -- {j, k}
  i -- l
  i -- m [weight=2]

}
```

<figcaption>Arbre Binaire <br/>Non équilibré</figcaption>

</div>

<div>

```dot
graph AB {
  size="2.8"
  node [label="", shape=circle, penwidth=2]
  //node [shape=circle, penwidth=2]
  edge [penwidth=2]
  1 -- {2, 3}
  2 -- v10 [color=none]
  v10 [color=none, fontcolor=none] 
  2 -- 4
  2 -- v9 [color=none]
  2 -- 5
  4 -- {v1, v7} [color=none]
  4 -- 6
  6 -- 7
  6 -- v2 [color=none]
  5 -- 8
  5 -- {v3} [color=none]
  3 -- 9
  3 -- {v4, v8} [color=none]
  9 -- v5 [color=none]
  9 -- 10
  10 -- 11
  10 -- v6 [color=none]
  v1, v2, v3, v4, v5, v6, v7, v8, v9, v10 [color=none, fontcolor=none]
}
```

<figcaption>Arbre Binaire <br/>Non équilibré</figcaption>

</div>

</div>


### Hauteur Logarithmique d'un Arbre Binaire Équilibré

!!! pte
    Un Arbre Binaire **Équilibré** admet une <bred>hauteur logarithmique</bred> en la taille $n$ de l'arbre :
    càd que la hauteur $h$ d'un Arbre Binaire **Équililibré** est proportionnelle à $\log_2(n)$
    càd que dans un arbre Binaire **Équilibré**, il existe une constante $C$ telle que :
    <center>
    <enc>$h\le C\times \log_2 (n)$</enc>
    </center>

!!! ex "Hauteur d'un Arbre Binaire Quelconque"
    1. Remarquons que l'arbre binaire soit équilibré est un argument nécessaire dans cette propriété, sinon cette propriété est fausse. Pourquoi?
    2. Quelle est la hauteur $h$ d'un arbre binaire peigne de taille $n$ ? (en fonction de $n$)
    3. Dans un **arbre binaire quelconque** de taille $n$, combien vaut la hauteur $h$ (en fonction de $n$) ...
        * dans le meilleur des cas? (i.e. la plus **petite** hauteur possible -à proportionnalité près-)
        * dans le pire des cas? (i.e. la plus **grande** hauteur possible)
    4. En déduire un encadrement, en fonction de $n$, (à proportionnalité près pour le minimum) pour la hauteur $h$ d'un arbre binaire quelconque:  

    $$minimum(n)? \le h \le maximum(n)?$$

### Généralisation de la Définition d'un Arbre Binaire Équilibré

En fait, il existe **plusieurs critères d'*équilibre*** pour les Arbres Binaires, certains plus restrictifs que d'autres :

<env>**Voici Deux Critères d'*Équilibre* Classiques**</env>

* Un ***critère d'équilibre <bred>partiel</bred>*** qui permettra de définir les ***Arbres Binaires AVL = Arbres Binaires de Recherche Équilibrés*** comme ceci :
    * Pour chaque noeud, les hauteurs des sous-arbres gauche (SAG) et sous-arbre droit (SAD) ne diffèrent au plus que de $1$
* Un ***critère d'équilibre <bred>total</bred>*** qui permettra de définir les **Arbres Binaires Complets (à gauche)**, comme ceci :
    * tous les niveaux de l'Arbre Binaire sauf (peut-être) le dernier sont ***totalement remplis/complets***
    * les noeuds du dernier niveau sont situés/entassés **à gauche** (sans aucun "trou" entre le 1er et le dernier noeud du dernier niveau)

Ces deux notions ont en commun, en tant que conséquence, de garantir une ***hauteur logarithmique*** de l'arbre binaire, càd que la hauteur $h$ de l'arbre (binaire) est proportionnelle au $\log_2$ de la taille $n$ de l'arbre. Ce qui est finalement la seule chose qui importe vraiment pour optimiser (ici minimiser) la "taille" (comprendre la *hauteur*) de la structure de données devant contenir $n$ objets. C'est pourquoi certains auteurs choisissent la définition suivante, plus générale, pour un arbre binaire **équilibré** :

!!! def "Arbre Binaire Équilibré, Version 2 : plus générale"
    Un Arbre Binaire est <bred>Équilibré</bred> $\Leftrightarrow$ la **hauteur est logarithmique** en la taille $n$ de l'arbre
    $\Leftrightarrow$ la hauteur $h$ est proportionnelle à $\log_2 (n)$

Sauf mention contraire, nous choisirons toujours la Version 1 (un peu plus générale) comme définition d'un arbre binaire équilibré, et qui implique que la hauteur d'un Arbre Binaire équilibré soit logarithmique.

## Arbres Binaires Complets

!!! def
    Un Arbre Binaire est <bred>Complet (à gauche)</bred> $\Leftrightarrow$

    * tous les niveaux de l'Arbre Binaire sont parfaitement remplis, **sauf (peut-être) le dernier niveau**
    * les noeuds du dernier niveau sont tous situés **à gauche** (sans aucun "trou" entre le 1er et le dernier noeud du dernier niveau)

!!! info "Remarque"
    :warning: **ATTENTION** :warning: Le terme **complet** peut avoir des significations variables selon les auteurs. Toujours Bien Lire la définition donnée.

### Cas de Figure : Arbres Binaires Complets. Justifiez

<div id="conteneur">

<div>

```dot
graph AB {
  size="1"
  node [label="", shape=circle, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  b -- d
  b -- v1 [color=none]
  v1 [color=none,fontcolor=none]
}
```

<figcaption>Arbre Binaire<br/>Complet<br/>(donc équilibré)</figcaption>

</div>

<div>

```dot
graph AB {
  size="1"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  b -- {d, e}
}
```

<figcaption>Arbre Binaire<br/>Complet<br/>(donc équilibré)</figcaption>

</div>

<div>

```dot
graph AB {
  size="1.5"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  b -- d [weight=2]
  b -- e
  c -- f
  c -- {v1,v2} [color=none]
  v1, v2 [color=none,fontcolor=none]
}
```

<figcaption>Arbre Binaire<br/>Complet<br/>(donc équilibré)</figcaption>

</div>

<div>

```dot
graph AB {
  size="1.5"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  c -- h
  c -- i [weight=2]
  b -- d [weight=2]
  b -- e
  d -- {f, g}
}
```

<figcaption>Arbre Binaire<br/>Complet<br/>(donc équilibré)</figcaption>

</div>

<div>

```dot
graph AB {
  size="1.6"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  c -- f
  c -- g [weight=2]
  b -- d [weight=2]
  b -- e
  d -- h [weight=3]
  d -- i
  e -- j
  e -- v1 [color=none, weight=3]
  v1, v2 [color=none, fontcolor=none]
}
```

<figcaption>Arbre Binaire<br/>Complet<br/>(donc équilibré)</figcaption>

</div>

<div>

```dot
graph AB {
  size="1.8"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  c -- f
  c -- g [weight=2]
  b -- d [weight=2]
  b -- e
  d -- h [weight=3]
  d -- i
  e -- j
  e -- k [weight=2]
  f -- l
  f -- m [weight=2]
}
```

<figcaption>Arbre Binaire<br/>Complet<br/>(donc équilibré)</figcaption>

</div>

</div>

### Cas de Figure : Arbres Binaires Non Complets. Justifiez

<div id="conteneur">

<div>

```dot
graph AB {
  size="1"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  b -- v1 [color=none]
  b -- d
  v1, v2 [color=none, fontcolor=none]
}
```

<figcaption>Arbre Binaire<br/>Non Complet<br/>(mais équilibré)</figcaption>

</div>

<div>

```dot
graph AB {
  size="1"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  b -- d
  b -- v1 [color=none]
  c -- v1 [color=none]
  c -- e
  v1 [color=none, fontcolor=none]
}
```

<figcaption>Arbre Binaire<br/>Non Complet<br/>(mais équilibré)</figcaption>

</div>

<div>

```dot
graph AB {
  size="1"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  b -- d
  b -- e [color=none]
  c -- e
  c -- f
}
```

<figcaption>Arbre Binaire<br/>Non Complet<br/>(mais équilibré)</figcaption>

</div>

<div>

```dot
graph AB {
  size="1.2"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- b
  a -- c
  b -- d [weight=2]
  b -- e
  c -- v1 [color=none]
  c -- f [weight=2]
  v1, v2 [color=none,fontcolor=none]
}
```

<figcaption>Arbre Binaire<br/>Non Complet<br/>(mais équilibré)</figcaption>

</div>

<div>

```dot
graph AB {
  size="1.5"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  c -- v1 [color=none]
  c -- i [weight=2]
  b -- d [weight=2]
  b -- e
  d -- {f, g}
  v1 [color=none, fontcolor=none]
}
```

<figcaption>Arbre Binaire<br/>Non Complet<br/>(mais équilibré)</figcaption>

</div>

<div>

```dot
graph AB {
  size="1.5"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  c -- f
  c -- g [weight=2]
  b -- d [weight=2]
  b -- e
  d -- h [weight=3]
  d -- i
  e -- v1 [color=none]
  e -- j [weight=2]
  v1 [color=none, fontcolor=none]
}
```

<figcaption>Arbre Binaire<br/>Non Complet<br/>(mais équilibré)</figcaption>

</div>

<div>

```dot
graph AB {
  size="1.5"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  c -- h
  c -- i [weight=2]
  b -- d [weight=2]
  b -- e
  d -- f [weight=2]
  d -- g
  d -- v2 [color=none]
  i -- j
  v1, v2 [color=none, fontcolor=none]
  i -- v1 [color=none]
}
```

<figcaption>Arbre Binaire <br/>Non Complet <br/>(mais équilibré)</figcaption>

</div>

<div>

```dot
graph AB {
  size="2.1"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}
  b -- v2 [color=none] 
  c -- {h, i}
  c -- v3 [color=none]
  b -- d
  b -- e
  e -- m
  e -- v1 [weight=3, color=none]
  d -- f [weight=2]
  d -- g
  g -- l
  g -- v2 [weight=3, color=none]
  i -- j
  i -- v4 [color=none]
  i -- v5 [label=" ", color=none]
  v1, v2, v3, v4, v5 [color=none, fontcolor=none]
}
```

<figcaption>Arbre Binaire <br/>Non Complet<br/>(mais équilibré)</figcaption>

</div>

</div>

## Arbres Binaires Parfaits

!!! def
    Un Arbre Binaire est <bred>parfait</bred> lorsque tous ses niveaux sont parfaitement remplis (absolument aucun noeux manquant)

<div id="conteneur">

<div>

```dot
graph AB {
  size="0.2"
  node [label="", shape=circle, penwidth=2]
  edge [penwidth=2]
  a

}
```

<figcaption>Arbre<br/> Binaire<br/>Parfait</figcaption>

</div>

<div>

```dot
graph AB {
  size="0.4"
  node [label="", shape=circle, penwidth=2]
  edge [penwidth=2]
  a -- {b, c}

}
```

<figcaption>Arbre<br/>Binaire<br/>Parfait</figcaption>

</div>

<div>

```dot
graph AB {
  size="0.8"
  node [label="", shape=circle, fontsize=28, penwidth=2]
  //node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -- b, c
  b -- d [weight=2]
  b -- e
  c -- g
  c -- h [weight=2]
}
```

<figcaption>Arbre Binaire<br/>Parfait</figcaption>

</div>

<div>

```dot
graph AB {
  size="1.8"
  node [label="", shape=circle, fontsize=30, penwidth=2]
  //node [shape=circle, fontsize=30, penwidth=2]
  edge [penwidth=2]
  1 -- 2, 3
  2 -- 4 [weight=2]
  2 -- 5
  3 -- 6
  3 -- 7 [weight=2]
  4 -- 8 [weight=3]
  4 -- 9
  5 -- 10
  5 -- 11 [weight=2]
  6 -- 12 [weight=2]
  6 -- 13
  7 -- 14
  7 -- 15 [weight=3]
}
```

<figcaption>Arbre Binaire<br/>Parfait</figcaption>

</div>

<div>

```dot
graph AB {
  size="3.5"
  node [label="", shape=circle, fontsize=30, penwidth=2]
  //node [shape=circle, fontsize=30, penwidth=2]
  edge [penwidth=2]
  1 -- 2, 3
  2 -- 4 [weight=2]
  2 -- 5
  3 -- 6
  3 -- 7 [weight=2]
  4 -- 8 [weight=3]
  4 -- 9
  5 -- 10
  5 -- 11 [weight=2]
  6 -- 12 [weight=2]
  6 -- 13
  7 -- 14
  7 -- 15 [weight=3]
  8 -- 16 [weight=4]
  8 -- 17
  9 -- 18
  9 -- 19 [weight=2]
  10 -- 20 [weight=2]
  10 -- 21
  11 -- 22
  11 -- 23 [weight=3]
  12 -- 24 [weight=3]
  12 -- 25
  13 -- 26
  13 -- 27 [weight=2]
  14 -- 28 [weight=2]
  14 -- 29
  15 -- 30
  15 -- 31 [weight=4]
}
```

<figcaption>Arbre Binaire<br/>Parfait</figcaption>

</div>

</div>

## Implémentations des Arbres Binaires

Plusieurs implémentations sont possibles : tuples, listes et dictionnaires Python, ou bien POO

### Implémentation par Tuples Python

* Un Arbre Binaire peut être implémenté par une imbrication de plusieurs triplets `(s, t1, t2)` où l'on a :
* Chaque noeud de l'arbre Binaire est représenté par tuple `t = (s, t1, t2)` représente un noeud pour lequel :
    * `s` est l'étiquette du noeud
    * `t1` est un tuple représentant le **SAG** Sous-Arbre Gauche (éventuellement vide)
    * `t2` est un tuple représentant le  **SAD** Sous-Arbre Droit (éventuellement vide)

<div id="conteneur">

<div style="flex:4">

```python
t = ('F', ('R',('I', (), ()), ()), ('A', ('S', (), ()), ('E', (), ())))
# ou bien, plus simplement, en remplaçant :
# ('I', (), ()) par 'I'   et
# ('S', (), ()) par 'S'   et
# ('E', (), ()) par 'E'
t = ('F', ('R','I', ()), ('A', 'S', 'E'))
```

</div>

<div style="font-size:2em;margin-top:1em;margin-left:1em;">$$\Leftarrow$$</div>

<div style="flex:2">

```dot
graph AB {
  size="1.5"
  node [shape=circle, fontsize=30, penwidth=2]
  edge [penwidth=2]
  F -- R
  F -- A
  R -- I 
  R -- S [color=none]
  A -- S
  A -- E 
  v1 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Arbre 1</figcaption>

</div>

</div>

<env>**Arbre Binaire Vide**</env>  
Noter que cette implémentation des AB revient à noter l'arbre Binaire vide :

```python
()              # comme le tuple vide, plutôt que comme...
((), (), ())    # un triplet de tuples vides...
```

### Implémentation par Listes Python

Le même type d'implémentation peut se faire avec des listes Python : cela revient à remplacer toutes les `()` par des `[]`
**L'habitude veut que l'on préfère réserver les listes Python pour des collections d'objets de même type**.
Mais c'est néanmoins parfaitement possible.

### Implémentation par Dictionnaires Python

Un Arbre Binaire peut être implémenté par un dictionnaire `{s0: [fg0, fd0], s1: [fg1, fd1], ...}` où l'on a :

* `s0`, `s1`, ... sont les étiquettes des noeuds
* `fg0`, `fg1`, ... sont les **clés des fils gauches** (éventuellement vides : `''`)
* `fd0`, `fd1`, ... sont les **clés des fils droits** (éventuellement vides : `''`)

<div id="conteneur">

<div style="flex:4;">

```python
ab = {
  'F' : ['R', 'A'],
  'R' : ['I', ''],
  'I' : ['', ''],
  'A' : ['S', 'E'],
  'S' : ['', ''],
  'E' : ['', '']
}
```

</div>

<div style="float:left;font-size:2em;margin-top:2em;margin-left:2em;">

$\Leftarrow$

</div>

<div style="flex:2;">

<center>

```dot
graph AB {
  size="1.5"
  node [shape=circle, fontsize=30, penwidth=2]
  edge [penwidth=2]
  F -- R
  F -- A
  R -- I 
  R -- S [color=none]
  A -- S
  A -- E 
  v1 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Arbre 1</figcaption>

</center>

</div>

</div>


### Implémentation POO avec une Classe `Noeud`

<div id="conteneur">

<div style="flex:4;">

```python
class Noeud:
  """Un Noeud d'un Arbre Binaire"""
  def __init__(self, valeur, gauche=None, droit=None):
    self.valeur = valeur
    self.gauche = gauche
    self.droit = droit
```

</div>

<div style="float:left;font-size:2em;margin-top:2em;margin-left:2em;">

$\Leftarrow$

</div>

<div style="flex:2;">

<center>

```dot
graph AB {
  size="1.5"
  node [shape=circle, fontsize=30, penwidth=2]
  edge [penwidth=2]
  F -- R
  F -- A
  R -- I 
  R -- S [color=none]
  A -- S
  A -- E 
  v1 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Arbre 1</figcaption>

</center>

</div>

</div>

Un objet de cette classe contient trois attributs:

* un attribut ***gauche*** pour modéliser le **sous-arbre gauche**
* un attribut ***valeur*** pour modéliser l'**étiquette du noeud**
* un attribut ***droit*** pour modéliser le **sous-arbre droit**

Pour construire un arbre, il suffit d'instancier la classe `Noeud` autant de fois qu'il y a de noeuds dans l'arbre :

```python
>>> ab = Noeud("F", 
      Noeud('R', Noeud('I'), None),
      Noeud('A', Noeud('S'), Noeud('E'))
    )
```

* L'arbre Binaire vide est modélisé par `None`, qui n'est donc pas, en particulier, une instance de la classe `Noeud`
* Si le noeud `n` est une feuille, Alors :
    * `n.gauche = None` donc vide
    * `n.droit = None` donc vide

### Implémentation POO avec une Classe `AB`

Voici un début d'implémentation d'une classe AB modélisant un <b>A</b>rbre <b>B</b>inaire en POO :

<div id="conteneur">

<div style="flex:4;">

```python
class AB:
  """Un AB = Arbre Binaire"""
  def __init__(self, valeur, gauche=None, droit=None):
    self.valeur = valeur
    self.gauche = gauche
    self.droit = droit

  def set_gauche(self, gauche=None):
    self.gauche = gauche

  def get_gauche(self):
    return self.gauche

  def set_droit(self, droit=None):
    self.droit = droit

  def get_droit(self):
    return self.droit

  def set_valeur(self, valeur):
    self.valeur = valeur

  def get_valeur(self):
    return self.valeur
```

</div>

<div style="font-size:2em;">$$\Leftarrow$$</div>

<div style="flex:3;">

```dot
graph AB {
  size="3"
  node [shape=circle, fontsize=30, penwidth=2]
  edge [penwidth=2]
  F -- R
  F -- A
  R -- I 
  R -- S [color=none]
  A -- S
  A -- E 
  v1 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Arbre 1</figcaption>

</div>

</div>

Un objet de cette classe contient trois attributs:

* un attribut ***gauche*** pour modéliser le **sous-arbre gauche**
* un attribut ***valeur*** pour modéliser l'**étiquette du noeud**
* un attribut ***droit*** pour modéliser le **sous-arbre droit**

Créer un Arbre Binaire avec uniquement une racine `'F'` :

```python
>>> ab = AB('F')
```

Pour ajouter/supprimer des noeuds dans l'arbre, il faudrait créer les méthodes adaptées. Cf le TD Arbres Binaires.

<env>Arbre Binaire Vide</env>
Remarquer qu'un Arbre Binaire Vide est ici implémenté par `None`, donc en particulier n'est PAS une instance de la classe AB.

## Parcours d'Arbres Binaires

!!! def
    Un <bred>Parcours d'Arbre</bred> définit un ordre dans lequel on parcoure (la totalité de) ses noeuds

### Parcours en Largeur

!!! def "Parcours en Largeur"
    Dans un <bred>Parcours en Largeur</bred> :  

    * l'arbre est parcouru **niveau par niveau** : 
        * niveau 0: la racine, puis 
        * niveau 1: les fils de la racine, puis 
        * niveau 2: les petits-fils de la racine,
        * etc...
        * jusqu'au dernier niveau: les feuilles  
    * Chaque étage est quant à lui parcouru **de gauche à droite**

### Parcours en Profondeur

!!! def "(+ Exercice) Parcours en Profondeur"
    Dans un <bred>Parcours en Profondeur</bred> : Pour chaque noeud, **l'un des deux sous-arbres est d'abord parcouru entièrement**, avant de commencer à explorer le deuxième sous-arbre.  

On distingue $3$ types de **parcours en profondeur** selon l'ordre relatif dans lequel est explorée  <env><bblue>la Racine</bblue></env> par rapport au fils Gauche et au fils Droit:

#### Parcours Préfixe

!!! def "Parcours Préfixe, ou Parcours Préordre"
    Le <bred>Parcours Préfixe</bred> ou <bred>Parcours Préordre</bred> est caractérisé par <env>**<bblue>Racine</bblue> -> Gauche -> Droit**</env>  
    Chaque noeud est visité **avant** que ses fils le soient.  
    On part de la racine, puis on visite le fils gauche, puis le fils droit.  
    Parcours Préfixe Arbre 1: **T-Y-P-O-H-N**  

<center>

```dot
graph AB {
  size="2"
  node [shape=circle, penwidth=2]
  edge [penwidth=2]
  T -- {Y, O}
  Y -- P
  O -- {H, N}
  Y -- H [color=none]
}
```

<figcaption>Arbre 1</figcaption>

</center>

!!! ex
    === "Énoncé"
        Déterminer le parcours préfixe de l'Arbre 2 ci-dessous: \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_
    === "Corrigé"
        ```console linenums="0"
        N-Y-D-C-O-P-E-O-T-H
        ```

<center>

```dot
graph AB {
  size="2.5"
  node [shape=circle, fontsize=40, penwidth=3]
  edge [penwidth=3]
  N -- {Y, O1}
  //Y -- {D, P}
  Y -- D [weight=2]
  Y -- P
  O1 -- T
  O1 -- H [weight=2]
  D -- C [weight=3]
  D -- O2
  P -- E
  P -- v1 [color=none, fontcolor=none, weight=3]
  O1, O2 [label="O"]
  v1 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Arbre 2</figcaption>

</center>

#### Parcours Infixe

!!! def "Parcours Infixe ou Parcours en ordre"
    Le <bred>Parcours Infixe</bred> ou <bred>Parcours en ordre</bred> est caractérisé par <env>**Gauche -> <bblue>Racine</bblue> -> Droit**</env>  
    Chaque noeud est visité **après** son fils gauche mais avant son fils droit.  
    Parcours Infixe Arbre 1: **P-Y-T-H-O-N**  

<center>

```dot
graph AB {
  size="2"
  node [shape=circle, penwidth=2]
  edge [penwidth=2]
  T -- {Y, O}
  Y -- P
  O -- {H, N}
  Y -- H [color=none]
}
```

<figcaption>Arbre 1</figcaption>

</center>

!!! ex
    === "Énoncé"
        Déterminer le parcours infixe de l'Arbre 2 ci-dessous : \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_
    === "Corrigé"
        ```console linenums="0"
        C-D-O-Y-E-P-N-T-O-H
        ```

<center>

```dot
graph AB {
  size="2.5"
  node [shape=circle, fontsize=40, penwidth=3]
  edge [penwidth=3]
  N -- {Y, O1}
  //Y -- {D, P}
  Y -- D [weight=2]
  Y -- P
  O1 -- T
  O1 -- H [weight=2]
  D -- C [weight=3]
  D -- O2
  P -- E
  P -- v1 [color=none, fontcolor=none, weight=3]
  O1, O2 [label="O"]
  v1 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Arbre 2</figcaption>

</center>

#### Parcours Postfixe ou Parcours Postordre

!!! def "Parcours Postfixe ou Parcours Postordre"
    Le <bred>Parcours Postfixe</bred> ou <bred>Parcours Postordre</bred> est caractérisé par <env>**Gauche -> Droit -> <bblue>Racine</bblue>**</env>  
    Chaque noeud est visité **après** que ses fils Gauche et Droit aient été visités.  
    Parcours Postfixe Arbre 1: **P-Y-H-N-O-T**  

<center>

```dot
graph AB {
  size="2"
  node [shape=circle, penwidth=2]
  edge [penwidth=2]
  T -- {Y, O}
  Y -- P
  O -- {H, N}
  Y -- H [color=none]
}
```

<figcaption>Arbre 1</figcaption>

</center>

!!! ex
    === "Énoncé"
        Déterminer le parcours postfixe de l'Arbre 2 ci-dessous : \_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_
    === "Corrigé"
        ```console linenums="0"
        C-O-D-E-P-Y-T-H-O-N
        ```

<center>

```dot
graph AB {
  size="2.5"
  node [shape=circle, fontsize=40, penwidth=3]
  edge [penwidth=3]
  N -- {Y, O1}
  //Y -- {D, P}
  Y -- D [weight=2]
  Y -- P
  O1 -- T
  O1 -- H [weight=2]
  D -- C [weight=3]
  D -- O2
  P -- E
  P -- v1 [color=none, fontcolor=none, weight=3]
  O1, O2 [label="O"]
  v1 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Arbre 2</figcaption>

</center>


