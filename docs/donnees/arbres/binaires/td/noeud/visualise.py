  ########################################################################
  ## RENDU DOT ET IMAGE
  ########################################################################

def toDot(self, filename="test.dot"):
    """Crée un fichier graph .dot, par défaut test.dot
    """
    assert ".dot" == filename[-4:], "filename must contain '.dot' extension"
    # print("dot Filename = ",filename)
    with open(filename,"w") as fichier:
        fichier.write("graph G {\n node [shape=circle]\n")
        for s in self.sommets():
            sommet = str(s)+";\n"
            fichier.write(sommet)
        for (s,v) in self.aretes():
          arete = str(s)+" -- "+str(v)+" [style=bold];\n"
          fichier.write(arete)
        fichier.write("}")

def extraire_nom_sans_extension(self, filename="test.dot"):
  """Enlève l'extension '.dot' au nom de fichier"""
  assert type(filename) is str, "Erreur : le nom de fichier DOIT être une chaîne STR"
  assert ".dot" == filename[-4:], "Erreur : le nom de fichier DOIT finir par une extension '.dot'"
  return filename[:-4]

def renderPicture(self, filename="test.dot"):
    name = self.extraire_nom_sans_extension(filename)
    # print("name = ",name)
    import os
    os.system(f"dot -Tpng {name}.dot -o {name}.png")
    from PyQt5 import QtGui, QtWidgets
    app = QtWidgets.QApplication([])
    window = QtWidgets.QWidget()
    HLayout = QtWidgets.QHBoxLayout()
    pixmap = QtGui.QPixmap(name)
    label = QtWidgets.QLabel()
    label.setPixmap(pixmap)
    HLayout.addWidget(label)
    window.setLayout(HLayout)
    window.setWindowTitle("Mon Beau Graphe!")
    window.show()
    app.exec_()

def show(self, name="test"):
  self.toDot(f"{name}.dot")
  self.renderPicture(f"{name}.dot")