# from ceFichier import cetteClasse
from File_par_Tableau import File

class Noeud:
    """Un Noeud d'un Arbre Binaire"""

    def __init__(self, valeur, gauche=None, droit=None):
        self.valeur = valeur
        self.gauche = gauche
        self.droit = droit
        self.parent = None
        if self.gauche is not None:
            self.gauche.parent = self
        elif self.droit is not None:
            self.droit.parent = self
        Noeud.niv = []
        Noeud.large = []
        Noeud.pref = []
        Noeud.inf = []
        Noeud.post = []

    def get_gauche(self):
        self.gauche.parent = self
        return self.gauche

    def set_gauche(self, valeur):
        self.gauche = Noeud(valeur)
        self.gauche.parent = self
        return self.gauche

    def get_droit(self):
        self.droit.parent = self
        return self.droit

    def set_droit(self, valeur):
        self.droit = Noeud(valeur)
        self.droit.parent = self
        return self.droit

    def get_valeur(self):
        return self.valeur

    def vers_tuple(self):
        if self is None:
            return None
        elif self.gauche is None and self.droit is None:
            return (self.valeur,)
        elif self.gauche is None:  # self.droit is not None
            return (self.valeur, None, self.droit.vers_tuple())
        elif self.droit is None:  # self.gauche is not None
            return (self.valeur, self.gauche.vers_tuple(), None)
        else:  # self is not None, self.gauche is not None, self.droit is not None
            return (self.valeur, self.gauche.vers_tuple(), self.droit.vers_tuple())

    def est_vide(self):
        # if self.valeur is None:
        #     return True
        # else:
        #     return False
        return self.valeur is None

    def est_feuille(self):
        # if self.gauche is None and self.droit is None:
        #   return True
        # else:
        #     return False
        return self.gauche is None and self.droit is None

    def get_racine(self):
        if self is None:
            return None
        if self.parent is None:
            return self
        else:
            return self.parent.get_racine()

    def profondeur(self):
        if self is self.get_racine():
            return 0
        else:
            return 1+self.parent.profondeur()

    def hauteur(self):
      if self is None:
        return -1
      elif self.gauche is None and self.droit is None: # ici self is not None
          return 0
      elif self.gauche is None: # ici self.droit is not None
          return 1+self.droit.hauteur()
      elif self.droit is None: # ici self.gauche is not None
          return 1+self.gauche.hauteur()
      else: # self.gauche is not None ET self.droit is not None
        return 1 + max(self.gauche.hauteur(), self.droit.hauteur())

    def taille(self):
        if self is None:
            return 0
        elif self.gauche is None and self.droit is None:
            return 1
        elif self.gauche is None:
            return 1 + self.droit.taille()
        elif self.droit is None:
            return 1 + self.gauche.taille()
        else:
            return 1 + self.gauche.taille() + self.droit.taille()

    def niveau(self, niveau):
        if self is None:
            return None
        elif niveau == 0:
            Noeud.niv.append(self.valeur)
        elif niveau>0:
            if self.gauche is None and self.droit is None:
                return None
            elif self.gauche is None:
                self.droit.niveau(niveau-1)
            elif self.droit is None:
                self.gauche.niveau(niveau-1)
            else:
                self.gauche.niveau(niveau-1)
                self.droit.niveau(niveau-1)

    def get_niveau(self, niveau):
        Noeud.niv = []
        self.niveau(niveau)
        return Noeud.niv

    def largeur(self):
        for i in range(self.hauteur()+1):
            Noeud.large.extend(self.get_niveau(i))
            Noeud.niv = []

    def get_largeur(self):
        Noeud.large = []
        self.largeur()
        # myList = Noeud.large
        # return myList
        return Noeud.large

    def largeurF(self):
        f = File([])
        f.enfile(self.valeur)
        i=1
        while f.est_vide() == False:
            if i<=self.hauteur():
                for a in range(len(self.get_niveau(i))):
                    f.enfile(self.get_niveau(i)[a])
                i+=1
            if i>self.hauteur():
                return f

    # def largeurF(self):
    #     f = File()
    #     # f.enfile(self.get_racine().valeur)
    #     f.enfile(self.valeur)
    #     while not (f.est_vide()):
    #         noeud = Noeud(f.defile())
    #         # noeud = self
    #         f.defile()
    #         Noeud.large.append(noeud.valeur)
    #         if noeud.gauche is not None:
    #             print("gauche not None")
    #             f.enfile(self.gauche.valeur)
    #             # noeud = self.gauche
    #         if noeud.droit is not None:
    #             print("droit not None")
    #             f.enfile(self.droit.valeur)
    #             # noeud = self.droit
    #         print(f)

    def get_largeurF(self):
        Noeud.large = []
        self.largeurF()
        # myList = Noeud.large
        # return myList
        return Noeud.large

    def prefixe(self):
        Noeud.pref.append(self.valeur)
        if self.gauche is not None:
            self.gauche.prefixe()
        if self.droit is not None:
            self.droit.prefixe()

    def get_prefixe(self):
        Noeud.pref = []
        self.prefixe()
        # myList = Noeud.pref
        # return myList
        return Noeud.pref

    def infixe(self):
        if self.gauche is not None:
            self.gauche.infixe()
        Noeud.inf.append(self.valeur)
        if self.droit is not None:
            self.droit.infixe()

    def get_infixe(self):
        Noeud.inf = []
        self.infixe()
        # myList = Noeud.inf
        # return myList
        return Noeud.inf

    def postfixe(self):
        if self.gauche is not None:
            self.gauche.postfixe()
        if self.droit is not None:
            self.droit.postfixe()
        Noeud.post.append(self.valeur)

    def get_postfixe(self):
        Noeud.post = []
        self.postfixe()
        # myList = Noeud.post
        # return myList
        return Noeud.post

    def __repr__(self):
        return str(self.vers_tuple())

if __name__ == "__main__":
    # Arbre du TD
    F = Noeud("F")
    A = F.set_gauche("A")
    C = F.set_droit("C")
    D = C.set_droit("D")
    E = D.set_gauche("E")
    B = A.set_droit("B")
    H = C.set_gauche("H")
    print("F=",F)
    print("====================")
    print("RACINE DE H=", H.get_racine().valeur)
    print("RACINE DE C=", C.get_racine().valeur)
    print("====================")
    print("HAUTEUR DE F:",F.hauteur())
    print("HAUTEUR DE A:",A.hauteur())
    print("TAILLE DE F:",F.taille())
    print("====================")
    print("NIVEAU 0", C.get_niveau(0))
    print("NIVEAU 1", C.get_niveau(1))
    print("NIVEAU 2", C.get_niveau(2))
    print("====================")
    print("PARCOURS EN LARGEUR DE F:", F.get_largeur())
    print("PARCOURS EN LARGEUR DE C:", C.get_largeur())
    print("====================")

    print("A:",A)
    print("PROFONDEUR DE F:",F.profondeur())
    print("PROFONDEUR DE C:",C.profondeur())
    print("PROFONDEUR DE D:",D.profondeur())
    # print("PROFONDEUR DE E:",E.profondeur())
    print("HAUTEUR DE F:",F.hauteur())
    print("HAUTEUR DE A:",A.hauteur())
    print("TAILLE DE F:",F.taille())
    print("NIVEAU 0", C.get_niveau(0))
    print("NIVEAU 1", C.get_niveau(1))
    print("NIVEAU 2", C.get_niveau(2))

    print("PARCOURS EN LARGEUR DE F:", F.get_largeur())
    print("PARCOURS EN LARGEUR DE C:", C.get_largeur())
    print("====================")
    print("GET_PREFIXE DE F:",F.get_prefixe())
    print("GET_PREFIXE DE C:",C.get_prefixe())
    print("GET_INFIXE DE F:",F.get_infixe())
    print("GET_INFIXE DE C:",C.get_infixe())
    print("GET_POSTFIXE DE F:",F.get_postfixe())
    print("GET_POSTFIXE DE C:",C.get_postfixe())

    print("PARCOURS EN LARGEUR FILES DE F:", F.get_largeurF())
    print("PARCOURS EN LARGEUR FILES DE C:", C.get_largeurF())
    # print("====================")
    F.prefixe()
    print("PARCOURS PROFONDEUR PREFIXE DE F + F.pref:",F.pref)
    F.infixe()
    print("PARCOURS PROFONDEUR INFIXE DE F + F.inf:",F.inf)
    F.postfixe()
    print("PARCOURS PROFONDEUR POSTFIXE DE F+ F.post:",F.post)

    print("====================")
    print("GET_PREFIXE DE F:",F.get_prefixe())
    print("GET_INFIXE DE F:",F.get_infixe())
    print("GET_POSTFIXE DE F:",F.get_postfixe())

    #Arbre du Cours N1
    # T = Noeud("T")
    # Y = T.set_gauche("Y")
    # O = T.set_droit("O")
    # P = Y.set_gauche("P")
    # H = O.set_gauche("H")
    # N = O.set_droit("N")
    # print("PARCOURS EN LARGEUR DE T:", T.get_largeur())
    # print("PARCOURS EN LARGEUR DE O:", O.get_largeur())
    # print("====================")
    # print("GET_PREFIXE DE T:",T.get_prefixe())
    # print("GET_PREFIXE DE O:",O.get_prefixe())
    # print("GET_INFIXE DE T:",T.get_infixe())
    # print("GET_INFIXE DE O:",O.get_infixe())
    # print("GET_POSTFIXE DE T:",T.get_postfixe())
    # print("GET_POSTFIXE DE O:",O.get_postfixe())

    #Arbre du Cours N2
    # N = Noeud("N")
    # Y = N.set_gauche("Y")
    # O = N.set_droit("O")
    # D = Y.set_gauche("D")
    # P = Y.set_droit("P")
    # T = O.set_gauche("T")
    # H = O.set_droit("H")
    # C = D.set_gauche("C")
    # O = D.set_droit("O")
    # E = P.set_gauche("E")

    # print("PARCOURS EN LARGEUR DE N:", N.get_largeur())
    # print("PARCOURS EN LARGEUR DE Y:", Y.get_largeur())
    # print("====================")
    # print("GET_PREFIXE DE N:",N.get_prefixe())
    # print("GET_PREFIXE DE Y:",Y.get_prefixe())
    # print("GET_INFIXE DE N:",N.get_infixe())
    # print("GET_INFIXE DE Y:",Y.get_infixe())
    # print("GET_POSTFIXE DE N:",N.get_postfixe())
    # print("GET_POSTFIXE DE T:",Y.get_postfixe())
