# TNSI : TD Arbres Binaires, `class AB`

On rappelle qu'un **AB - Arbre Binaire** peut être défini **récursivement** par :

* une `racine`
* un **Sous-Arbre (Binaire) Gauche (SAG)**
* un **Sous-Arbre (Binaire) Droit (SAD)**

On donne ci-dessous, le début d'une implémentation d'une classe `AB` qui modélise un **Arbre Binaire** en POO. Dans la modélisation proposée, l'instance `AB(t)` de la classe `AB` modélise l'Arbre Binaire qui peut être modélisé par le tuple `t` en Python.  

## Un Exemple & Code minimal

L'Arbre Binaire ci-dessous, peut être modélisé par :

* le tuple `t` suivant :  
`t = ('F', ('A', None, ('B',)), ('C', ('H',), ('D', ('E',), None)))` en Python.
* le code minimal situé encore plus bas.

<center>

```dot
graph G {
    node [shape=circle]
    F -- A
    A -- A0 [weight=2, color=none, fontcolor=none]
    A0 [label="", color=none, fontcolor=none]
    A -- B
    F -- C
    C -- H
    C -- D [weight=2]
    D -- E
    D -- E0 [color=none, fontcolor=none]
    E0 [label="", color=none, fontcolor=none]
}
```

</center>

Les instructions sous le `__name__` donnent quelques exemples/indications sur la manière dont on souhaite pouvoir instancier et utiliser la classe `AB`:

```python
class AB:
    """Un AB = Arbre Binaire"""
    def __init__(self, t=None):
        assert t is None or len(t) == 1 or len(t) == 3, "La modélisation en Python d'un arbre par un tuple doit être : soit vide (None), soit une feuille (un tuple avec une unique valeur str), soit avoir deux enfants (des tuples (racine, sag, sad) )"
        if t is None:
            self = # À compléter
        elif len(t) == 1:
            self.racine = # À compléter
            self.gauche = # À compléter
            self.droit = # À compléter
            self.parent = # À compléter
        else: # len(t) == 3:
            self.racine = # À compléter
            self.gauche = # À compléter
            self.droit =# À compléter
            self.parent = # À compléter
            # À compléter: lignes supplémentaires

    def get_gauche(self):
        return self.gauche

    def set_gauche(self, valeur):
        # À Compléter

    def get_droit(self):
        return self.droit

    def set_droit(self, valeur):
        # À Compléter

    def get_valeur(self):
        return self.valeur

if __name__ == "__main__":
    t = ('F', ('A', None, ('B',)), ('C', ('H',), ('D', ('E',), None)))
    F = AB(t=t)
    print("AB=", F)
    A = F.get_gauche()
    print("SOUS-ARBRE SOUS LE NOEUD A = ", A)
    print("RACINE DEPUIS A = ", A.racine)
    C = F.get_droit()
    print("SOUS-ARBRE SOUS LE NOEUD C = ", C)
    print("RACINE DEPUIS A = ", C.racine)
    # Pour ajouter 'X' en tant qu'un enfant gauche de 'A':
    X = A.set_gauche("X")
    print("SOUS-ARBRE SOUS LE NOEUD X = ", X)
    print("RACINE DEPUIS X =", X.racine)
```

!!! info "Arbre Binaire Vide"
    * Remarquer les valeurs par défaut des Sous-Arbres Gauche (`gauche=None`) et/ou Droit (`droit=None`) de la classe `AB`. Ces conventions reviennent à implémenter un **Arbre Binaire Vide** par `None`, donc en particulier dans notre implémentation : **un Arbre Binaire Vide n'est PAS une instance de la classe Arbre Binaire** mais un objet `None` dont le type est `NoneType` en Python.

## Logique de l'Arbre Binaire avec une classe `AB`

1°) Compléter les lignes `# À compléter` du constructeur `__init__(self, t:tuple)` de sorte que l'instance `AB(t)` modélise l'Arbre Binaire dont une modélisation en Python, sous forme de type de donnée primitif, soit le tuple `t`. Le constructeur `__init__` devra/pourra s'appeler **récursivement** (On pourra pour cela, instancier la classe `AB` depuis le constructeur __init__)

2°) Créer une méthode `set_gauche(self, valeur)` qui :

* Crée un arbre **gauche** dont l' (unique) étiquette vaut `valeur`
* définit l'arbre **gauche** en tant que sous-arbre gauche de (la racine de) l'arbre `self`
* renvoie l'arbre nouvellement créé (**gauche**), càd renvoie une instance de la classe `AB`

3°) Créer une méthode `set_droit(self, valeur)` qui 

* Crée un arbre **droit** dont l' (unique) étiquette vaut `valeur`
* définit l'arbre **droit** en tant que sous-arbre droit de (la racine de) l'arbre `self`
* renvoie l'arbre nouvellement créé (**droit**), càd renvoie une instance de la classe `AB`

4°) Créer une méthode `vers_tuple(self)` qui représente l'arbre Binaire comme un tuple  
  Exemple : La feuille `'F'` pourra se représenter:  

* `('F', None, None)`, ou bien, plus simplement
* `('F',)` (idéalement)

5°) En déduire une méthode `__repr__(self)` pour représenter un arbre Binaire sous forme de tuple dans le Terminal
NB : Vérifier que vous obtenez bien le tuple `t=('F', ('A', None, ('B',)), ('C', ('H',), ('D', ('E',), None)))` après la première instanciation `AB(t)`.

6°) Créer une méthode `est_vide(self)->bool` qui renvoie:

* `True` si le noeud (`self`) est vide (`None`)
* `False` sinon

7°) Créer une méthode `est_feuille(self)->bool` qui renvoie:

* `True` si le noeud (`self`) est une feuille
* `False` sinon

8°) Créer une méthode `get_racine(self)` qui renvoie la racine (absolue) de l'arbre auquel *appartient* le noeud `self` (en remontant tous les noeuds parents de `self`).  
Remarque : La racine d'un arbre binaire est:

* ou bien un noeud (non vide), si l'Arbre Binaire n'est pas vide: en pratique on renvoie l'instance de la classe `AB` dont le noeud en question est la racine
* ou bien `None`, si l'Arbre Binaire est vide

!!! hint "Hint / Aide"
    On pourra pour cela, ajouter judicieusement un atttribut `self.parent` aux bons endroits (par ex. dans les méthodes `get_gauche()`, `set_gauche()`, `get_droit()`, `set_droit()`, etc..), pour être en mesure à tout instant de déterminer le noeud (/arbre binaire) parent de tout noeud (arbre binaire).

## Paramètres de l'Arbre Binaire

9°) Créer une méthode `profondeur(self)->int` qui renvoie la profondeur du noeud courant `self` (par rapport à la racine absolue de l'arbre obtenur par `get_racine()` )

10°) Créer une méthode `hauteur(self)->int` qui calcule la hauteur de l'arbre sous le noeud `self`

11°) Créer une méthode `taille(self)->int` qui renvoie la taille de l'arbre sous le noeud `self`

## Parcours d'Arbres Binaires

### Parcours en Largeur

#### Implémentation avec une procédure récursive `niveau()`

12°) a°) Créer une **procédure récursive** `niveau(self, niveau)->None` qui :

* reçoive en entrée un noeud `self` et le `niveau` souhaité (relatif au noeud `self`)
* affiche dans le Terminal (les étiquettes des noeuds d') situés au niveau `niveau` depuis le noeud `self` (convention: la racine est située au niveau `0` et pas `1`)

b°) Modifier la procédure récursive précédente `niveau(self)->None` de sorte que:

* elle reçoive en entrée un noeud `self`
* à la fin de son exécution, la variable de classe `Noeud.niveau` doit la liste ordonnée des étiquettes des noeuds rencontrés dans l'ordre d'un parcours en largeur.  
!!! col __03
    <!-- NE PAS EFFACER : POUR DÉCALER DE 3% -->

!!! hint "Hint / Aide"
    On pourra par exemple (ou pas, si vous trouvez mieux..) définir une variable de classe, dans le constructeur `__init__()`, pour ensuite y stocker les (étiquettes des) noeuds rencontrés durant le parcours:  
    ```python linenums="0"
    # Syntaxe variable de classe:
    Noeud.niv = []
    ```

13°) a°) En déduire une **méthode récursive** `get_niveau(self)->list` telle que:

* reçoit en entrée un noeud `self`
* renvoie en sortie une liste des étiquettes des noeuds rencontrés lors d'un parcours en largeur depuis le noeud `self`

b°) Stocker une liste d'étiquettes plutôt qu'une liste de noeuds, lors d'un parcours, peut poser un problème dans un contexte général.

* Pourquoi?
* Comment résoudre ce problème? Faites-le

14°) En déduire une **procédure** (itérative) `largeur(self)->None` telle que:

* elle reçoive en entrée un noeud `self`
* à la fin de son exécution, la variable de classe `Noeud.largeur` contienne une liste des étiquettes des noeuds rencontrés dans l'ordre d'un parcours en largeur de l'arbre, depuis le noeud `self`
!!! col __03
    <!-- NE PAS EFFACER : POUR DÉCALER DE 3% -->

!!! hint "Hint / Aide"
    On pourra par exemple (ou pas, si vous trouvez mieux..) définir une variable de classe, dans le constructeur `__init__()`, pour ensuite y stocker les (étiquettes des) noeuds rencontrés durant le parcours:  
    ```python linenums="0"
    # Syntaxe variable de classe:
    Noeud.large = []
    ```

15°) En déduire une **méthode** (itérative) `get_largeur(self)->list` telle que:

* Elle reçoive en entrée un noeud `self`
* Elle renvoie en sortie une liste des étiquettes des noeuds rencontrés lors d'un parcours en largeur depuis le noeud `self`

#### Implémentation avec une File

16°) Créer une méthode itérative `largeur(self)` qui réalise un parcours en largeur de l'arbre depuis le noeud `self`

L'algorithme de parcours en largeur itératif peut s'implémenter grâce à des Files.

```console
largeur(self) {
   f = FileVide
   f.enfiler(self.racine())
   Tant que (la File f n'est pas vide) {
       noeud = f.defiler()
       Visiter(noeud)                        //On choisit de faire une opération
       Si (noeud.gauche n'est pas vide) Alors
           f.enfiler(noeud.gauche)
       Si (noeud.droit n'est pas vide) Alors
           f.enfiler(noeud.droit)
   }
}
```

### Parcours Préfixe (en Profondeur)

17°) a°) Créer une **procédure récursive** `prefixe(self)->None` qui :

* reçoive en entrée un noeud `self`
* affiche dans le Terminal (les étiquettes des noeuds d') un parcours en profondeur préfixe de l'arbre depuis le noeud `self`  

b°) Modifier la procédure (itérative) précédente `prefixe(self)->None` de sorte que:

* elle reçoive en entrée un noeud `self`
* à la fin de son exécution, la variable de classe `Noeud.pref` contienne une liste des étiquettes des noeuds rencontrés dans l'ordre d'un parcours préfixe, depuis le noeud `self`  
!!! col __03
    

!!! hint "Hint / Aide"
    On pourra par exemple (ou pas, si vous trouvez mieux..) définir une variable de classe, dans le constructeur `__init__()`, pour ensuite y stocker les (étiquettes des) noeuds rencontrés durant le parcours:  
    ```python linenums="0"
    # Syntaxe variable de classe:
    Noeud.pref = []
    ```

18°) a°) En déduire une **méthode récursive** `get_prefixe(self)->list` qui:

* reçoit en entrée un noeud `self`
* renvoie en sortie la liste des étiquettes des noeuds rencontrés dans l'ordre d'un parcours préfixe, depuis le noeud `self`

b°) Stocker une liste d'étiquettes plutôt qu'une liste de noeuds, lors d'un parcours, peut poser un problème dans un contexte général.

* Pourquoi?
* Comment résoudre ce problème? Faites-le

### Parcours Infixe (en Profondeur)

19°) a°) Créer une **procédure récursive** `infixe(self)->None` qui :

* reçoive en entrée un noeud `self`
* affiche dans le Terminal (les étiquettes des noeuds d') un parcours en profondeur infixe de l'arbre depuis le noeud `self`  

b°) Modifier la procédure (itérative) précédente `infixe(self)->None` de sorte que:

* elle reçoive en entrée un noeud `self`
* à la fin de son exécution, la variable de classe `Noeud.inf` contienne une liste des étiquettes des noeuds rencontrés dans l'ordre d'un parcours infixe, depuis le noeud `self`  
!!! col __03
    

!!! hint "Hint / Aide"
    On pourra par exemple (ou pas, si vous trouvez mieux..) définir une **variable de classe**, dans le constructeur `__init__()`, pour ensuite y stocker les (étiquettes des) noeuds rencontrés durant le parcours:  
    ```python linenums="0"
    # Syntaxe variable de classe:
    Noeud.inf = []
    ```

20°) a°) En déduire une **méthode récursive** `get_infixe(self)->list` qui:

* reçoit en entrée un noeud `self`
* renvoie en sortie la liste des étiquettes des noeuds rencontrés dans l'ordre d'un parcours infixe, depuis le noeud `self`

b°) Stoker une liste d'étiquettes des noeuds plutôt qu'une liste de noeuds, lors d'un parcours, pose toujours le même problème que le parcours préfixe. Résolvez ce problème.

### Parcours Postfixe (en Profondeur)

21°) a°) Créer une **procédure récursive** `postfixe(self)->None` qui :

* reçoive en entrée un noeud `self`
* affiche dans le Terminal (les étiquettes des noeuds d') un parcours en profondeur postfixe de l'arbre depuis le noeud `self`  

b°) Modifier la procédure (itérative) précédente `postfixe(self)->None` de sorte que:

* elle reçoive en entrée un noeud `self`
* à la fin de son exécution, la variable de classe `Noeud.post` contienne une liste des étiquettes des noeuds rencontrés dans l'ordre d'un parcours postfixe, depuis le noeud `self`  
!!! col __03
    

!!! hint "Hint / Aide"
    On pourra par exemple (ou pas, si vous trouvez mieux..) définir une variable de classe, dans le constructeur `__init__()`, pour ensuite y stocker les (étiquettes des) noeuds rencontrés durant le parcours:  
    ```python linenums="0"
    # Syntaxe variable de classe:
    Noeud.post = []
    ```

22°) a°) En déduire une **méthode récursive** `get_postfixe(self)->list` qui:

* reçoit en entrée un noeud `self`
* renvoie en sortie la liste des étiquettes des noeuds rencontrés dans l'ordre d'un parcours postfixe, depuis le noeud `self`

b°) Stoker une liste d'étiquettes des noeuds plutôt qu'une liste de noeuds, lors d'un parcours, pose toujours les mêmes problèmes que les précédent parcours. Pourquoi? et Résolvez ce problème.