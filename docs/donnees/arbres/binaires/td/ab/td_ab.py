class AB:
    """Un AB = Arbre Binaire"""
    def __init__(self, t=None):
        # print("racine=",racine)
        # print("t=",t)
        # assert type(t) is tuple, "Paramètre `t=` (tuple) obligatoire"
        assert t is None or len(t) == 1 or len(t) == 3, "La modélisation en Python d'un arbre par un tuple doit être : soit vide (None), soit une feuille (un tuple avec une unique valeur str), soit avoir deux enfants (des tuples (racine, sag, sad) )"
        if t is None: # fils gauche ou droit d'une feuille
            self = None
        elif len(t) == 1: # Feuilles
            self.racine = t[0]
            self.gauche = None
            self.droit = None
            self.parent = None
        else: # len(t) == 3:
            self.racine = AB(t[0])
            self.parent = None
            self.gauche = None if t[1] is None else self.set_gauche(t[1])
            if t[1] is not None:
                self.gauche.parent = self
            self.droit = None if t[2] is None else self.set_droit(t[2])
            if t[2] is not None:
                self.droit.parent = self
        AB.niv = []
        AB.large = []
        AB.pref = []
        AB.inf = []
        AB.post = []

    def get_gauche(self):
        self.gauche.parent = self
        return self.gauche

    def set_gauche(self, valeur):
        self.gauche = AB(valeur)
        self.gauche.parent = self
        return self.gauche

    def get_droit(self):
        self.droit.parent = self
        return self.droit

    def set_droit(self, valeur):
        self.droit = AB(valeur)
        self.droit.parent = self
        return self.droit

    def get_racine_courante(self):
        return self.racine

    def vers_tuple(self):
        if self is None:
            return None
        elif self.gauche is None and self.droit is None:
            return (self.racine,)
        elif self.gauche is None:  # self.droit is not None
            return (self.racine, None, self.droit.vers_tuple())
        elif self.droit is None:  # self.gauche is not None
            return (self.racine, self.gauche.vers_tuple(), None)
        else:  # self is not None, self.gauche is not None, self.droit is not None
            return (self.racine, self.gauche.vers_tuple(), self.droit.vers_tuple())

    def est_vide(self):
        # if self.valeur is None:
        #     return True
        # else:
        #     return False
        return self.racine is None

    def est_feuille(self):
        # if self.gauche is None and self.droit is None:
        #   return True
        # else:
        #     return False
        return self.gauche is None and self.droit is None

    def get_racine(self):
        # print("FROM GET RACINE")
        # print("SELF=", self)
        # print("SELF PARENT=", self.parent)
        if self is None:
            return None
        if self.parent is None:
            # return None
            return self
        else:
            return self.parent.get_racine()

    def profondeur(self):
        # print("SELF=", self)
        # print("SELF.GET_RACINE=", self.get_racine())
        if self is self.get_racine():
            return 0
        else:
            return 1+self.parent.profondeur()

    def hauteur(self):
      if self is None:
        return -1
      elif self.gauche is None and self.droit is None: # ici self is not None
          return 0
      elif self.gauche is None: # ici self.droit is not None
          return 1+self.droit.hauteur()
      elif self.droit is None: # ici self.gauche is not None
          return 1+self.gauche.hauteur()
      else: # self.gauche is not None ET self.droit is not None
        return 1 + max(self.gauche.hauteur(), self.droit.hauteur())

    def taille(self):
        if self is None:
            return 0
        elif self.gauche is None and self.droit is None:
            return 1
        elif self.gauche is None:
            return 1 + self.droit.taille()
        elif self.droit is None:
            return 1 + self.gauche.taille()
        else:
            return 1 + self.gauche.taille() + self.droit.taille()

    # REMARQUE : De manière théorique, dans la méthode niveau(), on voudrait écrire:
    # AB.niv.append(self.racine)
    # Mais dans notre implémentation, la Racine self.racine est un str si self n'est pas une feuille, 
    # tandis que (par ex.) self.racine = ('A',) lorsque self est une feuille: 
    # c'est pourquoi il faut lui préférer self.racine.racine = 'A' dans ce cas
    def niveau(self, niveau):
        if self is None:
            return None
        elif niveau == 0:
            AB.niv.append(self.racine) if type(self.racine) is str else AB.niv.append(self.racine.racine)
        elif niveau>0:
            if self.gauche is None and self.droit is None:
                return None
            elif self.gauche is None:
                self.droit.niveau(niveau-1)
            elif self.droit is None:
                self.gauche.niveau(niveau-1)
            else:
                self.gauche.niveau(niveau-1)
                self.droit.niveau(niveau-1)

    def get_niveau(self, niveau):
        AB.niv = []
        self.niveau(niveau)
        return AB.niv

    def largeur(self):
        for i in range(self.hauteur()+1):
            AB.large.extend(self.get_niveau(i))
            AB.niv = []

    def get_largeur(self):
        AB.large = []
        self.largeur()
        myList = AB.large
        return myList

    # REMARQUE : De manière théorique, dans la méthode prefixe(), on voudrait écrire:
    # AB.pref.append(self.racine)
    # Mais dans notre implémentation, la Racine self.racine est un str si self n'est pas une feuille, 
    # tandis que (par ex.) self.racine = ('A',) lorsque self est une feuille: 
    # c'est pourquoi il faut lui préférer self.racine.racine = 'A' dans ce cas
    def prefixe(self):
        AB.pref.append(self.racine) if type(self.racine) is str else AB.pref.append(self.racine.racine)
        # AB.pref.append(self.racine)
        if self.gauche is not None:
            self.gauche.prefixe()
        if self.droit is not None:
            self.droit.prefixe()

    def get_prefixe(self):
        AB.pref = []
        self.prefixe()
        myList = AB.pref
        return myList

    # REMARQUE : De manière théorique, dans la méthode infixe(), on voudrait écrire :
    # AB.inf.append(self.racine)
    # Mais dans notre implémentation, la Racine self.racine est un str si self n'est pas une feuille, 
    # tandis que (par ex.) self.racine = ('A',) lorsque self est une feuille: 
    # c'est pourquoi il faut lui préférer self.racine.racine = 'A' dans ce cas
    def infixe(self):
        if self.gauche is not None:
            self.gauche.infixe()
        AB.inf.append(self.racine) if type(self.racine) is str else AB.inf.append(self.racine.racine)
        if self.droit is not None:
            self.droit.infixe()

    def get_infixe(self):
        AB.inf = []
        self.infixe()
        myList = AB.inf
        return myList

    # REMARQUE : De manière théorique, dans la méthode infixe(), on voudrait écrire :
    # AB.post.append(self.racine)
    # Mais dans notre implémentation, la Racine self.racine est un str si self n'est pas une feuille, 
    # tandis que (par ex.) self.racine = ('A',) lorsque self est une feuille: 
    # c'est pourquoi il faut lui préférer self.racine.racine = 'A' dans ce cas
    def postfixe(self):
        if self.gauche is not None:
            self.gauche.postfixe()
        if self.droit is not None:
            self.droit.postfixe()
        AB.post.append(self.racine) if type(self.racine) is str else AB.post.append(self.racine.racine)

    def get_postfixe(self):
        AB.post = []
        self.postfixe()
        myList = AB.post
        return myList

    def __repr__(self):
        # if len(self.vers_tuple)
        return str(self.vers_tuple())

if __name__ == "__main__":
    t = ('F', ('A', None, ('B',)), ('C', ('H',), ('D', ('E',), None)))
    F = AB(t=t)
    print("F=", F)
    print("F PARENT=", F.parent)
    # print("A PARENT=", A.parent)
    # print("C PARENT=", C.parent)

    A = F.get_gauche()
    print("A=", A)
    C = F.get_droit()
    print("C=", C)
    H = C.get_gauche()
    print("H=", H)
    D = C.get_droit()
    print("D=", D)
    E = D.get_gauche()
    print("E=", E)

    print("----------------------------")
    # print("RACINE DE H=", H.get_racine().racine)
    # print("RACINE DE C=", C.get_racine().racine)
    # print("----------------------------")
    # print("F =", F)
    # print("GET RACINE DE F:", F.get_racine())
    # print("GET RACINE DE C:", C.get_racine())
    # print("GET RACINE DE D:", D.get_racine())
    # print("PARENT DE F =", F.parent)
    # print("PARENT DE C:", C.parent)
    # print("PARENT DE D:", D.parent)
    # print("PARENT DE E:", E.parent)

    # print("PROFONDEUR DE F:", F.profondeur())
    # print("PROFONDEUR DE C:", C.profondeur())
    # print("PROFONDEUR DE D:", D.profondeur())
    # print("PROFONDEUR DE E:", E.profondeur())
    # print("HAUTEUR DE F:", F.hauteur())
    # print("HAUTEUR DE A:", A.hauteur())
    # print("TAILLE DE F:", F.taille())
    print("F NIVEAU 0", F.get_niveau(0))
    print("F NIVEAU 1", F.get_niveau(1))
    print("F NIVEAU 2", F.get_niveau(2))
    print("PARCOURS EN LARGEUR DE F:", F.get_largeur())
    print("PARCOURS EN LARGEUR DE C:", C.get_largeur())
    print("====================")
    print("GET_PREFIXE DE F:",F.get_prefixe())
    print("GET_PREFIXE DE C:",C.get_prefixe())
    print("GET_INFIXE DE F:",F.get_infixe())
    print("GET_INFIXE DE C:",C.get_infixe())
    print("GET_POSTFIXE DE F:",F.get_postfixe())
    print("GET_POSTFIXE DE C:",C.get_postfixe())