from td_noeuds import Noeud

class ABR(Noeud):
  def __init__(self, valeur, gauche= None, droit=None):
    super().__init__(valeur, gauche, droit)
    
  def isABR(self):
    pass

if __name__ == "__main__":
    # Arbre du TD
    # n50 = Noeud(50)
    n50 = ABR(50)
    n40 = n50.set_gauche(40)
    n53 = n50.set_droit(53)
    n34 = n40.set_gauche(34)
    n55 = n40.set_droit(55)
    n38 = n34.set_droit(38)
    n49 = n55.set_gauche(49)
    n60 = n55.set_droit(60)
    n65 = n60.set_droit(65)
    n46 = n53.set_gauche(46)
    n57 = n53.set_droit(57)
    n47 = n46.set_droit(47)
    print(n50)
