# TNSI : Arbres AVL <br/>$=$ Arbres Binaires de Recherche Équilibrés

## Définition

Les deux inventeurs **<bred>A</bred>delson-<bred>V</bred>elskii** :gb: et **<bred>L</bred>andis** :gb:, ont étudié ces Arbres en $1962$ dans leur article *An Algorithm for the Organization of Information*. 

!!! def "Arbre AVL,  pour Adelson-Velskii et Landis, 1962"
    Un <bred>Arbre AVL</bred> est un Arbre Binaire de Recherche Équilibré, càd un **ABR** tel que, pour chaque noeud, **les hauteurs des sous-arbres gauche (SAG) et sous-arbre droit (SAD) ne diffèrent au plus que de $1$** 

## Caractérisation d'un Arbre AVL

On peut traduire cette définition en calculant, **pour chaque noeud**, un paramètre/nombre nommé **facteur d'équilibrage** :

!!! def "Facteur d'Équilibrage d'un noeud"
    Le <bred>facteur d'équilibrage</bred> $eq(s)$ **d'un noeud $s$** est égal à la différence entre la hauteur du SAD de $s$ et la hauteur du SAG de $s$:

    <center>

    <enc>$eq(s) = h_{SAD}-h_{SAG}$</enc>

    </center>

!!! pte "Caractérisation d'un Arbre AVL"
    Un ABR est un arbre AVL $\Leftrightarrow$ tous les noeuds ont un facteur d'équilibrage de $-1$, $0$ ou $1$

!!! pte "Caractérisation d'un Arbre Déséquilibré"
    Un ABR est déséquilibré $\Leftrightarrow$ il existe au moins un noeud tel que:

    * facteur $\le -2$
    * facteur $\ge 2$

## Cas de Figure

### Arbres AVL

<center>

```dot
graph ABR {
  size="3"
  //node [fontcolor=none, shape=circle, fixedsize=true, width=0.8,fontsize=38, penwidth=3]
  node [shape=circle, fixedsize=true, width=0.8, fontsize=38, penwidth=3]
  edge [penwidth=3]
  50 -- 40, 53
  40 -- 34 [weight=2]
  40 -- 55
  53 -- 46 
  53 -- 57 [weight=2]
  34 -- v1 [color=none, weight=3]
  34 -- 38
  55 -- 49
  55 -- 60 [weight=2]
  60 -- v2 [color=none]
  60 -- 65
  46 -- v3 [color=none]
  46 -- 47
  v1, v2, v3 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Arbre AVL</figcaption>

</center>

### Un Exemple d'Arbres AVL : Les Arbres de Fibonacci : $AF_{n+1}=1+AF_{n}+AF_{n-1}$

<env>**Principe Général**</env> 
Pour créer un nouvel arbre $AF_{n+1}$, à partir de la connaissance des deux précédents:

* On ajoute un nouveau noeud $r$ à la Racine
* L'enfant gauche de $r$ est $AF_{n}$
* L'enfant droit de $r$ est $AF_{n-1}$

<center>

```dot
graph ABR {
  size="7"
  node [shape=circle, fontsize=30, penwidth=3]
  edge [penwidth=3]
  subgraph cluster01 {
    r
    "AF_{n}" [shape=triangle]
    "AF_{n-1}" [shape=triangle]
    r -- "AF_{n}", "AF_{n-1}"
    fontsize=30
    label="AF_{n+1}"
  }
}
```

<figcaption>

$$AF_{n+1}$$

</figcaption>

</center>

<env>**Exemple des Premiers Arbres de Fibonacci**</env>

<div id="conteneur">

<div>

<center>

```dot
graph ABR {
  size="0.5"
  node [fontcolor=none, shape=circle, fixedsize=true, width=0.8,fontsize=38, penwidth=3]
  edge [penwidth=3]
  1
  //v1, v2, v3, v4, v5, v6 [color=none, fontcolor=none]
}
```

<figcaption>AF1</figcaption>

</center>

</div>

<div>

<center>

```dot
graph ABR {
  size="1.2"
  node [fontcolor=none, shape=circle, fixedsize=true, width=0.8, fontsize=38, penwidth=3]
  edge [penwidth=3]
  1 -- {2, 3}
}
```

<figcaption>AF2</figcaption>

</center>

</div>

<div>

<center>

```dot
graph ABR {
  size="2"
  node [fontcolor=none, shape=circle, fixedsize=true, width=0.8,fontsize=38, penwidth=3]
  edge [penwidth=3]
  1 -- {2, 3}
  2 -- {4, 5}
}
```

<figcaption>AF3</figcaption>

</center>

</div>

<div style="flex:2;">

<center>

```dot
graph ABR {
  size="3"
  node [fontcolor=none, shape=circle, fixedsize=true, width=0.8,fontsize=38, penwidth=3]
  edge [penwidth=3]
  1 -- {2, 3}
  2 -- v1 [color=none]
  2 -- {4, 5}
  3 -- {6, 7}
  3 -- v2 [color=none]
  4 -- {8, 9}
  v1, v2 [color=none, fontcolor=none, label=""]
}
```

<figcaption>AF4</figcaption>

</center>

</div>

<div style="flex:2;">

<center>

```dot
graph ABR {
  size="10"
  node [fontcolor=none, shape=circle, fixedsize=true, width=0.8,fontsize=38, penwidth=3]
  //node [shape=circle, fixedsize=true, width=0.8,fontsize=38, penwidth=3]
  edge [penwidth=3]
  1 -- {2, 3}
  2 -- v1 [color=none]
  2 -- 4
  2 -- 5
  3 -- {6, 7}
  3 -- v2 [color=none]
  4 -- v3 [color=none, weight=2]
  4 -- {8, 9}
  5 -- 10
  5 -- 11
  5 -- v4 [color=none]
  8 -- {14, 15}
  6 -- 12
  6 -- 13 [weight=2]
  v1, v2, v3, v4 [color=none, fontcolor=none, label=""]
}
```

<figcaption>AF5</figcaption>

</center>

</div>

<div style="flex:3;">

<center>

```dot
graph ABR {
  size="10"
  node [fontcolor=none, shape=circle, fixedsize=true, width=0.8,fontsize=38, penwidth=3]
  //node [shape=circle, fixedsize=true, width=0.8,fontsize=38, penwidth=3]
  edge [penwidth=3]
  1 -- {2, 3}
  2 -- v1 [color=none]
  2 -- 4 
  2 -- 5
  3 -- 6
  3 -- 7
  3 -- v2 [color=none]
  4 -- v3 [color=none]
  4 -- 8 [weight=2]
  4 -- 9
  5 -- 10
  5 -- 11
  5 -- v4 [color=none]
  8 -- 16 [weight=4]
  8 -- 17
  6 -- 12 [weight=3]
  6 -- 13 [weight=2]
  7 -- 14
  7 -- 15 [weight=3]
  9 -- 18
  9 -- 19 [weight=2]
  10 -- 20 [weight=2]
  10 -- 21
  11 -- v5 [color=none]
  11 -- v6 [color=none]
  12 -- 22 [weight=3]
  12 -- 23 [weight=4]
  16 -- {24, 25}
  v1, v2, v3, v4, v5, v6 [color=none, fontcolor=none, label=""]
}
```

<figcaption>AF6</figcaption>

</center>

</div>

</div>

<div id="conteneur">

<div>

<center>

```dot
graph ABR {
  size="4"
  node [shape=circle, fixedsize=true, width=0.8,fontsize=38, penwidth=3]
  edge [penwidth=3]
  50 -- {18, 70}
  18 -- 12 [weight=2]
  18 -- 24
  12 -- v1 [color=none, weight=3]
  12 -- 16
  16 -- 14
  16 -- v3 [color=none, weight=2]
  24 -- 19
  24 -- v2 [color=none, weight=2]
  70 -- 56
  70 -- v4 [color=none, weight=2]
  56 -- v5[color=none, weight=2]
  56 -- 65
  65 -- 62
  65 -- v6 [color=none, weight=2]
  v1, v2, v3, v4, v5, v6 [color=none, fontcolor=none, label=""]
}
```

<figcaption>un ABR <b>Non équilibré</b>,<br/>AVANT Rééquilibrage</figcaption>

</center>

</div>

<div style="float:left;font-size:3em;margin-top:1em;">$$\Rightarrow$$</div>

<div>

<center>

```dot
graph ABR {
  size="10"
  node [shape=circle, fixedsize=true, width=0.8,fontsize=38, penwidth=3]
  edge [penwidth=3]
  50 -- {18, 65}
  18 -- 14 [weight=2]
  18 -- 24
  14 -- v1 [color=none, weight=2]
  14 -- 12
  14 -- 16
  12 -- v3 [color=none, weight=2]
  24 -- 19
  24 -- v2 [color=none, weight=2]
  65 -- 56
  65 -- 70 [weight=2]
  56 -- v5 [color=none]
  56 -- 62 [weight=2]
  v1, v2, v3, v4, v5 [color=none, fontcolor=none, label=""]
}
```

<figcaption style="margin-left:2em;">un arbre AVL <br/>= un ABR <b>équilibré</b>,<br/>APRÈS Rééquilibrage</figcaption>

</center>

</div>

</div>

## Hauteur Logarithmique d'un Arbre AVL

!!! pte
    Dans un arbre AVL, La hauteur $h$ est **logarithmique** en $n$
    càd que $h$ est proportionnelle à $\log_2 (n)\,\,$ avec $C\approx 1,45$
    càd qu'il existe une constante $C$ telle que : <enc>$h \le C\times \log_2 (n)$</enc>$\,\,\,$ avec $C\approx 1,45$

## Opérations de Base des Arbres AVL

### Recherche dans un Arbre AVL

!!! mth "Recherche d'une Clé dans un Arbre AVL"
    La Recherche d'une Clé dans un **arbre AVL** se déroule exactement comme dans un **Arbre Binaire de Recherche (ABR)**, 

La complexité de la Recherche d'une Clé dans un Arbre AVL est donc encore en $O(h)$, comme pour un arbre ABR, et de plus, puisque la hauteur $h$ d'un arbre AVL est en $O ( \log_2 ⁡ ( n ) )$, Alors :


!!! pte "Complexité de la Recherche dans un Arbre AVL"
    **Dans un Arbre AVL**, La Recherche d'une Clé se fait en <enc>$O ( \log_2 ⁡ ( n ) )$</enc>

<env>**Remarque**</env> La Recherche d'une Clé ne modifie pas la structure de l'arbre AVL

### Insertion dans un Arbre AVL

#### Principe Général de l'insertion dans un Arbre AVL

!!! mth "Insertion d'une Clé dans un Arbre AVL"
    L'Insertion d'un noeud dans un arbre AVL se déroule en deux étapes :  
    :one: **Insertion dans l'arbre AVL** : Tout d'abord, on insère le noeud à la bonne place, exactement comme dans un ABR  
    :two: **Rééquilibrage** (si besoin) : Si le facteur d'équilibrage $eq(s)$ d'un noeud $s$ est : <enc>$\le -2$</enc> $\,$ ou <enc>$\ge 2$</enc>  
    Alors on **rééquilibre** l'arbre AVL en **remontant (uniquement) sur le chemin vers la racine** depuis le noeud inséré en effectuant :
        * une <bred>Rotation Simple</bred> : complexité en $O(1)$, ou bien
        * une <bred>Rotation Double</bred> (= $2$ rotations simples connexes) : complexité en $O(1)$

    Pour chaque insertion, il sera nécessaire de réaliser $0$ ou $1$ **rotation simple** ou **double**. 

#### Rééquilibrage d'un Arbre AVL : Rotation Simple et Rotations Double

Hein :question:  :fearful: des ***Rotations*** :question: C'est quoi çà :question:

<env>**Rotations Simples**</env> Une Rotation Simple (Gauche ou Droite) se réalise en temps constant : en <enc>$O(1)$</enc>

<div id="conteneur">

<div>

```dot
graph ROT {
  size=4
  node [shape=circle, fontname="fira code"]
  z -- q
  q -- p
  q -- C
  p -- A, B
  p, q [style=filled]
  p [fillcolor=green]
  q [fillcolor=red]
  A, B, C [shape=triangle]
  z [color=none, fontcolor=none]
}
```

</div>

<div>

$$\xrightarrow[]{\text{Rotation Droite autour de q}}$$
$$\xleftarrow[\text{Rotation Gauche autour de p}]{}$$

</div>

<div>

```dot
graph ROT {
  size=4
  node [shape=circle, fontname="fira code"]
  z -- p
  p -- A, q
  q -- B, C
  p, q [style=filled]
  p [fillcolor=green]
  q [fillcolor=red]
  A, B, C [shape=triangle]
  z [color=none, fontcolor=none]
}
```

</div>

</div>

<env>**Rotations Doubles**</env>

* **Rotation Double à Droite autour de r** :
$=$ Rotation Gauche-Droite

<div id="conteneur">

<div style="flex:2;">

```dot
graph ROT {
  size=4
  node [shape=circle, fontname="fira code"]
  z -- r
  r -- p, D
  p -- A, q
  q -- B, C
  p, q, r [style=filled]
  p [fillcolor=green]
  q [fillcolor=red]
  r [fillcolor="#C4EDFF"]
  A, B, C, D [shape=triangle]
  z [color=none, fontcolor=none]
}
```

</div>

<div style="float:left;margin-top: 5em;">

$$\xrightarrow[]{\text{Rotation Gauche autour de p}}$$

</div>

<div style="flex:2;">

```dot
graph ROT {
  size=4
  node [shape=circle, fontname="fira code"]
  z -- r
  r -- q, D
  q -- p
  q -- C
  p -- A, B
  p, q, r [style=filled]
  p [fillcolor=green]
  q [fillcolor=red]
  r [fillcolor="#C4EDFF"]
  A, B, C, D [shape=triangle]
  z [color=none, fontcolor=none]
}
```

</div>

<div style="float:left;margin-top: 5em">

$$\xrightarrow[]{\text{Rotation Droite autour de r}}$$

</div>

<div style="flex:3;">

```dot
graph ROT {
  size=4
  node [shape=circle, fontname="fira code"]
  z -- q
  q -- p
  q -- r
  p, q, r [style=filled]
  p -- 0 [color=none, fontcolor=none]
  p -- A
  p -- B
  r -- C
  r -- D
  p [fillcolor=green]
  q [fillcolor=red]
  r [fillcolor="#C4EDFF"]
  A, B, C, D [shape=triangle]
  0, z [color=none, fontcolor=none, label=""]
}
```

</div>

</div>

* **Rotation Double à Gauche autour de r**
$=$ Rotation Droite-Gauche

<div id="conteneur">

<div>

```dot
graph ROT {
  size=4
  node [shape=circle, fontname="fira code"]
  z -- r
  r -- D, p
  p -- q, A
  q -- C, B
  p, q, r [style=filled]
  p [fillcolor=green]
  q [fillcolor=red]
  r [fillcolor="#C4EDFF"]
  A, B, C, D [shape=triangle]
  z [color=none, fontcolor=none, label=""]
}
```

</div>

<div style="float:left;margin-top: 5em">

$$\xrightarrow[]{\text{Rotation Droite autour de p}}$$

</div>

<div>

```dot
graph ROT {
  size=4
  node [shape=circle, fontname="fira code"]
  z -- r
  r -- D, q
  q -- C
  q -- p
  p -- B, A
  p, q, r [style=filled]
  p [fillcolor=green]
  q [fillcolor=red]
  r [fillcolor="#C4EDFF"]
  A, B, C, D [shape=triangle]
  z [color=none, fontcolor=none, label=""]
}
```

</div>

<div style="float:left;margin-top: 5em">

$$\xrightarrow[]{\text{Rotation Gauche autour de r}}$$

</div>

<div>

```dot
graph ROT {
  size=4
  node [shape=circle, fontname="fira code"]
  z -- q
  q -- r
  q -- p
  p, q, r [style=filled]
  p -- 0 [color=none, fontcolor=none]
  p -- B
  p -- A
  r -- D
  r -- C
  p [fillcolor=green]
  q [fillcolor=red]
  r [fillcolor="#C4EDFF"]
  A, B, C, D [shape=triangle]
  0, z [color=none, fontcolor=none, label=""]
}
```

</div>

</div>

#### Un Vrai Exemple détaillé d'insertion dans un AVL

On considère l'arbre AVL suivant: 

<center>

```dot
graph AVL {
  size=3
  node [shape=circle, fontname="fira code", fontsize=24, penwidth=2]
  edge [penwidth=2]
  10 -- 8, 20
  8 -- 6 [weight=2]
  8 -- v1 [color=none]
  20 -- 18
  20 -- 23 [weight=2]
  23 -- v2 [color=none]
  23 -- 25
  v1, v2 [color=none, fontcolor=none, label=""]
}
```

</center>

:one: <env>**Rotation Simple Droite $\Leftrightarrow$ Facteurs d'Equilibrage $(-1;-2)$**</env> $\Rightarrow$ Exemple de l'Insertion de $5$

<div id="conteneur">

<div>

<center>

```dot
digraph AVL {
  size=3
  node [shape=circle, fontname="fira code", fontsize=30, penwidth=3]
  edge [penwidth=3, dir=none]
  10 -> 8 [xlabel="<10", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee]
  10 -> 8
  10 -> 20 [weight=2]
  8 -> 6 [xlabel="<8", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee, weight=2]
  8 -> 6
  8 -> v1 [color=none]
  20 -> 18 [weight=2]
  20 -> 23 [weight=4]
  23 -> v4 [color=none]
  23 -> 25 [weight=3]
  18 -> v2 [color=none, weight=2]
  6 -> 5 [xlabel="<6", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee]
  6 -> 5 [color="#C4EDFF", penwidth=6, weight=2]
  6 -> v3 [color=none, weight=3]
  6 [xlabel="-1", shape=doublecircle, style=filled, fillcolor=red, color=red]
  5 [xlabel="0", shape=doublecircle, style=filled, fillcolor="#C4EDFF", color="#C4EDFF"]
  8 [xlabel="-2", shape=doublecircle, style=filled, fillcolor=green, color=green]
  10 [xlabel="0"]
  8 [xlabel="-2"]
  20 [xlabel="+1"]
  6 [xlabel="-1"]
  18 [xlabel="0"]
  23 [xlabel="+1"]
  25 [xlabel="0"]
  v1, v2, v3, v4 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Après Insertion de 5,<br/>L'Arbre AVL devient<br/> provisoirement déséquilibré :<br/>


$$eq(6)=-1; eq(8)=-2$$

</figcaption>

</center>

</div>

<div style="float:left;margin-top: 4em">

$$\xrightarrow[eq(6)=-1; \,\, eq(8)=-2]{\text{Rotation Droite autour de 6}}$$

</div>

<div>

<center>

```dot
graph AVL {
  size=4
  node [shape=circle, fontname="fira code", fontsize=30, penwidth=3]
  edge [penwidth=3, dir=none]
  10 -- 6 [weight=1]
  10 -- 20 
  6 -- 5 [color="#C4EDFF", penwidth=6, weight=2]
  6 -- 8
  20 -- 18
  20 -- 23 [weight=2]
  23 -- v4 [color=none, weight=1]
  23 -- 25
  6 [xlabel="0", shape=doublecircle, style=filled, fillcolor=red, color=red]
  5 [xlabel="0", shape=doublecircle, style=filled, fillcolor="#C4EDFF", color="#C4EDFF"]
  8 [xlabel="0", shape=doublecircle, style=filled, fillcolor=green, color=green]
  10 [xlabel="+1"]
  20 [xlabel="+1"]
  5 [xlabel="0"]
  18 [xlabel="0"]
  23 [xlabel="+1"]
  25 [xlabel="0"]
  v1, v2, v3, v4 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Après Rééquilibrage par Rotation Droite,<br/>L'Arbre AVL redevient équilibré.<br/>TOUS les facteurs d'équilibrage vérifient :<br/>

$$eq(s)\in \{-1;0;+1\}$$

</figcaption>

</center>

</div>

</div>

:two: <env>**Rotation Simple Gauche : Facteurs d'Equilibrage $(+1;+2)$**</env> $\Rightarrow$ Exemple de l'Insertion de $27$

<div id="conteneur">

<div>

```dot
digraph AVL {
  size=4
  node [shape=circle, fontname="fira code", fontsize=30, penwidth=3]
  edge [penwidth=3, dir=none]
  10 -> 8 [weight=2]
  10 -> 20
  10 -> 20 [xlabel=">10", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee]
  8 -> 6 [weight=3]
  8 -> v1 [color=none]
  20 -> 18
  20 -> 23 [weight=2] 
  20 -> 23 [xlabel=">20", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee]
  23 -> v4 [color=none]
  23 -> 25
  23 -> 25 [xlabel=">23", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee]
  25 -> v5 [color=none, weight=4]
  25 -> 27 [color="#C4EDFF", penwidth=6, weight=2]
  25 -> 27 [xlabel=">25", dir=forward, fontsize=30, fontcolor=red, arrowhead=vee, color=red, penwidth=6]
  18 -> v2 [color=none, weight=2]
  6 -> v3 [color=none]
  25 [xlabel="-1", shape=doublecircle, style=filled, fillcolor=red, color=red]
  27 [xlabel="0", shape=doublecircle, style=filled, fillcolor="#C4EDFF", color="#C4EDFF"]
  23 [xlabel="-2", shape=doublecircle, style=filled, fillcolor=green, color=green]
  10 [xlabel="+2"]
  8 [xlabel="-1"]
  20 [xlabel="+2"]
  6 [xlabel="0"]
  18 [xlabel="0"]
  23 [xlabel="+2"]
  25 [xlabel="+1"]
  v1, v2, v3, v4, v5 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Après Insertion de 27,<br/>L'Arbre AVL devient<br/> provisoirement déséquilibré :<br/>

$$eq(25)=+1; \,\, eq(23)=+2$$

</figcaption>

</div>

<div style="margin-top:4em;">

$$\xrightarrow[eq(25)=+1; \,\, eq(23)=+2]{\text{Rotation Gauche autour de 23}}$$

</div>

<div>

```dot
graph AVL {
  size=4
  node [shape=circle, fontname="fira code", fontsize=30, penwidth=3]
  edge [penwidth=3, dir=none]
  10 -- 8 [weight=1]
  10 -- 20
  8 -- 6 [weight=2]
  8 -- v1 [color=none]
  20 -- 18
  20 -- 25 [weight=2]
  //23 -- v4 [color=none, weight=2]
  25 -- 23
  25 -- 27 [color="#C4EDFF", penwidth=6]
  25 [xlabel="0", shape=doublecircle, style=filled, fillcolor=red, color=red]
  27 [xlabel="0", shape=doublecircle, style=filled, fillcolor="#C4EDFF", color="#C4EDFF"]
  23 [xlabel="0", shape=doublecircle, style=filled, fillcolor=green, color=green]
  10 [xlabel="+1"]
  8 [xlabel="-1"]
  20 [xlabel="+1"]
  6 [xlabel="0"]
  18 [xlabel="0"]
  23 [xlabel="0"]
  25 [xlabel="0"]
  v1, v2, v3, v4, v5 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Après Rééquilibrage par Rotation Gauche,<br/>L'Arbre AVL redevient équilibré.<br/>TOUS les facteurs d'équilibrage vérifient :<br/>

$$eq(s)\in \{-1;0;+1\}$$

</figcaption>

</div>

</div>

:three: <env>**Rotation Double Droite : Facteurs d'Equilibrage $(+1;-2)$**</env> $\Rightarrow$ Exemple de l'Insertion de $7$

<div id="conteneur">

<div>

<center>

```dot
digraph AVL {
  size=4
  node [shape=circle, fontname="fira code", fontsize=30, penwidth=3]
  edge [penwidth=3, dir=none]
  10 -> 8 [xlabel="<10", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee]
  10 -> 8
  10 -> 20 [weight=2]
  8 -> 6 [xlabel="<8", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee, weight=2]
  8 -> 6
  8 -> v1 [color=none]
  20 -> 18 [weight=2]
  20 -> 23 [weight=4]
  23 -> v4 [color=none]
  23 -> 25 [weight=3]
  18 -> v2 [color=none, weight=2]
  6 -> v3 [color=none, weight=4]
  6 -> 7 [xlabel=">6", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee]
  6 -> 7 [color="#C4EDFF", penwidth=6, weight=2]
  6 [xlabel="+1", shape=doublecircle, style=filled, fillcolor=red, color=red]
  7 [xlabel="0", shape=doublecircle, style=filled, fillcolor="#C4EDFF", color="#C4EDFF"]
  8 [xlabel="-2", shape=doublecircle, style=filled, fillcolor=green, color=green]
  10 [xlabel="0"]
  8 [xlabel="-2"]
  20 [xlabel="+1"]
  18 [xlabel="0"]
  23 [xlabel="+1"]
  25 [xlabel="0"]
  v1, v2, v3, v4 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Après Insertion de 7,<br/>L'Arbre AVL devient<br/> provisoirement déséquilibré :<br/>

$$eq(6)=+1; \,\, eq(8)=-2$$

</figcaption>

</center>

</div>

<div style="float:left;margin-top: 4em;">

$$\xrightarrow[eq(6)=+1; \,\, eq(8)=-2]{\text{Rotation Double Droite autour de 8}}$$

</div>

<div>

<center>

```dot
graph AVL {
  size=4
  node [shape=circle, fontname="fira code", fontsize=30, penwidth=3]
  edge [penwidth=3, dir=none]
  10 -- 7 [weight=1]
  10 -- 20 
  7 -- 6 [color="#C4EDFF", penwidth=6, weight=2]
  7 -- 8
  20 -- 18
  20 -- 23 [weight=2]
  23 -- v4 [color=none, weight=1]
  23 -- 25
  8 [xlabel="0", shape=doublecircle, style=filled, fillcolor=green, color=green]
  6 [xlabel="0", shape=doublecircle, style=filled, fillcolor=red, color=red]
  7 [xlabel="0", shape=doublecircle, style=filled, fillcolor="#C4EDFF", color="#C4EDFF"]
  10 [xlabel="+1"]
  20 [xlabel="+1"]
  18 [xlabel="0"]
  23 [xlabel="+1"]
  25 [xlabel="0"]
  v1, v2, v3, v4 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Après Rééquilibrage par Rotation Double Droite,<br/>L'Arbre AVL redevient équilibré.<br/>TOUS les facteurs d'équilibrage vérifient :<br/>

$$eq(s)\in \{-1;0;+1\}$$

</figcaption>

</center>

</div>

</div>

:four: <env>**Rotation Double Gauche : Facteurs d'Equilibrage $(-1;+2)$**</env> $\Rightarrow$ Exemple de l'Insertion de $24$

<div id="conteneur">

<div>

<center>

```dot
digraph AVL {
  size=4
  node [shape=circle, fontname="fira code", fontsize=30, penwidth=3]
  edge [penwidth=3, dir=none]
  10 -> 8 [weight=2]
  10 -> 20
  10 -> 20 [xlabel=">10", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee]
  8 -> 6 [weight=3]
  8 -> v1 [color=none]
  20 -> 18
  20 -> 23 [weight=2] 
  20 -> 23 [xlabel=">20", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee]
  23 -> v4 [color=none]
  23 -> 25
  23 -> 25 [xlabel=">23", dir=forward, color=red, fontsize=30, fontcolor=red, arrowhead=vee]
  25 -> 24 [color="#C4EDFF", penwidth=6, weight=2]
  25 -> 24 [xlabel="<25", dir=forward, fontsize=30, fontcolor=red, arrowhead=vee, color=red, penwidth=6]
  25 -> v5 [color=none, weight=4]
  18 -> v2 [color=none, weight=2]
  6 -> v3 [color=none]
  25 [xlabel="-1", shape=doublecircle, style=filled, fillcolor=red, color=red]
  24 [xlabel="0", shape=doublecircle, style=filled, fillcolor="#C4EDFF", color="#C4EDFF"]
  23 [xlabel="-2", shape=doublecircle, style=filled, fillcolor=green, color=green]
  10 [xlabel="+2"]
  8 [xlabel="-1"]
  20 [xlabel="+2"]
  6 [xlabel="0"]
  18 [xlabel="0"]
  23 [xlabel="+2"]
  v1, v2, v3, v4, v5 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Après Insertion de 24,<br/>L'Arbre AVL devient<br/> provisoirement déséquilibré :<br/>

$$eq(25)=-1; \,\, eq(23)=+2$$

</figcaption>

</center>

</div>

<div style="margin-top:4em;">

$$\xrightarrow[eq(25)=-1; \,\, eq(23)=+2]{\text{Rotation Double Gauche autour de 23}}$$

</div>

<div>

<center>

```dot
graph AVL {
  size=4
  node [shape=circle, fontname="fira code", fontsize=30, penwidth=3]
  edge [penwidth=3, dir=none]
  10 -- 8 [weight=1]
  10 -- 20
  8 -- 6 [weight=2]
  8 -- v1 [color=none]
  20 -- 18
  20 -- 24 [weight=2]
  //23 -- v4 [color=none, weight=2]
  24 -- 23
  24 -- 25 [color="#C4EDFF", penwidth=6]
  25 [xlabel="0", shape=doublecircle, style=filled, fillcolor=red, color=red]
  24 [xlabel="0", shape=doublecircle, style=filled, fillcolor="#C4EDFF", color="#C4EDFF"]
  23 [xlabel="0", shape=doublecircle, style=filled, fillcolor=green, color=green]
  10 [xlabel="+1"]
  8 [xlabel="-1"]
  20 [xlabel="+1"]
  6 [xlabel="0"]
  18 [xlabel="0"]
  23 [xlabel="0"]
  24 [xlabel="0"]
  v1, v2, v3, v4, v5 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Après Rééquilibrage par Rotation Double Gauche,<br/>L'Arbre AVL redevient équilibré.<br/>TOUS les facteurs d'équilibrage vérifient :<br/>

$$eq(s)\in \{-1;0;+1\}$$

</figcaption>

</center>

</div>

</div>

#### Complexité de l'Insertion d'une Clé dans un Arbre AVL

La Complexité de l'Insertion d'une Clé dans un Arbre AVL est donc encore, dans le pire des cas, en $O(h)+O(1)=O(h)$, et puisque la hauteur $h$ d'un arbre AVL est en $O ( \log_2 ⁡ ( n ) )$, Alors :

!!! pte "Complexité de l'Insertion d'une Clé dans un Arbre AVL"
    **Dans un Arbre AVL**, L'insertion d'une Clé se fait en <enc>$O(\log_2 ⁡ (n))$</enc> $\,$ :

    * Chercher l'endroit où insérer la clé prend un temps $O(\log_2 (n))$
    * Trouver un noeud déséquilibré (s'il y en a un) prend un temps $O(\log_2 (n))$
    * Rééquilibrer l'arbre prend un temps $O(1)$

### Suppression dans un Arbre AVL

!!! mth "Suppression d'une Clé dans un Arbre AVL"
    La Suppression d'un noeud dans un arbre AVL se déroule en deux étapes :  
    :one: **Suppression dans l'arbre AVL** : Tout d'abord, on Supprime le noeud, exactement comme dans un ABR  
    :two: **Rééquilibrage** (si besoin) : Si le facteur d'équilibrage $eq(s)$ d'un noeud $s$ est : <enc>$\le -2$</enc> $\,$ ou <enc>$\ge 2$</enc>  
    Alors on **rééquilibre** l'arbre AVL en **remontant (uniquement) sur le chemin vers la racine** depuis le noeud supprimé en effectuant :

    * une <bred>Rotation Simple</bred> : complexité en $O(1)$, ou bien
    * une <bred>Rotation Double</bred> (= $2$ rotations simples connexes) : complexité en $O(1)$

    Pour chaque suppression, il sera nécessaire de réaliser au total entre $0$ et $h$ **rotations simples** ou **doubles**, où $h$ est la hauteur (initiale) de l'arbre.

!!! pte "Complexité de la Suppression d'une Clé dans un Arbre AVL"
    La suppression se fait aussi en <enc>$O ( \log_2 ⁡ ( n ) )$</enc> $\,$ :

    * Chercher le noeud à supprimer prend un temps $O(\log_2 (n))$
    * On devra faire, au plus, $O(\log_2 (n))$ Rééquilibrages
    * Chaque Rééquilibrage prend un temps $O(1)$

### Résumé des Opérations dans un Arbre AVL

!!! pte
    **Dans un Arbre AVL**, dans le pire des cas, on peut déterminer les complexités en temps pour les opérations suivantes :

    * Recherche d'une Clé : <enc>$O(\log_2 (n))$</enc>
    * Insertion d'une Clé : <enc>$O(\log_2 (n))$</enc>
    * Suppression d'une Clé : <enc>$O(\log_2 (n))$</enc>

