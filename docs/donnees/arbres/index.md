# TNSI : Introduction aux Arbres

## Joke :sweat_smile:

<center>

<img src="img/banniere.png" style="width:90%;">

</center>

<br/>

Vous pensez que ça n'existe pas?   

<center>

<img src="img/realTree.png" style="width:30%;">

<figcaption>

<em>"Finalement, après des années de recherche,<br/>
j'ai trouvé un vrai Arbre" !</em>

</figcaption>

</center>

## Exemples d'Utilisation des Arbres

### Arbre des Expressions Syntaxiques

<center>

```dot
graph G {
  size="1.8"
  node [shape=circle, fontname="fira code", fontsize=28, penwidth=3]
  edge [penwidth=3]
  "-" -- {"x", "+"} 
  "x" -- 3 [weight=2]
  "x" -- 5
  "+" -- 2
  "+" -- 4 [weight=2]
}
```

<figcaption><b>Arbre 1 : Arbre des Expressions Arithmétiques</b>

</figcaption>

</center>

Les $4$ opérations $+, −, ×$ et $\div$ sont des opérations binaires. Une expression ne comportant que ces symboles peut donc être représentée sous forme d'un arbre (binaire) comportant deux sortes de noeuds : les *noeuds internes* sont des opérations, les *feuillles* sont des nombres.

Dans un tel arbre, l'arrangement des noeuds joue le rôle des parenthèses auxquelles nous sommes habitués.
Ici, l'arbre peut représenter l'opération $(3\times 5) - (2+4)$

### Arbre de Jeu

!!! def "Arbre de Jeu"
    Un <bred>arbre de jeu</bred> permet de représenter toutes les positions et tous les coups possibles d'un jeu à l'aide d'un arbre (pas forcément binaire)

#### Exemple 1 : Jeu de Nim

<center>

```dot
graph G {
  size="3"
  forcelabels=true
  node [shape=circle, fontname="fira code", fontsize=24, penwidth=3];
  edge [penwidth=3]
  5 -- {4, 32} [color=red]
  31, 32 [label="3"]
  21, 22, 23 [label="2"]
  11, 12, 13, 14, 15 [label="1"]
  01, 02, 03, 04, 05, 06, 07, 08 [label="0"]
  4 -- 31 [color=blue, weight=2]
  4 -- 22 [color=blue]
  31 -- 21 [color=red, weight=3]
  31 -- 12 [color=red]
  32 -- 23 [color=blue]
  32 -- 15 [color=blue, weight=2]
  21 -- 11 [color=blue, weight=3]
  21 -- 02 [color=blue]
  11 -- 01 [color=red]
  12 -- 03 [color=blue]
  22 -- 13 [color=red]
  22 -- 05 [color=red, weight=2]
  13 -- 04 [color=blue]
  23 -- {14, 07} [color=red]
  14 -- 06 [color=blue]
  15 -- 08 [color=red]
  07, 08 [shape=doublecircle]
}
```

<figcaption><b>Arbre 2 : Arbre de Jeu (de Nim)</b></figcaption>

</center>

L'arbre ci-dessus modélise un **jeu de Nim**, dont on rappelle les **Règles du Jeu** :  
On dispose de $5$ allumettes. Chacun son tour, chaque joueur peut en prendre $1$ ou $2$. Celui qui prend la dernière allumette gagne.  

L'état du jeu est totalement défini par le nombre d'allumettes à jouer restantes et le nom du prochain joueur à jouer.

* Les (valeurs des) noeuds indiquent le nombre d'allumettes à jouer restantes,
* la couleur de l'arête indique quel joueur doit jouer (Rouge ou Bleu). Le Rouge commence

Cet arbre montre que les deux joueurs ont chacun $4$ possiblités de gagner, mais que le joueur Rouge a une stratégie gagnante car, s'il choisit bien ses coups, il est sûr de gagner sans que le joueur Bleu ne pourra empêcher.

#### Exemple 2: Jeu du Morpion (Algorithme MiniMax)

<center>

<img src="img/jeu.jpg"/>

<figcaption><b>Arbre 4 : Arbre de Jeu du Morpion (Algorithme MiniMax)</b>.<br/>Dans un jeu où on ne retrouve pas deux fois la même position dans la même partie, le graphe des positions est sans cycle, donc c'est un arbre.
</figcaption>

</center>

Dans cet exemple modélisant les positions du jeu de Morpion (Algorithme <em>MiniMax</em>) :
 
:one: On commence par étiqueter les feuilles, qui modélisent des parties finies, en convenant de coder le résultat final (**Gain** ou **Utilité**) :

* `+1` pour une victoire de X, et 
* `−1` pour une défaite de X,
* `0` pour un nul (égalité) 

:two: À l'étage supérieur, on étiquette chaque noeud parent `p` (récursivement), en raisonnant comme suit :

* Si le noeud parent correspond au tour de X (ou *Joueur Max*), le (minimax du) noeud parent vaut le **maximum** des valeurs (minimax) des noeuds enfants
* Si le noeud parent correspond au tour de O (ou *Joueur Min*), le (minimax du) noeud parent vaut le **minimum** des valeurs (minimax) des noeuds enfants

:three: <env>**Stratégies de Jeu**</env> À chaque tour, la **meilleure stratégie de jeu**:

* pour le jour Max (X), consiste à choisir le **Max** des noeuds enfants
* pour le jour Min (O), consiste à choisir le **Min** des noeuds enfants

:four: On peut prouver qu'il y a match nul pour deux joueurs qui ont la meilleure stratégie. 

### Arbre Syntaxique de Code

<center>

```dot
graph Code {
  size="2.9"
  node [penwidth=2, fontsize=38]
  edge [penwidth=2]
  {i, Range, Body2, a2}
  Body1, Body2, Assign1, Assign2, For, Print, Range, "\*" [shape=rectangle]
  Body1, Body2 [label="Body", color=red, fontcolor=red]
  Assign1, Assign2, For, Print, Range, "\*" [color=blue, fontcolor=blue]
  Assign1, Assign2 [label="Assign"]
  i, a1, a2, a3, a4, 2, 5, 3 [color="#08a82d", fontcolor="#08a82d", shape=circle]
  a1, a2, a3, a4 [label="a"]
  Body1 -- {Assign1, For, Print}
  Assign1 -- {a1, 2}
  For -- {i, Range, Body2}
  Range -- 3
  Body2 -- Assign2
  Assign2 -- {a2, "\*"}
  "\*" -- {a3, 5}
  Print -- a4
}
```

<figcaption><b>Arbre 3 : Arbre Syntaxique du code ci-dessous ...</b></figcaption>

</center>

```python
a = 2
for i in range(3):
  a = a * 5
print(a)
```

Comme tous les langages de programmation, Python dispose d'une <b>grammaire</b> qui indique comment former des programmes corrects du point de vue de la syntaxe, qu'il puisse <em>comprendre</em>. L'<b>interpréteur Python</b> lit le code source puis construit l'arbre syntaxique du code.
En simplifiant un peu, le code source précédent pourrait être représenté par l'arbre ci-dessus. Tous les traitements futurs du code seront faits à partir de cet arbre.

### Tas

<center>

```dot
graph Code {
  size="2"
  node [shape=circle, penwidth=3, fontsize=28]
  edge [penwidth=3]
  10, 8
  41, 42 [label=4]
  21, 22 [label=2]
  3
  v1 [color=none, label=""]
  10 -- 8
  10 -- 42
  8 -- 5 [weight=2]
  8 -- 6
  5 -- 41 [weight=3]
  5 -- 21
  6 -- v1 [color=none, weight=3]
  6 -- 3
  42 -- 22
  42 -- 1 [weight=2]
}
```

<figcaption><b>Arbre 5 : Un Tas (ici un <em>Tas-max</em>)</b></figcaption>

</center>

!!! def "Tas"
    Un <bred>Tas</bred> est un arbre *"complet" (à gauche)* dont chaque noeud est supérieur ou égal à chacun de ses enfants

Cette structure de données est utilisée dans deux situations classiques:

* pour implémenter les **Files de Priorité**, car un tas permet de retirer efficacement l'élément de priorité maximale (resp. minimale)
* pour implémenter le **Tri par Tas** : qui est un algorithme de tri efficace

### Arbres Binaires de Recherche

<center>

```dot
graph Code {
  size="2"
  node [shape=circle, fontsize=28, penwidth=3]
  edge [penwidth=3]
  8 -- {3, 10}
  3 -- 1 [weight=2]
  3 -- 6
  6 -- {4, 7}
  v1 [color=none, label=""]
  10 -- v1 [color=none]
  10 -- 14
  14 -- 13
  v2 [color=none, label=""]
  14 -- v2 [color=none]
}
```

<figcaption><b>Arbre 6 : Arbre Binaire de Recherche (ABR)</b></figcaption>

</center>

!!! def "ABR - Arbres Binaires de Recherche"
    Un <bred>Arbre Binaire de Recherche - ABR</bred> est un Arbre Binaire (chaque noeud admet un sous-arbre Gauche et un sous-arbre Droit) étiquetté avec des **clés** (=les valeurs des noeuds), pour lequel :

    * les clés du **sous-arbre-gauche** sont (toutes) **inférieures ou égales** à celles de la racine
    * les clés du **sous-arbre-droit** sont (toutes) **supérieures ou égales** à celles de la racine
    * les deux sous-arbres (Gauche et Droit) sont eux-mêmes des arbres Binaires de Recherche

Comme son nom l'indique, cette structure de données est utilisée pour rechercher efficacement un élément dans un arbre (Complexité en temps logarithmique lorsque l'arbre est dit *bien équilibré*)

## Principal Intérêt des Arbres? (Spoiler) :see_no_evil: :speak_no_evil: :hear_no_evil:

Dans les Listes / Piles / Files, la Recherche / l'Insertion / la Suppression d'un élément est proportionnel à la taille $n$ de la Liste / Pile / File, donc **en temps linéaire** <enc>$O(n)$</enc>  

Le principal intérêt des arbres est la complexité de leurs opérations:

* Pour un arbre quelconque: 
    * La <bred>Recherche</bred> d'un élément admet une **complexité en temps en $O(h)$, où $h$ désigne la *hauteur* de l'arbre**.
    * L'<bred>Insertion</bred> d'un élément admet une **complexité en temps en $O(h)$, où $h$ désigne la *hauteur* de l'arbre**.
    * La <bred>Suppression</bred> d'un élément admet une **complexité en temps en $O(h)$, où $h$ désigne la *hauteur* de l'arbre**.  
    Cela peut sembler déjà intéressant, mais il existe des cas dégénérés le rendant moins intéressant. Néanmoins :
* Si de plus on choisit l'arbre de sorte qu'il soit ***bien équilibré*** (en un certain sens à préciser), Alors la hauteur $h$ sera **logarithmique** : $h= O(\log_2(n))$.  
Par conséquent, la Recherche/Insertion/Suppression d'un élément dans un Arbre ***équilibré*** sera **en temps logarithmique** <enc>$O(\log_2(n))$</enc>
