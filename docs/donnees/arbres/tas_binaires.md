# TNSI : Tas Binaires

!!! def
    * Un <bred>Tas Binaire</bred> (à gauche) :fr: ou <bred>heap</bred> :gb: est un **arbre binaire complet** (à gauche), tel que :
        * **Ordre du Tas** : le noeud racine porte une donnée **plus grande** à celle de tous les autres noeuds, et tels que
        * ses deux sous-arbres soient aussi des tas  
    Cet arbre est appelé un <bred>Tas-max</bred> :fr: ou <bred>max-heap</bred> :gb:
    * Un <bred>Tas-min</bred> :fr: ou <bred>min-heap</bred> :gb: a la même structure, mais chaque noeud porte une information **plus petite** que tous ses descendants.

## Cas de Figure sur les Tas Binaire

### Tas Binaire de type *Tas-max*

<div id="conteneur">

<div>

```dot
graph TAS {
  size="0.7"
  node [shape=circle, fontsize=30, penwidth=3]
  edge [penwidth=3]
  5 -- {3, 2}

}
```

<figcaption>Tas Binaire <br/>(Tas-max)</figcaption>

</div>

<div>

```dot
graph TAS {
  size="1.1"
  node [shape=circle, fontsize=30, penwidth=3]
  edge [penwidth=3]
  9 -- {5, 7}
  5 -- {4, 1}
}
```

<figcaption>Tas Binaire <br/>(Tas-max)</figcaption>

</div>

<div>

```dot
graph TAS {
  size="2"
  node [shape=circle, fontsize=30, penwidth=3]
  edge [penwidth=3]
  10 -- {8,6}
  8 -- 3 [weight=2]
  8 -- 7
  6 -- 2
  6 -- v1 [color=none, weight=2]
  v1, v2 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Tas Binaire <br/>(Tas-max)</figcaption>

</div>

<div style="flex:2;">

```dot
graph TAS {
  size="3"
  node [shape=circle, fontsize=38, penwidth=3]
  edge [penwidth=3]
  50 -- {39, 46}
  39 -- v1 [color=none, weight=1]
  39 -- {17,13}
  17 -- {12, 15}
  46 -- {11, 25}
  46 -- v2 [color=none]
  v1, v2 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Tas Binaire (Tas-max)</figcaption>

</div>

<div style="flex:3;">

```dot
graph TAS {
  size="5"
  node [shape=circle, fixedsize=true, width=0.8,fontsize=38, penwidth=3]
  edge [penwidth=3]
  100 -- {82, 56}
  82 -- v1 [color=none]
  82 -- {401,62}
  401 -- 35 [weight=3]
  401 -- 24
  56 -- {38, 27}
  56 -- v2 [color=none]
  62 -- 54
  62 -- 402 [weight=2]
  38 -- 22 [weight=2]
  38 -- 30
  27 -- 14
  27 -- 15 [weight=3]
  35 -- 23 [weight=4]
  35 -- 28
  24 -- 20
  24 -- 16 [weight=2]
  54 -- {46}
  54 -- v3 [color=none, weight=3]
  v1, v2, v3 [color=none, fontcolor=none, label=""]
  401, 402 [label="40"]
  
}
```

<figcaption>Tas Binaire (Tas-max)</figcaption>

</div>

</div>

### Tas Binaire de type *Tas-min*

<div id="conteneur">

<div>

```dot
graph TAS {
  size="0.6"
  node [shape=circle, fontsize=30, penwidth=3]
  edge [penwidth=3]
  2 -- {6, 8}

}
```

<figcaption>Tas Binaire <br/>(Tas-min)</figcaption>

</div>

<div>

```dot
graph TAS {
  size="1.1"
  node [shape=circle, fontsize=30, penwidth=3]
  edge [penwidth=3]
  3 -- {9, 4}
  9 -- 12
  9 -- v1 [color=none]
  v1 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Tas Binaire <br/>(Tas-min)</figcaption>

</div>

<div style="flex:2;">

```dot
graph TAS {
  size="2"
  node [shape=circle, fontsize=30, penwidth=3]
  edge [penwidth=3]
  1 -- {4, 7}
  4 -- 101 [weight=2]
  4 -- 8
  7 -- 102
  101, 102 [label="10"]
  7 -- v1 [color=none, weight=2]
  v1, v2 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Tas Binaire <br/>(Tas-min)</figcaption>

</div>

<div style="flex:3;">

```dot
graph TAS {
  size="4"
  node [shape=circle, fontsize=38, penwidth=3]
  edge [penwidth=3]
  4 -- {8, 5}
  8 -- v1 [color=none, weight=1]
  8 -- {10,14}
  10 -- {16, 25}
  5 -- {12, 30}
  5 -- v2 [color=none]
  14 -- {20, 18}
  12 -- 15
  12 -- v3 [color=none, weight=2]

  v1, v2, v3 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Tas Binaire (Tas-min)</figcaption>

</div>

<div style="flex:3;">

```dot
graph TAS {
  size="5"
  node [shape=circle, fixedsize=true, width=0.8, fontsize=38, penwidth=3]
  edge [penwidth=3]
  7 -- {12, 17}
  12 -- v1 [color=none]
  12 -- {25, 20}
  25 -- 26 [weight=3]
  25 -- 31
  17 -- {38, 27}
  17 -- v2 [color=none]
  20 -- 34
  20 -- 501 [weight=2]
  38 -- 42 [weight=2]
  38 -- 40
  27 -- 503
  27 -- 54 [weight=3]
  26 -- 29 [weight=4]
  26 -- 43
  31 -- 502
  31 -- 36 [weight=2]
  34 -- 46 [weight=2]
  34 -- 41
  501 -- 52
  501 -- 56 [weight=3]
  42 -- 45
  42 -- v3 [weight=2]
  v1, v2, v3 [color=none, fontcolor=none, label=""]
  501, 502, 503 [label="50"]
  
}
```

<figcaption>Tas Binaire (Tas-min)</figcaption>

</div>

</div>

### PAS un Tas Binaire. Justifiez

<div id="conteneur">

<div>

```dot
graph TAS {
  size="1.1"
  node [shape=circle, fontsize=30, penwidth=3]
  edge [penwidth=3]
  5 -- {3, 6}
  3 -- v1 [color=none]
  3 -- 1
  v1 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Pas un <br/>Tas Binaire</figcaption>

</div>

<div>

```dot
graph TAS {
  size="1.1"
  node [shape=circle, fontsize=30, penwidth=3]
  edge [penwidth=3]
  8 -- {2, 9}
  2 -- {4, 3}
}
```

<figcaption>Pas un <br/>Tas Binaire</figcaption>

</div>

<div>

```dot
graph TAS {
  size="2"
  node [shape=circle, fontsize=30, penwidth=3]
  edge [penwidth=3]
  10 -- {8,7}
  8 -- 9 [weight=2]
  8 -- 6
  7 -- v1 [color=none, weight=2]
  7 -- 2
  v1, v2 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Pas un <br/>Tas Binaire</figcaption>

</div>

<div>

```dot
graph TAS {
  size="3"
  node [shape=circle, fontsize=38, penwidth=3]
  edge [penwidth=3]
  40 -- {36, 43}
  36 -- v1 [color=none, weight=1]
  36 -- {24,13}
  24 -- {20, 18}
  43 -- {11, 25}
  43 -- v2 [color=none]
  11 -- {8, 10}
  v1, v2 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Pas un Tas Binaire</figcaption>

</div>

<div>

```dot
graph TAS {
  size="5"
  node [shape=circle, fixedsize=true, width=0.8,fontsize=48, penwidth=3]
  edge [penwidth=3]
  90 -- {85, 63}
  85 -- v1 [color=none]
  85 -- {401,62}
  401 -- 35 [weight=3]
  401 -- 24
  63 -- {49, 21}
  63 -- v2 [color=none]
  62 -- 54
  62 -- 402 [weight=2]
  49 -- 22 [weight=2]
  49 -- 34
  21 -- 18
  21 -- 6 [weight=3]
  35 -- 23 [weight=4]
  35 -- 36
  24 -- 20
  24 -- v4 [color=none, weight=2]
  54 -- 48
  54 -- 42 [weight=3]
  //54 -- v3 [color=none, weight=3]
  v1, v2, v3, v4 [color=none, fontcolor=none, label=""]
  401, 402 [label="40"]
}
```

<figcaption>Pas un Tas Binaire</figcaption>

</div>

</div>

## Implémentations des Tas Binaires

* **Files de Priorité** : Les Tas Binaires de type **tas-max** (resp. **tas-min**) sont efficaces pour déterminer rapidement la plus grande valeur, donc le *max* (resp. la plus petite valeur, donc le *min*) dans une structure de données sous forme d'arbre binaire. C'est leur utilité principale.
Les Tas Binaires sont donc utiles pour implémenter le type abstrait de données (TAD) appelé des **[Files de Priorité](https://fr.wikipedia.org/wiki/File_de_priorit%C3%A9)**
* **Tri par Tas** : Les Tas Binaires de type **tas-max** (resp. **tas-min**) sont utilisés pour un **algorithme de tri ("*par tas*") efficace** ascendant (resp. descendant)

## Limitations des Tas Binaires

Un Tas Binaire n'est pas adapté aux Opérations suivantes d'un Arbre :

* Recherche d'un élément quelconque
* Insertion d'un élément quelconque
* Suppression d'un élément quelconque

En effet, la Complexité de chacune des opérations précédentes est en $O(n)$, donc lent.
Pour ce type d'utilisation, on préférera les Arbres Binaires de Recherche (ABR), qui sont bien plus adaptés.

