# TNSI : Arbres Quelconques

## Définition d'un Arbre : Arbre Enraciné vs Non Enraciné

!!! def "Arbres : Arbres enracinés vs non enracinés"
    * un <red>Arbre</red> est constitué de <red>noeuds / sommets</red> et d'<red>arêtes</red> reliant certains de ces noeuds :  
    Plus précisément : c'est un **graphe connexe, acyclique (sans aucun cycle) et non orienté**.

        * Chaque noeud peut être <red>étiqueté</red> par une information (selon les contextes : le *nom* du noeud, la *valeur* du noeud, etc..)
        * Chaque information est appelée une <red>étiquette</red>

    * un <red>Arbre Enraciné</red> (ou arborescence) est un arbre **organisé de manière hiérarchique**, càd dans lequel on a choisi un noeud *particulier* appelé la <red>Racine</red>. 
    Dans toute la suite, par défaut, sauf mention contraire, tous les arbres considérés seront enracinés.
    * Un <red>Arbre Non Enraciné</red> est un arbre **sans racine**, càd dans lequel aucun noeud ne joue de rôle particulier.  
    En particulier, un arbre non enraciné n'admet :

        * pas de relation hiérarchique, 
        * pas de relation de descendance, etc..

    * une <red>Forêt</red> est la réunion de plusieurs Arbres disjoints

```dot
digraph arbre {
  size="4";
  splines=true;
  node [shape=circle, fontsize=28, penwidth=3];
  edge [arrowhead=none, penwidth=3]
  761 [label="76"]
  762 [label="76"]
  69 [color=red, fontcolor=red, style=bold]
  racine [label="{Noeud | 'Racine'}", shape=record, color=none, fontcolor=red, fontsize=32]
  racine:sw -> 69:ne [arrowhead=vee, color=red]
  69 -> {761, 762, 68 } [style=line]
  671, 672 [label="67"]
  671, 65, 762, 73, 79 [color=blue, fontcolor=blue, style=bold]
  761 -> {671, 65}
  subgraph cluster0 {
  68
  label="Noeud Père de 79 et 67";
  color=deeppink
  fontcolor=deeppink
  fontsize=32
  }
  subgraph cluster1 {
    672, 79
    label="Noeuds fils de 68"
    color=deeppink
    fontcolor=deeppink
    fontsize=32
  }
  68 -> {672, 79}
  672 -> 73
  Feuilles [label="Feuilles", shape=record, color=none, fontcolor=blue, fontsize=32]
  671:se -> Feuilles:n [dir=back, color=blue, style=dashed, penwidth=2]
  65:se ->  Feuilles:n [dir=back, color=blue, style=dashed, penwidth=2]
  762:se -> Feuilles:n [dir=back, color=blue, style=dashed, penwidth=2]
  79:se ->  Feuilles:n [dir=back, color=blue, style=dashed, penwidth=2]
  73:se ->  Feuilles:n [dir=back, color=blue, style=dashed, penwidth=2]
  }
```

<figcaption><b>Arbre Enraciné, étiqueté<br/> par des nombres entiers</b>
</figcaption>

<div>

<figure> 

```dot
graph G {
  size="3"
  layout=neato
  node [shape=circle, fontsize=28, penwidth=3]
  edge [penwidth=3]
  a -- b
  c -- b
  b -- d
  d -- {e, f}
  e -- {g, h}
  f -- i
  g -- j
}
```

<figcaption><b>Arbre Non Enraciné</b></figcaption>

</figure>

</div>

<clear></clear>

<figure>

<img src="../img/arbreNonEnracine.jpg"/>

<figcaption><b>Exemple d'Arbre Non enraciné en Phylogénétique</b></figcaption>
La <em>Phylogénie</em> est la science s'intéressant à l'évolution des organismes vivants<br/>
Ici : Arbre Phylogénétique non enraciné, représentant les grands groupes d'Archée et le Phylum nanoarchaeota

</figure>

## Sous-Arbres

!!! def
    Un <red>Sous-arbre</red> A d'un arbre T est un arbre tel que:

    * Tous les sommets de A sont aussi des sommets de T
    * Toutes les arêtes de A sont aussi des arêtes de T

<center>

```dot
graph SousArbre {
  size="5"
  color="#999999"
  fontcolor="#999999"
  node [shape=circle, fontname="Fira Code", fontsize=28, penwidth=3]
  edge [penwidth=3]
  1, 2, 3, 4, 5
  1 -- 2
  subgraph cluster01 {
    color=none
    2 -- 5
    5 -- 10 [weight=2]
    5 -- 11
    fontsize=28
  }
  subgraph cluster02 {
    3 -- 6, 7
    7 -- 12, 13, 14
    fontsize=28
    label="Sous-Arbre"
  }
  1 -- 3, 4
  4 -- 8
  subgraph cluster03 {
    8 -- 15 [weight=2] 
    8 -- 16
    fontsize=28
    label="Autre Sous-Arbre"
  }
  4 -- 9 [weight=2]
}
```

</center>

## Noeuds d'un arbre enraciné : Noeuds Externes/Feuilles vs Internes

!!! def
    Dans un arbre (enraciné):

    * Chaque noeud a exactement un seul <red>noeud père</red>, à l'exception de la <red>racine</red> qui n'a pas de père
    * Chaque noeud (père) peut avoir un nombre quelconque de <red>(noeuds) fils</red> (de $0$ à l'$\infty$)
    * Les noeuds qui n'ont pas de fils sont appelés les <red>feuilles</red> ou <red>noeuds externes</red>. 
    * Les autres noeuds sont des <red>noeuds internes</red>
    (noeud interne = tout noeud qui n'est pas une feuille, càd tout noeud ayant des fils)
    * Des noeuds ayant le même père sont dits <red>noeuds frères</red>
    * Des noeuds reliés par une arête sont des <red>noeuds adjacents</red>.

<center>

```dot
graph G {
  size="4.9"
  node [shape=circle, fontsize=34, penwidth=2]
  edge [penwidth=2]
  r
  r -- a
  r -- b
  subgraph cluster01 {
    c
    d
    e
    label="Noeuds Frères"
    fontsize=38
  }
  a -- c
  a -- d
  a -- e
  b -- f
  subgraph cluster02 {
    f [color=red, fontcolor=red, style=bold]
    label="Feuilles"
    fontsize=34
    color=none
    fontcolor=red
  }
  subgraph cluster03 {
    r [color=red, fontcolor=red, style=bold]
    label="Noeuds Internes"
    fontsize=34
    color=none
    fontcolor=blue
  }
  e -- o
  e -- p
  c -- {i, j}
  subgraph cluster04 {
    i
    label="Père de k, de l, et de m"
    fontsize=34
  }
  i -- {k, l ,m}
  subgraph cluster05 {
    k
    l
    m
    label="Les 3 Fils de i"
    fontsize=34
  }
  k -- q -- s
  m -- {u, t}
  r, a, b, c, e, i, j, k, l, m, q [color=blue, fontcolor=blue, penwidth=3]
  j, l, d, o, p, s, u, t  [color=red, fontcolor=red, style=bold]
}
```

<figcaption>Noeuds Internes vs Noeuds Externes/Feuilles.<br/>Noeud Père et Noeuds Fils.</br/>Noeuds Frères</figcaption>

</center>

## Arité d'un Noeud. Arbre N-aire. Arbre Binaire

!!! def
    * l'<red>Arité</red> (quelquefois le <red>degré</red>) d'un noeud est **son nombre de fils**.
    * Un <red>arbre $N$-aire</red> ou <red>arbre d'arité $N$</red>, est un arbre dont **tous les noeuds ont une arité *au plus* égale à $N$**
    $\Leftrightarrow$ un arbre dont chaque noeud a **au plus $N$ fils**
    * Un <red>arbre Binaire</red> est un **arbre 2-aire**,
    càd dont chaque noeud admet au plus $2$ fils (ou enfants): classiquement appelés **Fils Gauche** et **Fils Droit** (ou **enfant gauche** et **enfant droit**)

<center>

```dot
graph G {
  splines=false
  size="3.5"
  node [shape=circle, fontsize=28, penwidth=3]
  edge [penwidth=3]

  subgraph cluster00 {
    a
    color=red
    fontcolor=red
    fontsize=28
    label="arité 4"
  }
  subgraph cluster01 {
    b
    color=blue
    fontcolor=blue
    fontsize=28
    label="arité 2"
  }
  a -- {b, c, d, e} [color=red]
  subgraph cluster02 {
    e
    color=deeppink
    fontcolor=deeppink
    fontsize=28
    label="arité 3"
  }
    subgraph cluster03 {
    h
    color="#22CC22"
    fontcolor="#22CC22"
    fontsize=28
    label="arité 0"
    style=bold
  }
  b -- {f, g} [color=blue]
  e -- {h, i, j} [color=deeppink]
  f, g, c, d, h, i, j [color="#22CC22", fontcolor="#22CC22"]
}
```

<figcaption>Arbre 4-aire</figcaption>

</center>


!!! exp

    <div id="conteneur">

    <div style="flex:2;">

    <center>

    ```dot
    graph G {
      splines=false
      size="4"
      node [shape=circle, fontsize=20]
      1 -- {2, 3}
      2 -- 4 [weight=2]
      2 -- 5
      5 -- {6, 7}
      3 -- v3 [color=none]
      3 -- 8
      8 -- 9
      8 -- v4 [color=none]
      v1, v2, v3, v4, v5, v6 [color=none, fontcolor=none, label=""]
    }
    ```

    <figcaption>Arbre Binaire (2-aire)</figcaption>

    </center>
    
    </div>

    <div>

    <center>

    ```dot
    graph G {
      splines=false
      size="2.5"
      node [shape=circle, fontsize=20]
      1 -- {2, 3, 4}
      2 -- 5
    }
    ```

    <figcaption>Arbre 3-aire</figcaption>

    </center>

    </div>

    <div style="flex:2;">

    <center>

    ```dot
    graph G {
      splines=false
      size="3"
      node [shape=circle, fontsize=20]
      1 -- 2
      2 -- {3, 7, 9}
      3 -- {4, 5, 6}
      7 -- 8
      {rank=same;3;7;9;}
    }
    ```

    <figcaption>Arbre 3-aire</figcaption>

    </center>

    </div>

    <div style="flex:2;">

    <center>

    ```dot
    graph G {
      splines=false
      size="3"
      node [shape=circle, fontsize=20]
      1 -- {2, 3, 4}
      2 -- 5
      3 -- {6, 7, 8, 9}
      7 -- {10, 11, 12}
    }
    ```

    <figcaption>Arbre 4-aire</figcaption>

    </center>

    </div>

    </div>

On pourrait penser naïvement qu'un arbre N-aire est une généralisation d'un arbre Binaire, en fait il n'en est rien (c'est ce qui explique, du moins partiellement, l'intérêt particulier porté aux Arbres Binaires par la suite)

!!! pte
    Tout arbre $N$-aire peut être transformé/implémenté en arbre Binaire

<div id="conteneur">

<div style="flex:3;">

```dot
digraph G {
  size="5"
  node [shape=circle, fontsize=28, penwidth=2]
  edge [penwidth=2]
  a -> b [color=red, penwidth=3]
  b -> f [label="    ", color=red, penwidth=3]
  a -> {b, c, d, e} [dir=none]
  b -> {f, g} [dir=none]
  e -> h [label="    ", color=red, penwidth=3]
  e -> {h, i, j} [dir=none]
  b -> c -> d -> e [label=" ", color=green, penwidth=3]
  f -> g [color=green, penwidth=3]
  h -> i -> j[color=green, penwidth=3]
  {rank=same;b;c;d;e;}
  {rank=same;f;g;}
  {rank=same;f;g;h;i;j}
}
```

<figcaption>Arbre 4-aire</figcaption>

</div>

<div style="font-size:2em;">$$\Leftrightarrow$$</div>

<div style="flex:2;">

```dot
digraph G {
  size="4"
  node [shape=circle, fontsize=38, penwidth=3]
  edge [penwidth=5]
  a -> b [color=red]
  a -> v1 [color=none]
  b -> f [color=red]
  f -> {v2, v3} [color=none]
  f -> g [color=green]
  b -> c [color=green]
  c -> v4 [color=none]
  d -> v5 [color=none]
  c -> d -> e [color=green]
  e -> h [color=red]
  e -> v6 [color=none]
  h -> v7 [color=none]
  h -> i [color=green]
  i -> v8 [color=none]
  i -> j [color=green]
  v1, v2, v3, v4, v5, v6, v7, v8 [color=none, fontcolor=none, label=""]
}
```

<figcaption>Arbre Binaire équivalent</figcaption>

</div>

</div>

<env>**Implémentation d'un Arbre N-aire en Arbre Binaire**</env>
Lorsque $N$ n'est pas fixe, ou tout simplement pour éviter les pertes de place, les arbres $N$-aires sont implémentés par des arbres Binaires : Chaque sommet comporte un pointeur vers son premier fils et vers son premier frère.  Autrement dit :

* le **fils aîné** d'un noeud n-aire père est implémenté par le **fils gauche** d'un noeud binaire père
* le **frère cadet** d'un noeud n-aire est implémenté par le **fils droit** d'un noeud binaire père

!!! ex
    Transformer/Implémenter les arbres $3$-aires et $4$-aires de l'exemple précédent en arbres Binaires.

## Arêtes. Chemins

!!! def
    Une <red>Arête</red> est un trait modélisant une relation entre deux sommets dits **contigüs**, ou **adjacents**.

!!! def
    * Un <red>Chemin</red> (de $s$ à $v$) est une succession de plusieurs arêtes consécutives (reliant $s$ à $v$)
    * La <red>longueur d'un chemin</red> est le nombre d'arêtes sur ce chemin

<div id="conteneur">

<div>

```dot
graph G {
  size="3"
  //splines=false
  node [penwidth=3, shape=circle, fontsize=32]
  edge [penwidth=3, fontsize=28]
  r -- a [label="Chemin", color=red, fontcolor=red]
  r -- b
  a -- c [label="Arête", color=blue, fontcolor=blue]
  a -- d 
  a -- e [color=red, fontcolor=red]
  b -- f
  e -- g [label="Arête", color=blue, fontcolor=blue]
  e -- h [color=red, fontcolor=red]
  r, a, e, h [color=red, fontcolor=red]
}
```

<figcaption>Chemin de la racine <em>r</em> vers <em>h</em><br/>Longueur 3</figcaption>

</div>

<div>

```dot
graph G {
  size="3"
  //splines=false
  node [penwidth=3, shape=circle, fontsize=32]
  edge [fontsize=28, penwidth=3]
  //r, a, b [color=red, fontcolor=red]
  r -- a [label="Chemin", color=red, fontcolor=red]
  r -- b [color=red, fontcolor=red, penwidth=3]
  a -- c [label="Arête", color=blue, fontcolor=blue]
  a -- d
  a -- e [color=red, fontcolor=red]
  b -- f
  e -- g [color=red, fontcolor=red]
  e -- h [label="Arête", color=blue, fontcolor=blue]
  r, a, b, e, g [color=red, fontcolor=red]
}
```

<figcaption>Chemin de <em>b</em> à <em>g</em><br/>Longueur 4</figcaption>

</div>

</div>

!!! pte
    Un arbre est *connexe*, donc il existe toujours au moins un chemin entre deux sommets $s$ et $v$ quelconques
    Un arbre est *acyclique*, donc il existe au plus (donc exactement) un chemin entre deux sommets $s$ et $v$ quelconques

!!! pte
    Il existe exactement un chemin depuis la racine vers chacun des noeuds

## Taille d'un Arbre

!!! def
    La <red>taille</red> d'un arbre est le **nombre de noeuds** qu'il contient. On la note $n$ en général

<div id="conteneur">

<div>

```dot
graph G {
  size="1"
  node [shape=circle, label=" "]
  a --b
}
```

<figcaption><b>Taille n = 2</b></figcaption>

</div>

<div>

```dot
graph G {
  size="1"
  node [shape=circle, label=" "]
  a -- {b, c}
}
```

<figcaption><b>Taille n = 3</b></figcaption>

</div>

<div>

```dot
graph G {
  size="1.5"
  node [shape=circle, label=" "]
  a -- {b, c}
  c -- d
}
```

<figcaption><b>Taille n = 4</b></figcaption>

</div>

<div>

```dot
graph G {
  size="1.5"
  node [shape=circle, label=" "]
  a -- {b, c}
  c -- {g, h}
}
```

<figcaption><b>Taille n = 5</b></figcaption>

</div>

</div>

<div id="conteneur">

<div>

```dot
graph G {
  size="2"
  node [shape=circle, label=" "]
  a -- {b, c}
  b -- {d, e, f}
  c -- {g, h}
  f -- j
}
```

<figcaption><b>Taille n = 9</b></figcaption>

</div>

<div>

```dot
graph G {
  size="2.5"
  node [shape=circle, label=" "]
  a -- {b, c}
  b -- {d, e, f}
  c -- {g, h}
  d -- i
  e -- {j, k}
  g -- l
  j -- m
}
```

<figcaption><b>Taille n = 13</b></figcaption>

</div>

</div>

## Profondeur d'un Noeud. Niveau $p$

!!! def
    La <red>profondeur</red> **d'un noeud** est la **longueur (=nb d'arêtes) du plus court chemin vers la racine**.
    La profondeur de la racine vaut donc $0$

<center>

```dot
graph G {
  size="5"
  node [shape=circle, fontsize=10, label="     ", penwidth=1]
  edge [penwidth=1]
  r
  r -- a
  r -- b
  a -- c
  a -- d
  a -- e
  b -- f
  e -- g
  e -- h
  subgraph cluster01 {
    p0 [label="Profondeur 0"]
    p1 [label="Profondeur 1"]
    p2 [label="Profondeur 2"]
    p3 [label="Profondeur 3"]
    p0, p1, p2, p3 [color=none]
    p0 -- p1 -- p2 -- p3 [color=none]
    label="cycle"
    //labeljust="l"
    fontcolor=red
    fontsize=10
    {rank = same; p0; p1;}
  }
  r -- p0 [style=dashed, penwidth = 1, label="        "]
  b -- p1 [style=dashed, penwidth = 1, label="        "]
  f -- p2 [style=dashed, penwidth = 1, label="        "]
  h -- p3 [style=dashed, penwidth = 1, label="        "]
  {rank=same;r;p0;}
  {rank=same;b;p1;}
  {rank=same;f;p2;}
  {rank=same;h;p3;}
}
```

</center>

!!! def
    Le <red>niveau $p$</red> d'un arbre est la réunion de tous les noeuds ayant la même profondeur $p$

<center>

```dot
graph G {
  size="4"
  node [shape=circle, fontsize=20, label="     "]
  subgraph cluster00 {
    r
    fontsize=24
    label="Niveau 0"
    labeljust="r"
  }
  subgraph cluster01 {
    b, a
    fontsize=24
    label="Niveau 1"
    labeljust="r"
  }
  subgraph cluster02 {
    f, e, d, c
    fontsize=24
    label="Niveau 2"
    labeljust="r"
  }
  subgraph cluster03 {
    h, g
    fontsize=24
    label="Niveau 3"
    labeljust="r"
  }
  r -- a
  r -- b
  a -- c
  a -- d
  a -- e
  b -- f
  e -- g
  e -- h
}
```

</center>

## Hauteur d'un Arbre

!!! def
    La <red>hauteur</red> d'un arbre est **la profondeur du noeud le plus profond**.  
    On la note $h$ en général.

    * la hauteur de l'arbre vide vaut $-1$ par convention
    * la hauteur de l'arbre réduit à la racine vaut $0$

!!! pte
    La hauteur d'un arbre est le nombre d'arêtes du plus long chemin de la racine vers une feuille

<div id="conteneur">

<div>

```dot
graph G {
  size="1"
  node [shape=circle, label=" "]
  a --b
}
```

<figcaption><b>Hauteur h = 1</b></figcaption>

</div>

<div>

```dot
graph G {
  size="1"
  node [shape=circle, label=" "]
  a -- {b, c}
}
```

<figcaption><b>Hauteur h = 1</b></figcaption>

</div>

<div>

```dot
graph G {
  size="1.5"
  node [shape=circle, label=" "]
  a -- {b, c}
  c -- d
}
```

<figcaption><b>Hauteur h = 2</b></figcaption>

</div>

<div>

```dot
graph G {
  size="1.5"
  node [shape=circle, label=" "]
  a -- {b, c}
  c -- {g, h}
}
```

<figcaption><b>Hauteur h = 2</b></figcaption>

</div>

<div style="flex:2;">

```dot
graph G {
  size="2"
  node [shape=circle, label=" "]
  a -- {b, c}
  b -- {d, e, f}
  c -- {g, h}
  f -- j
}
```

<figcaption><b>Hauteur h = 3</b></figcaption>

</div>

<div style="flex:2;">

```dot
graph G {
  size="2"
  node [shape=circle, label=" "]
  a -- {b, c}
  b -- {d, e, f}
  c -- {g, h}
  d -- i
  e -- {j, k}
  g -- l
  j -- m
}
```

<figcaption><b>Hauteur h = 4</b></figcaption>

</div>

</div>

!!! info ":warning: **ATTENTION** :warning:"
    Certains auteurs définissent différemment, comme suit, la hauteur d'un arbre :

    * la hauteur de l'arbre vide $0$
    * la hauteur de l'arbre réduit à une racine vaut $1$
    * Plus généralement, la hauteur est alors le nombre de noeuds du plus long chemin de la racine à une feuille 
    ce qui revient à compter le ***nombre de sommets*** du chemin le plus long entre la racine et une feuille (au lieu de compter les arêtes)
    Bien vérifier la définition choisie dans l'énoncé...


