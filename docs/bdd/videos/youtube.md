# Chaînes et Vidéos Youtube

## Chaîne John Taieb

### Playlist "Cours SQL"

<iframe width="560" height="315" src="https://www.youtube.com/embed/V_jNPU3ewvs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Chaîne Grafikart

### Playlist "Apprendre SQL de A à Z"

<iframe width="560" height="315" src="https://www.youtube.com/embed/iHKE_4KeNWQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>