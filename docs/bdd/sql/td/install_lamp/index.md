# TD : Installer et Configurer LAMP

!!! hint "Pré-supposé"
    :warning: Ce TD pré-suppose que l'OS utilisé soit <rb>Manjaro Linux</rb> (dans une machine Virtuelle) :warning:

* Pour Linux : **LAMP** $=$ <rb>L</rb>inux <rb>A</rb>pache <rb>M</rb>ySQL/MariaDB <rb>P</rb>HP
* Pour Windows : il existe <rb>W</rb>AMP
* Pour Mac : Il existe <rb>M</rb>AMP

Hein :question: quoi :question: :fearful: :question: C'est quoi çà :question: Il s'agit d'un ensemble logiciel (une sorte de pack tout-en-un) constitué :

* d'un **serveur Web (<rb>A</rb>pache)**
* d'un **serveur MariaDB/MySQL (<rb>M</rb>ariaDB)**
* d'un **serveur PHP (<rb>P</rb>HP)**

## Installer LAMP (Linux Apache MariaDB/MySQL PHP)

Installer les paquets nécessaires :

```bash
$ sudo pacman -S apache php php-apache mariadb
$ sudo pacman -S php-sqlite php-sodium
```

**LAMP est maintenant installé** : :stuck_out_tongue_winking_eye:. **MAIS** Il reste encore OBLIGATOIREMENT à **configurer** (au moins un minimum) séparément, chacun des serveurs suivants :

* le serveur Web (<rb>A</rb>pache)
* le serveur MariaDB/<rb>M</rb>ySQL de BDD (MariaDB)
* le serveur PHP (<rb>P</rb>HP)

## Configuration d'Apache (le serveur Web)

* Vérifiez que vous disposez bien de PHP8:

  ```bash
  $ php --version    (ou php -v)
  PHP 8.0.6 (cli) (built: May  5 2021 04:49:35) ( NTS )
  Copyright (c) The PHP Group
  Zend Engine v4.0.6, Copyright (c) Zend Technologies
  ```

* Modifier le fichier de configuration d'Apache `/etc/httpd/conf/httpd.conf` :

  ```bash
  $ sudo nano /etc/httpd/conf/httpd.conf
  ```

  Commentez (ajoutez un `#` en début de ligne) ou bien Décommentez (enlevez le `#` du début de ligne) la/les lignes suivantes, de sorte que le fichier de configuration d'Apache soit cohérent avec  :

  ```bash
  #LoadModule mpm_event_module modules/mod_mpm_event.so
  LoadModule mpm_prefork_module modules/mod_mpm_prefork.so
  ```

  A la fin des Chargements de modules (la liste des **LoadModule**) :

  ```bash
  # Load PHP8 module
  LoadModule php_module modules/libphp.so
  ```

  A la fin des Inclusions de Fichiers (la liste des **Includes**) :

  ```bash
  # PHP8 settings
  Include conf/extra/php_module.conf
  ```

* Sauvegarder vos modifications dans nano : `Ctrl+O` puis
* Sortir de nano : `Ctrl+X`
* Créer un fichier `test.php` pour tester la bonne configuration de PHP:
  * Aller dans le dossier /srv/http
    ```bash
    [eleve@poste1 ~]$ su
    mot de passe : (normalement c'est 'root')
    [poste1 eleve]# cd /srv/http
    [poste1 http]# touch test.php
    ```
  * Editer ce fichier `test.php `
    ```bash
    [poste1 http]# nano test.php
    ```
  * ajouter ces lignes de test de PHP :
    ```php
      <?php
        phpinfo ();
      ?>
    ```
  * sauvegarder vos modifs + sortez du fichier : `Ctrl+O` puis `Ctrl+X`
  * revenez à votre utilisateur normal:
    ```bash
    [poste1 http]#su eleve
    ```
  * Lancez le **serveur Apache**
    ```bash
    [eleve@poste1 http]$ sudo systemctl start httpd
    ```
    Vous ne devriez pas avoir d'erreurs au lancement d'Apache
  * Lancer un navigateur (n'importel lequel), dans la barre d'adresse, tapez:
    ```bash
    localhost    (ou 127.0.0.1)
    ```
    Si tout s'est bien passé vous devriez voir apparaître la page
    
    <img src="img/testPHP.png"/>

    Cliquez sur `test.php`, ou bien tapez directement l'URL: `localhost/test.php`, 
    
    **Si tout est OK,** vous devriez voir quelque chose qui ressemble à çà:
    
    <img src="img/phpinfo.png"/>

## Configuration de PHP

### modifiez le fichier `/etc/php/php.ini`

Modifier le fichier de configuration de PHP :

```bash
$ sudo nano /etc/php/php.ini
```

Chercher (rechercher dans nano : `Ctrl+W`), puis décommentez les lignes suivantes (càd enlever les `;` en début de ligne) :

```bash
extension=mysqli 
extension=pdo_mysql
extension=sodium
extension=sqlite3
```

Rien de spécial à vérifier (si vous n'avez pas fait de bêtises...)

## Configuration de MariaDB

### Installez les *tables MariaDB* (aucune question) :

```bash
$ sudo /usr/bin/mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql

"Installing MariaDB/MySQL system tables in '/var/lib/mysql' ...
OK

To start mysqld at boot time you have to copy
support-files/mysql.server to the right place for your system

Two all-privilege accounts were created.
One is root@localhost, it has no password, but you need to
be system 'root' user to connect. Use, for example, sudo mysql
The second is mysql@localhost, it has no password either, but
you need to be the system 'mysql' user to connect.
After connecting you can set the password, if you would need to be
able to connect as any of these users with a password and without sudo

See the MariaDB Knowledgebase at https://mariadb.com/kb or the
MySQL manual for more instructions.

You can start the MariaDB daemon with:
cd '/usr' ; /usr/bin/mysqld_safe --datadir='/var/lib/mysql'

You can test the MariaDB daemon with mysql-test-run.pl
cd '/usr/mysql-test' ; perl mysql-test-run.pl

Please report any problems at https://mariadb.org/jira

The latest information about MariaDB is available at https://mariadb.org/.
You can find additional information about the MySQL part at:
https://dev.mysql.com
Consider joining MariaDB's strong and vibrant community:
https://mariadb.org/get-involved/"
```

Ensuite:

### Lancez le serveur Mysql

  ```bash
  $ sudo systemctl start mysqld
  ```

### configurez MariaDB la toute première fois

...en exécutant la commande suivante et en répondant simplement aux questions comme indiqué:

```bash
$ sudo mysql_secure_installation
(ou bien)
$ sudo /usr/bin/mysql_secure_installation

"NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, well need the current
password for the root user. If you've just installed MariaDB, and
haven't set the root password yet, you should just press enter here.

Enter current password for root (enter for none): (Appuyer simplement sur la touche Entrée)

OK, successfully used password, moving on...

Setting the root password or using the unix_socket ensures that nobody
can log into the MariaDB root user without the proper authorisation.

You already have your root account protected, so you can safely answer 'n'.

Switch to unix_socket authentication [Y/n] n (Répondre n)

You already have your root account protected, so you can safely answer 'n'.

Change the root password? [Y/n] Y (Répondre Y)

New password: root (choisir OBLIGATOIREMENT ce mot de passe SVP)
Re-enter new password: root (choisir OBLIGATOIREMENT ce mot de passe SVP)
Password updated successfully!
Reloading privilege tables..
 ... Success!

By default, a MariaDB installation has an anonymous user, allowing anyone
to log into MariaDB without having to have a user account created for
them.  This is intended only for testing, and to make the installation
go a bit smoother.  You should remove them before moving into a
production environment.

Remove anonymous users? [Y/n] Y

 ... Success!

Normally, root should only be allowed to connect from 'localhost'.  This
ensures that someone cannot guess at the root password from the network.

Disallow root login remotely? [Y/n] Y

... Success!

By default, MariaDB comes with a database named 'test' that anyone can
access.  This is also intended only for testing, and should be removed
before moving into a production environment.

Remove test database and access to it? [Y/n] Y

 - Dropping test database...
 ... Success!
 - Removing privileges on test database...
 ... Success!

Reloading the privilege tables will ensure that all changes made so far
will take effect immediately.

Reload privilege tables now? [Y/n] Y

 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!"
```

### Vérifiez votre bonne installation de MariaDB

* (si pas encore fait) Lancez le serveur MySQL (le **D**émon)

  ```bash
  $ sudo systemctl start mysqld
  ```

* Connectez-vous au compte `root` du **SGBD MariaDB** :

  ```bash
  $ mysql -u root -p
  Enter password: ('root' normalement)
  ```

* Si tout est OK, vous devriez obtenir la réponse:

  ```sql
  Welcome to the MariaDB monitor.  Commands end with ; or \g.
  Your MariaDB connection id is 4
  Server version: 10.5.10-MariaDB Arch Linux

  Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

  Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

  MariaDB [(none)]>
  ```
