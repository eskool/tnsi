# TNSI : TD SQL Base de Données Films

**MySQL** a été racheté en $2008$ par [Sun Microsystems](https://fr.wikipedia.org/wiki/Sun_Microsystems), lui-même racheté par [Oracle Corporation](https://fr.wikipedia.org/wiki/Oracle_(entreprise)) en $2009$. Conclusion : MySQL n'est plus libre.

**MariaDB** est un fork libre de MySQL: Dans ce TD[^1], **nous travaillerons avec MariaDB**.

## Choisissez un mode de Travail : Online vs Local

Vous pouvez travailler de deux manières différentes :  
(:warning: mais ce ne sera pas le même fichier `Films.sql`, attention :warning:) :

* ou bien *online*, sur le site https://sqliteonline.com/
* ou bien *avec LAMP, en local*, dans votre machine virtuelle **Manjaro Linux**, en ayant préalablement installé LAMP (ce qui est normalement le cas à ce moment du cours). Sinon installer LAMP dans votre VM Linux, en suivant le [TD : Installer et Configurer LAMP](../install_lamp/index.md)

### Si vous travaillez Online sur https://sqliteonline.com/

Il vous faut créer la Base de Données `Films.sql` en ligne :

1. Télécharger la Base de Données [Films.sql](online/Films.sql) en local (sur votre ordi)  
2. Rendez-vous sur le site https://sqliteonline.com/  
3. Créer une **connexion MariaDB** sur le site : :fa-plug: Click to connect
4. Importer la Base de Données `Films.sql` sur le site : File -> :fa-folder-open: Open SQL, puis télécharger le fichier `Films.sql` depuis votre ordi
5. Cliquer sur **Run** ou bien ++shift+enter++ pour :

    * exécuter la création de la Base de Données, et
    * remplir les tables avec certaines données

### Si vous travaillez en local

Dans votre VM Linux, connectez-vous en tant que `eleve:eleve`

1. Télécharger la Base de Données [Films.sql](localLycee/Films.sql) en local (sur votre ordi)  
2. Lancer le démon **mysqld** : `$ sudo systemctl start mysqld`  
3. Copier-coller le fichier `Films.sql` dans le répertoire `Documents` (depuis le répertoire `Téléchargements`), ou bien déplacez-le en ligne de commande:
```console
[eleve@posteXX ~]$ cd Téléchargements
[eleve@posteXX Téléchargements]$ cp Films.sql ~/Documents
```
4. Changer de répertoire en ligne de commandes pour vous rendre dans le dossier `Documents` contenant le fichier `Films.sql` :  
```console
[eleve@posteXX Téléchargements]$ cd ~/Documents
[eleve@posteXX Documents]$
```
5. Importer la Base de Données `Films.sql` en tant que `eleve` dans MySQL:  

    * `$ mysql -u eleve -p < Films.sql`  
Ou bien, alternativement, avec la commande `source`, après s'être connecté à MySQL comme root :
    * `$ mysql -u eleve -p`
    * `mysql> source Films.sql`

## Prendre un Bon Départ & Travail à Faire

### USE : Choisir la Base

(Uniquement) Si vous travaillez en local, vous devez commencer par choisir de travailler la base de données `Films` (car vous pourriez souhaiter travailler avec d'autres bases, qui pourraient exister, à priori):  

```sql linenums="0"
MariaDB [(none)]> use Films;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [Films]>
```

### Bonne Lecture de la Base

Vérifier que vous parvenez bien à lire les données de la base de données `Films`, par exemple :  
`SELECT * FROM Artiste;` doit vous lister tous les artistes. Vous devriez voir quelque chose comme ceci :  
```sql linenums="0"
+-----------+----------------------+---------------+-------------+
| idArtiste | nom                  | prénom        | annéeNaiss  |
+-----------+----------------------+---------------+-------------+
|         1 | Lucas                | George        |        1944 |
|         2 | Hamill               | Mark          |        1951 |
|         3 | Ford                 | Harrison      |        1942 |
...
```

### SHOW TABLES

Afficher les tables de la Base de données `Films`, vous devriez voir quelque chose comme ceci :  

```sql linenums="0"
MariaDB [Films]> show tables;
+-----------------+
| Tables_in_Films |
+-----------------+
| Artiste         |
| Film            |
| Internaute      |
| Notation        |
| Pays            |
| Rôle            |
+-----------------+
6 rows in set (0.001 sec)
```

### Travail à Faire

Dans CHACUNE des parties ci-dessous, lisez les rappels de syntaxe proposés, puis Déterminer la (ou les) bonnes requêtes permetttant de répondre aux questions posées.

## DESCRIBE / DESC

Afficher des détails (Décrire) sur une table, ce qui revient à obtenir son **schéma** :

```sql linenums="0"
MariaDB [Films]> describe Artiste;
-- ou bien :
MariaDB [Films]> desc Artiste;

+-------------+-------------+------+-----+---------+-------+
| Field       | Type        | Null | Key | Default | Extra |
+-------------+-------------+------+-----+---------+-------+
| idArtiste   | int(11)     | NO   | PRI | NULL    |       |
| nom         | varchar(30) | NO   | MUL | NULL    |       |
| prénom      | varchar(30) | NO   |     | NULL    |       |
| annéeNaiss  | int(11)     | YES  |     | NULL    |       |
+-------------+-------------+------+-----+---------+-------+
4 rows in set (0.001 sec)
```

1. Déterminer le Schéma (Relationnel) des autres tables.  
2. Que signifie `NULL` ? Est-ce en contradiction avec `Null -> NO` ? 
3. Que signifie `PRI` ? `MUL` ?

## SELECT .. FROM .. et Clauses ORDER BY, DISTINCT, ..

<env>**Rappel de Syntaxe Générale d'un SELECT**</env>

```sql linenums="0"
SELECT [DISTINCT] {colonne1, colonne2,...}
FROM ma_table
[ WHERE condition_recherche ]
[ GROUP BY {nom_col | expr | position} [ASC | DESC] ]
[ HAVING condition ]
[ ORDER BY {nom_col | expr_tri | position} [ASC | DESC] ]
[ LIMIT n ]
```

Les Clauses GROUP BY et HAVING ne seront pas étudiées ici.

### SELECT .. FROM..

Sélectionner certains attributs d'une table (ou bien tous les attributs, en utilisant le caractère `*`).  
Par exemple, pour sélectionner l'attribut `nom` de la Table `Artistes` :  

```sql linenums="0"
MariaDB [Films]> SELECT nom FROM Artiste;
+----------------------+
| nom                  |
+----------------------+
|                      |
|                      |
| A. Fox               |
| Abrams               |
| Adames               |
...
| Zane                 |
| Zeng Li              |
| Ziyi                 |
| Đan Phạm             |
+----------------------+
1094 rows in set (0.008 sec)
```

1. Sélectionner le nom ET et le prénom de tous les Artistes  
2. Sélectionner le titre de tous les Films  
3. Sélectionner le titre et l'année de tous les Films  
4. Sélectionner le titre de tous les Films, ainsi que l'année du film et le codePays  

### ORDER BY

Une requête où l'on souhaite filtrer l'ordre des résultats utilise la commande `ORDER BY` :  

```sql linenums="0"
SELECT colonne1, colonne2
FROM ma_table
ORDER BY colonne1 [ASC, DESC], colonne2 [ASC, DESC] [, etc..]
```

*Par défault*, les résultats sont classés par ordre **ascendant/croissant** (sans utiliser ORDER BY, ou bien en l'utilisant avec `ASC`). Il est possible de trier sur dans l'ordre décroissant/descendant avec `DESC`.  

1. Quels sont les noms de familles des Artistes, classés par ordre décroissant (des noms) ?  
2. Quelle est la (totalité de tous les attributs de la) table des Artistes, triée par ordre décroissant de l'année de naissance? (les plus jeunes d'abord..)  
3. Quels sont les années de production des films (classés par ordre descendant, càd les plus récents d'abord), et leurs titres (classées par ordre ascendant - à égalité d'année)  

### DISTINCT

DISTINCT permet de ne PAS retenir les DOUBLONS : cette clause ne retient que les entrées DISTINCTES dans les résultats.

```sql linenums="0"
SELECT DISTINCT ma_colonne
FROM ma_table;
```

1. Quels sont les *différents* genres de tous les Films ? Puis, la même chose, mais classés par ordre ascendant de genre.  
2. Quels sont les *différents* codes Pays de tous les Films ?  
3. Quelles sont les *différentes* années pour lesquelles des Films de la Base de Données ont été tournés ? Puis, la même chose, mais classés par ordre descendant des années (càd les plus récentes d'abord)  
4. Quels sont les différents prénoms des Artistes?  

### LIMIT

Pour limiter la taille des (jeux de) résultats, on peut utiliser LIMIT:

```sql linenums="0"
SELECT ma_colonne
FROM ma_table
[..]
LIMIT nombre_entier;
```

1. Quels sont les noms des 10 premiers Artistes ?  
2. Quelles sont les 5 premiers genres de films distincts, classés par ordre ascendant?  
3. Quels sont les 7 derniers pays, par ordre de codePays, parmi ceux ayant produit des films?  
4. Quels sont dix dernières années pour lesquelles des Films existent dans la base de données (les 10 années les plus récentes?  

### WHERE

Pour ne retenir que les résultats qui vérifient une certaine `condition` :

```sql linenums="0"
SELECT ma_colonne
FROM ma_table
WHERE condition;
```

Les `conditions` peuvent utiliser des **opérateurs logiques** :  

| Opérateur| Signification | Exemple |
| :-:| :-: | :- |
|`AND`| ET | SELECT nom_colonnes<br/>FROM ma_table<br/>WHERE condition1 AND condition2|
|`OR`| OU |SELECT nom_colonnes<br/>FROM ma_table<br/>WHERE condition1 OR condition2|
|`IN`| APPARTIENT À |SELECT nom_colonne<br/>FROM ma_table<br/>WHERE nom_colonne IN (valeur1, valeur2, ..)|
|`BETWEEN`| EST COMPRIS ENTRE |SELECT nom_colonne<br/>FROM ma_table<br/>WHERE nom_colonne BETWEEN 'valeur1' AND 'valeur2'|
|`LIKE`| suit le modèle.. |SELECT nom_colonne<br/>FROM ma_table<br/>WHERE nom_colonne LIKE modèle<br/><ul><li>Caractères Spéciaux du modèle :<ul><li>`%` : modélise un nombre quelconque de caractères quelconques</li><li>`_` : modélise un unique caractère quelconque</li></ul></li><li>Exemples :<ul><li>`.. LIKE 'a%'` : Commence par un 'a'</li><li>`.. LIKE '%a'` : Termine par un 'a'</li><li>`.. LIKE '%a%'` : Contient un 'a'</li><li>`.. LIKE 'pa%on'` : commence par `pa`, finit par `on`</li></ul></li> |

1. Quels sont les titres de tous les films produits en 2007 ?
2. Quels sont les noms des artistes dont le nom de famille commence par une lettre après le `R` (inclus) ?  
3. Quels sont les titres de tous les films FRancais (le codePays vaut `FR`)
4. Quels sont les titres des 2 premiers films, au sens de l'ordre alphabétique croissant, produits en 2015 ?
5. Quels sont les noms des 10 premiers Artistes, classés par ordre ascendant de nom, dont les noms de famille commencent par une lettre après `V` (inclus) ?  
6. Quels sont les titres des 3 premiers films de 2018, classés par ordre ascendant de titre ?  
7. Dans la table Artiste, l'idActeur de `Scarlett Johansson` vaut 1245. Quels sont tous les rôles qu'elle a joué?

### AND

L'Opérateur AND signifie ET, impose à deux conditions d'être simultanément vraies pour faire partie du jeux de résultats :  

```sql linenums="0"
SELECT nom_colonnes
FROM ma_table
WHERE condition1 AND condition2
```

1. Quels sont les noms des artistes dont le nom de famille commence par une lettre après le `C` et avant le `E` (inclus) ? (on pourra utiliser `AND`)
2. Quels sont les noms des artistes dont le nom de famille commence par une lettre avant (ou égal à) 'B' et qui sont nés en 1969 ?  
3. Quels sont les titres des Films après 1990 et avant 2002 (inclus) ?  

### OR

L'Opérateur OR signifie OU, impose que l'une des conditions soit vraie, *ou l'autre* (indépendamment l'une de l'autre) pour faire partie du jeux de résultats :  

```sql linenums="0"
SELECT nom_colonnes
FROM ma_table
WHERE condition1 OR condition2
```

1. Quels sont les noms des artistes dont le nom de famille commence par une lettre inférieure à `B` (inclus) ou supérieure à `V` (inclus) ?  
2. Quels sont les titres des Films de 1994 et aussi ceux de 2007 ?

### IN

L'opérateur IN permet de vérifier que les valeurs des résultats APPARTIENNENT À une liste de valeurs entre parenthèses.

```sql linenums="0"
SELECT nom_colonne
FROM ma_table
WHERE nom_colonne IN ( valeur1, valeur2, valeur3, ... )
```

1. Quelles sont les titres des films des années 1989, 1990, 1991 ou 1995
2. Quelles sont les noms des Artistes nés en 1985, 1992, 1993, ou 1994

### BETWEEN

L'opérateur BETWEEN permet de vérifier que les valeurs des résultats sont COMPRISES ENTRE deux valeurs.

```sql linenums="0"
SELECT nom_colonne
FROM ma_table
WHERE nom_colonne BETWEEN valeur1 AND valeur2
```

1. Quelles sont les noms des Artistes dont les noms sont compris entre `Stewart` et `Stowe`?
2. Quelles sont les titres des Films tournées entre 1995 (inclus) et 2004 (inclus) ?

### LIKE

1. Quels sont les noms des artistes dont le nom :

    * contient la lettre `D` ?  
    * commence par la lettre `D` ?  
    * termine par la lettre `D` ?  
    * commence par la lettre `D` ou `E` ?  
    * commence par une lettre comprise entre `R` et `T` ?  
2. Quels sont les titres et leur année précise, des films des années 1990's ?
3. Quels sont les titres et le genre de tous les fims dont le genre est 'Action ' ou 'Aventure' (Aide : il n'y a pas d'autre genre commençant par la lettre 'A')
4. Quels sont les titres exacts des films dont le titre contient 'Kill Bill' ?
5. Quels sont les titres de tous les films dont le titre contient 'Star' ?

### IS NULL, IS NOT NULL

Pour filtrer les résultats où les champs d’une colonne sont à NULL il convient d’utiliser la syntaxe suivante:

```sql linenums="0"
SELECT ma_colonne
FROM ma_table
WHERE nom_colonne IS NULL
```

**A l'inverse** pour filtrer les résultats et obtenir uniquement les enregistrements qui ne sont pas null, il convient d’utiliser la syntaxe suivante :

```sql linenums="0"
SELECT ma_colonne
FROM ma_table
WHERE nom_colonne IS NOT NULL
```

La table Artiste n'est pas complète : Certains Artistes ont une année de naissance qui n'est pas rensignée : elle vaut NULL.

1. Quels sont les noms et prénoms (et annéeNaiss) des artistes ayant une année de Naissance définie à NULL?
2. Quels sont les artistes ayant une année de Naissance correctement définie?

## Fonctions d'Agrégation

### COUNT

COUNT() compte le nombre de résultats :  

```sql linenums="0"
SELECT COUNT(nom_colonne)
FROM ma_table
```

1. Quel est le nombre total de Films dans la Base de Données?  
2. Quel est le nombre total d'Artistes dans la Base de Données?  
3. Quel est le nombre de genres de films distincts?  
4. Combien de Films en tout dont le genre soit `Action`?
5. Combien d'Artistes en tout dont le prénom soit `William`?
6. Combien de Films FRançais (codePays='FR') en tout ?

### SUM

SUM() fait la somme de toutes les valeurs d'une colonne :  

```sql linenums="0"
SELECT SUM(nom_colonne)
FROM ma_table
```

1. Quelle est la somme de toutes les notes de tous les films de la base?  
2. Quelle est la somme de toutes les années de tous les films de la base?  

### AVG pour AVERAGE

AVG() calcule la moyenne des valeurs dans une colonne de résultats :  

```sql linenums="0"
SELECT AVG(nom_colonne)
FROM ma_table
```

1. En quelle année, en moyenne, un film de la base a-t-il été produit ?  
2. En quelle année, en moyenne, est né un Artiste de la base?  

### MAX

MAX() renvoie la valeur maximum dans une colonne de résultats :  

```sql linenums="0"
SELECT MAX(nom_colonne)
FROM ma_table
```

1. Quel est la meilleure note obtenue par un film de la base?  
2. Quelle est l'année la plus récente de production d'un film?  

### MIN

MIN() renvoie la valeur minimale dans une colonne de résultats :  

```sql linenums="0"
SELECT MIN(nom_colonne)
FROM ma_table
```

1. Quel est est la plus mauvaise note obtenue par un film de la base?  
2. Quelle est l'année la plus ancienne de production d'un film?  

## JOIN - JOINTURES

Le principe des jointures entre deux tables, est basé sur une (ou des) **conditions** `ON ..` devant être vérifiées par un (ou plusieurs) attributs de l'une des tables comparés avec un (ou des) attributs de l'autre table. Classiquement, mais non obligatoirement, il s'agit de faire correspondre un attribut de l'un (une clé primaire), avec l'attribut correspondant de l'autre table (une clé étrangère).

### INNER JOIN

La jointure interne appelée `INNER JOIN`, ou simplement `JOIN` sur MariaDB, quelquefois EQUIJOIN, renvoie les enregistrements/lignes de la table de gauche (table1) pour lesquels il existe au moins un enregistrement/ligne dans la table de droite (table2) correspondant (à la *condition* `ON`..).  
:warning: **Aucune ligne du résultat d'une jointure interne ne contient donc de NULL**.

![INNER JOIN](inner-join.svg){.center}

<center>
<b>INNER JOIN - JOINTURE INTERNE</b>
</center>

```sql linenums="0"
SELECT colonne1, colonne2, ..
FROM table1
INNER JOIN table2 ON table1.id = table2.fk_id
```

Syntaxe Alternative :  

```sql linenums="0"
SELECT *
FROM table1
INNER JOIN table2
WHERE table1.id = table2.fk_id
```

1. Quels sont (toutes) les informations sur tous les films et les noms des Réalisateurs les ayant réalisés?
2. Quels sont (uniquement) les titres des films et les noms des Réalisateurs les ayant réalisés?
3. Quels sont (uniquement) les titres des films et les noms des Réalisateurs les ayant réalisés, classés par ordre croissant des réalisateurs? par ordre de Films ?
4. Quels sont tous les noms et prénom de chaque acteur, et tous les noms de rôles qu'ils ont joué ?
5. Quels sont tous les **ids** de films, avec les noms et prénoms de chaque acteur qui y jouent, et les rôles correspondant ?
Idem que 4, mais en commençant par les id de films correspondants ?
6. Quels sont tous les Rôles par Film, classés par ordre croissant d'idFilm?
7. En sachant que le film `Fargo` admet un `idFilm` qui vaut 275 dans la table Film. Quelles sont tous les rôles de ce film?
8. Quels sont tous les titres de films, et les rôles de `Scarlett Johansson` dont l'id est 1245?
9. :rocket: :rocket: (:warning: double jointure :warning:) Quels sont tous les **noms** de films, avec les noms et prénoms de chaque acteur qui y jouent, et les noms des rôles correspondant. Vous devriez voir quelque chose comme ceci:  
```sql linenums="0"
+-----------------------+--------+----------+----------------------+
| titre                 | nom    | prénom   | nomRôle              |
+-----------------------+--------+----------+----------------------+
| La Guerre des étoiles | Hamill | Mark     | Luke Skywalker       |
| La Guerre des étoiles | Ford   | Harrison | Han Solo             |
| La Guerre des étoiles | Fisher | Carrie   | Princess Leia Organa |
...
+-----------------------+--------+----------+----------------------+
```
10. :rocket: :rocket: (:warning: double jointure :warning:) Quels sont tous les titres de Films dans lesquels a joué `Brad Pitt`, et les noms de ses Rôles correspondant, classés par ordre croissant de titre de films?  
Vous devriez voir quelque chose comme ceci :  

```sql linenums="0"
+----------------------+------------------------------------------+
| titre                | nomRôle                                  |
+----------------------+------------------------------------------+
| Babel                | Richard Jones                            |
| Fight Club           | Tyler Durden                             |
..
```

Est-ce possible de personnaliser l'entête `nomRôle` en `nom des Rôles de Brad Pitt`? Et si OUI, comment? Vous devriez voir quelque chose comme ceci :  
```sql linenums="0"
+----------------------+------------------------------------------+
| titre                | nom des Rôles de Brad Pitt               |
+----------------------+------------------------------------------+
| Babel                | Richard Jones                            |
| Fight Club           | Tyler Durden                             |
..
```

### LEFT (OUTER) JOIN

La <rb>Jointure Externe Gauche</rb>, ou <rb>LEFT JOIN</rb>, ou <rb>LEFT OUTER JOIN</rb>, permet de renvoyer tous les enregistrements/lignes de la table de gauche (table1), même s’ils n’ont pas de correspondance dans la table de droite (table2). :warning: **Dans une jointure (externe) gauche, les lignes de la table de droite sans correspondance vaudront toutes NULL**.

![LEFT JOIN](left-join.svg){.center}

<center>
<b>LEFT (OUTER) JOIN - JOINTURE (EXTERNE) GAUCHE</b>
</center>

```sql linenums="0"
SELECT colonne1, colonne2, ..
FROM table1
LEFT JOIN table2 ON table1.id = table2.fk_id
```

Syntaxe Alternative :  

```sql linenums="0"
SELECT colonne1, colonne2, ..
FROM table1
LEFT OUTER JOIN table2 ON table1.id = table2.fk_id
```

1. Quels sont les titres et années de productions de films et les noms des artistes nés la même année, classés par ordre croissant des années des films ? (on acceptera comme résultat les années de production de films pour lesquelles aucun acteur n'est né)
2. Quels sont tous les codes de Pays et leurs titres de films associés, y compris les codes pays n'ayant produit aucun film ?
3. On souhaite obtenir/déduire les idArtiste, nom et prénom des artistes qui ne sont pas des Réalisateurs? Quelle jointure **gauche** sur la table Artiste affiche un jeu de résultats commençant par les résultats ci-dessous ?  
```sql linenums="0"
+-----------+----------------------+---------------+----------------+
| idArtiste | nom                  | prénom        | idRéalisateur  |
+-----------+----------------------+---------------+----------------+
|     37131 |                      | Bourvil       |           NULL |
|     29427 |                      | Terry-Thomas  |           NULL |
|      2535 | A. Fox               | Vivica        |           NULL |
|     15344 | Abrams               | J.J.          |          15344 |
|     15344 | Abrams               | J.J.          |          15344 |
...
```
4. (En déduire une requête avec jointure gauche modifiée pour savoir) Quels sont les idArtiste, nom et prénom des Artistes qui ne sont pas des Réalisateurs?
5. (En déduire une jointure externe gauche, ou une jointure interne, permettant de savoir) Quels sont les idArtiste, nom et prénom des Artistes qui sont aussi des Réalisateurs?  

### RIGHT (OUTER) JOIN

La <rb>Jointure Externe Droite</rb>, ou <rb>RIGHT JOIN</rb>, ou <rb>RIGHT OUTER JOIN</rb>, permet de renvoyer tous les enregistrements/lignes de la table de droite (table2), même s’il n’y a pas de correspondance dans la table de gauche (table1). :warning: **Dans une jointure (externe) droite, les lignes de la table de gauche sans correspondance vaudront toutes NULL**.

![RIGHT JOIN](right-join.svg){.center}

<center>
<b>RIGHT (OUTER) JOIN - JOINTURE (EXTERNE) DROITE</b>
</center>

```sql linenums="0"
SELECT colonne1, colonne2, ..
FROM table1
RIGHT JOIN table2 ON table1.id = table2.fk_id
```

Syntaxe Alternative :  

```sql linenums="0"
SELECT colonne1, colonne2, ..
FROM table1
RIGHT OUTER JOIN table2 ON table1.id = table2.fk_id
```

1. Quels sont tous les titres de films, et leurs codes de Pays associés, y compris les codes pays n'ayant produit aucun film ?
2. On souhaite obtenir/déduire les idArtiste, nom et prénom des artistes qui ne sont pas des Réalisateurs? Quelle jointure **droite** sur la table Artiste affiche un jeu de résultats commençant par les résultats ci-dessous ?  
```sql linenums="0"
+----------------+-----------+----------------------+---------------+
| idRéalisateur  | idArtiste | nom                  | prénom        |
+----------------+-----------+----------------------+---------------+
|           NULL |     37131 |                      | Bourvil       |
|           NULL |     29427 |                      | Terry-Thomas  |
|           NULL |      2535 | A. Fox               | Vivica        |
|          15344 |     15344 | Abrams               | J.J.          |
|          15344 |     15344 | Abrams               | J.J.          |
...
```
3. (En déduire une requête avec jointure droite modifiée pour savoir) Quels sont les idArtiste, nom et prénom des Artistes qui ne sont pas des Réalisateurs? (la requête)
4. (En déduire une jointure externe droite, ou une jointure interne, permettant de savoir) Quels sont les idArtiste, nom et prénom des Artistes qui sont aussi des Réalisateurs?  

## UPDATE

L'instruction UPDATE permet la mise à jour/modification d'enregistrement déjà existants :  

```sql linenums="0"
UPDATE table
SET colonne_1 = 'valeur 1', colonne_2 = 'valeur 2', colonne_3 = 'valeur 3'
WHERE condition
```

Cette syntaxe permet d’attribuer une nouvelle valeur à la colonne nom_colonne_1 pour les lignes qui respectent la condition stipulé avec WHERE.

1. Grâce à des requêtes séparées, Modifier le titre des Films contenant la chaîne `Kill Bill`, de sorte que ceux-ci soient modifiés comme suit:  

    * `Kill Bill : Volume 1` --> est renommé en --> `Kill Bill 1`
    * `Kill Bill : Volume 2` --> est renommé en --> `Kill Bill 2`
2. La table `Artiste` est incomplète : certains Artistes ne bénéficient pas d'une année de naissance correcte (leur `annéeNaiss` vaut NULL). Grâce à une requête, Lister tous les Artistes dans cette situation. Chercher sur internet une année de naissance pour l'un d'entre eux, par exemple `Paul Amiot` dont l'`annéeNaiss` vaut <rb>1886</rb>. Mettre à jour la base de données pour cet artiste. Et quelques autres.  
3. L'Artiste `David REY` a son nom de famille écrit en majuscules dans la table Artiste. Modifier son nom pour qu'il devienne `Rey`

## INSERT

L’insertion de données dans une table s’effectue à l’aide de la commande INSERT INTO. Une seule ligne, ou plusieurs d'un coup peuvent être ajoutées.

### Syntaxe pour ajouter une seule ligne

```sql linenums="0"
INSERT INTO ma_table (nom_colonne_1, nom_colonne_2, ...)
VALUES ('valeur1', 'valeur2', ...)
```

**Remarque :** Il est possible de ne pas renseigner toutes les colonnes. De plus, l’ordre des colonnes n’est pas important.

### Syntaxe pour ajouter plusieurs lignes simultanément

```sql linenums="0"
INSERT INTO ma_table (nom_colonne_1, nom_colonne_2, ...)
VALUES
('valeur1', 'valeur2', ...)
('valeur3', 'valeur4', ...)
('valeur5', 'valeur6', ...)
```

### Ajouter quelques films récents

Le film 'Dont't Look Up : Déni Cosmique", sorti en 2021, dont le réalisateur/artiste est `Adam McKay` n'est pas dans la base, par contre ce réalisateur appartient déjà à la table artiste.

1. Quel est l'**idArtiste** de `Adam McKay` ?  
2. Dans la table Film, ajouter le film `Don't Look Up : Déni Cosmique` avec les informations suivantes[^3] (tirées de [cette page](https://www.senscritique.com/film/don_t_look_up_deni_cosmique/41371853) du site [Sens Critique](https://www.senscritique.com/film/)) :  

    * `idFilm` : 500000
    * `titre` : Don't Look Up : Déni Cosmique
    * `année` : 2021
    * `idRéalisateur` : 55710
    * `genre` : Science-Fiction
    * `résumé` : Deux piètres astronomes s'embarquent dans une gigantesque tournée médiatique pour prévenir l'humanité qu'une comète se dirige vers la Terre et s'apprête à la détruire. 
    * `codePays` : US  

3. Dans la table Film, ajouter deux ou trois films très récents (après 2019 pour éviter les doublons). On pourra s'inspirer des critiques du site Sens Critique pour les résumés. Idéalement les Réalisateurs existent déjà dans la base (pour éviter devoir gérer plusieurs problèmes)  
4. Dans la table Artiste, ajouter les réalisateurs suivants, avec les informations suivantes :

    * `idArtiste`: 2050100
    * `nom` : Kervern
    * `prénom` : Gustave
    * `annéeNaiss` : 1962

    et

    * `idArtiste`: 2050200
    * `nom` : Délépine
    * `prénom` : Benoît
    * `annéeNaiss` : 1958

## DELETE

La commande DELETE en SQL permet de supprimer des lignes dans une table.

```sql linenums="0"
DELETE FROM ma_table
WHERE condition
```

1. Dans la table Film, supprimer le film `Don't Look Up : Déni Cosmique` ajouté précédemment  
2. Dans la table Artiste, supprimer le réalisteur Gustave Kervern ajouté précédemment

## Références

[^1]: La base de Données `Films.sql` proposée dans ce TD est issue de [http://webscope.bdpedia.fr](http://webscope.bdpedia.fr)
[^2]: [SQL.sh : Ce site référence les Syntaxes des Requêtes et Clauses SQL Usuelles](https://sql.sh/)
[^3]: [Sens Critique](https://www.senscritique.com/film/) : Un site qui référence des critiques de films