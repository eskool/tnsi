# TNSI : BDD - TD SQL du CNAM

Ce TD provient du site du **CNAM - Centre National d'Enseignement à Distance**

## TD à faire

Le TD à faire se trouve sur cette page : https://deptfod.cnam.fr/bd/tp/ 

Déterminez toutes les requêtes demandées (seul l'onglet Requêtes est pertinent pour nous) :

* pour la Base de Données `films`
* pour la Base de Données `voyageurs`
* pour la Base de Données `immeubles`
* pour la Base de Données `messagerie`

Les réponses sont disponibles pour chaque requête, si vraiment vous bloquez..

## Références 

Un Cours de Référence sur le Langage SQL, **qui dépasse largement (:warning:) le simple programme de TNSI**, est disponible sur ces pages :

* http://sql.bdpedia.fr/ pour la partie Modèles & Langages
* http://sys.bdpedia.fr/ pour les Aspects Systèmes
* http://www.bdpedia.fr/ pour l'Accueil Général
* http://b3d.bdpedia.fr pour des cours de Bases de Données Documentaires et Distribuées (NoSQL, TOTALEMENT HORS PROGRAMME)
