# TNSI : BDD - Opérations entre Tables/Relations

## Introduction - Algèbre Relationnelle

L'idée est de donner un sens à des **opérations** entre les tables, inspiré de ce qui se fait en *mathématiques*, en particulier de la ***théorie des ensembles***. L'ensemble des tables et des opérations entre les tables est appelée **Algèbre Relationnelle**.

## Union

!!! def "Union"
    Soit $T_1$ et $T_2$ deux tables/entités d'une base de données.
    L'<bred>Union</bred> des deux tables $T_1$ et $T_2$, est l'ensemble des éléments qui sont $T_1$ ou bien dans $T_2$. On la note <enc>$T_1 \cup T_2$</enc>

    $$T_1 \cup T_2 = \{ e\in T_1 \text{ ou } e \in T_2 \}$$

!!! exp "Union de deux Tables"
    On dispose des tables $T_1$ et $T_2$ suivantes:

    !!! col __25 center
        | $T_1$ |
        | :-: |
        | **nom** |
        | Dupont |
        | Dupond |
        | Durand |
        | Dufour |
        | Dumiel |

    !!! col __10 center
        <div style="font-size:3em;margin-top:3em;">$\cup$</div>

    !!! col __25 center
        | $T_2$ |
        | :-: |
        | **nom** |
        | Dumont |
        | Dufour |
        | Durville |
        | Durail |
        | Dupont |

    !!! col __10 center
        <div style="font-size:3em;margin-top:3em;">$=$</div>

    !!! col __25 center
        | $T_1 \cup T_2$ |
        | :-: |
        | **nom** |
        | Dupont |
        | Dupond |
        | Durand |
        | Dufour |
        | Dumiel |
        | Dumont |
        | Durville |
        | Durail |

<clear></clear>

## Intersection

!!! def "Intersection"
    Soit $T_1$ et $T_2$ deux tables/entités d'une base de données.
    L'<bred>Intersection</bred> des deux tables $T_1$ et $T_2$, est l'ensemble des éléments qui sont $T_1$ et aussi dans $T_2$. On la note <enc>$T_1 \cap T_2$</enc>

    $$T_1 \cap T_2 = \{ e\in T_1 \text{ et } e \in T_2 \}$$

!!! exp "Intersection de deux Tables"
    On dispose des tables $T_1$ et $T_2$ suivantes:

    !!! col __25
        | $T_1$ |
        | :-: |
        | **nom** |
        | Dupont |
        | Dupond |
        | Durand |
        | Dufour |
        | Dumiel |

    !!! col __10 center
        <div style="font-size:3em;margin-top:3em;">$\cap$</div>

    !!! col __25
        | $T_2$ |
        | :-: |
        | **nom** |
        | Dumont |
        | Dufour |
        | Durville |
        | Durail |
        | Dupont |

    !!! col __10 center
        <div style="font-size:3em;margin-top:3em;">$=$</div>

    !!! col __25
        | $T_1 \cap T_2$ |
        | :-: |
        | **nom** |
        | Dupont |
        | Dufour |

<clear></clear>

## Différence

!!! def "Différence"
    Soit $T_1$ et $T_2$ deux tables/entités d'une base de données.
    La <bred>Différence</bred> des deux tables $T_1$ et $T_2$, est l'ensemble des éléments qui sont $T_1$ mais pas dans $T_2$. On la note <enc>$T_1 - T_2$</enc>

    $$T_1 - T_2 = \{ e\in T_1 \text{ et } e \not \in T_2 \}$$

!!! exp "Différence de deux Tables"
    On dispose des tables $T_1$ et $T_2$ suivantes:

    !!! col __25 center
        | $T_1$ |
        | :-: |
        | **nom** |
        | Dupont |
        | Dupond |
        | Durand |
        | Dufour |
        | Dumiel |

    !!! col __10 center
        <div style="font-size:3em;margin-top:3em;">$-$</div>

    !!! col __25 center
        | $T_2$ |
        | :-: |
        | **nom** |
        | Dumont |
        | Dufour |
        | Durville |
        | Durail |
        | Dupont |

    !!! col __10 center
        <div style="font-size:3em;margin-top:3em;">$=$</div>

    !!! col __25 center
        | $T_1 - T_2$ |
        | :-: |
        | **nom** |
        | Dupond |
        | Durand |
        | Dumiel |

<clear></clear>

## Produit Cartésien

!!! def "Produit Cartésien"
    Soit $T_1$ et $T_2$ deux tables/entités d'une base de données.
    Le <bred>Produit Cartésien</bred> des deux tables $T_1$ et $T_2$, est l'ensemble des couples $(e_1,e_2)$ lorsque $e_1$ parcourt $T_1$, et $e_2$ parcourt $T_2$. On le note <enc> $T_1 \times T_2$</enc>

    $$T_1 \times T_2 = \{ (e_1,e_2) \,\, | \,\, e_1 \in T_1 \text{ et } e_2 \in T_2 \}$$

!!! exp "Produit Cartésien de deux Tables"
    On dispose des tables $T_1$ et $T_2$ suivantes:
    
    !!! col __45 center
        | $T_1$ | |
        | :-: | :-:
        | **id** | **nom** |
        | 1 | Dupont |
        | 2 | Dufour |
        | 3 | Dumiel |

    !!! col __10 center
        <div style="font-size:3em;margin-top:2em;">$\times$</div>

    !!! col __45 center
        | $T_2$ | |
        | :-: | :-: |
        | **quantite** | **prix** |
        | $260$ | $40$ |
        | $350$ | $17$ |

    <clear></clear>

    !!! col __10 center
        <div style="font-size:3em;margin-top:3em;">$=$</div>

    !!! col __90 center
        | $T_1 \times T_2$ | | |  |
        | :-: | :-: | :-: | :-: |
        | **id** | **nom** | **quantite** | **prix** |
        | $1$ | Dupont | $260$ | $40$ |
        | $1$ | Dupont | $350$ | $17$ |
        | $2$ | Dufour | $260$ | $40$ |
        | $2$ | Dufour | $350$ | $17$ |
        | $3$ | Dumiel | $260$ | $40$ |
        | $3$ | Dumiel | $350$ | $17$ |

<clear></clear>

## Sélection $\sigma$

!!! def "Sélection"
    Soit $T_1$ une table/entité d'une base de données.
    La <bred>Sélection</bred> dans une table $T_1$ est l'opération consistant à ne retenir que les enregistrements de $T_1$ vérifiant une certaine condition (ou filtre) $C$ donnée. On la note <enc>$\sigma_C(T_1)$</enc>.

!!! exp "Sélection dans une Table"
    On dispose de la table $T_1$ suivante:

    !!! col __100 center
        | $T_1$ |  |  |  |
        | :-: | :-: | :-: | :-: |
        | **id** | **nom** | **prenom** | **genre** |
        | $1$ | Dupont  | Gaelle | Femme |
        | $2$ | Dupond  | Jean | Homme |
        | $3$ | Durand  | Laura | Femme |
        | $4$ | Dufour  | Sarah | Femme |
        | $5$ | Dumiel  | Paul | Homme |
        | $6$ | Durmont  | Karl | Autre |

    Sélection dans $T_1$, avec la Requête/Condition $C=\{\text{genre=Femme}\}$, notée $\sigma_C(T_1)$ :  

    !!! col __100 center
        | Sélection dans $T_1$ |  |  |  |
        | :-: | :-: | :-: | :-: |
        | **id** | **nom** | **prenom** | **genre** |
        | $1$ | Dupont  | Gaelle | Femme |
        | $3$ | Durand  | Laura | Femme |
        | $4$ | Dufour  | Sarah | Femme |

        <figcaption>Résultat de la Requête "Sélection avec Condition $C=\{\text{genre=Femme}\}$ "

        </figcaption>

        </figure>

<clear></clear>

## Projection $\pi$

!!! def "Projection"
    Soit $T_1$ une table/entité d'une base de données.
    La <bred>Projection</bred> d'une table $T_1$ est l'opération consistant à ne retenir que certains champs/attributs $A_1, A_2,.., A_k$ de $T_1$. On la note <enc>$\pi_{A_1, A_2, ..,A_k}(T_1)$</enc>

!!! exp "Projection d'une Table sur des champs/attributs"
    On dispose de la table $T_1$ suivante:

    !!! col __68 center
        | $T_1$ |  |  |  |
        | :-: | :-: | :-: | :-: |
        | **id** | **nom** | **prenom** | **genre** |
        | $1$ | Dupont  | Gaelle | Femme |
        | $2$ | Dupond  | Jean | Homme |
        | $3$ | Durand  | Laura | Femme |
        | $4$ | Dufour  | Sarah | Femme |
        | $5$ | Dumiel  | Paul | Homme |
        | $6$ | Durmont  | Karl | Autre |

    !!! col __32 center
        | Projection de $T_1$ sur `'id'` et `'prenom'` |  |
        | :-: | :-: |
        | **id** | **prenom** |
        | $1$ | Gaelle |
        | $3$ | Laura |
        | $4$ | Sarah |

        <figcaption>

        Projection $\pi_{id,prenom}(T_1)$, de $T_1$ sur `'id'` et `'prenom'`

        </figcaption>

<clear></clear>

## Jointures

Une **jointure** est une manière de fusionner virtuellement deux ou plusieurs tables, de manière à ne conserver comme résultats que les enregistrements qui vérifient certaines **conditions caractéristiques** (de la jointure).
Il existe en effet plusieurs types de jointures : C'est la condition choisie qui détermine le type de jointure.

### Jointure Interne

!!! def "Jointure Interne"
    Soit $T_1$ et $T_2$ deux tables/entités.
    La <bred>jointure interne</bred> de $T_1$ et $T_2$ **sur un champ/attribut donné $A$**, est une table formée en ne conservant que les enregistrements de $T_1$ et de $T_2$ vérifiant la **condition**: <enc>$T1.A = T2.A$</enc> (**égalité des champs $A$ de T1 et de T2**).  
    
!!! nota
    On la note <enc>$T_1 \Join T_2$</enc> ou quelquefois <enc>$T_1 \underset{A} \Join T_2$</enc> , pour être plus précis.

!!! pte "En Pratique"
    La jointure interne renvoie les enregistrements/lignes de la table de gauche (table1) pour lesquels il existe au moins un enregistrement/ligne dans la table de droite (table2) correspondant à la **condition** d'égalité du champ $A$.  
    :warning: **Aucune ligne du résultat d'une jointure interne ne contient donc de NULL**.

On peut schématiser cette jointure par le schéma suivant:

![INNER JOIN](inner-join.svg){.center}

<center>
<b>INNER JOIN - JOINTURE INTERNE</b>
</center>

!!! exp "Jointure Interne de deux Tables sur un champ/attribut donné"
    On dispose des tables $T_1$ et $T_2$ suivantes :

    !!! col __61 center
        | $T_1$ : utilisateur |  |  |  |
        | :-: | :-: | :-: | :-: |
        | **id** | **id_ville** | **nom** | **prenom** |
        | $1$ | $3$ | Dupont | Gaelle | Femme |
        | $2$ | $4$ | Dutronc | Jacques | Homme |
        | $3$ | $1$ | Dupond | Jean | Homme |
        | $4$ | $3$ | Durand | Laura | Femme |
        | $5$ | $1$ | Dufour | Sarah | Femme |
        | $6$ | $1$ | Dumiel | Paul | Homme |
        | $7$ | $3$ | Durmont | Karl | Autre |

    !!! col __7 center
        <div style="font-size:2em;margin-top:2.8em;">$\underset{id\_ville} \Join$</div>

    !!! col __32 center
        | $T_2$ : ville |  |
        | :-: | :-: |
        | **id** | **nom_ville** |
        | $1$ | Marseille |
        | $2$ | Toulouse |
        | $3$ | Bordeaux |

    <clear></clear>

    !!! col __10 center
        <div style="font-size:3em;margin-top:3em;">$=$</div>

    !!! col __90 center
        | Jointure Interne de $T_1$ et $T_2$ sur `'id_ville'` |  |  |  |  |  |
        | :-: | :-: | :-: | :-: | :-: | :-: |
        | **id** | **id_ville** | **nom** | **prenom** | **id_ville** | **nom_ville** |
        | $1$ | $3$ | Dupont  | Gaelle | $3$ | Bordeaux |
        | $3$ | $1$ | Dupond  | Jean | $1$ | Marseille |
        | $4$ | $3$ | Durand  | Laura | $3$ | Bordeaux |
        | $5$ | $1$ | Dufour  | Sarah | $1$ | Marseille |
        | $6$ | $1$ | Dumiel  | Paul | $1$ | Marseille |
        | $7$ | $3$ | Durmont  | Karl | $3$ | Bordeaux |

        <figcaption>

        Jointure Interne de $T_1$ et $T_2$ sur `'id_ville'`

        </figcaption>

        </figure>

<clear></clear>

### Jointure Externe Gauche

!!! def "Jointure Externe Gauche de T1 et T2 sur un attribut A"
    Soit $T_1$ et $T_2$ deux tables/entités.
    La <bred>jointure externe gauche</bred> de $T_1$ avec $T_2$ **sur un champ/attribut donné $A$** est une table unissant :

    * les enregistrements de $T_1$ et de $T_2$ ayant la même valeur commune de $A$ (comme pour une jointure interne), et aussi
    * les enregistrements **de la table gauche** $T_1$ pour lesquels la valeur de l'attribut $A$ ne correspond avec aucune valeur de l'attribut $A$ de $T_2$

!!! pte "En Pratique"
    La <rb>Jointure Externe Gauche</rb>, ou <rb>LEFT JOIN</rb>, ou <rb>LEFT OUTER JOIN</rb>, permet de renvoyer tous les enregistrements/lignes de la table de gauche (table1), même s’ils n’ont pas de correspondance dans la table de droite (table2). :warning: **Dans une jointure (externe) gauche, les lignes de la table de droite sans correspondance vaudront toutes NULL**.

!!! nota
    On la note $T_1 \leftouterjoin T_2$ (to solve ...)
On peut schématiser cette jointure par le schéma suivant:

![LEFT JOIN](left-join.svg){.center}

<center>
<b>LEFT (OUTER) JOIN - JOINTURE (EXTERNE) GAUCHE</b>
</center>

!!! exp "Jointure Externe Gauche de deux Tables sur un champ/attribut donné"
    On dispose des tables $T_1$ et $T_2$ suivantes :

    !!! col __62 center
        | $T_1$ : utilisateur |  |  |  |
        | :-: | :-: | :-: | :-: |
        | **id** | **id_ville** | **nom** | **prenom** |
        | $1$ | $3$ | Dupont  | Gaelle | Femme |
        | $2$ | $4$ | Dutronc | Jacques | Homme |
        | $3$ | $1$ | Dupond  | Jean | Homme |
        | $4$ | $3$ | Durand  | Laura | Femme |
        | $5$ | $1$ | Dufour  | Sarah | Femme |
        | $6$ | $1$ | Dumiel  | Paul | Homme |
        | $7$ | $3$ | Durmont  | Karl | Autre |

    !!! col __38 center
        | $T_2$ : ville |  |
        | :-: | :-: |
        | **id** | **nom_ville** |
        | $1$ | Marseille |
        | $2$ | Toulouse |
        | $3$ | Bordeaux |

    <clear></clear>

    !!! col __10 center
        <div style="font-size:3em;margin-top:3em;">$=$</div>

    !!! col __90 center
        | Jointure Externe Gauche de $T_1$ et $T_2$ sur `'id_ville'` |  |  |  |  |  |
        | :-: | :-: | :-: | :-: | :-: | :-: |
        | **id** | **id_ville** | **nom** | **prenom** | **id_ville** | **nom_ville** |
        | $1$ | $3$ | Dupont  | Gaelle | $3$ | Bordeaux |
        | $2$ | $4$ | Dutronc | Jacques | NULL | NULL |
        | $3$ | $1$ | Dupond  | Jean | $1$ | Marseille |
        | $4$ | $3$ | Durand  | Laura | $3$ | Bordeaux |
        | $5$ | $1$ | Dufour  | Sarah | $1$ | Marseille |
        | $6$ | $1$ | Dumiel  | Paul | $1$ | Marseille |
        | $7$ | $3$ | Durmont  | Karl | $3$ | Bordeaux |

        <figcaption>

        Jointure Externe Gauche de $T_1$ et $T_2$ sur `'id_ville'`

        </figcaption>

        </figure>

<clear></clear>

### Jointure Externe Droite

!!! def "Jointure Externe Droite de T1 et T2 sur un attribut A"
    Soit $T_1$ et $T_2$ deux tables/entités.
    La <bred>jointure externe droite</bred> de $T_1$ avec $T_2$ **sur un champ/attribut donné $A$** est une table unissant :

    * les enregistrements de $T_1$ et de $T_2$ ayant la même valeur commune de $A$ (comme pour une jointure interne), et aussi
    * les enregistrements **de la table droite** $T_2$ pour lesquels la valeur de l'attribut $A$ ne correspond avec aucune valeur de l'attribut $A$ de $T_1$
    
!!! pte "En Pratique"
    La <rb>Jointure Externe Droite</rb>, ou <rb>RIGHT JOIN</rb>, ou <rb>RIGHT OUTER JOIN</rb>, permet de renvoyer tous les enregistrements/lignes de la table de droite (table2), même s’il n’y a pas de correspondance dans la table de gauche (table1). :warning: **Dans une jointure (externe) droite, les lignes de la table de gauche sans correspondance vaudront toutes NULL**.

!!! nota
    On la note $T_1 \rightouterjoin T_2$ (to solve ...)
On peut schématiser cette jointure par le schéma suivant:

![RIGHT JOIN](right-join.svg){.center}

<center>
<b>RIGHT (OUTER) JOIN - JOINTURE (EXTERNE) DROITE</b>
</center>

!!! exp "Jointure Externe Droite de deux Tables sur un champ/attribut donné"
    On dispose des tables $T_1$ et $T_2$ suivantes :

    !!! col __62 center
        | $T_1$ : utilisateur |  |  |  |
        | :-: | :-: | :-: | :-: |
        | **id** | **id_ville** | **nom** | **prenom** |
        | $1$ | $3$ | Dupont  | Gaelle | Femme |
        | $2$ | $4$ | Dutronc | Jacques | Homme |
        | $3$ | $1$ | Dupond  | Jean | Homme |
        | $4$ | $3$ | Durand  | Laura | Femme |
        | $5$ | $1$ | Dufour  | Sarah | Femme |
        | $6$ | $1$ | Dumiel  | Paul | Homme |
        | $7$ | $3$ | Durmont  | Karl | Autre |

    !!! col __32 center
        | $T_2$ : ville |  |
        | :-: | :-: |
        | **id** | **nom_ville** |
        | $1$ | Marseille |
        | $2$ | Toulouse |
        | $3$ | Bordeaux |

    <clear></clear>

    !!! col __10 center
        <div style="font-size:3em;margin-top:3em;">$=$</div>

    !!! col __90 center
        | Jointure Externe Droite de $T_1$ et $T_2$ sur `'id_ville'` |  |  |  |  |  |
        | :-: | :-: | :-: | :-: | :-: | :-: |
        | **id** | **id_ville** | **nom** | **prenom** | **id_ville** | **nom_ville** |
        | $1$ | $3$ | Dupont  | Gaelle | $3$ | Bordeaux |
        | NULL | NULL | NULL | NULL | $2$ | Toulouse |
        | $3$ | $1$ | Dupond  | Jean | $1$ | Marseille |
        | $4$ | $3$ | Durand  | Laura | $3$ | Bordeaux |
        | $5$ | $1$ | Dufour  | Sarah | $1$ | Marseille |
        | $6$ | $1$ | Dumiel  | Paul | $1$ | Marseille |
        | $7$ | $3$ | Durmont  | Karl | $3$ | Bordeaux |

        <figcaption>

        Jointure Externe Droite de $T_1$ et $T_2$ sur `'id_ville'`

        </figcaption>

        </figure>

## Références

* Algèbre Relationnelle : Cours [Algèbre Relationnelle, Université de Nice](https://www.i3s.unice.fr/~edemaria/cours/c3.pdf)