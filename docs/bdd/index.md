# TNSI - BDD Introduction Historique

## Introduction

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/iu8z5QtDQhY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Marie Duflot-Kremer](http://comscicomca.org/2017/11/19/marie-duflot-kremer-informagicienne-et-enseignante-chercheuse-a-luniversite-de-lorraine/), Informagicienne & Enseignante Chercheuse Univ. Lorraine
</center>

## Repères Historiques

<div style="width:35%; float:right;margin:0; padding:0;">

<center>

<img src="./img/codd.jpg" alt="Frank Edgar 'Ted' Codd, 1970">
<figcaption>Frank Edgar 'Ted' Codd, 1970</figcaption>

<img src="./img/ellison.png" alt="Larry Ellison">
<figcaption>Larry Ellison, fondateur d'Oracle</figcaption>

</center>

</div>

Inventées dans les années $1960's$, les bases de données sont d'abord utilisées dans le domaine militaire. En $1970$, c'est l'informaticien britannique **[Edgar Frank "Ted" Codd](https://fr.wikipedia.org/wiki/Edgar_Frank_Codd)** qui  définit formellement (comprendre mathématiquement) le modèle relationnel, avant toute implantation.  
Dix années de recherche auront été nécessaires avant de publier son article fondateur "**A Relationnal Model of Data for Large Shared Data Banks (le $6$ Juin $1970$)**" (*un modèle de données relationnel pour de grandes banques de données*).
Il est donc assez indépendant des technologies de constructeurs/développeurs, et a bien vieilli. C'est donc un "vrai modèle". Bien que ***Codd*** travaillât pour **IBM (International Business Machines)** à cette époque, cette société n'a pas souhaité immédiatement concrétiser ses recherches dans une application concrète, venant de mettre sur le marché **IBM Information Management System**, conçu dans le cadre du **Programme Apollo**. En $1977$, **Larry Ellison** fonde la société **Oracle**, qui devient l'éditeur de la base de données **Oracle DB**, premier produit inspiré par le modèle relationnel, lui-même inspiré par les travaux de **Codd**.

