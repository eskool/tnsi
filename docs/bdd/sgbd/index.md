# TNSI : BDD - SGBD = Système de Gestion de Base de Données

|Contenus|Capacités<br/>Attendues|Commentaires|
|:-:|:-:|:-:|
|Système de gestion de bases de<br/>données relationnelles.|Identifier les services rendus par un<br/>système de gestion de bases den<br/>données relationnelles :<br/>persistance des données, gestion<br/>des accès concurrents, efficacité den<br/>traitement des requêtes,<br/>sécurisation des accès.|Il s’agit de comprendre le rôle<br/>et les enjeux des différents<br/>services sans en détailler le<br/>fonctionnement.|

## SGBD vs BDD

### Notion de SGBD

!!! def
    Un <bred>Système de Gestion de Bases de Données (SGBD)</bred> :fr: / <bred>DataBase Management System (DBMS)</bred> :gb: est un ensemble d'outils informatiques permettant de réaliser des opérations de bases (CRUD), ou plus évoluées, sur les données :

    * <bred>C</bred>**reate** ou **Création/Sauvegarde**
    * <bred>R</bred>**ead** ou **Lecture/Interrogation**
    * <bred>U</bred>**pdate** ou **Mise à Jour**
    * <bred>D</bred>**elete** ou **Suppression**

    Selon le SGBD, d'autres opérations plus évoluées sont permises.

Un SGBD est donc un **ensemble logiciel** permettant aux programmeurs de réaliser des opérations sur des données spécifiques, contenues dans une grande masse d'information partagées par plusieurs utilisateurs.

### Base de Données

!!! def
    Une <bred>Base De Données (BDD)</bred> :fr: ou <bred>DataBase (DB)</bred> :gb: est la structure où sont stockées les données

### SGBD vs BDD

* Le SGBD est en charge de toute la gestion, la configuration, l'administration et la sécurité de la structure de la BDD.
* La BDD ne s'occupe que du stockage des données en elles-mêmes, et des relations entre ces données

## Fonctions d'un SGBD

Selon le SGDB considéré, il **peut** également permettre d'autres opérations plus évoluées, notamment :

* la **mise en forme** des données: description des types de données par des noms, des formats, etc...
* l'**authentification** des utilisateurs
* **sécurisation/protection** des données :
    Pourquoi? pour réduire le risque de vol de :
    * propriété intelectuelle
    * documents professionnels
    * de données médicales,
    * d'emails
    * de secrets commerciaux, etc..

    Quelques Techniques Utilisées: 

    * gestion de Droits
    * gestion de l'identité
    * gestion de l'accès
    * détection des menaces
    * analyse de la sécurité

* **intégrité** des données : regroupe un ensemble de critères s'intéressant (intuitivement) à la **qualité de l'état actuel des données, et leur préservation dans ce bon état**, notamment:
    * la **fiabilité/exactitude** des données.
    * la **cohérence** des données
    
    Quelques règles que doivent respecter les données, pour garantir l'intégrité initiale des données, et pour la préserver:
    
    * validation des entrées
    * validation des données
    * suppression des doublons
    * sauvegarde des données
    * contrôles d'accès: gestion des droits utilisateurs et protection des accès physiques aux serveurs, etc..
    * audits automatiques permettant de remonter à la source en cas de vol de données

* **optimisation des performances** (minimisation des temps de recherche notamment)
* **outils de gestion**:
    * métadonnées :
* **outils d'administration**
* **cloud** : Le SGBD peut offrir une indépendance logique et physique des données. Cela signifie qu'il peut protéger les utilisateurs et les applications du besoin de savoir où les données sont stockées où d'avoir à s'inquiéter des changements dans la structure physique des données (stockage et matériel).

!!! exp "de SGBD classiques"
    Quelques exemples nominatifs de SGBD classiques et usuels en 2021 :

    * ***[Oracle Database](https://www.oracle.com/fr/database/)*** appelé **Oracle DB**. Principaux Clients : Grandes Entreprises: Banques, Défense, Sécurité, $\approx $48% du parc mondial. **Oracle Corporation** est une entreprise américaine fondée en $1977$ par **Larry Ellison**, leader mondial dans le domaine des Bases de Données (BDD). 
    Payant (cher : $10 To$ $\approx 1800$ €/mois en $2018$). Quelques Exemples de Points Forts:
      * Gestion de données:
        * pools de session
        * connexions
        * gestion de larges objets
      * Sécurisation des Données : Évaluation, détection et prévention des menaces de sécurité, grâce au cryptage, gestion des clés, masquage des données, contrôles d'accès privilégiés, surveillance des activités et audit.
      * Administration : Gestion, Sécurisation, et Maintenance automatisées des BDD, par Machine Learning
      * Cloud
    * ***[4D](https://fr.4d.com/)*** (4ième Dimension)
    * ***[Microsoft SQL Server](https://www.microsoft.com/fr-fr/sql-server/sql-server-2019)***
    * ***[SQLite](https://www.sqlite.org/index.html)*** : Open Source et Gratuit, pour les BDD *légères* (petit site, applications sur tél portables)
    * ***[MySQL](https://www.mysql.com/fr/)*** : initialement Open Source et Gratuit, racheté depuis par ***Sun Microsystems*** en $2008$ pour 1 milliard de dollars..., puis ***Oracle*** a racheté **Sun** en $2009$). Très Utilisé pour les sites Web et intégration PHP
    * ***[MariaDB](https://mariadb.org/)*** est le remplaçant de *MySQL* dans le monde de l'Open Source Gratuit, depuis le rachat de *MySQL* par *Oracle* en $2009$.
    * ***[PostgreSQL](https://www.postgresql.org/)*** Open Source et Gratuit. Un peu plus complet que MySQL.
    * ***[NoSQL]()*** : leader pour les **Centres de Données** :fr: ou les **Data Center** :gb:, et donc le **Big Data**

## Les Propriétés *ACID*

En plus des fonctions précédentes, citées dans la définition, un SGBD :

* assure le partage des données
* garantit qu'une opération est réalisée de façon **fiable**, notamment grâce à des propriétés classiques, appelées les <bred>*Propriétés ACID*</bred> :
    * L'<env><bred>A</bred>***tomicité***</env> vérifie qu'une opération s'effectue entièrement, ou pas du tout
    * La <env><bred>C</bred>***ohérence***</env> assure que chaque transaction amèrera le système d'un état ***valide*** à un autre état ***valide***, en tenant compte notamment, mais pas exclusivement, des **contraintes d'intégrité**, des **rollbacks** en cascade, des **déclencheurs** et combinaisons d'**événements**.
    * <env><bred>I</bred>***solation***</env> : toute transaction doit s'exécuter comme si elle était la seule sur le système. Il ne doit y avoir aucune dépendance entre transactions, de sorte qu'une exécution simultanée de plusieurs transactions produise le même état que celui qui serait obtenu par l'exécution en série des transactions.
    * La <env><bred>D</bred>***urabilité***</env> assure que lorsqu'une transaction a été enregistrée, elle demeure enregistrée même à la suite d'une panne d'électricité, d'ordinateur ou d'un autre problème.

## Les Différents Modèles de SGBD

Un **modèle de SGBD** illustre la **structure logique** du SGBD.
Il existe de nombreux modèles de SGBD, parmi les plus courants :

### Le modèle Hiérarchique

Les données sont stockées hiérarchiquement, selon une arborescence descendante (comme dans un arbre). 

* Ce modèle utilise des **pointeurs unidirectionnels** (chaque **enregistrement** mène vers uniquement vers un autre) entre les différents **enregistrements**. 
* Requêtes procédurales (comment aller chercher les données, plutôt que quoi aller chercher)
* modèle **défini sur des produits technologiques** et non sur un modèle abstrait (d'où une grande dépendance aux technologies)
* produit le plus connu: Information Management System (IMS) par IBM ($1968$)
Historiquement, il s'agit du premier modèle de SGBD.

<center>

```dot
digraph G {
  node [shape=rectangle, label=""]
  //node [shape=rectangle]
  a -> b, c
  b -> d, e
  c -> f, g, h
  a [label="Produit"]
  b [label="Commerciaux"]
  c [label="Techniciens"]
  d [label="Ambulants"]
  e [label="Sédentaires"]
  f [label="Web"]
  g [label="Graphistes"]
  h [label="Sécurité"]
}
```

<figcaption>Le modèle Hiérarchique</figcaption>

</center>

Ce modèle Hiérarchique est aujourd'hui **obsolète**.

### Le modèle Réseau

Le **modèle réseau** est une extension du modèle hiérarchique, et lui succède historiquement : celui-ci utilise donc encore des pointeurs vers des enregistrements. Il autorise même des relations **plusieurs à plusieurs** entre des enregistrements liés, ce qui autorise plusieurs (ou pas) enregistrements parents.
La structure n'est donc **plus obligatoirement descendante**. 
Son pic de popularité remonte aux années $1970$, après qu'il ait été officiellement défini par la conférence sur les langages de système de traitement des données (Conference on Data Systems Languages, CODASYL)

<center>

```dot
digraph G {
  node [shape=rectangle, label=""]
  //node [shape=rectangle]
  a -> b, c
  b -> c, d, e
  c -> f
  c -> g
  c -> h [weight=2]
  d -> e
  f -> g
  d -> i
  {rank=same; b,c}
  {rank=same; d,e}
  {rank=same; f,g}
}
```

<figcaption>Le modèle Réseau</figcaption>

</center>

Ce modèle Réseau est aujourd'hui **obsolète**.

### Le modèle Relationnel (SGBDR)

Les données sont enregistrées dans des **Tableaux appelés** <bred>Tables</bred> **, à deux dimensions :** <bred>lignes</bred> **et** <bred>colonnes</bred>

<center>

| $\,$ | $\,$ | $\,$ |
|:-:|:-:|:-:|
| $\,$ | $\,$ | $\,$ |
| $\,$ | $\,$ | $\,$ |
| $\,$ | $\,$ | $\,$ |
| $\,$ | $\,$ | $\,$ |

</center>

<center>

| $\,$ | $\,$ | 
|:-:|:-:|
| $\,$ | $\,$ |
| $\,$ | $\,$ |
| $\,$ | $\,$ |

</center>

* Le modèle relationnel est **le plus utilisé actuellement**, et ce depuis la fin des années $1990$ : environ les $\displaystyle \frac 34$ des SGBD/bases de données utilisent ce modèle. 
* Parmi les SGBD Relationnels les plus populaires on peut citer:
    * **Oracle**
    * **MySQL**
    * **Microsoft SQL Server**
    * **PostgreSQL**
    * **MariaDB** 
    * **SQLite**
    * **Microsoft Access**
    * ..

* <env>**Limites**</env> L'évolution entre autres du **matériel informatique**, des **langages de programmation**, des exigences des **interfaces-utilisateurs**, des **applications multimédia** ainsi que de la **réseautique** a mis en lumière des limitations du modèle relationnel, et a ainsi poussé d'autres modèles à émerger. Plus particulièrement, ses limites pour les **systèmes distribués à grande échelle** sur le Web (comprendre les **Centres de Données** :fr:, ou **Data Centers** :gb: utilisés pour le **Big Data**), comme par exemple ***Google***, ***Twitter*** et ***Facebook***, ont conduit à l'apparition des familles de bases de données appelées des **SGBD non-relationnels** ou **familles NoSQL (Not Only SQL)** :gb:

Dans toute la suite, nous ne nous intéresserons qu'au ce modèle relationnel.

### Le modèle Déductif

Les données sont présentées sous forme de Tables, comme le modèle relationnel, mais la manipulation des tables se fait différemment (par calcul des prédicats : hors-programme)

### Le modèle Objet (SGBDO)

Les données sont stockées sous forme d'Objets, c'est à dire en pratique sous forme de ***Classes***, présentant des données membres.
Les champs sont des instances de la classe.

### Familles NoSQL

* L'appellation <bred>*NoSQL (Not only SQL)*</bred> date de $2009$. **Il ne s'agit pas, à proprement parler, d'un *modèle NoSQL***, mais de plusieurs ***familles NoSQL***, qui répondent à des besoins différents. En effet, le modèle relationnel, se révèle **peu efficace** et rencontre **quelques limites** dans sa manière de représenter et de manipuler les données, particulièrement dans le contexte d'environnements Web distribués à grande échelle, comprendre pour les **Centres de Données** :fr:, ou **Data Centers** :gb:, qui sont amenés à traiter de très grands volumes de données (ce que l'on appelle le **Big Data**, p. ex ***Google***, ***Twitter***, ***Facebook***, ***eBay***).

* Les ***familles NoSQL***, délaissent les propriétés des transactions relationnelles qui ont été pensés pour garantir et de maintenir la cohérence des données (**propriétés ACID**) au profit de contraintes qui priorisent la **disponibilité** des données (**contraintes BASE**).

* <env>**Exemples de Familles NoSQL**</env>
  * **Familles Orientées Graphes**: Encore plus flexible qu'un modèle réseau car il permet la connexion entre tous les noeuds, quels qu'ils soient. Exemple: ***Twitter***
  * **Orientés Colonnes**
  * **Orientés Clés/Valeurs**. Exemple: Système de Sauvegarde type ***Dropbox***
  * **Orientés "*Document*"** : Gestion des documents et/ou des données semi-structurées (e.g. métadonnées), plutôt que des données atomiques. Exemple : métadonnées des produits sur eBay.
* <env>**Exemples d'Implémentations**</env>
  * MongoDB (OpenSource, orienté document de types JSON)
  * Amazon Dynamo DB
  * Cassandra (Facebook)
  * Big Table (Google)
  * Apache HBase (pour la base de données Big Table de Google)
  * Oracle NoSQL Database
  * etc..

Voir par exemple ces références[^1][^2] pour une introduction à ces Modèles NoSQL (MongoDB, Cassandra, et Elasticsearch)

## Les SGBD les plus populaires

[![SGBD](../img/sgbd-db-engines-2022.png)](https://db-engines.com/en/ranking_trend/relational+dbms){.center style="width:100%;"}
<center>
source: :sk-copyright: [db-engines.com](https://db-engines.com/en/ranking_trend/relational+dbms)
voir leur [Méthodologie de Calcul](https://db-engines.com/en/ranking_definition)
</center>

<center><div id="my_dataviz"></div>
Les SGBD les plus *Populaires* en 2022,<br/>
source: [Sondage 2021 de Développeurs, par StackOverflow](https://insights.stackoverflow.com/survey/2021)
</center>

<script>

// set the dimensions and margins of the graph
const margin = {top: 30, right: 30, bottom: 70, left: 60},
    width = 700 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

// append the svg object to the body of the page
const svg = d3.select("#my_dataviz")
  .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", `translate(${margin.left},${margin.top})`);

// Parse the Data
d3.csv("dbms.csv").then( function(data) {

// X axis
const x = d3.scaleBand()
  .range([ 0, width ])
  .domain(data.map(d => d.DBMS))
  .padding(0.2);
svg.append("g")
  .attr("transform", `translate(0, ${height})`)
  .call(d3.axisBottom(x))
  .selectAll("text")
    .attr("transform", "translate(-10,0)rotate(-45)")
    .style("text-anchor", "end");

// Add Y axis
const y = d3.scaleLinear()
  .domain([0, 60])
  .range([ height, 0]);
svg.append("g")
  .call(d3.axisLeft(y));

// Bars
svg.selectAll("mybar")
  .data(data)
  .join("rect")
    .attr("x", d => x(d.DBMS))
    .attr("y", d => y(d.Pourcentage))
    .attr("width", x.bandwidth())
    .attr("height", d => height - y(d.Pourcentage))
    .attr("fill", "#41a6f8")

})
</script>

## Références

* [db-engines.com](https://db-engines.com/en/ranking)

[^1]: [Cours de Bases de Doonées Documentaires et Distribuées](http://b3d.bdpedia.fr/)
[^2]: [Support de Cours Bases de Données Documentaires et Distribuées](http://b3d.bdpedia.fr/files/b3d.pdf)
