# TNSI : BDD - Types Relations entre Tables

## Différents Types de Relations et Schématisations UML vs ERD

La schématisation précédente - un trait simple -  est imparfaite et ne suffit pas totalement à résumer le lien complexe entre deux tables quelconques. En effet, revenons à notre site de VOD proposant de louer des vidéos (pour 24H) :

* un utilisateur dispose d'**une seule** zone géographique
* mais une même zone géographique peut être choisie par **plusieurs** utilisateurs

La schématisation précédente ne permet en effet pas, en son état actuel, de rendre compte de ces subtilités.
Pour combler cela, on ajoute plusieurs informations à l'entrée à et à la sortie des tables, qui permettent de rendre compte des divers **types de relations entre tables**. Il existe (au moins) deux notations concurrentes : 

* la notation <bred>UML : Unified Modeling Language</bred>
* la notation <bred>ERD : Entity Relation Diagram</bred>
* etc ..

!!! mth "Diagrammes ERD / Diagramme Entité-Association"
    On ajoute trois informations à la relation symbolisée par un trait entre les tables :

    * à gauche : un symbole représentant le <bred>min</bred> (encore appelé <bred>ordinalité</bred>) qui modélise le nombre **minimum** de fois qu'une instance dans une table/entité (sortante) peut être associée à une instance de l'autre table/entité (cible)
    * à droite : un symbole représentant le <bred>max</bred> (encore appelé <bred>cardinalité</bred>) qui modélise le nombre **maximum** de fois qu'une instance dans une table/entité (sortante) peut être associée à une instance de l'autre table/entité (cible)
    * l'<bred>association</bred> (en général un verbe, un extrait de phrase) qui détaille plus précisément la relation entre ces deux tables/entités

## Relations `1` à `1`

### Notation UML - Unified Modeling Language

<clear></clear>

* Un utilisateur du site dispose d'une (unique) carte client matérielle dès son inscription sur le site.
* Une carte client appartient à un seul client

!!! col __30 center
    ```mermaid
    classDiagram
        class utilisateur {
        * id
        prenom
        nom
        email
        # id_zone
        # id_carte
        }
        class carte_client {
        * id_carte
        # id_utilisateur
        validite
        }
        utilisateur "1..1" -- "1..1" carte_client:appartenance
    ```

!!! col __30 center
    ```mermaid
    classDiagram
        class utilisateur {
        * id
        prenom
        nom
        email
        # id_zone
        # id_carte
        }
        class carte_client {
        * id_carte
        # id_utilisateur
        validite
        }
        utilisateur "1,1" -- "1,1" carte_client:appartenance
    ```

!!! col __40 center
    <env>**Notation UML**</env>

    | Symbole <br/> $min..max$ <br/> $min,max$ | Signification |
    |:-:|:-:|
    | $0$ | Zéro |
    | $1..1$ <br/> (quelquefois $1$) | Un |
    | $1$ <br/> $0..1$ <br/> $0,1$ | Zéro ou Un |
    | $n$ <br/> $0..n$ <br/> $0,n$ | Zéro ou Plusieurs |
    | $1..n$ <br/> $1,n$ <br/> (quelquefois $n$) | Un ou Plusieurs |
    | $\infty$ | une infinité |

    <env>**Signification**</env>

    Un utilisateur "est le propriétaire" d'exactement **une** carte client.  
    Une carte client appartient exactement à **un** utilisateur

    <env>**Association**</env>

    ***Appartenance*** : doit quelquefois être pensée à l'envers<br/>
    quand on la lit dans l'autre sens...

<clear></clear>

### Notation ERD - Entity Relation Diagram

<clear></clear>

!!! col _2 center
    ```mermaid
    erDiagram
        utilisateur ||--|| carte_client : "appartenance"
        utilisateur {
            int id
            str prenom
            str nom
            str email
            int id_zone
            int id_carte
        }
        carte_client {
            int id_carte
            int id_utilisateur
            date validite
        }
    ```

!!! col _2 center
    <env>**Notation ERD**</env>
    
    | Symbole | Signification |
    |:-:|:-:|
    | <img src="../img/erdOne.png" style="width:100%; box-shadow:none;"> | Un |
    | <img src="../img/erdMany.png" style="width:100%; box-shadow:none;"> | Plusieurs <br/> (ou $\infty$) |
    | <img src="../img/erdOneOnlyOne.png" style="width:100%; box-shadow:none;"> | Un (et seulement Un) |
    | <img src="../img/erdZeroOne.png" style="width:100%; box-shadow:none;"> | Zéro ou Un |
    | <img src="../img/erdOneMany.png" style="width:100%; box-shadow:none;"> | Un ou Plusieurs <br/>(ou $\infty$) |
    | <img src="../img/erdZeroMany.png" style="width:100%; box-shadow:none;"> | Zéro ou Plusieurs <br/>(ou $\infty$) |

    <env>**Signification: *Idem***</env>

    <env>**Association: *Idem***</env>

## Relations `1` à `n` (plusieurs)

* un utilisateur *habite* dans une (unique) zone géographique
* une zone géographique est *peuplée*/*associée à* (aucun ou) plusieurs utilisateurs du site

<clear></clear>

!!! col _2 center
    ```mermaid
    classDiagram
        class utilisateur {
        * id
        prenom
        nom
        email
        # id_zone
        # id_carte
        }
        class zone_geographique {
        * id_zone
        nom
        }
        utilisateur "1" -- "n" zone_geographique:est associé à
    ```

    <env>**Signification**</env>

    Un utilisateur habite/*est associé à* exactement une zone géographique
    Une zone géographique *est associé à* **aucun ou plusieurs** utilisateurs

    <env>**Association**</env>

    "est associé à"

!!! col _2 center
    ```mermaid
    erDiagram
        utilisateur ||--o{ zone_geographique : "est associé à"
        utilisateur {
            int id
            str prenom
            str nom
            str email
            int id_zone
            int id_carte
        }
        zone_geographique {
            int id_zone
            str nom
        }
    ```

    <env>**Signification : *Idem***</env>

    <env>**Association : Idem**</env>

<clear></clear>

## Relations `n` (plusieurs) à `n` (plusieurs)

* Un utilisateur peut louer aucun ou plusieurs films
* Un film peut être loué par aucun ou plusieurs utilisateurs

<clear></clear>

!!! col _2 center
    ```mermaid
    classDiagram
        class utilisateur {
        * id
        prenom
        nom
        email
        # id_zone
        # id_carte
        # Code
        }
        class film {
          * Code
          Titre
          Réalisateur
          Durée
          Date de Sortie
          Note Spectateurs
        }
        utilisateur "n" -- "n" film:louer
    ```

    <env>**Signification**</env>

    Un utilisateur peut *louer* **aucun ou plusieurs** films.  
    Un film peut être *loué* (à la demande...) par **aucun ou plusieurs** utilisateurs

    <env>**Association**</env>

    "louer"

!!! col _2 center
    ```mermaid
    erDiagram
        utilisateur o{--o{ film : "louer"
        utilisateur {
            int id
            str prenom
            str nom
            str email
            int id_zone
            int id_carte
            int Code
        }
        film {
            int Code
            str Titre
            str Realisateur
            int Duree
            int Date_de_Sortie
            float Note_Spectateurs
        }
    ```

    <env>**Signification : *Idem***</env>

    <env>**Association : Idem**</env>