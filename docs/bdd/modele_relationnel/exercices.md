# TNSI : Exercices sur le modèle Relationnel[^1]

https://sqliteonline.com/
https://xkcd.com/327/
http://webtic.free.fr/sql/exint/q1.htm

## Exercice 1

On décide de créer de nouvelles relations/tables pour le site de VOD du cours :

=== "Question 1°) Clé Primaire de la relation *acteur*"
    Déterminer des clés primaires possibles pour la relation *acteur* :

    <center>

    |acteur ||||
    |:-:|:-:|:-:|:-:|
    | id | prenom | nom | email |
    | 96 | Scarlett | JOHANSSON | tureves@gmail.com |
    | 52 | Leonardo | DICAPRIO | leonardo.theboss@gmail.com |
    | 84 | Jean | DUJARDIN | jeannot.denice@gmail.com |
    | 66 | Virginie | EFFIRA | the.deesse@gmail.com |
    | 234 | Angelina | JOLIE | thegirl@gmail.com |
    | 128 | Gilles | LELLOUCHE | gillou.lecool@gmail.com |
    | 200 | Denzel | Washington | denzelll@gmail.com |
    | 45 | Marion | COTILLARD | marinou.celibataire@gmail.com |
    | 95 | Brad | PITT | bigbrad@gmail.com |
    | 48 | John | TRAVOLTA | dancemachine@gmail.com |
    | 64 | Will | SMITH | willy.thesmasher@gmail.com |
    | ... | ... | ... | ... |

    <figcaption>Relation/Table <b>acteur</b> </figcaption>

    </center>

=== "Correction"
    Des choix possibles pour la clé primaire sont les suivants

    * l'attribut `id` est le choix le plus naturel de clé primaire de la relation `acteur`
    * l'attribut `email` est un choix possible de clé primaire de la relation `acteur`
    * le couple `(id, email)` est un choix possible de clé primaire de la relation `acteur`
    * le couple `(id, prenom)` est un choix possible de clé primaire de la relation `acteur`
    * le couple `(prenom, email)` est un choix possible de clé primaire de la relation `acteur`
    * etc..

<clear></clear>
=== "Question 2°) Clé Primaire de la relation *role*"
    Déterminer des clés primaires possibles pour la relation *role* :

    <center>

    |role ||||||
    |:-:|:-:|:-:|:-:|:-:|:-:|
    | id_acteur | date | film | personnage | Réalisateur | Code |
    | 96 | 06/08/2014 | Lucy | Lucy Miller | Besson | 305 |
    | 52 | 21/07/2010 | Inception | Dominic Cobb | Nolan | 310 |
    | 96 | 19/03/2014 | Her | Samantha l'IA | Jonze | 128 |
    | ... | ... | ... | ... | ... | ... | ... | ... |

    <figcaption>Relation/Table <b>role</b></figcaption>

    </center>

=== "Correction"
    Des choix possibles pour la clé primaire sont les suivants

    * l'attribut `id_acteur` n'est pas une clé primaire possible pour la relation `role`, car un même acteur peut jouer dans plusieurs films
    * l'attribut `Code` n'est pas une clé primaire possible pour la relation `role`, car plusieurs acteurs peuvent jouer dans un même film
    * le couple d'attributs `(personnage, Code)` est une clé primaire possible pour la relation `role`, car un même personnage d'un même film est unique

## Exercice 2

On considère une Bibliothèque dont la base de données possède une Relation/Table livres : 

![](../img/relation_livres.png)

=== "Question 1°)"
    Donner un enregistrement de la Relation/Table *livre*

=== "Correction"
    Par exemple : `(1563, Chanson douce, Leïla SLIMANI, Gallimard, 978-2070196678)`

<clear></clear>
=== "Question 2°)"
    Lister tous les attributs de la Relation/Table *livre*
=== "Correction"
    Les attributs de la relation sont : `code`, `Titre`, `Auteur`, `Éditeur`, `ISBN`

<clear></clear>
=== "Question 3°)"
    Quel est le degré de la Relation/Table *livre*
=== "Correction"
    Il y a $5$ attributs en tout, donc le degré de la table vaut $5$

<clear></clear>
=== "Question 4°)"
    Quel est le domaine des attributs `code`, `Titre`, `Auteur`, `Editeur`, `ISBN`
=== "Correction"
    * domaine de `code`: `int`
    * domaine de `Titre`: `str`
    * domaine de `Auteur`: `str`
    * domaine de `Éditeur`: `str`
    * domaine de `ISBN`: `str` (à cause du tiret `-`)

<clear></clear>
=== "Question 5°)"
    Quels atttributs de la relation/table *livre* pourraient être la clé primaire ?
=== "Correction"
    Les attributs suivants pourraient être la clé primaire (par exemple) :
    * `ISBN`
    * `code`
    * `(ISBN, code)`
    * `(ISBN, Titre)`
    * `(ISBN, Auteur)`
    * etc..

<clear></clear>
=== "Question 6°)"
    Quel est le schéma pour la Relation/Table *livre*?
=== "Correction"
    **Schéma** : livre(<u>code</u>:int, Titre:`str`, Auteur: `str`, Éditeur:`str`, ISBN:`str`)

## Exercice 3

Deux relations modélisent la flotte de voitures d'un réseau de location de voitures.

<env>**Agences**</env>

<center>

| id_agence | ville     | département |
|:-:|:-:|:-:|
| 1         | Bordeaux  | 33          |
| 2         | Brest     | 29          |
| 3         | Lyon      | 69          |
| 4         | Marseille | 13          |
| 5         | Aix       | 13          |

</center>

<env>**Voitures**</env>

<center>

| id_voiture | marque      | modèle | kilométrage | couleur | id_agence |
|:-:|:-:|:-:|:-:|:-:|:-:|
| 1          | Renault     | Twingo | 10000       | Rouge   | 3         |
| 2          | Peugeot     | 2008   | 20000       | Noir    | 5         |
| 3          | Peugeot     | 308    | 25000       | Noir    | 2         |
| 4          | Volkswagen  | T-roc  | 31000       | Noir    | 3         |

</center>

=== "Énoncé"
    1. Combien la relation ```Voitures``` comporte-t-elle d'attributs ?
    2. Que vaut son cardinal ?
    3. Quel est le domaine de l'attribut ```ig_agence```  dans la relation ```Voitures``` ?
    4. Quel est le schéma relationnel de la relation ```Agences ``` ?
    5. Quelle est la clé primaire de la relation ```Agences ``` ?
    6. Quelle est la clé primaire de la relation ```Voitures ``` ?
    7. Quelle est la clé étrangère de la relation ```Voitures ``` ?

=== "Correction"
    1. $6$
    2. $4$
    3. Entier (`Int` )
    4. Agences((<u>id_agence</u>, Int), (ville, String), (département, Int))
    5. `id_agence`
    6. `id_voiture`
    7. `id_agence`


## Exercice 4

On se donne une base de données Tour de France 2020[^2], contenant les relations suivantes : (d'après une idée de [Didier Boulle](http://webtic.free.fr/sql/mldr.htm))

<env>**Equipes**</env>

<center>

| codeEquipe | nomEquipe             |
|------|-----------------------------|
| ALM  |  AG2R La Mondiale           |
| AST  |  Astana Pro Team            |
| TBM  |  Bahrain - McLaren          |
| BOH  |  BORA - hansgrohe           |
| CCC  |  CCC Team                   |
| COF  |  Cofidis, Solutions Crédits |
| DQT  |  Deceuninck - Quick Step    |
| EF1  |  EF Pro Cycling             |
| GFC  |  Groupama - FDJ             |
| LTS  |  Lotto Soudal               |
| ...  | ...                         |

</center>

Le schéma relationnel de cette table est :    
Equipes ( <u>codeEquipe</u> `String`, nomEquipe `String` ) 

<env>**Coureurs**</env>

<center>

| dossard       |  nomCoureur | prénomCoureur | codeEquipe |
|---------------|-------------|---------------|------------|
| 141           | LÓPEZ       | Miguel Ángel  | AST        |
| 142           | FRAILE      | Omar          | AST        |
| 143           | HOULE       | Hugo          | AST        |
| 11            | ROGLIČ      | Primož        | TJV        |
| 12            | BENNETT     | George        | TJV        |
| 41            | ALAPHILIPPE | Julian        | DQT        |
| 44            | CAVAGNA     | Rémi          | DQT        |
| 45            | DECLERCQ    | Tim           | DQT        |
| 121           | MARTIN      | Guillaume     | COF        |
| 122           | CONSONNI    | Simone        | COF        |
| 123           | EDET        | Nicolas       | COF        |
| …             | …           | …             | …          |

</center>

Schéma : Equipes ( <u>dossard</u> `Int`, nomCoureur `String`, prénomCoureur `String`, codeEquipe# `String` ) 

<env>**Etapes**</env>

<center>

| numéroEtape | villeDépart | villeArrivée      | km  |
|-------------|-------------|-------------------|-----|
| 1           | Nice        | Nice              | 156 |
| 2           | Nice        | Nice              | 185 |
| 3           | Nice        | Sisteron          | 198 |
| 4           | Sisteron    | Orcières-Merlette | 160 |
| 5           | Gap         | Privas            | 198 |
| ...         | ...         | ...               | ... |

</center>

Schéma : Etapes ( <u>numéroEtape</u> `Int`, villeDépart `String`, villeArrivée `String`, km `Int` ) 

<env>**Temps**</env>

<center>

| dossard       | numéroEtape | tempsRéalisé |
|:-------------:|:-----------:|:------------:|
| 41            | 2           | 04:55:27     |
| 121           | 4           | 04:07:47     |
| 11            | 5           | 04:21:22     |
| 122           | 5           | 04:21:22     |
| 41            | 4           | 04:08:24     |
| ...           | ...         | ...          |

</center>

Schéma : Temps ( <u>dossard</u> # `Int`, <u>numéroEtape</u> # `Int`, tempsRéalisé `String`)

<env>Représentation Base de Données</env>

<center>

```mermaid
classDiagram
    Temps --|> Coureurs
    Temps --|> Etapes
    Coureurs --|> Equipes
    class Temps {
      🔑 dossard#
      🔑 numéroEtape#
      tempsRéalisé
    }
    class Coureurs {
      🔑 dossard
      nomCoureur
      prénomCoureur
      codeEquipe#
    }
    class Etapes {
      🔑 numéroEtape
      villeDépart
      villeArrivée
      km
    }
    class Equipes {
      🔑 codeEquipe
      nomEquipe
    }
```

</center>

=== "Énoncé"
    1. Quel temps a réalisé Guillaume MARTIN sur l'étape Sisteron / Orcières-Merlette ?
    2. Quel est le nom de son Équipe?
    3. À l'arrivée à Privas, qui est arrivé en premier entre Primož ROGLIČ et Simone CONSONNI ?

=== "Correction"
    1. 04:07:47
    2. COF, Cofidis Solutions Crédits
    3. Il y a eu égalité : ils sont arrivés en même temps (04:21:22)



## Références

[^1]: Cette page d'Exercices est tirée du [Site de Gilles Lassus](https://glassus.github.io/terminale_nsi/)
[^2]: [Pro Cycling Stats](https://www.procyclingstats.com/)