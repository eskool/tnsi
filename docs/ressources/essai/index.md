# TNSI - ESSAI

<div align="center" style="background-color: lightgrey; width:100%;">
    <div align="center" id="jsconverter" style="display:none">
        <div style="font-size: 120%; font-weight: bold; padding: 0.5em;">
            IEEE 754 Converter (JavaScript), V0.22
        </div>
        <div id="convnumeric">
            <table id="numbertable" style="border-collapse: collapse;">
                <tr>
                    <td style="text-align: left;   padding: 0em 1em;">
                    </td>
                    <td style="text-align: center; padding: 0.5em 1em; font-weight: bold; background-color: #d2d2e7;">
                        Sign
                    </td>
                    <td style="text-align: center; padding: 0.5em 1em; font-weight: bold; background-color: #c0ddc2;">
                        Exponent
                    </td>
                    <td style="text-align: center; padding: 0.5em 1em; font-weight: bold; background-color: #ddd0c4;">
                        Mantissa
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;   padding: 0em 1em; font-weight: bold;">
                        Value:
                    </td>
                    <td style="text-align: center; padding: 0em 1em; background-color: #d2d2e7;">
                        <span id="sign_value">0</span>
                    </td>
                    <td style="text-align: center; padding: 0em 1em; background-color: #c0ddc2;">
                        2<span id="exponent_value" style="vertical-align: super;">0</span>
                    </td>
                    <td style="text-align: center; padding: 0em 1em; background-color: #ddd0c4;">
                        <span id="mantissa_value">0</span>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;   padding: 0em 1em; font-weight: bold;">
                        Encoded as:
                    </td>
                    <td style="text-align: center; padding: 0em 1em; background-color: #d2d2e7;">
                        <span id="actual_sign">0</span>
                    </td>
                    <td style="text-align: center; padding: 0em 1em; background-color: #c0ddc2;">
                        <span id="actual_exponent">0</span>
                    </td>
                    <td style="text-align: center; padding: 0em 1em; background-color: #ddd0c4;">
                        <span id="actual_mantissa">0</span>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;   padding: 0em 1em; margin-right: 0.5em; font-weight: bold;">
                        Binary:
                    </td>
                    <td style="text-align: center; padding: 0em 1em; margin-right: 0.5em; background-color: #d2d2e7;">
                        <form action="javascript:binaryChanged();">
                            <input type="checkbox" id="cbsign" onchange="binaryChanged();" onsubmit="binaryChanged();">
                        </form>
                    </td>
                    <td style="text-align: center; padding: 0em 1em; margin-right: 0.5em; background-color: #c0ddc2;">
                        <form action="javascript:binaryChanged();">
                            <input type="checkbox" id="cbexp0" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbexp1" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbexp2" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbexp3" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbexp4" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbexp5" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbexp6" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbexp7" onchange="binaryChanged();" onsubmit="binaryChanged();">
                        </form>
                    </td>
                    <td style="text-align: center; padding: 0em 1em; margin-right: 0.5em; background-color: #ddd0c4;">
                        <form action="javascript:binaryChanged();">
                            <input type="checkbox" id="cbmant0" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant1" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant2" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant3" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant4" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant5" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant6" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant7" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant8" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant9" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant10" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant11" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant12" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant13" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant14" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant15" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant16" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant17" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant18" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant19" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant20" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant21" onchange="binaryChanged();" onsubmit="binaryChanged();">
                            <input type="checkbox" id="cbmant22" onchange="binaryChanged();" onsubmit="binaryChanged();">
                        </form>
                    </td>

                </tr>
            </table>
        </div>
        <div id="convtext">
            <table>
                <tr>
                    <td id="dec_repr_text">Decimal Representation</td>
                    <td><form action="javascript:decimalChanged();"><input type="text" size="60" id="decimal" onchange="decimalChanged()"></form></td>
                    <td rowspan="4">
                        <div><button type="button" style="width: 3em; padding: 0.5em; margin: 0.5em; border-style: solid; border-color: black; border-width: 2px; background-color: #90A090;" onclick="increment();">+1</button></div>
                        <div><button type="button" style="width: 3em; padding: 0.5em; margin: 0.5em; border-style: solid; border-color: black; border-width: 2px; background-color: #9090A0;" onclick="decrement();">-1</button></div>
                    </td>
                </tr>
                <tr>
                    <td>Value actually stored in float:</td>
                    <td><input type="text" size="60" id="highprecision_decimal" readonly></td>
                </tr>
                <tr>
                    <td>Error due to conversion:</td>
                    <td><input type="text" size="60" id="representation_error" readonly></td>
                </tr>
                <tr>
                    <td>Binary Representation</td>
                    <td><form action="javascript:binaryTextfieldChanged();"><input type="text" size="60" id="binary" onchange="binaryTextfieldChanged();"></form></td>
                </tr>
                <tr>
                    <td>Hexadecimal Representation</td>
                    <td><form action="javascript:hexChanged();"><input type="text" size="60" id="hexadecimal" onchange="hexChanged();"></form></td>
                </tr>
            </table>
            <div id="convstatus" style="margin: 0.5em;">
                &nbsp;
            </div>
        </div>
        <script type="text/javascript" src="../conversion.js" defer></script>
        <script type="text/javascript">
            <!--
            update("decimal=0");
            -->
        </script>
    </div>

    <!-- Make sure converter is only shown when Javascript is enabled. -->
    <script type="text/javascript">
        <!--
        window.document.getElementById("jsconverter").style.display = "inline";
        // -->
    </script>
    <noscript>
        (this page requires Javascript)
    </noscript>

</div>