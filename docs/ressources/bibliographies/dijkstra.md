# Bibliographie Edger Dijkstra

Né à Rotterdam le 11 mai 1930 et mort à Nuenen le 6 août 2002, est un mathématicien et
informaticien néerlandais du XXe siècle

## Citations

* « Tester un programme peut démontrer la présence de bugs, jamais leur absence. »
* « Se demander si un ordinateur peut penser est aussi intéressant que de se demander si un sous-marin
peut nager. »
* « La programmation par objets est une idée exceptionnellement mauvaise qui ne pouvait naître qu'en
Californie. »
* « Les progrès ne seront possibles que si nous pouvons réfléchir sur les programmes sans les imaginer
comme des morceaux de code exécutable. »
* « Autrefois les physiciens répétaient les expériences de leurs collègues pour se rassurer. Aujourd'hui ils
adhèrent à FORTRAN et s'échangent leurs programmes, bugs inclus. »
* « À propos des langages : il est impossible de tailler un crayon avec une hache émoussée. Il est vain
d'essayer, à la place, de le faire avec dix haches émoussées. »
* « Il est pratiquement impossible d'enseigner la bonne programmation aux étudiants qui ont eu une
exposition antérieure au BASIC : comme programmeurs potentiels, ils sont mentalement mutilés, au-delà
de tout espoir de régénération. »
* « Le plus court chemin d'un graphe n'est jamais celui que l'on croit, il peut surgir de nulle part, et la plupart
du temps, il n'existe pas. »