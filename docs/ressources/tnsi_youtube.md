# Channels & Vidéos YouTube pour la NSI

* [Cédric GERLAND, 1NSI et TNSI](https://www.youtube.com/channel/UC-gmNqo2JEyMNoH8mTvh1oQ/videos)
* [David Latouche](https://www.youtube.com/c/DavidLatouche/videos)
* [NovelClass](https://www.youtube.com/playlist?list=PLJrJPz8qhVicGklhTd5ITITCFf2y-t7DU)
* [Les Bons Profs](https://www.youtube.com/channel/UCL7FXdd8TW0hxNHmzlPlAQQ)
* [École Hexagone](https://www.youtube.com/channel/UCOS3iEX86oB1BIAu9KP016g)