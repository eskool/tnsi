# Prépas MP2I

## Programmes Officiels MP2I

* Programmes des filières MP2I : [BO Spécial n°1 du 11 Février 2021](https://cache.media.education.gouv.fr/file/SPE1-MEN-MESRI-4-2-2021/64/6/spe777_annexe_1373646.pdf)

## Sites de MP2I

* Prépas MP2I: https://prepas-mp2i.github.io/
