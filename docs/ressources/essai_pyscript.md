---
template: pyscript.html
---

# Essai

{%if page.pyscript %}
ALLO
{% endif %}

<div>

<py-repl> 2 < 3</py-repl>
<py-repl id="r1" auto-generate="true">
def indice_min(nombres):
    indice = 0
    minimum = nombres[0]
    for i in range(len(nombres)):
        if minimum > nombres[i]:
            minimum = nombres[i]
            indice = i
    return indice
</py-repl>
<py-repl id="r2" auto-generate="true">
# test 1 : doit renvoyer 2
indice_min([2, 4, 1, 1]) == 2
</py-repl>
<py-repl id="r3" auto-generate="true">
# test 2 : doit renyoyer True
indice_min([5]) == 0
</py-repl>
<py-repl id="r4" auto-generate="true">
# tests 3 : ne doit pas renvoyer de message d'erreur
assert indice_min([2, 4, 1, 1]) == 2
</py-repl>
<py-repl id="my-repl" auto-generate="true">
# Réaliser vos propres tests :
indice_min()
</py-repl>

<py-script>
    print("Hello World")
</py-script>
<br/>
<p><strong>Today is <label id='date'></label></strong></p>
<py-script>
import time
pyscript.write('date', time.strftime('%d/%m/%Y %H:%M:%S'))
</py-script>
<br/>

<py-script>  
    print("Let's evaluate π :")
    def eval_pi(n):
        pi = 2
        for i in range(1,n):
            pi *= 4 * i ** 2 / (4 * i ** 2 - 1)
        return pi
    pi = eval_pi(100000)
    s = "&nbsp;" * 10 + f"π is approximately {pi:.5f}"
    print(s)
</py-script>
<br/>

<div id="hey"></div>
<py-script>
pyscript.write('hey', f'Hey Mate !')
</py-script>
</div>