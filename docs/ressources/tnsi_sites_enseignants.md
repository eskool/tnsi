# Ressources : Autres Sites d'Enseignants de Terminale NSI

* Fabrice Nativel : [Ressources pour l'Enseignement de la Spécialité NSI](https://fabricenativel.github.io/)
* Alain BUSSER : [https://alainbusser.frama.io/NSI-IREMI-974/](https://alainbusser.frama.io/NSI-IREMI-974/)
* Guillaume Connan: [https://gitlab.com/lyceeND/tale](https://gitlab.com/lyceeND/tale)
* Romain Janvier : [nsiterminale.janviercommelemois.fr](http://nsiterminale.janviercommelemois.fr/)
* Gilles LASSUS: [https://glassus.github.io/terminale_nsi/](https://glassus.github.io/terminale_nsi/)
* Anne GELLIER : [https://angellier.gitlab.io/nsi/terminale/](https://angellier.gitlab.io/nsi/terminale/)
* [Le site de NovelClass](https://novelclass.com/nos-cours/terminale/terminale-nsi/)

* [Portes Logiques, Univ du Sénégal](http://unis.sn/wp-content/uploads/2019/10/04-Les-portes-logiques.pdf)
