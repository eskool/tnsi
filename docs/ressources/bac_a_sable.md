# Bac à Sable TNSI

```mathtable
\begin{tikzpicture}
	\tkzTabInit[espcl=6]
		{$x$/1 , $f'(x)$/1 , $f(x)$/2}
		{$0$ , $\sqrt{e}$ , $+\infty$}
	\tkzTabLine{d,+,0,+,}
	\tkzTabVar{D- / $-\infty$ , R/ , + / $0$ }
	\tkzTabVal[draw]{1}{3}{0.4}{$1$}{$-e$}
\end{tikzpicture}
```

```mathtable
\begin{tikzpicture}
\newcommand*{ \E}{ \ensuremath{ \mathrm{e}}}.
\tkzTabInit{$x$ /1,$g'(x)$ /1,$g(x)$ /2}
{$-\infty$,$-2$,$0$,$+\infty$}
\tkzTabLine{,+,z,-,z,-}
\tkzTabVar{-/$-\infty$, +/$2$, R/, -/$-\infty$}
\tkzTabIma{2}{4}{3}{$0$}            
\end{tikzpicture}
```

```mathtable
\begin{tikzpicture}
	\tkzTabInit[espcl=6]
		{$x$/1 , $f'(x)$/1 , $f(x)$/2}
		{$0$ , $\sqrt{e}$ , $+\infty$}
	\tkzTabLine{d,+,0,+,}
	\tkzTabVar{D- / $-\infty$ , R/ , + / $0$ }
	\tkzTabVal[draw]{1}{3}{0.4}{$1$}{$-e$}
\end{tikzpicture}
```

```mathtable
\begin{tikzpicture}
   \tkzTabInit[lgt=3,espcl=1.5]
     {$x$ / 1 , $f'(x)$ / 1, $f(x)=\sqrt{\dfrac{x-1}{x+1}}$ / 2 }
     {$-\infty$, $-1$, $1$, $+\infty$}
   \tkzTabLine
     { , + , d , h , d, +, }
   \tkzTabVar
     { -/1 , +DH/$+\infty$, -C/0, +/1}
\end{tikzpicture}
```

```mathtable
\begin{tikzpicture}
   \tkzTabInit[lgt=2.75]
     { $x$/1  ,  $f'(x)$/1  ,  $f(x) = tan(x)$/2  }
     { $0$,  $\dfrac{\pi}{2}$,  $\pi$}
   \tkzTabLine
     { , + , d , + , }
   \tkzTabVar
     { -/$0$ ,  +D-/$+\infty$/$-\infty$ , +/$0$}
\end{tikzpicture}
```

```mathtable
\begin{tikzpicture}
	\tkzTabInit[espcl=6]
		{$x$/1 , $f'(x)$/1 , $f(x)$/2}
		{$0$ , $\sqrt{e}$ , $+\infty$}
	\tkzTabLine{d,+,0,+,}
	\tkzTabVar{D- / $-\infty$ , R/ , + / $0$ }
	\tkzTabVal[draw]{1}{3}{0.4}{$1$}{$-e$}
\end{tikzpicture}
```

!!! ex
    ```mathtable
    \begin{tikzpicture}
        \tkzTabInit[lgt=3, espcl=6, deltacl=0.7]{$x$ /1, $(e^x)'=e^x$ /1, $e^x$ /1.5} {$-\infty$ , $+\infty$}
    \tkzTabLine{, -,}
    \tkzTabVar{-/ , +/ }
    \tkzTabVal{1}{2}{0.3}{$0$}{1}
    \tkzTabVal{1}{2}{0.6}{$1$}{$e$}
    \end{tikzpicture}
    ```

