---
toc:
  toc_depth: 2
---

# TNSI : Épreuves Pratiques

## Où Trouver des Corrigés des Sujets de La BNS - Banque Nationale de Sujets ?

* [Epreuves Pratiques 2024 Corrigées, Pixees](https://pixees.fr/informatiquelycee/term/index.html#ep_prat)
* [Epreuves pratiques 2024 Corrigées, par Gilles Lassus](https://glassus.github.io/terminale_nsi/T6_6_Epreuve_pratique/BNS_2024/)
* [Epreuves pratiques 2024 Corrigées, par Dominique Laporte, Le Web Pédagogique](https://lewebpedagogique.com/dlaporte/corrections-epreuves-pratiques-2021-nsi/)

<!-- * [BNS 2022, énoncés & corrigés, par Sébastien HOARAU,](https://sebhoa.gitlab.io/nsi/BNS_2022/) -->

## Visualisation des Sujets, au format PDF, et leurs Fichiers Python correspondants

Page de Référence: [éduscol](https://eduscol.education.fr/2661/banque-des-epreuves-pratiques-de-specialite-nsi)

{{ epreuves_pratiques() }}

