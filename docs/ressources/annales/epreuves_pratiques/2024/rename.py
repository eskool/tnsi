import os

names = os.listdir()

def change(name:str):
    return name.replace("-","_")

for name in names:
    newName = change(name)
    os.rename(name, newName)
