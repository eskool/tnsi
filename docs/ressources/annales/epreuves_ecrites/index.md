---
toc:
  toc_depth: 2
---

# TNSI : Épreuves Écrites

## Sites avec Corrigés

* Pixees : [https://pixees.fr/informatiquelycee/term/suj_bac/](https://pixees.fr/informatiquelycee/term/suj_bac/)
* [https://kxs.fr/sujets/terminale-ecrit](https://kxs.fr/sujets/terminale-ecrit)
* [https://www.sujetdebac.fr/annales/specialites/spe-numerique-informatique/](https://www.sujetdebac.fr/annales/specialites/spe-numerique-informatique/)
* [https://lewebpedagogique.com/dlaporte/corrections-epreuves-pratiques-2021-nsi/](https://lewebpedagogique.com/dlaporte/corrections-epreuves-pratiques-2021-nsi/)

## Les Sujets des Épreuves Écrites

{{ epreuves_ecrites() }}


## B.O - Bulletin Officiel

Page de Référence: [éduscol](https://eduscol.education.fr/2661/banque-des-epreuves-pratiques-de-specialite-nsi)
