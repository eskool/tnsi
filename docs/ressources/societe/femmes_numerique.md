# TNSI - Femmes & Numérique

* https://www.ins2i.cnrs.fr/fr/les-decodeuses-du-numerique
    * LA BD des Décodeuses du Numérique : https://fr.calameo.com/read/006841715804996467dcf?authid=cG5djzzVzuiW&page=1
    * https://www.ins2i.cnrs.fr/sites/institut_ins2i/files/page/2022-02/femmes_numerique.pdf
* https://femmes-numerique.fr/quelle-place-pour-les-femmes-dans-le-numerique-en-2020/

<iframe src="https://fr.calameo.com/read/006841715804996467dcf?authid=cG5djzzVzuiW&page=1" style="width:90%; max-width:1000px; height:960px;"></iframe>