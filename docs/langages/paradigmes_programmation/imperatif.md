# TNSI - Paradigme de Programmation Impérative

En informatique, la <bred>programmation impérative</bred> :fr: est un **paradigme de programmation** qui décrit les opérations en **séquences d'instructions** exécutées par l'ordinateur pour modifier l'état du programme. Ce type de programmation est le plus répandu parmi l'ensemble des langages de programmation existants, 

La quasi-totalité des processeurs qui équipent les ordinateurs sont de nature impérative : ils sont faits pour exécuter une suite d'instructions élémentaires, codées sous forme d'opcodes (pour operation codes). L'ensemble des opcodes forme le langage machine spécifique à l'architecture du processeur. L'état du programme à un instant donné est défini par le contenu de la mémoire centrale à cet instant. 

### Instructions de la Base Impérative

La plupart des langages de haut niveau comporte cinq types d'instructions principales :

#### la Séquence d'Instructions ou Bloc d'Instruction

Une séquence d'instructions, (ou bloc d'instruction) désigne le fait de faire exécuter par la machine une instruction, puis une autre, etc., en séquence. 

Par exemple: `ouvrirConnexion ; envoyerMessage ; fermerConnexion;` est une séquence d'instructions. Cette construction se distingue du fait d'exécuter en parallèle des instructions. 

#### L'Assignation ou Affectation

On affecte/assigne une valeur à une varaible

#### L'instruction conditionnelle

Si (il fait nuageux) Alors (je vais au cinéma) Sinon (je me promène)

#### La boucle

C'est une répétition d'une suite d'instructions un nombre prédéfini de fois (voir Boucle **for**), ou jusqu'à ce qu'une certaine condition soit réalisée (Boucle **while**).

#### Les branchements non conditionnels

Les branchements sans condition permettent à la séquence d'exécution d'être transférée à un autre endroit du programme. Cela inclut le **saut**, appelé **goto** (*aller à*) dans de nombreux langages, et les sous-programmes, ou appels de procédures. Les instructions de bouclage peuvent être vues comme la combinaison d'un branchement conditionnel et d'un saut. Les appels à une fonction ou une procédure (donc un Sous-programme) correspondent à un saut, complété du passage de paramètres, avec un saut en retour. 
