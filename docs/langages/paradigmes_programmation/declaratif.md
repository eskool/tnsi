# TNSI : Paradigme de Programmation Déclarative

La <bred>programmation déclarative</bred> est un **paradigme de programmation** qui consiste à créer des applications sur la base de composants logiciels indépendants du contexte et ne comportant aucun état interne. Autrement dit, l'appel d'un de ces composants avec les mêmes arguments produit exactement le même résultat, quel que soit le moment et le contexte de l'appel. 

En programmation déclarative, on décrit le quoi, c'est-à-dire le problème. Par exemple, les pages HTML sont déclaratives car elles décrivent ce que contient une page (texte, titres, paragraphes, etc.) et non comment les afficher (positionnement, couleurs, polices de caractères…). Alors qu'en programmation impérative (par exemple, avec le C ou Java), on décrit le comment, c'est-à-dire la structure de contrôle correspondant à la solution. 

C'est une forme de programmation sans effets de bord, ayant généralement une correspondance avec la logique mathématique.

Il existe plusieurs formes de programmation déclarative :

* la **programmation descriptive**, à l'expressivité réduite, qui permet de décrire des structures de données, comme HTML ou LaTeX ;
* la **programmation fonctionnelle**, qui perçoit les applications comme un ensemble de fonctions mathématiques, comme Lisp, Caml, Haskell et Oz ;
* la **programmation logique**, pour laquelle les composants d'une application sont des relations logiques, comme Prolog et Mercury ;
* la *$*.

## Programmation Fonctionnelle 

La <bred>programmation fonctionnelle</bred> est un **paradigme de programmation** de type déclaratif qui considère le **calcul** en tant qu'**évaluation de fonctions mathématiques**.

Comme le changement d'état et la mutation des données ne peuvent pas être représentés par des évaluations de fonctions1 la programmation fonctionnelle ne les admet pas, au contraire elle met en avant l'application des fonctions, contrairement au modèle de programmation impérative qui met en avant les changements d'état2.

Un langage fonctionnel est donc un langage de programmation dont la syntaxe et les caractéristiques encouragent la programmation fonctionnelle. Alors que l'origine de la programmation fonctionnelle peut être trouvée dans le **lambda-calcul**, le langage fonctionnel le plus ancien est Lisp, créé en 1958 par McCarthy. Lisp a donné naissance à des variantes telles que Scheme (1975) et Common Lisp (1984)3 qui, comme Lisp, ne sont pas ou peu typées. Des langages fonctionnels plus récents tels ML (1973), Haskell (1987), OCaml, Erlang, Clean et Oz, CDuce, Scala (2003), F# ou PureScript (2013), Agda (en) sont fortement typés. 

## Programmation Logique

La programmation logique est un **paradigme de programmation** de type déclaratif, qui définit les applications à l'aide :

* d'une base de faits : ensemble de faits élémentaires concernant le domaine visé par l'application,
* d'une base de règles : règles de logique associant des conséquences plus ou moins directes à ces faits,
* d'un moteur d'inférence (ou démonstrateur de théorème ) : exploite ces faits et ces règles en réaction à une question ou requête.

Cette approche se révèle beaucoup plus souple que la définition d'une succession d'instructions que l'ordinateur exécuterait. La programmation logique est considérée comme une programmation déclarative plutôt qu’impérative, car elle s'attache davantage au quoi qu'au comment, le moteur assumant une large part des enchaînements. Elle est particulièrement adaptée aux besoins de l’intelligence artificielle, dont elle est un des principaux outils. 

### Exemples de Langages de programmation logique

* Prolog : Le premier langage de programmation logique
* Oz
* CLIPS
* Python : PyPy permet de l'utiliser pour la programmation logique



## Références et Notes

### Références

### Notes

[^1] [Programmation Déclarative, Wikipedia](https://fr.wikipedia.org/wiki/Programmation_d%C3%A9clarative)
[^2]: [Programmation Fonctionnelle, Wikipedia](https://fr.wikipedia.org/wiki/Programmation_fonctionnelle)
[^3] [Programmation Logique, Wikipedia](https://fr.wikipedia.org/wiki/Programmation_logique)