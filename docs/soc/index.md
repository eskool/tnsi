# Système sur Puce, ou SoC - System On Chip

<center>

| Contenus | Capacités Attendues | Commentaires |
|:-:|:-:|:-:|
| Composants intégrés d’un système<br/> sur puce. | Identifier les principaux<br/>composants sur un schéma de<br/>circuit et les avantages de leur<br/>intégration en termes de vitesse et<br/>de consommation | Le circuit d’un téléphone peut<br/>être pris comme un exemple :<br/>microprocesseurs, mémoires<br/>locales, interfaces radio et<br/>filaires, gestion d’énergie,<br/>contrôleurs vidéo,<br/>accélérateur graphique,<br/>réseaux sur puce, etc. |

</center>

## Introduction

* Dans les cartes mères modernes, en particulier celle des ordinateurs portables, l'intégration est beaucoup plus poussée.  
* Dans les dispositifs nomades comme les téléphones portables (Smartphones), Tablettes ou nano ordinateurs (par ex. Raspberry Pi ou l'Arduino), cette intégration passe encore à un niveau supérieur.

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/L4XemL7t6hg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</center>

## Loi de Moore et miniaturisation progressive

### La Loi de Moore

![image](./img/moore.png){: .center width=20%}

En $1965$, Gordon Moore postule que le nombre de transistors présents sur une puce de microprocesseur doublera tous les deux ans.

Cette prédiction s'est révélée étonnamment juste (à quelques approximations près) et les équipements électroniques n'ont depuis jamais cessé de devenir toujours plus performants / miniatures / économes en énergie.

![image](./img/Loi_de_Moore.png){: .center width=60%}
![image](./img/loi-moore.png){: .center width=100%}

:sk-copyright: [Our World in data](https://ourworldindata.org/)

### Évolution de la taille des ordinateurs

#### IBM 650, le premier ordinateur fabriqué en série (1955)

![image](./img/ibm650.jpg){: .center width=50%}

Cet ordinateur n'a pas encore de transistors mais des tubes à vide.

#### IBM 7090, le premier ordinateur à transistors (1959)

![image](./img/ibm7090.webp){: .center width=50%}

!!! info inline end "Le transistor"
    Le transistor est un composant électronique essentiel : il permet de laisser (ou non) passer un courant électrique.
    ![image](./img/trans.png){: .center width=100%}

#### Le rôle crucial de la taille des transistors

Ainsi que l'avait prédit Moore, c'est la progression du nombre de transistors gravables sur le processeur qui guidera pendant des années l'évolution de l'informatique :

![image](./img/evol_transistors.png){: .center width=70%}


## Composition d'un PC actuel

![image](./img/inside.jpg){: .center width=50%}

Chaque composant a un rôle spécifique. Ils communiquent entre eux par des **bus** de différentes vitesses. Chaque composant est remplaçable, et il est possible d'ajouter de nouveaux composants sur la carte mère qui possède des slots d'extension.


## Tout un PC sur une seule puce : les SoC

Un <rbl>Chipset</rbl> :fr: :gb: est un circuit intégré d'un ordinateur qui centralise le CPU, la RAM, de la mémoire de stockage et quelques autres périphériques.
Un <rbl>SoC - System on Chip</rbl> intègre preque tous ces composants dans une seule et unique puce en silicone. Comparés aux chipsets, les SoCs contiennent également des composants supplémentaires (cf ci-dessous). Puisqu'elles intègrent aussi bien le logiciel que le matériel, elles ont de meilleurs performances en termes de consommation électrique, prennent moins d'espace, et sont plus fiables que les chipsets[^22].

### Définition

!!! def "Soc - System on Chip"
    Un <red>SoC - System on Chip</red> :gb: / <red>Système sur Puce</red> :fr: est un **circuit intégré** qui accueille sur une même puce, dans une taille très réduite, de très nombreux circuits (voire de nombreuses puces), appelés usuellement **modules**, parmi lequels :

    * un (ou plusieurs) microprocesseur **CPU - Central Processing Unit**
    * de la mémoire :  
        * vive **RAM - Random Access Memory** :
            * La mémoire **LPDDR - Low Power Double Data Rate** (Vitesse de données double à faible consommation) / **LPDDR SDRAM** / **DDR Mobile** / **mDDR**
            * Remarquer que, dans certaines architectures de SoC, le package de la RAM ne rentre pas dans La SoC et est donc placée à l'extérieur sous un format appelé <rbl>PoP - Package on Package</rbl>
            ![PoP Simple](./img/pop_simple.png)
            ![PoP](./img/pop.png){.center style="width: 70%; max-width:700px;"}  
            :sk-copyright: [Chen,Xiong,Xu,Li, Univ Nanging, Chine](https://www.researchgate.net/publication/336948178_Thermal_Covert_Channels_Leveraging_Package-on-Package_DRAM) :cn: & [Szefer,Yale Univ, USA](https://www.researchgate.net/publication/336948178_Thermal_Covert_Channels_Leveraging_Package-on-Package_DRAM) :us:
        * statique **ROM - Read Only Memory, Flash, EPROM,..** :
            * l'**UFS- Universal Flash Storage** qui est un standard de mémoire Flash ... 
    * un circuit/chipset Graphique **GPU - Graphical Processing Unit**
    * un circuit/chipset Neuronal **NPU - Neural Processing Unit** : pour l'accélération des algorithmes de Machine Learning : 
        * Traduction Automatique
        * Reconnaissance Faciale
        * Diagnostics Médicaux
        * Détection de Fraudes
        * Voitures Autonomes
        * Prédiction de Traffic, 
        * Recommendation de Produits, etc..
    * une puce d'Image ou **ISP - Image Signal Processor** / **IPU - Image Processing Unit** pour la **Photo** : une puce prenant en charge la création d’images numériques. Dans la réalité et de par leurs tailles minuscules, les capteurs photo de nos smartphones sont mauvais. La qualité qu’il est actuellement possible d’obtenir est intimement liée à cette puce. En effet, c’est grâce à elle que votre smartphone va traiter la prise et la création de votre photo.
    * une puce **DSP - Digital Signal Processing** pour le **Multimedia** : c'est une puce pour le traitement du signal numérique, par exemple:
        * Traitement Audio
        * Traitement Vidéo
        * (quelques) Traitements de l'Affichage
    * **modems** / **puces de Connectivité** : 
        * Wifi
        * Bluetooth
        * 2G/3G/4G
        * Radio FM
        * GPS
        * LoRa[^1] :fr: $\geq 2009$ (échange de paquets pour objets connectés)
        * etc..  
    * une puce de Sécurité **SPU - Secure Processing Unit** : le bouclier de votre smartphone. Son alimentation électrique est indépendante afin de ne pas pouvoir être éteint en cas d’attaque sur celui-ci. Le SPU est d’une importance capitale, celui-ci va stocker vos données biométriques, bancaires, votre SIM ou encore vos titres de transport. C’est lui qui contient les clés de chiffrement de vos données.)
    * une puce pour les **Capteurs**
    * une puce pour la **Caméra**
    * des Circuits Intégrés (CI) de Gestion de l'Alimentation / Power Management Integrated Circuit (PMIC) :gb: 
    * des Régulateurs de Voltage / Voltage Regulator
    * le Système d'Exploitation et logiciels facilitateurs
    * etc..

![Die Shot Exynos 9820, Samsung S10](./img/exynos9820.png){.center}
<center>
Die Shot Exynos 9820, Samsung S10 :sk-copyright: [ChipRebel.com](https://www.chiprebel.com/downloads/samsung-exynos-9820/)
</center>

### Utilisations des SoC

Les meilleurs SoC, plus fins que les microprocesseurs *classiques* de dernière génération, sont gravés en $5$ nm et comprennent des milliards de transistors. Cette miniaturisation est idéale pour des dispositifs mobiles comme :

* les **Smartphones** (téléphones portables), 
* les **Tablettes**, 
* les **Nano-ordinateurs** : Raspberry Pi, Arduino, etc..
* les **Consoles de Jeu** : Nintendo Switch (Nvidia Tigra X1), PS5 (AMD),..
* les **Objets Connectés** / **IoT - Internet of Objects**, 
* les **Systèmes Embarqués / eMbedded Systems**, au sens large
* à noter aussi que de grands constructeurs comme Apple, veulent aussi en équiper toute leur gamme d'ordinateurs.

### Comment est-ce possible ?

Une première question que l'on peut se poser est: Comment est-ce possible de pouvoir placer autant de fonctionnalités sur si peu de place[^24] ? (en particulier, pourquoi ne fait-on pas la même chose pour les ordinateurs classiques?)

* Les processeurs des SoC ne sont pas aussi puissants que ceux des PCs/Ordinateurs portables (donc ils ont besoin de moins de place)
* Les SoC sont construits (principalement) à base d'Architectures **ARM - Advanced RISC Machine**, qui sont suffisamment puissantes, mais sans trop consommer d'énergie.
* Les OS des smartphones sont **optimisés** pour les SoCs

### Avantages des SoC :heart:

Les avantages des SoC sont :

* La **Taille plus petite** (miniaturisation) : tous les composants sont sur la même puce, donc un gain de place important
* La **Vitesse plus grande** : 
    * les distances sont réduites, donc la fréquence d'horloge peut augmenter notablement
    * Les composants et leur connexion sont plus facilement prévisibles, donc moins de variations
* **Moindre Coût** :
    * Un circuit imprimé moins complexe
    * Moins de soudage
    * Fréquence d'horloge faible sur la carte, mais importante sur puce, cela réduit les frais liés au circuit imprimé
* **Consommation Énergétique très réduite** (par rapport à un système classique, à puissance de calcul équivalente), en raison des distances réduites (moins de déperdition) et
* **Sécurité** : meilleure sécurité (vue globale sur la sécurité qui n'est plus dépendante d'une multitude de composants)
* **Absence de Système de Refroidissement** lourds, volumineux et énergivores (comme par ex. des ventilateurs)

### Désavantages des SoC :heart:

Des inconvénients des SoC sont:

* **Impossiblité de choisir indépendamment ses composants** 
* **Pas de mise à jour / remplacement / ajout d'un composant** : tous les composants sont intégrés
* **Pannes** : Une panne (quelconque) d'un seul composant entraîne la panne totale du SoC. 

## Architecture d'un SoC

### Schéma de Circuit d'un SoC

![Schéma de Circuit SoC](./img/circuit-soc.svg){.center}

* Le <rbl>NorthBridge</rbl> est une puce (un Contrôleur Mémoire) reliée directement au CPU via un bus, appelé **Bus Système** :fr: / (améliorés en) **FSB - Front Side Bus** :gb: (remplacés par des **bus QPI - QuickPath Interconnect**): **Cette puce (/bus) est plus rapide mais consomme davantage d'énergie**. Le NorthBridge est connecté :
    * au GPU - Processeur Graphique
    * à la RAM
    * à la **DMA - Direct Access Memory** : un périphérique (contrôleur) de certains systèmes permettant l'accès à la mémoire directement (càd sans passer par le CPU). Dans un SoC, il est utilisé pour transférer des données vers/depuis les Entrées/Sorties (I/O), par exemple vers/depuis le GPU. La DMA permet ainsi de décharger le CPU, de plus hauts transferts, et autorise les interruptions matérielles [^23].
    * aux slots PCI Express
    * au SouthBridge
* Le <rbl>SouthBridge</rbl> est une puce (un Contrôleur d'Entrées/Sorties I/O) qui n'est PAS directement connecté au CPU, mais qui relie le NorthBridge à des périphériques plus lents (d'Entrées/Sorties I/O), via un bus appelé **Bus Interne** : **Cette puce (/bus) est plus lent, mais consomme moins d'énergie**. Le SouthBridge est connecté :
    * aux Bus PCI (plus lents que PCI Express)
    * aux connecteurs SATA
    * aux connecteurs IDE
    * aux Ports USB
    * au NorthBridge
* Dans les SoCs, ils sont maintenant tous deux intégrés aux CPU (Refs : [1](https://linearmicrosystems.com/typical-soc/), [2](https://itigic.com/fr/soc-architecture-how-heterogeneous-processors-work/))

### Types d'Architectures de SoC : ARM vs x86

Une Architecture de SoC est avant tout basée sur l'architecture de son processeur (CPU), qui est elle-même caractérisée par son **jeu d'instruction** (du processeur). Il existe différents types d'architectures pour les SoC, principalement deux :

* <rbl>Architecture ARM - Advanced RISC Machine</rbl> : Jeux d'instructions ARM. **Plus simple**, basée sur le modèle d'architecture de processeur appelé **RISC - Reduced Instruction Set Computer** :gb: / **Processeur à Jeu d'Instruction Réduit** :fr:, qui se caractérise par des instructions de base aisées à décoder. Les architectures RISC ont pour avantages
d’être plus rapides et moins gourmandes en énergie que les CISC, l’inconvénient est un code machine moins compact.
* <rbl>Architecture x86</rbl> : Jeux d'instructions x86. **Plus Complexe**, basée sur le modèle d'architecture de processeur appelé **CISC - Complex Instruction Set Computer** / **Processeur à Jeu d'Instruction Complexe**, qui se caractérise par un jeu d'instructions comprenant de très nombreuses instructions.

### Les Architectures ARM

L'Architecture ARM est fortement inspirée du modèle d'architecture de processeur appelé **RISC - Reduced Instruction Set Computer** :gb: / **Processeur à Jeu d'Instruction Réduit** :fr:. Elle dispose de $16$ registres généraux de $32$ bits. Les instructions, codées sur $32$ bits jusqu'à l'ARMv$7$ peuvent toutes disposer d'exécution conditionnelle. Sur l'architecture $64$ bits (ARMv$8$), quelques instructions seulement peuvent disposer d'exécution conditionnelle. Le jeu d'instructions a reçu des extensions au fil du temps.

#### La SoCiété ARM Ltd et les fabricants de processeurs ARM

La propriété intellectuelle appartient à une société britannique, historiquement **Acorn Computers**, de nos jours **ARM Ltd** :gb:. En $2016$,  ARM est rachetée par la Holding japonaise **Softbank**, et en $2020$, une OPA - Offre Publique d'Achat par **NVidia** :us: sera finalement rejetée en $2021$ par la **FTC - Federal Trade Commision** :us: (les autorités de la concurrence américaine). Les processeurs ARM sont fabriqués *sous licence*, suivant le principe ditdes **IPs - Intellectual Properties** :gb: / **Propriétés Intellectuelles** :fr:, qui sont des sortes de **Bibliothèques Matérielles** que l'on peut acheter pour monter soi-même (puis commercialiser). Les processeurs ARM sont ainsi construits par différentes entreprises de par le monde (cf [liste complète sur wikipedia](https://fr.wikipedia.org/wiki/Architecture_ARM#Fabricants_de_processeurs_ARM)[^13])). Parmi les entreprises fabriquant les modèles des **Séries Cortex** (les plus avancées), la majorité se trouve en Asie (20), suivie par les États-Unis (13) et enfin par l'Europe (6). 

#### Quelques Exemples de processeurs ARM

##### par Familles de Processeur

* Famille Cortex-A : processeur <rbl>A</rbl>pplicatif
* Famille Cortex-R : processeur Temps-<rbl>R</rbl>éel (Real Time)
* Famille Cortex-M : processeur e<rbl>M</rbl>barqué (eMbedded System)

##### Exemples de Processeurs ARM de quelques SoC

* L'**iPhone 13** :mac: utilise un SoC ARM A15 Bionic
* Le **Samsung Galaxy Note 20 Ultra 5G** utilise un SoC ARM Exynos 990, basé sur :
    * des processeurs Samsung Exynos M5 \@2,7GHz, et
    * des processeurs ARM Cortex A76 \@2,5GHz
    * des processeurs ARM Cortex A55 \@2,0 GHz
* Le **Raspberry Pi4** :raspberry: utilise un SoC ARM Broadcom 2711 (basé sur une architecture ARM- Cortex A72)

##### par Architectures / Jeux d'Instruction

| Date | Architecture | Famille(s) |
|:-:|:-:|:-:|
| $1985$ | ARMv1 | ARM1 |
| $1987$ | ARMv2 | ARM2, ARM3 |
| | ARMv3 | ARM6, ARM7 |
| | ARMv4 | StrongARM, ARM7TDMI, ARM8, ARM9TDMI |
| | ARMv5 | ARM7EJ, ARM9E, ARM10E, XScale, FA626TE, Feroceon, PJ1/Mohawk |
| $1990$ | ARMv6 | ARM11 (en) |
| | ARMv6-M | ARM Cortex-M (ARM Cortex-M0, ARM Cortex-M0+, ARM Cortex-M1)
| | ARMv7-A | ARM Cortex-A (Gen1 : ARM Cortex-A8, Gen2 : ARM Cortex-A9 MPCore,<br/> ARM Cortex-A5 MPCore, Gen3 : ARM Cortex-A7 MPCore, <br/>ARM Cortex-A12 MPCore, ARM Cortex-A15 MPCore, <br/>Adaptation tierce : Scorpion, Krait, PJ4/Sheeva, Swift |
| | ARMv7-M | ARM Cortex-M (ARM Cortex-M3, ARM Cortex-M4, ARM Cortex-M7) |
| | ARMv7-R | ARM Cortex-R (ARM Cortex-R4, ARM Cortex-R5, ARM Cortex-R7) |
| | ARMv8-A | ARM Cortex-A35, ARM Cortex-A50 (ARM Cortex-A53, ARM Cortex-A57),<br/>ARM Cortex-A72, ARM Cortex-A73, X-Gene, Denver, Cyclone, Exynos M1/M2 |
| | ARMv8.2-A | ARM Cortex-A55, ARM Cortex-A65, ARM Cortex-A75, ARM Cortex-A76 |
| | ARMv8.3-A | ARM Cortex-A65AE (seulement LDAPR, le reste en 8.2),<br/>ARM Cortex-A76AE (idem A65AE) |
| | ARMv8-M | ARM Cortex-M23, ARM Cortex-M33 |
| | ARMv8-R | ARM Cortex-R53 |


### Les Architectures x86

Ce ne sont pas les architectures les plus répandues.
Voir par exemple [Intel NUC](https://www.intel.fr/content/www/fr/fr/products/details/nuc.html).

### Principaux Concepteurs de SoC

* AMD : Architecture ARM
* Apple : Architecture ARM (exclusivement)
* Broadcom : Architecture ARM
* <rbl>Intel : Architecture x86</rbl>
* [Mediatek](https://fr.wikipedia.org/wiki/MediaTek)[^3] : Architecture ARM
* Nvidia : Architecture ARM
* [Qualcomm](https://fr.wikipedia.org/wiki/Qualcomm)[^4] [^5] : Architecture ARM
* Samsung : Architecture ARM
* Texas Instrument : Architecture ARM
* etc..[^13]

### Parts de Marché des SoC

```mermaid
pie title Parts de Marché des SoCs (2021)
    "MediaTek" : 43
    "Qualcomm" : 28
    "Apple" : 14
    "Samsung Exynos" : 7
    "Autres" : 8
```

#### Répartition des Coûts d'une SoC

```mermaid
pie title Répartition des Coûts d'un Samsung Galaxy S9+
    "Processeur Base/Applications" :  68
    "Batterie" : 5.5
    "Caméra/Image" : 48
    "Connectivité" : 12
    "Écran Tactile" : 72.5
    "Mémoire Von Volatile" : 12
    "Mémoire Volatile" : 39
    "Non Électroniques" : 29
    "Autres" : 15
    "Alimentation / Audio" : 8.5
    "Composants Radio Fréquence" : 23.5
    "Capteurs" : 5
    "Substrats" : 19.5
    "Assemblage & Tests" : 12.5
```

<center>

(source[^10])

</center>

## Le Cas d'un Smartphone

### SoC d'un Smartphone

=== "Samsung Galaxy Note 9"
    ![Annotated Die Shot d'un Exynos 9810, Galaxy Note 9](./img/exynos9810.png){.center}

    <center>

    Annotated Die Shot d'un Exynos 9810, Galaxy Note 9 (2019)

    </center>

    La SoC Exynos 9810 est gravé en $10$ nm. Il est octacore (8 coeurs) répartis sur deux CPUs :

    * CPU Quadcore Exynos M3 @ 2,9GHz. (+ d'infos [ici](https://en.wikichip.org/wiki/samsung/microarchitectures/m3)) pouvant lui-même être décomposé comme ceci  
        ![Annotated Samsung Exynos M3 CPU](./img/exynos-m3-cpu.png){.center}
        
        <center>

        CPU Samsung Exynos M3, Quadcore \@2,9GHz

        </center>

        * `pL2` : mémoire L2 Cache privée, on voit ici le cache de 512 Ko implémenté en ce qu'il semble être deux aires/tranches 
        * `FPB` : Le *Floating point data path* (le chemin des données à Virgule Flottante). Les Unités d'exécutions FP et ASIMD en elles-mêmes
        * `FRS` : Les *Floating point schedulers* (les ordonnanceur à virgule flottante) avec les mémoires de registre FP/vectoriels.
        * `MC` : Mid-core. Les Unités de décodage (decode) et de renommage (rename).
        * `DFX` : *Design for X* = Toute la Logique de Test / Débuggage. Par exemple, le *DFD (Design for debug)*, *DFT (Design for test)*, et *DFM (Design for manufacturability)*, et autres logiques diverses.
        * `LS` : Unité de Chargement (Load)/Stockage(store) avec 64Ko de mémoire cache L1.
        * `IXU` : Integer execution unit (Unité d'exécution des Entiers). Elle contient les unités d'exécution, les ordonnanceurs et les registres physiques de mémoire d'entiers.
        * `TBW` : Transparent Buffer Writes (Écriture Transparente en Mémoire Tampon). Inclut les structures TLB.
        * `FE` : Les prédictions de Branches Front-End, unités de récupération et les 64 Ko de mémoire cache L1 pour les instructions.
    * CPU Quadcore  ARM Cortex A55 \@ 1,8GHz
    * GPU ARM MAli G72 MP18
    * Nombre de Transistors [^25] : 5,3 milliards

=== "Samsung Galaxy S10"
    ![Annotated Die Shot d'un Exynos 9820, Galaxy S10](./img/exynos9820.png){.center}

    <center>

    Annotated Die Shot d'un Exynos 9820, Samsung Galaxy S10 (2020) :sk-copyright: [Chiprebel](https://www.chiprebel.com/exynos-9820/)

    </center>

    La SoC Exynos 9820 est gravé en $8$ nm. Il est octacore (8 coeurs) répartis sur 3 CPUs :

    * CPU Dualcore Exynos M4 \@2,9 GHz.
    * CPU Dualcore Cortex A75 \@2,8 GHz.
    * CPU Quadcore Cortex A55 \@1,95 GHz.
    * GPU ARM Mali G76 MP12 \@702 MHz
    * NPU intégré
    * Capteurs : 
        * Proximité
        * Lumière
        * Accéléromètre (Détection d'accélérations)
        * Boussole
        * Gyroscope (Détection de mouvement)
        * Baromètre (pour la pression)
        * Lecteur d'Empreintes
        * Capteur à effet Hall (pour champ magnétique)
        * Pulsations cardiaques
    * Nombre de Transistors : (à trouver)

=== "Apple iPhone 13"
    La puce Apple A15 Bionic des iphone 13 est fabriquée par [TSMC](https://fr.wikipedia.org/wiki/Taiwan_Semiconductor_Manufacturing_Company)

    ![image](./img/Apple_A15.jpg){: .center  width=20%}

    ![image](./img/A15_inside.png){: .center width=80%}

    Cette puce A15 Bionic est gravée en $5$ nm contient :

    * $15$ milliards de transistors (gravés à 5 nm)
    * un processeur central à $6$ coeurs :
        * $2$ coeurs hautes performances: $2$x Apple Avalanche \@3,23 GHz
        * $4$ coeurs plus économes en énergie : $4$x Apple Blizzard \@1,82 GHz 
    * un GPU (processeur dédié uniquement au calcul du rendu graphique) de 5 coeurs \@1200 MHz
    * une puce dédiée au Machine Learning (*Neural Engine*)

Samsung a choisi de ne pas commercialiser les mêmes puces pour ses smartphones dans toutes les régions du monde :

* **Snapdragon** de **Qualcomm** : Amérique du Nord, Chine, Asie, commercialisés dans les Samsung
* **Exynos** de **Samsung** : Europe & reste du monde  

![Exynos vs Snapdragon](./img/exynos-vs-snapdragon.png){.center}  
<center>

Exynos vs Snapdragon, 2021, Galaxy S20
:sk-copyright: [phonandroid](https://www.phonandroid.com/galaxy-s21-voila-pourquoi-la-france-est-privee-de-snapdragon-888.html)
</center>

### Carte Mère d'un Smartphone

=== "Samsung Galaxy S20"
    ![Carte Mère S20 Recto](./img/carte-mere-galaxy-s20-recto.png){.center}
    <center>
    Carte Mère d'un Galaxy S20 (2022), Recto
    </center>

    * PoP (Package on Package) = Qualcomm Snapdragon 865 (SM8250) Application Processor & Samsung K3LK4K40BM-BGCN 12 GB LPDDR5 (RAM)
    * Qualcomm Snapdragon X55 5G Modem (SDX55)
    * Samsung KLUEG8UHDB-C2D1 256 GB UFS 3.0
    * CI (Circuit intégré) Maxim MAX77705 Gestion Alimentation / PMIC - Power Management Integrated Circuit
    * STMicroelectronics STM32G0786 MCU (Microcontrôleur)
    * Qualcomm PM3003 PMIC (Gestion d'Alimentation)
    * Cirrus Logic CS35L40 Audio Amplifier (Ampli Audio)
    * Qualcomm QPM5677 PAM (Band N77/78)
    * Qualcomm QDM5873 FEM - Front End Module (puce de connectivité)
    * CI (Circuit Intégré) Samsung S2MPB02 Gestion Alimentation (PMIC) Caméra
    * ON Semi NCP59744 Régulateur de Voltage
    * Maxim MAX77816 Régulateur de Buck-boost
    * Qualcomm QDM4820 FEM

    ![Carte Mère S20 Verso](./img/carte-mere-galaxy-s20-verso.png){.center}
    <center>
    Carte Mère d'un Galaxy S20 (2022), Verso
    </center>

    * Qualcomm QDM4870 FEM
    * Samsung Electro-Mechanics Wi-Fi/BT Module
    * Qualcomm PM8150C PMIC
    * Qualcomm PMX55 PMIC
    * Qualcomm PM8250 PMIC
    * Qualcomm QET6100 Envelope Tracking IC
    * ON Semi NCP59744 Voltage Regulator
    * CI Samsung S2MPB03 Gestion Alimentation (PMIC) Caméra 

=== "Samsung Galaxy S10"
    ![Carte Mère S10 Recto](./img/carte-mere-s10-recto.png){.center}
    <center>
    Carte Mère d'un Galaxy S10 (2020), Recto
    </center>

    ![Carte Mère S10 Verso](./img/carte-mere-s10-verso.png){.center}
    <center>
    Carte Mère d'un Galaxy S10 (2020), Verso
    </center>
=== "Samsung Galaxy S9"
    ![Carte Mère S9 Recto](./img/carte-mere-s9-recto.png){.center}

    <center>
    Carte Mère d'un Galaxy S9, Recto
    </center>

    * Samsung 82LBXS2 NFC Controler & Samsung Secure Element
    * Broadcom BCM43570 : Wifi Bluetooth Module
    * Heart Rate Sensor : Capteur Cardiaque
    * Maxim MAX98512 Audio Amplifier : Amplificateur Audio
    * Samsung S2MPB02 Camera PMIC : - Power Management Integrated Circuit - Gestion Alimentation Caméra
    * Shannon 560 PMIC - Power Management Integrated Circuit : Gestion d'Alimentation
    * Cirrus Logic CS47L93 Audio Codec
    * Samsung S2DOS05 Display Power Management Integrated Circuit : Gestion Alimentation Écran
    * Shannon 965 RF Transceiver : Radio Émetteur/Récepteur fontionne avec le modem LTE inclus dans l'Exynos 9810, pour implémenter des débits

    ![Carte Mère S9 Recto](./img/carte-mere-s9-verso.png){.center}
    <center>
    Carte Mère d'un Galaxy S9, Verso
    </center>

    * Maxim MAX98512 Audio Amplifier
    * Avago AFEM-9090 Front End Module : PAM - Power Amplifier Module pour QuadBand GSM/GPRS/EDGE
    * Skyworks SKY77365-11 Power Amplifier Module
    * Murata fL05B Power Amplifier Module
    * Shannon 735 Envelope Tracker : Amplificateur pour Radio Frequence (RF)
    * SoC Samsung Exynos 9810 + Mémoire Samsung 6 GB LPDDR4X (PoP)
    * Samsung KLUCG2K1EA-B0C1 NAND Flash : Mémoire Flash de type UFS - Universal Flash Storage
    * Maxim MAX77705F PMIC
    * Broadcom BCM47752 GNSS Receiver (Global Navigation Satellite System), càd le GPS
    * IDT P9320S Wireless Charging Receiver
=== "Apple iPhone 13 Pro"
    !!! col __60
        ![image](./img/iphone13-1.png){style="width:100%;"}
    !!! col __40
        ![image](./img/iphone13-1-legende.png)
    !!! col __60
        ![image](./img/iphone13-2.png){style="width:100%;"}

        <center>

        :sk-copyright: [ifixit, Vue éclatée de l'iphone 13 Pro](https://fr.ifixit.com/Tutoriel/Vue+%C3%A9clat%C3%A9e+de+l'iPhone+13+Pro/144928)

        </center>
    !!! col __40 clear
        ![image](./img/iphone13-2-legende.png)
    <!-- ![carte mère iphone13-1](./img/carte-mere-iphone13-1.png){ .center width=80% } -->
    <!-- ![image](./img/ifixit1.png){: .center width=80%} -->
    <!-- ![image](./img/ifixit2.png){: .center width=80%}
    ![image](./img/ifixit3.png){: .center width=80%}  -->

    On voit que (par exemple) qu'il existe une puce spécifique pour gérer l'audio, une puce spécifique pour le module WiFi, une puce spécifique pour le module Modem 5G...

<env>Quelques Définitions</env>

* Dans les terminaux mobiles, un <rbl>émetteur-récepteur</rbl> :fr: / <rbl>transceiver</rbl> :gb: est le système permettant à la fois d’émettre ou de recevoir un signal et de coder ou décoder l’information contenu dans ce signal
* Le <rbl>FEM - Front-End Module</rbl> est le bloc de **Radio Fréquence (RF)** effectuant toutes les opérations entre la partie « bande de base » (qui génère le signal modulé ou le décode) et le module d’antenne. Il est composé de plusieurs chaînes d’émission-réception RF, chacune dédiée à une bande de fréquences/standard. 

### L'Influence de la Taille de la gravure

!!! pte "Gain de 20% des Performances"
    Une baisse de la taille de la gravure ac pour conséquence :  

    * une augmentation des les performances 
    * une baisse de la consommation électrique

!!! exp
    :one: Le passage d'une gravure de $10$ nm (Exynos 9810) à une gravure de $8$ nm (Exynos 9820) a eu pour conséquence[^28] :
    
    * une augmentation des performances, pour chaque coeur, de $20$%
    * Une baisse de 40% de la consommation électrique

    :two: Le fondeur taïwannais TSMC annonce en $2022$, une taille de gravure de $3$ nm qui a pour conséquence :

    * une augmentation des performances, pour chaque coeur, de $10$% à $15$%
    * Une baisse de la consommation électrique entre $25$% et $30$%

![Finesse de la Gravure](./img/finesse-gravure.png){.center style="width:80%;"}

<center>

Évolution Historique de la Taille des Transistors

</center>

## Le cas d'un Raspberry Pi 4

Le Raspberry Pi $4$ intègre ainsi sur quelques centimètres carrés, une carte mère complète autour d'un processeur ARM **Broadcom** Quad Core Cortex A-$72$ (ARMv$8$) à $1,5$ GHz  

![Rasberry Pi 4](./img/raspberry-pi4.svg){.center}
![Rasberry Pi 4 Détaillé](./img/raspberry-pi4.png){.center}

<center>
Raspberry Pi $4$
</center>

* Les ports <rbl>GPIO - General Purpose Input/Output</rbl> :gb: / **Entrée-sortie à usage général** :fr: sont des ports d'entrées-sorties très utilisés dans le monde des microcontrôleurs/soc. Ce sont des détecteurs ou senseurs pour capter des données, ou encore de contrôler des commandes.

* Un <rbl>UART - Universal Asynchronous Receiver Transmitter</rbl>, est un émetteur-récepteur asynchrone universel. En langage courant, c'est le composant utilisé pour faire la liaison entre l'ordinateur et le port série : en efet l'ordinateur envoie les données en parallèle, il faut donc **sérialiser** ces données, càd les transformer pour les faire passer sur un port série.

## Applications Android/iOS

### Android

* CPU-Z
* CPU-Z Hardware Information / CPU-Z Information matérielle

### iOS

* CPU DasherX - CPU Z
* System Status - Battery & Network Manager

## Références & Notes

### Références

* [ARM Developper Documentation](https://developer.arm.com/documentation)
* [Wikichip](https://en.wikichip.org/wiki/WikiChip)
* Guillaume Connan, Laurent Signac, Prépabac TNSI, Hatier
* [RaspBerry Pi 4, Phonandroid](https://www.phonandroid.com/raspberry-pi-4-officiel-4-go-de-ram-4k-et-un-cpu-plus-puissant-pour-38-e.html)
* Raspberry Datasheets : https://datasheets.raspberrypi.com/
* TechInsights
* ChipRebel
* AnandTech
* PC Benchmarks : https://www.pcbenchmarks.net
* CPU Banchmarks : https://www.cpubenchmark.net/market_share.html 

### Notes

[^1]: [LoRa ](https://www.journaldunet.fr/web-tech/dictionnaire-de-l-iot/1197635-lora-comment-fonctionne-le-reseau-quelles-differences-avec-sigfox-20210305/)
[^2]: [SoC, Pages Perso, Vahid MEGHDADI](http://www.unilim.fr/pages_perso/vahid/SoC/CoursSystemOnChip.pdf)
[^3]: [MediaTek, page wikipedia](https://fr.wikipedia.org/wiki/MediaTek)
[^4]: [Qualcomm, page wikipedia](https://fr.wikipedia.org/wiki/Qualcomm)
[^5]: [Série Snapdragon de Qualcomm, page wikipedia](https://fr.wikipedia.org/wiki/Liste_des_microprocesseurs_Qualcomm_Snapdragon)
[^6]: [MediaTek dépasse Qualcomm et devient le Leader des Processeurs pour Smartphones](https://www.phonandroid.com/mediatek-domine-le-marche-des-smartphones-loin-devant-qualcomm-et-apple.html)
[^7]: [AppLa SoC A13, 2020](https://www.nextinpact.com/article/67716/apple-vante-ses-soc-arm-leurs-fonctionnalites-et-restauration-ses-prochains-mac)
[^8]: [Exynos 9810, anandtech.com](https://www.anandtech.com/show/13199/hot-chips-2018-samsungs-exynosm3-cpu-architecture-deep-dive/3)
[^9]: [TechInsights Exynos 9810, Samsung S9, Cartes Mères](https://www.techinsights.com/blog/samsung-galaxy-s9-teardown)
[^10]: [TechInsights Exynos 9810, Samsung S9 Teardown](https://www.techinsights.com/blog/samsung-galaxy-s9-teardown)
[^11]: [Slideshare Samsung S9 Snapdragon](https://www.slideshare.net/jjwu6266/introducing-samsung-galaxy-s9s9)
[^11]: [ifixit.com, Vue éclatée du Galaxy S9](https://fr.ifixit.com/Tutoriel/Vue+%C3%A9clat%C3%A9e+du+Samsung+Galaxy+S9+/104308)
[^12]: [Exynos 9820 blog with motherboards, chiprebel.com](https://www.chiprebel.com/galaxy-s10-teardown/)
[^13]: [Exynos 9820 blog with motherboards, chiprebel.com](https://www.chiprebel.com/galaxy-s10-teardown/)
[^14]: [Exynos 9820, anandtech.com](https://www.anandtech.com/show/13199/hot-chips-2018-samsungs-exynosm3-cpu-architecture-deep-dive/3)
[^15]: [Liste de Fabricants de processeurs ARM, wikipedia](https://fr.wikipedia.org/wiki/Architecture_ARM#Fabricants_de_processeurs_ARM)
[^16]: [Systeme sur Puce, Mon Lycée Numérique](https://monlyceenumerique.fr/nsi_terminale/arse/a1_systeme_%20sur%20_puce.html)
[^17]: [Samsung Galaxy s20, TechInsights teardown analysis](https://www.techinsights.com/blog/samsung-galaxy-s20-teardown-analysis)
[^18]: [A look inside Galaxy S20, par omdia.tech.informa.com, galaxy S20 ultra](https://omdia.tech.informa.com/-/media/tech/omdia/assetfamily/2020/04/28/criticality-of-5g-modem-to-rf-integration-a-look-inside-samsung-galaxy-s20-ultra/exported/criticality-of-5g-modem-to-rf-integration-a-look-inside-samsung-galaxy-s20-ultra-pdf.pdf) :gb:
[^19]: [Quelques Définitions, Thèse d'Optique & Radio Fréquence, Mohammed BOUTAYEB, Univ Grenoble, 2020](https://www.theses.fr/2020GRALT055.pdf)
[^20]: [Apple iphone 13, Unitedlex](https://www.unitedlex.com/insights/apple-iphone-13-pro-max-teardown-report)
[^21]: [intel : Chipset vs SoC](https://www.intel.com/content/www/us/en/support/articles/000056236/intel-nuc.html)
[^22]: [Introduction to Intel Architecture](https://www.intel.com/content/dam/www/public/us/en/documents/white-papers/ia-introduction-basics-paper.pdf) :gb:
[^23]: [Architecture SoC-FPGA & DMA Controller, Etienne Messerli, 2021, Haute École d'Ingénierie du Canton de Vaud, Suisse :ch:](https://reds.heig-vd.ch/share/cours/SoCF/P03_SOCF_Arch_SoC_DMA.pdf)
[^24]: [Microprocessor vs Microcontroller vs SoC](https://youtu.be/9Rrt0n1oY8E)
[^25]: [SoC : Tout ce qu'il faut savoir sur les processeurs mobiles, FRANDROID](https://www.frandroid.com/comment-faire/comment-fonctionne-la-technologie/383586_tout-ce-quil-faut-savoir-sur-les-soc-et-processeurs-mobiles)
[^26]: [Specs Exynos 9810](https://www.plusmobile.fr/processeurs/samsung/exynos-9810/)
[^27]: [Specs Exynos 9820](https://www.plusmobile.fr/processeurs/samsung/exynos-9820/)
[^28]: [Influence de la Gravure](https://www.techcenturion.com/7nm-10nm-14nm-fabrication)