# TNSI : Annales - SoC System on Chip

## Amérique du Nord, Sujet 1

<env>Exercice 2 du Sujet</env>

=== "Énoncé"
    Un constructeur automobile intègre à ses véhicules des systèmes embarqués, comme par exemple un système de guidage par satellites (GPS), un système de freinage antiblocage (ABS)... 

    Ces dispositifs utilisent des systèmes sur puces (SoC : System on a Chip).

    Citer deux avantages à utiliser ces systèmes sur puces plutôt qu'une architecture classique d'ordinateur.
=== "Correction"
        Question de Cours

## Polynésie 2021, Sujet 1

<env>Exercice 4, partie D, du sujet</env>

=== "Énoncé"
    « Un "système sur une puce", souvent désigné dans la littérature scientifique par le
    terme anglais "system on a chip" (d'où son abréviation SoC), est un système complet
    embarqué sur une seule puce ("circuit intégré"), pouvant comprendre de la mémoire, un
    ou plusieurs microprocesseurs, des périphériques d'interface, ou tout autre composant
    nécessaire à la réalisation de la fonction attendue. » source : Wikipédia
    
    1. Citer un des avantages d'avoir plusieurs processeurs.  
    2. Expliquer pourquoi les systèmes sur puces intègrent en général des bus ayant des vitesses de transmission différentes.  
    3. Citer un des avantages d'un circuit imprimé de petite taille.  
    4. Citer un des inconvénients de cette miniaturisation.
=== "Corrigé"
    1. Accélération des traitements. Parallélisation des calculs  
    2. Il y a des bus à haute vitesse de transmission, qui sont réservés pour les périphériques capables de travailler à haute vitesse (par ex. les accès mémoire et la DMA - Direct Access Memory, via le NorthBridge) et pour les calculs à haute performance, qui sont très gourmands. Néanmoins, il existe également des bus à (plus) faible vitesse de transmission pour les périphériques plus lents (par exemples les interfaces d'Entrées Sorties I/O, comme les Horloges, les ports GPIO, le standard universel d'émetteurs-récepteurs *UART*, via le SouthBridge et pour les calculs moins gourmands, afin d'**économiser de la consommation électrique au maximum**.  
    3. Question de Cours  
    4. Question de Cours


