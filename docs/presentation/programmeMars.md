# Programme de Mars 2023

Référence : [BO Éduscol](https://www.education.gouv.fr/bo/22/Hebdo36/MENE2227884N.htm)

Lors de l'épreuve terminale, aussi bien pour l'**épreuve écrite** que pour l'**épreuve pratique**, dans l'enseignement de spécialité numérique et sciences informatiques, les candidats peuvent être évalués sur les parties suivantes du programme de la classe de terminale :

## Rubrique « Structures de données »

Uniquement les items suivants :

* Structures de données, interface et implémentation
* Vocabulaire de la programmation objet : classes, attributs, méthodes, objets
* Listes, piles, files : structures linéaires. Dictionnaires, index et clé
* Arbres : structures hiérarchiques. Arbres binaires : nœuds, racines, feuilles, sous-arbres gauches, sous-arbres droits

## Rubrique « Bases de données »

Uniquement les items suivants :

* Modèle relationnel : relation, attribut, domaine, clef primaire, clef étrangère, schéma relationnel
* Base de données relationnelle
* Langage SQL : requêtes d'interrogation et de mise à jour d'une base de données

## Rubrique « Architectures matérielles, systèmes d'exploitation et réseaux »

Uniquement les items suivants :

* Gestion des processus et des ressources par un système d'exploitation
* Protocoles de routage

## Rubrique « Langages et programmation »

Uniquement les items suivants :

* Récursivité
* Modularité
* Mise au point des programmes. Gestion des bugs

## Rubrique « Algorithmique »

Uniquement les items suivants :

* Algorithmes sur les arbres binaires et sur les arbres binaires de recherche
* Méthode « diviser pour régner »
