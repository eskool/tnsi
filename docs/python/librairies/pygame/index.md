# TNSI : POO, Tutoriel Pygame

Voici quelques tutoriels vidéos sur :youtube: YouTube conseillés.

## :youtube: TNtube - Apprendre Pygame de A à Z

### Episode 1 : Introduction, Installation et Première Fenêtre

<iframe width="560" height="315" src="https://www.youtube.com/embed/WxEmflz009U?si=PuL5H5G9e9s9PpTb" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

**Boucle de Détection des Événements**:

```python
import pygame
pygame.init()

# Générer la fenêtre du jeu
pygame.display.set_caption("Shoot'em Up")
screen = pygame.display.set_mode((1080,720))

running = True

while running:
    # boucle de détection des événements
    for event in pygame.event.get():
        # si l'événement est une fermeture de fenêtre
        if event.type == pygame.QUIT:
            running = False
pygame.quit()
```

### Episode 2 : Image, Événements, Déplacements

<iframe width="560" height="315" src="https://www.youtube.com/embed/W4cbtcyONag?si=Encg4FvP2H1S2Ucz" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

#### Boucle des Événements SANS Répétitions

```python linenums="1"
import pygame

pygame.init()
screen = pygame.display.set_mode((400,400))
running = True

image = pygame.image.load("ball.png").convert()

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                print("Gauche !")
            if event.key == pygame.K_RIGHT:
                print("Droite !")
            if event.key == pygame.K_UP:
                print("Haut !")
            if event.key == pygame.K_DOWN:
                print("Bas !")

    screen.blit(image, (0,0))
    pygame.display.flip()

pygame.quit()
```

#### Boucle des Événements AVEC répétitions

La fonction `pygame.event.get_pressed()` renvoie un dictionnaire `{pygame.K_a: 0, pygame.K_b:0, pygame.K_c:0, ...}` dont les valeurs sont :

* soit `0` pour une touche NON appuyée
* soit `1` pour une touche APPUYÉE

Cette fonction fonctionne **uniquement avec les touches du clavier**.

```python linenums="1"
import pygame

pygame.init()
screen = pygame.display.set_mode((400,400))
running = True

image = pygame.image.load("ball.png").convert()

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    pressed = pygame.key.get_pressed()
    if pressed[pygame.K_LEFT]:
        print("Gauche !")
    if pressed[pygame.K_RIGHT]:
        print("Droite !")
    if pressed[pygame.K_UP]:
        print("Haut !")
    if pressed[pygame.K_DOWN]:
        print("Bas !")

    screen.blit(image, (0,0))
    pygame.display.flip()

pygame.quit()
```

Balle se déplaçant en 60fps (au lieu de 5000fps par défaut)

```python linenums="1"
import pygame

pygame.init()
screen = pygame.display.set_mode((400,400))
running = True

image = pygame.image.load("ball.png").convert()

x = 0
y = 0

clock = pygame.time.Clock()

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    pressed = pygame.key.get_pressed()
    if pressed[pygame.K_LEFT]:
        x -= 1
    if pressed[pygame.K_RIGHT]:
        x += 1
    if pressed[pygame.K_UP]:
        y -= 1
    if pressed[pygame.K_DOWN]:
        y += 1

    screen.fill((0,0,0))
    screen.blit(image, (x,y))
    pygame.display.flip()
    clock.tick(60)
pygame.quit()
```

### Episode 3 : Structure POO, Rects, Gestion des Collisions

<iframe width="560" height="315" src="https://www.youtube.com/embed/N56R1V5XZBw?si=IjRV1in5U9MoT0cx" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

=== "Fichier `game.py`"
    ```python linenums="1"
    import pygame
    from player import Player


    class Game:
        def __init__(self, screen):
            self.screen = screen
            self.running = True
            self.clock = pygame.time.Clock()
            self.player = Player(0, 0)
            self.area = pygame.Rect(300, 150, 300, 300)
            self.area_color = "red"

        def handling_events(self):
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.running = False

            keys = pygame.key.get_pressed()
            if keys[pygame.K_LEFT]:
                self.player.velocity[0] = -1
            elif keys[pygame.K_RIGHT]:
                self.player.velocity[0] = 1
            else:
                self.player.velocity[0] = 0

            if keys[pygame.K_UP]:
                self.player.velocity[1] = -1
            elif keys[pygame.K_DOWN]:
                self.player.velocity[1] = 1
            else:
                self.player.velocity[1] = 0

        def update(self):
            self.player.move()
            if self.area.colliderect(self.player.rect):
                self.area_color = "blue"
            else:
                self.area_color = "red"

        def display(self):
            self.screen.fill("white")
            pygame.draw.rect(self.screen, self.area_color, self.area)
            self.player.draw(self.screen)
            pygame.display.flip()

        def run(self):
            while self.running:
                self.handling_events()
                self.update()
                self.display()
                self.clock.tick(60)


    pygame.init()
    screen = pygame.display.set_mode((1080, 720))
    game = Game(screen)
    game.run()

    pygame.quit()
    ```
=== "Fichier `player.py`"
    ```python linenums="1"
    import pygame

    class Player:
        def __init__(self, x, y):
            self.image = pygame.image.load("player.png")
            self.image
            self.rect = self.image.get_rect(x=x, y=y)
            self.speed = 5
            self.velocity = [0, 0]

        def move(self):
            self.rect.move_ip(self.velocity[0] * self.speed, self.velocity[1] * self.speed)

        def draw(self, screen):
            screen.blit(self.image, self.rect)
    ```