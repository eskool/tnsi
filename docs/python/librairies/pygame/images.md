# TNSI : POO, Tutoriel Pygame

## Générer une image

Pour générer une image comme fond d'écran, cela se fait en $3$ temps :

1. On **charge** une image avec `background = pygame.image.load("chemin/vers/image.png")`
2. On **applique** l'image sur l'écran `screen` avec `screen.blit(background, (largeur, hauteur))`
3. On **met à jour** l'écran avec `pygame.display.flip()`

```python
import pygame
pygame.init()

# Générer la fenêtre du jeu
pygame.display.set_caption("Shoot'em Up")
screen = pygame.display.set_mode((1080,720))

# importer charger l'arrière plan
background = pygame.image.load("assets/bg.jpg")

running = True

while running:
    # Appliquer le background à la position (largeur, hauteur)
    screen.blit(background, (0,0))

    # Mettre à jour l'écran
    pygame.display.flip()

    # boucle de détection des événements
    for event in pygame.event.get():
        # si l'événement est une fermeture de fenêtre
        if event.type == pygame.QUIT:
            running = False
            pygame.quit()
```

Cette image est stable et devrait s'afficher dans une fenêtre.

