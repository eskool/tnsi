# TNSI : POO, Tutoriel Pygame

## Sprites

Un **Sprite** dans un jeu vidéo est un élément graphique pouvant se déplacer.
Pour créer une classe (e.g. une classe `Player`, ou `Projectile`) qui soit un sprite dans `Pygame`, il FAUT que la classe `Player` (ou `Projectile`) hérite de la super classe `pygame.sprite.Sprite`.

```python
# Classe du Joueur
class Player(pygame.sprite.Sprite):
    def __init_(self):
        # Pour hériter du contructeur de la classe parent
        super().__init__()
        ...
```

