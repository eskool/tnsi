# TNSI : Librairie `pynput`

`pynput` est une librarie Python pour **détecter les événements du clavier** et de la **souris**.  
Ce n'est PAS une librairie de la bibliothèque standard.  
Elle doit être installée séparément, en plus de votre installation standard de Python :python:.  
Elle est disponible sur [PyPI](https://pypi.org/).  
Installez-la sur vos PC personnels à la maison.

## Installation

Dans un Terminal (sur votre ordi perso), taper :

!!! danger ":warning: PAS SUR LES POSTES DU LYCÉE SVP :warning:"
    En effet, cette librairie est déjà installée sur vos postes au Lycée, donc nul besoin de la (ré)installer.  

```code
$ pip install pynput
```

## Utilisation

```python
from pynput import keyboard

with keyboard.Events() as events:
    for event in events:
        if event.key == keyboard.Key.esc:
            print("Appui de la touche ESC détecté")
        if event.key == keyboard.KeyCode.from_char("A") :
            print("Appui de la touche A détecté")
```

Référence : [pynput.readthedocs.io](https://pynput.readthedocs.io/en/latest/)

On détecte l'appui sur une touche avec l'une des deux syntaxes suivantes :

* <bd>Touches Alpha-Numériques</bd>  
Ce sont les lettres de l'Alphabet, majuscules (de `A` à `Z`) ou minuscules (de `a` à `z`), ou bien les chiffres de `0` à `9`.  
Ces touches sont détectées via la syntaxe :

    * `keyboard.KeyCode.from_char("X")` où `X` désigne la **lettre** appuyée
    * `keyboard.KeyCode.from_char("n")` où `n` désigne le **chiffre** appuyé

* <bd>Touches Spéciales</bd>  
Ce sont toutes les autres touches du clavier (non alpha-numériques).  
Ces touches sont accessibles via la syntaxe :  

    * `keyboard.Key.XXX` où `XXX` désigne l'une des valeurs du tableau suivant :

| `keyboard.Key.XXX` | Touche correspondante |
|:-:|:-:|
|`alt`|A generic Alt key. This is a modifier.|
|`alt_gr`|The AltGr key. This is a modifier.|
|`alt_l`|The left Alt key. This is a modifier.|
|`alt_r`|The right Alt key. This is a modifier.|
|`backspace`|The Backspace key.|
|`caps_lock`|The CapsLock key.|
|`cmd`|A generic command button.<br/>On PC platforms, this corresponds<br/> to the Super key or Windows key,<br/> and on Mac it corresponds<br/>to the Command key.<br/>This may be a modifier.|
|`cmd_l`|The left command button.<br/>On PC platforms, this corresponds<br/>to the Super key or Windows key,<br/>and on Mac it corresponds <br/>to the Command key.<br/>This may be a modifier.|
|`cmd_r`|The right command button.<br/>On PC platforms, this corresponds<br/>to the Super key or Windows key,<br/>and on Mac it corresponds<br/>to the Command key.<br/>This may be a modifier.|
|`ctrl`|A generic Ctrl key. This is a modifier.|
|`ctrl_l`|The left Ctrl key. This is a modifier.|
|`ctrl_r`|The right Ctrl key. This is a modifier.|
|`delete`|The Delete key.|
|`down`|A down arrow key.|
|`end`|The End key.|
|`enter`|The Enter or Return key.|
|`esc`|The Esc key.|
|`f1` à `f20`|The function keys. F1 to F20 are defined.|
|`home`|The Home key.|
|`insert`|The Insert key.<br/>This may be undefined<br/>for some platforms.|
|`left`|A left arrow key.|
|`media_next`|The next track button.|
|`media_play_pause`|The play/pause toggle.|
|`media_previous`|The previous track button.|
|`media_volume_down`|The volume down button.|
|`media_volume_mute`|The volume mute button.|
|`media_volume_up`|The volume up button.|
|`menu`|The Menu key.<br/>This may be undefined<br/>for some platforms.|
|`num_lock`|The NumLock key.<br/>This may be undefined<br/>for some platforms.|
|`page_down`|The PageDown key.|
|`page_up`|The PageUp key.|
|`pause`|The Pause/Break key.<br/>This may be undefined<br/>for some platforms.|
|`print_screen`|The PrintScreen key.<br/>This may be undefined<br/>for some platforms.|
|`right`|A right arrow key.|
|`scroll_lock`|The ScrollLock key.<br/>This may be undefined<br/>for some platforms.|
|`shift`||A generic Shift key. This is a modifier.|
|`shift_l`|The left Shift key. This is a modifier.|
|`shift_r`|The right Shift key. This is a modifier.|
|`space`|The Space key.|
|`tab`|The Tab key.|
|`up`|An up arrow key.|
