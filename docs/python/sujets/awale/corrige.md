# 1NSI : TD AWALÉ

## Partie I - Présentation et règles

I.4 - Compréhension des règles

Q1
Alice peut jouer la case 4  ou la case 2 mais elle ne pourra pas récolter de graines. Elle peut jouer la case 5 qui lui permettra de récolter 8 graines.
Q2

Q3








Dans la situation de gauche, Alice n’a qu’une case à jouer. En posant ses graines, elle se retrouve en situation prendre toutes les graines de son adversaire. Donc Alice ne peut pas jouer, donc le jeu se termine.

Dans la situation de droite, Alice peut jouer mais il ne restera qu’une graine sur le plateau donc le jeu s’arrêtera aussi après son coup.

Partie II - Programmation de la structure de jeu
II.1 - Représentation du jeu
Q4
jeu[n] est pair lorsque c’est au joueur 1 de jouer et impair lorsque c’est au joueur 2 de jouer

def tour_joueur1(jeu):
    return jeu['n']%2 == 0

Q5
def tourner_plateau(jeu):
    jeu['plateau'] = jeu['plateau'][6:]+ jeu['plateau'][0:6]

Q6 
Le jeu contient 48 graines (25< 48 <26) , cela représente 6 bits pour coder cette valeur.
Il ne me semble pas probable que les 48 graines puissent se retrouver dans un seul logement.

Q7
def copie(jeu):
    newJeu = initialisation(jeu['joueur1'], jeu['joueur2'])
    newJeu['score'] = [ score for score in jeu['score']]
    newJeu['n'] = jeu['n']
    newJeu['plateau'] = [val for val in jeu['plateau']]
    return newJeu


II.2 - Programmation d’un tour de jeu
Q8
def deplacer_graines(plateau, case):
    nombreDeGraines = plateau[case]
    plateau[case] = 0
    for i in range(1, nombreDeGraines+1):
        if i%12 == 0:
            case += 1
        position = case + i
        plateau[position %12 ] += 1
    return position %12 

Q9
def case_ramassable(plateau, case):
    """ Renvoie True si les graines de la case du camp adverse peut être ramassée
    Ne prend pas en compte la condidion3 sur la famine """
    return case >= 6   and   2 <= plateau[case] <= 3 

Q10
def ramasser_graines(plateau, case):
    ''' Ramasse les graines chez l'adversaire si c'est possible (2 ou 3 graines).
        renvoie le nombre de graines total ramassées. '''
    if not case_ramassable(plateau, case):
        return 0
    else:
        ndGraineCase = plateau[case]
        plateau[case] = 0
        return ndGraineCase + ramasser_graines(plateau, case-1)

Q11
def test_famine(plateau, case):
    """ Renvoie True si le ramassage n'entraînera pas de famine chez l'adversaire,
        sinon False (famine provoqiée)"""
    nbGrainesAdversaire = 0
    for nbGraineCase in plateau[6:] :
        nbGrainesAdversaire += nbGraineCase
    nbGraineRammassees = 0
    while case_ramassable(plateau, case):
        nbGraineRammassees += plateau[case]
        case -= 1
    critereFamine = nbGrainesAdversaire - nbGraineRammassees > 0
    return critereFamine 


Q12
Le rôle de la case concernée dans le sujet n’est pas clair. S’agit-il de la case de départ du joueur ou celle de la case de départ du  ramassage ?
La fonction ci-dessous se place dans le cas de la case de départ du ramassage.
def test_case(plateau, case) :
    """ Vérifie si la case de départ du ramassage est apte à être ramassée 
    sans provoquer la famine de l'adversaire avec le ramassage complet
    renvoie True si la case est acceptable, False sinon """
    condition3 = test_famine(plateau, case)   # Case acceptable
    test =  case_ramassable(plateau, case) and condition3  # à compléter
    return test 

Q13
def cases_possibles(jeu):
    """ Renvoie la liste des indices de toutes les cases jouables par le joueur actif.
    Les cases jouables sont celles qui contiennent au moins 1 graine du coté joueur """
    casesJouables = []
    for case in range(6):
        if jeu['plateau'][case]>0:
            casesJouables.append(case)
    return casesJouables

Q14
def tour_suivant(jeu):
    """Cette fonction renvoie True si le jeu peut continuer et False sinon"""
    condition1 = jeu['score'][0] >= 25 or jeu['score'][1] >= 25
    condition2 = jeu['n'] >= 100
    sommeGrainePlateau = 0
    for i in range(12):
        sommeGrainePlateau +=  jeu['plateau'][i]
    condition3 = sommeGrainePlateau <= 3
    sommeGraineJoueur = 0
    for i in range(6):
        sommeGraineJoueur +=  jeu['plateau'][i]
    condition4 = sommeGraineJoueur == 0
    return not (condition1 or condition2 or condition3 or condition4)

Q15
J’ai modifié l’algorithme car pour poser ses graines, puis récolter après, les conditions sont différentes.
def tour_jeu(jeu, case):
    plateau = jeu['plateau']
    if case in cases_possibles(jeu):            # La case jouée est acceptable
        case = deplacer_graines(plateau, case)  # Instruction1 : deplacer les graines
        graines_gagnees = 0
        if test_case(plateau, case):
            graines_gagnees = ramasser_graines(plateau, case)  # Instruction2 : ramasser les graines
        """ Pour augmenter le score, il faut savoir qui joue grâce à la
        parité du nombre de tours """
        if tour_joueur1(jeu):                               # Condition 1 :
            jeu['score'][0] = jeu['score'][0] + graines_gagnees
        else :
            jeu['score'][1] = jeu['score'][1] + graines_gagnees
        jeu['n'] += 1            # Instruction 3 # On incrémente le nombre de tours
        tourner_plateau(jeu)                                # Echanger les plateaux
        return tour_suivant(jeu)
    else :
        print("La case choisie n’est pas valable")
        return True

Q16
def gagnant(jeu):
    totalGrainesJoueur1  = jeu['plateau'][0]
    for i in range(6):
        totalGrainesJoueur1 +=  jeu['plateau'][i]
    totalGrainesJoueur2  = jeu['plateau'][1]
    for i in range(6,12):
        totalGrainesJoueur2 +=  jeu['plateau'][i]
    if totalGrainesJoueur1 > totalGrainesJoueur2:
        return jeu['joueur1']
    if totalGrainesJoueur2 > totalGrainesJoueur1:
        return jeu['joueur2']
    return "Egalité"

III.1 - Arbre des configurations
Q17
def gain(jeu, case):
    case = deplacer_graines(jeu['plateau'], case) # Instruction1 : deplacer les graines
    graines_gagnees = 0
    if test_case(jeu['plateau'], case):
        graines_gagnees = ramasser_graines(jeu['plateau'], case)  # Instruction2 : ramasser les graines
    """ Pour augmenter le score, il faut savoir qui joue grâce à la
    parité du nombre de tours """
    if tour_joueur1(jeu):                               # Condition 1 :
        jeu['score'][0] = jeu['score'][0] + graines_gagnees
    else :
        jeu['score'][1] = jeu['score'][1] + graines_gagnees
    jeu['n'] += 1                    # Instruction 3 # On incrémente le nombre de tours
    tourner_plateau(jeu)                                # Echanger les plateaux
    return graines_gagnees, copie(jeu)


III.2 - Algorithme MinMax
Q18
La case à jouer par Alice est la 5 (celle qui contient 2) car c’est celle qui donne la valeur G – VJ la plus grande.



Q19
def NegaAwale(jeu, profondeur_max, profondeur):
    if tour_suivant(jeu) == False: # Condition 1
        if (tour_joueur1(jeu) and gagnant(jeu) == jeu['joueur1'] ) or \
            (not (tour_joueur1(jeu)) and gagnant(jeu) == jeu['joueur2']):
            return 500
        elif (tour_joueur1(jeu) and gagnant(jeu) == jeu['joueur2']) or \
            (not (tour_joueur1(jeu)) and gagnant(jeu) == jeu['joueur1']) :
            return -500
        else : # Egalité
            return 0
    elif profondeur_max == profondeur : # Condition 2
        return 0
    else :
        choix_cases = cases_possibles(jeu)
        vals_jeu = []
        for case in choix_cases :
            g, newGame = gain(copie(jeu), case)                  # Instruction1 : Détermination du gain et du nouveau jeu
            p = NegaAwale(newGame, profondeur_max, profondeur+1) # Instruction2 : Remontée de la valeur de jeu du noeud enfant
            vals_jeu.append([case, g-p])
        return max_vals(vals_jeu, profondeur)

Q20
def max_vals(vals_jeu, profondeur):
    gpMax = vals_jeu[0][1]
    caseMax = vals_jeu[0][0]
    for case, gp in vals_jeu:
        if gp > gpMax:
            caseMax = case
            gpMax = gp 
    if profondeur == 0: # Noeud de départ
        return caseMax     # on renvoie l'indice de la case à choisir
    else :              # niveau intermédiaire
        return gpMax       # on renvoie la valeur gp intemédiaire

Q21
def awale_jcIa(nom_joueur1, nom_joueur2="IA computer") :
    jeu = initialisation(nom_joueur1, nom_joueur2 )
    jeu_continue = True
    while jeu_continue:
        affiche(jeu['plateau'] )
        if tour_joueur1(jeu):
            case_choisie = int( input("Choisir une case  : ") )
        else:
            case_choisie = NegaAwale(jeu, 6, 0)
        jeu_continue = tour_jeu(jeu, case_choisie)
    return gagnant(jeu)
III.3 - Bibliothèque d’ouverture

Q22. 

SELECT id_joueur
FROM Joueur
WHERE niveau > 1900;

Q23. 

Requête obtenue avec ChatGpt (langue au chat !!). Les résultats de la requête sont corrects par rapport à la base de données testée.

SELECT 
    (SUM(CASE WHEN resultat = 1 THEN 1 ELSE 0 END) * 100.0 / COUNT(*)) AS pourcentage_victoires_joueur1
FROM 
    Partie
WHERE 
    jeu LIKE 'a%';

Q24. 
SELECT nom, prenom
FROM Joueur
ORDER BY niveau
LIMIT 3;


Q25. 
Requête obtenue avec ChatGpt (langue au chat !!). Les résultats de la requête sont corrects par rapport à la base de données testée.

SELECT 
    j.nom, 
    j.prenom, 
    (COALESCE(victoires_joueur1, 0) + COALESCE(victoires_joueur2, 0)) AS nombre_victoires
FROM 
    Joueur j
LEFT JOIN 
    (SELECT id_joueur1 AS id_joueur, COUNT(*) AS victoires_joueur1 
     FROM Partie 
     WHERE resultat = 1 
     GROUP BY id_joueur1) v1 
ON j.id_joueur = v1.id_joueur
LEFT JOIN 
    (SELECT id_joueur2 AS id_joueur, COUNT(*) AS victoires_joueur2 
     FROM Partie 
     WHERE resultat = 0 
     GROUP BY id_joueur2) v2 
ON j.id_joueur = v2.id_joueur
WHERE 
    (COALESCE(victoires_joueur1, 0) + COALESCE(victoires_joueur2, 0)) > 100
ORDER BY 
    nombre_victoires DESC;
