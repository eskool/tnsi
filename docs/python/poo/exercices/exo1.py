class CompteBancaire:
    def __init__(self, numeroCompte:str="112233", nom:str="DUPOND", solde:float=0):
        self.numeroCompte = numeroCompte
        self.nom = nom
        self.solde = solde
    def depot(self, montant:float):
        self.solde += montant
        print("Après ajout de",montant,"nouveau solde=",self.solde)
    def retrait(self, montant:float):
        self.solde -= montant
        print("Après retrait de",montant,"nouveau solde=",self.solde)
    def agios(self,p:float):
        assert self.solde<0,"pas d'agios car vous êtes en positif"
        if self.solde <0:
            self.solde = self.solde - p/100*abs(self.solde)
            print("après les agios, nouveau solde=",self.solde)
    def infos(self):
        print("numeroCompte=",self.numeroCompte)
        print("nom=",self.nom)
        print("solde=",self.solde)
    def getSolde(self):
        return self.solde
    def setSolde(self, valeur:float):
        self.solde = valeur
        return self.solde
    def __repr__(self):
        return f"numeroCompte={self.numeroCompte}\nnom= {self.nom}\nsolde={self.solde}"

compteLaura = CompteBancaire("237533","DURAND",100)
print(compteLaura.getSolde())
nouveauSolde = compteLaura.setSolde(1000)
print(nouveauSolde)
# print(compteLaura)