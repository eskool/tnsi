class Personnage:
    # contructeur = méthode __init__ avec 2 arguments (sans compter self !) le nombre de vies et le nom du personnage
    def __init__(self, nbreDeVie, nomDuPerso):
        self.vie = nbreDeVie
        self.nom = nomDuPerso
    # voici la méthode qui affiche le nombre de vies du personnage.
    def affichePointVie(self):
        print('Il reste '+str(self.vie)+' points de vie à '+self.nom)
    # voici la méthode qui fait perdre 1 point de vie au personnage qui a subi une attaque
    def perdVie(self):
        print(self.nom+' subit une attaque, il perd une vie')
        self.vie = self.vie-1
    def perdVies(nombre:int):
        for i in range(nombre):
            self.perdVie()
    def setPointVie(nombre):
        assert nombre>=0, "La vie doit être positive"
        self.vie = nombre
    def __repr__(self):
        return f"Nom={self.nom}\nPoints de Vie={self.vie}"

# la classe Magicien hérite de la classe Personnage
class Magicien(Personnage):
    # dans def __init__ on retrouve nbreDeVie et nomDuPerso comme dans le def __init__ de la classe Personnage
    def __init__(self, nbreDeVie, nomDuPerso, pointMagie):
        # la ligne suivante est très importante dans le cas d'héritage, il faut systématiquement faire ce genre d'appel :
        # classeparente.__init__(self,arg1,arg2.....) pour hériter de tous les attributs et méthodes de la classe Personnage
        # Personnage.__init__(self, nbreDeVie, nomDuPerso)
        super().__init__(nbreDeVie, nomDuPerso)
        # le seul nouvel attribut est self.magie, tous les autres sont hérités de la classe Personnage
        self.magie = pointMagie
    # une méthode uniquement disponible pour les instances de magiciens
    def faireMagie(self):
        print (self.nom+' fait de la magie')
        self.magie = self.magie - 1
        print('Il reste '+str(self.magie)+' points de magie à '+self.nom+'.')
    # les autres méthodes des instances de magicien sont héritées de la classe personnage
    def creeVie(self):
        self.vie += 1
    def creeVies(self, nombre):
        for i in range(nombre):
            self.creeVie()
    def __repr__(self):
        return f"""Nom={self.nom}
Points de Vie={self.vie}
Points MagieArcs={self.magie}"""

class Archer(Personnage):
    # dans def __init__ on retrouve nbreDeVie et nomDuPerso comme dans le def __init__ de la classe Personnage
    def __init__(self, nbreDeVie, nomDuPerso, nbArcs=1, nbFleches=10):
        # la ligne suivante est très importante dans le cas d'héritage, il faut systématiquement faire ce genre d'appel :
        # classeparente.__init__(self,arg1,arg2.....) pour hériter de tous les attributs et méthodes de la classe Personnage
        # Personnage.__init__(self, nbreDeVie, nomDuPerso)
        super().__init__(nbreDeVie, nomDuPerso)
        self.__nbArcs = nbArcs
        self.__nbFleches = nbFleches
    def getNbArcs(self):
        return self.__nbArcs
    def setNbArcs(self, nombre):
        assert nombre >=0, "Le nombre d'Arcs doit être positif"
        self.__nbArcs = nombre
        return self.__nbArcs
    def getNbFleches(self):
        return self.__nbFleches
    def setNbFleches(self, nombre):
        assert nombre >=0, "Le nombre de Flèches doit être positif"
        self.__nbFleches = nombre
        return self.__nbFleches
    def tirerFleche(self):
        self.__nbFleches -= 1
        print(f"{self.nom} a lancé une flèche")

    def __repr__(self):
        return f"""Nom={self.nom}
Points de Vie={self.vie}
Nombre d'Arcs={self.__nbArcs}
Nombre de Flèches={self.__nbFleches}"""

greenarrow = Archer(10,'GreenArrow',2,30)
greenarrow.affichePointVie()
greenarrow.tirerFleche()

# print(greenarrow)

# on crée une instance 'merlin' de Magicien
# merlin = Magicien(30,'Merlin',17)
# merlin.affichePointVie()
# merlin.creeVies(5)
# merlin.affichePointVie()

