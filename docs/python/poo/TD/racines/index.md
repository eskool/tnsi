# TNSI : TD Classe Racine

On se donne la classe `Racine` suivante (à compléter) :

```python
class Racine:
  def __init__(self, ...etc...):
    # ... à compléter ...
```

1. Compléter le constructeur `__init__()` de sorte qu'il reçoive en entrée deux arguments :

    * l'instance courante `self` de la racine (du deuxième paramètre `n`)
    * et un entier `n` (au sens Python)

2. Créer une méthode magique `__repr__()` qui représente la racine de l'instance courante `self` dans un Terminal, sous la forme :  

    * si vous y arrivez, sous la forme `a*rac(b)` avec :
        * `a` est un entier le plus grand possible et 
        * `b` est un entier le plus petit possible  
        On pourra utiliser la méthode `decomposition_carre()` de la classe `Entier` du TD Arithmétique.
    * ou bien (dans le pire des cas, où vous n'y arrivez pas), sous la forme `rac(n)`

3. Créer une méthode magique `__mul__(k:int)->Racine` qui *multiplie* par un entier `k`, l'instance courante `self`
4. Créer une méthode magique `__add__(self,r2:Racine)->Racine` qui **ajoute** deux racines, **(uniquement) dans le cas où le résultat de l'addition soit encore une racine,** càd lorsque: <enc>$a \sqrt c + b\sqrt c=(a+b)\sqrt c$</enc> :  
    Cette méthode reçoit en entrée

    * l'instance courante `self`, et
    * `r2` qui est une instance de la classe Racine  

    Cette méthode renvoit en sortie une instance de la classe `Racine`, modélisant l'addition des deux racines.

    <bd @gris>Remarque</bd> Les deux syntaxes des méthodes magiques doivent donc fonctionner pour cette méthode précise, lorsque `racineGauche` et `racineDroite` sont deux instances de la classe `Racine` :

    * ou bien `racineGauche.__add__(racineDroite)`
    * ou bien `racineGauche + racineDroite`

5. Créer une méthode magique `__sub__(self,r2:Racine)->Racine` qui **soustrait** deux racines, **(uniquement) dans le cas où le résultat de la soustraction soit encore une racine,** càd lorsque: <enc>$a \sqrt c - b\sqrt c=(a-b)\sqrt c$</enc> :  
    Cette méthode reçoit en entrée

    * l'instance courante `self`, et
    * `r2` qui est une instance de la classe Racine  

    Cette méthode renvoit en sortie une instance de la classe `Racine`, modélisant la soustraction des deux racines.

    <bd @gris>Remarque</bd> Les deux syntaxes des méthodes magiques doivent donc fonctionner pour cette méthode précise, lorsque `racineGauche` et `racineDroite` sont deux instances de la classe `Racine` :

    * ou bien `racineGauche.__sub__(racineDroite)`
    * ou bien `racineGauche - racineDroite`

6. Créer une méthode magique `__mul__(self,r2:Racine)->Racine` qui **multiplie** deux racines : <enc>$\sqrt a \times \sqrt b=\sqrt{ab}$</enc> :  
    Cette méthode reçoit en entrée

    * l'instance courante `self`, et
    * `r2` qui est une instance de la classe Racine  

    Cette méthode renvoit en sortie une instance de la classe `Racine`, modélisant la multiplication des deux racines.

    <bd @gris>Remarque</bd> Les deux syntaxes des méthodes magiques doivent donc fonctionner pour cette méthode précise, lorsque `racineGauche` et `racineDroite` sont deux instances de la classe `Racine` :

    * ou bien `racineGauche.__mul__(racineDroite)`
    * ou bien `racineGauche * racineDroite`

7. Que peut-on proposer pour la **division** de deux racines de nombres entiers, avec la classe proposée ci-dessus? Détaillez.