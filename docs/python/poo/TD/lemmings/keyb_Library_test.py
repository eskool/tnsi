from pynput import keyboard

# The event listener will be running in this block
# with keyboard.Events() as events:
#     for event in events:
#         if event.key == keyboard.KeyCode.from_char("1"):
#             print("1 DETECTED")

class bcolors: 
    VIOLET = '\033[95m' 
    BLEU = '\033[94m' 
    CYAN = '\033[96m' 
    VERT = '\033[92m' 
    JAUNE = '\033[93m' 
    ROUGE = '\033[91m' 
    ENDC = '\033[0m' 
    BOLD = '\033[1m' 
    UNDERLINE = '\033[4m' 
    
print(f"Voici un {bcolors.ROUGE}texte{bcolors.ENDC} {bcolors.BOLD}avec{bcolors.ENDC} des {bcolors.BLEU}couleurs{bcolors.ENDC}")