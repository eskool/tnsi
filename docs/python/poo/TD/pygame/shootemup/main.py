import pygame
pygame.init() # Initialisation : Charge les éléments de pygame

WIDTH = 1080
HEIGHT = 720
# from player import Player
from game import Game

# Générer la fenêtre du jeu
pygame.display.set_caption("Shoot'Em Up")
# renvoie une Surface `screen`
screen = pygame.display.set_mode((WIDTH,HEIGHT))

# importer / charger l'arrière plan dans une variable python
background = pygame.image.load("assets/bg.jpg")

# player = Player()
# Charge le Jeu
game = Game()

running = True

while running:
    # Appliquer le background au screen, à la position (largeur, hauteur)
    screen.blit(background, (0,-200))

    # Appliquer l'image du joueur au screen, à la position (largeur, hauteur) càd rect
    screen.blit(game.player.image, game.player.rect)

    if game.pressed.get(pygame.K_RIGHT) and game.player.rect.x + game.player.rect.width <= screen.get_width(): # vérifie si cette touche est pressée
        game.player.move_right()
    elif game.pressed.get(pygame.K_LEFT) and game.player.rect.x >= 0: # vérifie si cette touche est pressée
        game.player.move_left()

    # Mettre à jour l'écran
    pygame.display.flip()

    # boucle de détection des événements
    for event in pygame.event.get():
        # si l'événement est une fermeture de fenêtre
        if event.type == pygame.QUIT:
            running = False
            pygame.quit()
        # Détecte si une touche de clavier est pressée
        elif event.type == pygame.KEYDOWN:
            game.pressed[event.key] = True
        # Détecte si une touche de clavier est relâchée
        elif event.type == pygame.KEYUP:
            game.pressed[event.key] = False
        