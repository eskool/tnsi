import pygame

from player import Player

# Classe du Joueur
class Game(pygame.sprite.Sprite):
    def __init__(self):
        # hérite du constructeur parent
        super().__init__()
        self.player = Player()
        self.pressed = {}