# TD Pygame: Shoot'Em Up

Nous vous proposons de créer jeu vidéo du type *shoot'em up*, composé de:

* une classe **Player**
* une classe **Game**
* une classe **Monster**
* une classe **Projectile**
* une classe **Comet**

![Classes du jeu](./img/classesDuJeu.png){.center}

## Assets

Télécharger le fichier `assets.zip` en [cliquant ici](https://gitlab.com/eskool/tnsi/-/raw/main/docs/python/poo/TD/pygame/shootemup/assets.zip)

Puis décompressez-le (vous obtiendrez un dossier `assets/` contenant tous les assets) :  
Pour décompresser un fichier zippé, en ligne de commande, sur Linux :

1. Commencer par déplacer le fichier `assets.zip` dans votre répertoire de travail (par exemple dans le dossier `Documents/votre_prenom/`)
1. Déplacez-vous en ligne de commande dans votre répertoire de Travail :

    * `$ cd Documents/votre_prenom`
    * `$ unzip assets.zip`

## Fichier main.py

Créer un fichier `main.py` qui contient le code principal du jeu:

```python
import pygame
pygame.init()

# Générer la fenêtre du jeu
pygame.display.set_caption("Shoot'em Up")
screen = pygame.display.set_mode((1080,720))

# importer charger l'arrière plan
background = pygame.image.load("assets/bg.jpg")

running = True

while running:
    # Appliquer le background à l'endroit (largeur, hauteur)
    screen.blit(background, (0,0))

    # Mettre à jour l'écran
    pygame.display.flip()

    # boucle de détection des événements
    for event in pygame.event.get():
        # si l'événement est une fermeture de fenêtre
        if event.type == pygame.QUIT:
            running = False
            pygame.quit()
```

## Classe Player

Créer un fichier `player.py` dans lequel vous définirez la classe `Player` :

```python
import pygame

class Player:
    def __init__(self):
        # A compléter
        pass
pygame.display.set_caption("")
```

1. Créer les attributs d'instance suivants pour la classe Player:

    * `self.health` : contient la vie restante du joueur (par défaut `100`)
    * `self.max_health` : contient la valeur maximale de vie d'un joueur (par défaut `100`)
    * `self.attack` : contient la valeur des dégâts d'un joueur (par défaut `10`)
    * `self.velocity` : contient la vitesse de déplacement d'un joueur (par défaut `5`)

2. Pour que la classe `Player` soit pris en compte par **Pygame** comme étant un composant graphique du jeu, visible et déplaçable, il FAUT que la classe `Player` soit **un Sprite** : Il faut donc que la classe `Player` doit hériter de la (super) classe `pygame.sprite.Sprite`

    ```python
    import pygame

    class Player(pygame.sprite.Sprite):
        def __init__(self):
            # Pour hériter du contructeur de la classe parent
            super().__init__()
            # A compléter
            ...
    pygame.display.set_caption("")
    ```

    * Créer un attribut `self.image` qui associe une image à la classe `Player` avec : `self.image = pygame.image.load("chemin/vers/image.png")`
    * Créer un attribut `self.rect` qui récupèrer le rectangle de l'image du Joueur avec : `self.rect = self.image.get_rect()`

3. Créer une variable `player` qui instancie la classe `Player`, dans le fichier `main.py` :

    * après `background = `
    * avant `running = True`

4. Appliquer l'image du joueur dans l'écran avec `screen.blit()` à la position `player.rect`
5. Déplacer le player à la position `(x,y)=(300,300)` (par exemple) en définissant:

    * `self.rect.x = 300`
    * `self.rect.y = 300`

    Puis, en utilisant cette syntaxe, positionner le player en bas de l'image, au milieu.

Vous n'y arrivez pas ? Repartez de la solution [ici]()

## Classe Game

Créer un fichier `game.py` dans lequel vous définirez la classe `Game` :

Cette classe sera en charge d'instancier tout ce dont on a besoin pour le jeu

```python
import pygame
from player import Player

class Game:
    def __init__(self):
        # A compléter
        self.player = Player()
```

1. Modifier et adaptez le code du fichier `main.py` de sorte que:

    * le player soit instancié dans la classe Game (comme montré ci-dessus)
    * le player ne sera plus instancié dans `main.py`
    * Désormais, ce sera la variable `game` qui instancie la classe `Game` dans le fichier `main.py`, par exemple grâce à l'instruction : `game = Game()`

Vous n'y arrivez pas ? Repartez de la solution [ici]()

## Déplacements du player

