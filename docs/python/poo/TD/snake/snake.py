# Import the necessary modules
import pygame
import random

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)

# Define the size of the game screen
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

# Initialize the game
pygame.init()

# Set the screen size and title
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption('Snake')

# Set the initial position of the snake
snake_pos = [100, 50]

# Set the initial direction of the snake
snake_dir = "RIGHT"

# Set the initial speed of the snake (in pixels per frame)
snake_speed = 15

# Set the initial length of the snake
snake_body = [[100, 50], [90, 50], [80, 50]]

# Set the initial position of the food
food_pos = [random.randrange(1, (SCREEN_WIDTH // 10)) * 10, random.randrange(1, (SCREEN_HEIGHT // 10)) * 10]
food_spawn = True

# Set the initial score
score = 0

# Create a clock to control the game speed
clock = pygame.time.Clock()

# Set the game loop to run indefinitely
while True:
  # Set the game speed (in frames per second)
  clock.tick(20)
  
  # Check for events
  for event in pygame.event.get():
    # If the event is to quit the game, stop the loop
    if event.type == pygame.QUIT:
      pygame.quit()
      exit()
    # If a key is pressed, check if it is an arrow key
    if event.type == pygame.KEYDOWN:
      if event.key == pygame.K_UP:
        snake_dir = "UP"
      if event.key == pygame.K_DOWN:
        snake_dir = "DOWN"
      if event.key == pygame.K_LEFT:
        snake_dir = "LEFT"
      if event.key == pygame.K_RIGHT:
        snake_dir = "RIGHT"
  
  # Update the snake's position
  if snake_dir == "UP":
    snake_pos[1] -= snake_speed
  if snake_dir == "DOWN":
    snake_pos[1] += snake_speed
  if snake_dir == "LEFT":
    snake_pos[0] -= snake_speed
  if snake_dir == "RIGHT":
    snake_pos[0] += snake_speed
  
  # Update the snake's body
  snake_body.insert(0, list(snake_pos))
  if snake_pos[0] == food_pos[0] and snake_pos[1] == food_pos[1]:
    score += 1
    food_spawn = False
  else:
    snake_body.pop()
  
  # Spawn new food if necessary
  if not food_spawn:
    food_pos = [random
