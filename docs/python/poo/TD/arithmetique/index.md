# TNSI, Exercices POO: Applications à l'Arithmétique

On se donne la classe `Entier` suivante (à compléter) :

```python
class Entier:
  def __init__(self, ...etc...):
    # ... à compléter ...
```

1. Créer un constructeur `__init__(self, n:int)` qui :  

    * reçoit en entrée un entier `n` (au sens Python) et 
    * définit un attribut public d'instance `self.n = n` 

2. Créer une méthode `table_multiplication()->list` qui renvoie une liste contenant la table de multiplication (de $0$ jusqu'à $10$) de l'instance courante `self`.  
3. Créer une méthode `factorielle()->int` qui renvoie le factorielle de l'instance courante `self`. Tester la méthode en instanciant la classe.  
4. Créer une méthode `somme()->int` qui renvoie la somme `1+2+3+..+n` des `n` premiers entiers, dans laquelle `n` désigne l'instance courante `self`. Tester la méthode.  
5. Créer une méthode `est_premier()->bool` qui renvoie:  

    * `True` lorsque l'instance courante `self` est un nombre premier. 
    * `False` sinon

    Tester la méthode.  

    !!! info
        Un entier $n$ est un **nombre premier** lorsque $n$ est un nombre entier divisible uniquement par $1$ et par lui-même ($n$)

6. Créer une méthode **récursive** `pgcd(b:Entier)->int` qui renvoie le **pgcd (Plus Grand Commun Diviseur)** de l'instance courante de la classe Entier (`self`) et de l'(instance de classe) Entier `b`

    !!! info
        Si $a$ et $b$ sont deux entiers tels que $a\gt b$. Alors :  

        * `pgcd(a,b) = pgcd(b, a-b)`
        * `pgcd(a,0) = a`

7. Créer une méthode `est_premier_avec(m:int)->bool` qui renvoie :

    * `True` lorsque l'instance courante `self` est premier avec l'entier `m`
    * `False` sinon

    !!! info
        Soient $n$ et $m$ deux nombres entiers.   
        
        On dit, de manière équivalente, que:

        * $n$ et $m$ sont **premiers entre eux**, 
        * $n$ **est premier avec** $m$ 
        * $m$ **est premier avec** $n$

        lorsque l'une des conditions équivalentes suivantes est vraie:   

        * $pgcd(n,m) = 1$, ou bien ($\Leftrightarrow$)
        * $n$ et $m$ n'ont AUCUN diviseur commun $d$ (autre que $1$)

8. Créer une méthode `liste_diviseurs()->list` qui renvoie la liste de tous les **diviseurs** de l'instance courante `self`.  
9. Créer une méthode `liste_diviseurs_stricts()->list` qui renvoie la liste de tous les **diviseurs** de l'instance courante `self`, SAUF $1$ et elle-même (`self`).  
10. Créer une méthode `liste_diviseurs_premiers()->list` qui renvoie la liste de tous les **diviseurs premiers** de l'instance courante `self` (en tenant compte des répétitions éventuelles, par exemple, la liste des diviseurs premiers de $360$ est `[2,2,2,3,3,5]` car $360=2\times 2 \times 2 \times 3 \times 3 \times 5$).  

    !!! info
        $p$ est un **diviseur premier** de $n$ lorsque :

        * $p$ est un nombre premier
        * $p$ divise $n$

11. En déduire une méthode `decomposition_carre()->tuple(int,int)` qui renvoie un tuple d'entiers $(a,b)$ tels que:

    * $a$ et $b$ sont des entiers
    * $a$ est **le plus grand entier possible** et 
    * $b$ est **le plus petit entier possible** tels que 
    
    <center><enc>$n=a^2\times b$</enc></center>

    Par exemple :  
    $360=6^2 \times 10$ $\quad$ (càd $a=6$, $b=10$)  
    car $360=2\times 2 \times 2 \times 3 \times 3 \times 5$ donc $360=(2\times 3)^2 \times 2 \times 5$