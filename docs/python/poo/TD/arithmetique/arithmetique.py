from math import sqrt

class Entier:
    def __init__(self, n:int):
        self.n = n
    def factorielle(self)->int:
        if self.n == 0:
            return 1
        else:
            return self.n * (Entier(self.n-1).factorielle())
    def somme(self)->int:
        if self.n == 1:
            return 1
        else:
            return self.n + (Entier(self.n-1).somme())
    def est_premier(self):
        for i in range(2,int(sqrt(self.n)+1)):
            if self.n % i == 0:
                return False
        return True
    def liste_diviseurs(self):
        diviseurs = [1]
        for i in range(2,int(self.n/2)+1):
            if self.n % i == 0:
                diviseurs.append(i)
        diviseurs.append(self.n)
        return diviseurs
    def liste_diviseurs_stricts(self):
        return self.liste_diviseurs()[1:-1]

    # def est_premier_avec(self, m:int)->bool:

if __name__ == "__main__":
    n = Entier(6)
    print(n.liste_diviseurs())
    print(n.liste_diviseurs_stricts())
 




