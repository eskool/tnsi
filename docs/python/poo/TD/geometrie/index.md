# TNSI, Exercices POO : Applications à la Géométrie

!!! ex "Classe Point"
    1. Créer une classe `Point` ayant les **attributs** suivants :  

        * `__x` : un attribut **privé** pour l'**abscisse**, qui est de type `float`
        * `__y` : un attribut **privé** pour l'**ordonnée** qui est de type `float`
        * Par défaut, un Point est initialisé à `O(0;0)`  
        On pourra utiliser un constructeur `__init__()` ayant deux paramètres `x` et `y`
    2. Créer les **getters** `getX()` et `getY()` qui permettent de lire respectivement l'abscisse et l'ordonnée d'un point.  
    Créer les **setters** `setX(self, valeur)` et `setY(self, valeur)` qui permettent de modifier respectivement les abscisses et l'ordonnée d'un point
    3. Créer la méthode magique `__repr__()` qui renvoie la repré́sentation mathématique d’un point dans le Terminal : `(x,y)`.
    4. Créer la méthode ```distance(self, p: `Point`) -> float``` qui renvoie la distance entre le point de l’objet courant (`self`) et l’objet `p` passé en paramètre.  
    <bd @gris>Rappel</bd> La distance entre deux points $A(x_A,y_A)$ et $B(x_B,y_B)$, en mathématiques, est égale à :  
    <center><enc>$AB = \sqrt {(x_B−x_A)^2+ (y_B−y_A)^2)}$</enc></center>
    5. Créer la méthode ```milieu(self, p: `Point`) -> `Point` ``` : qui permet de calculer les coordonnées du point `M`, milieu du segment défini par le point de l’objet courant `self` et l’objet Point `p` passé en paramètre. Cette méthode renvoie l'objet Point `M`.  
    Rappel : Les coordonnées du milieu $M(x_M,y_M)$ de $A(x_A,y_A)$ et $B(x_B,y_B)$, sont :  
    <center><enc>$x_M = \dfrac {x_A+x_B}{2} \quad \text{et} \quad y_M = \dfrac {y_A+y_B}{2}$</enc></center>  
    <bd @gris>Remarque</bd> La méthode doit renvoyer un objet `Point` et pas les coordonnées du milieu.  
    6. Créer la méthode ```sym_par_rapport_a(self,p: `Point`)->Point``` qui renvoie l'objet Point qui est le symétrique de l'objet `self` de l'instance courante de la classe Point, par rapport au point `p`.
    <bd @gris>Remarque</bd> La méthode doit renvoyer un objet `Point` et pas les coordonnées du symétrique.  

!!! ex "Classe Droite"
    On considère une classe `Droite` disposant des attributs suivants:  

    * `p1` : un attribut **public** de type Point  
    * `p2` : un attribut **public** de type Point  

    <clear></clear>

    1. Créer une méthode `coeff_dir()->float` ou `pente()->float`, qui renvoie en sortie un float : le coefficient directeur `a` de la droite :  
    Rappel : <center><enc>$a = \dfrac {y_B-y_A}{x_B-x_A}$</enc></center>
    2. Créer une méthode `parallele(self, d2:Droite)->bool` qui accepte en argument d'entrée deux droites `self` et `d2`, et renvoie en sortie si elles sont parallèles (`True`), ou pas (`False`)
    3. Créer une méthode `secantes(self, d2:Droite)->bool` qui accepte en argument d'entrée deux droites `self` et `d2`, et renvoie en sortie si elles sont sécantes (`True`), ou pas (`False`)
    4. La droite $(AB)$, passant par les points $A(x_A;y_A)$ et $B(x_B;y_B)$, créer les deux méthodes suivantes:  
        * une méthode `equation_reduite()->str` qui renvoie les deux coefficients `a` et `b` de l'équation réduite `y=ax+b`, donnés par les deux formules suivantes :  
        <center><enc>$a=\dfrac{y_B-y_A}{x_B-x_A} \quad et \quad b=\dfrac{x_By_A-x_Ay_B}{x_B-x_A}$</enc></center>
        * une méthode `affiche_equation_reduite()` qui renvoie une chaîne de caractères affichant l'équation réduite de la droite `y=ax+b` de l'instance courante.  
    5. Créer une méthode `passePar(p:Point)->` qui renvoie un booléen:  
        * `True` si l'instance courante de la droite passe par le point `p` ($\Leftrightarrow$ les coordonnées du point `p` vérifient l'équation de la droite)
        * `False` sinon  

!!! ex "Classe Triangle"
    On considére maintenant une classe appelée **Triangle** ayant les **attributs** suivants :  

    * `__p1` : un attribut **privé** de type Point  
    * `__p2` : un attribut **privé** de type Point  
    * `__p3` : un attribut **privé** de type Point  

    <clear></clear>

    1. Écrire les **getters** `getP1()`, `getP2()`, `getP3()`  
    Écrire les **setters** ```setP1(p : `Point`)```, ```setP2(p : `Point`)```, ```setP3(p : `Point`)```
    et un constructeur ```__init__()``` acceptant trois paramètres (et `self`)
    2. Écrire  une  méthode `estPlat(self) -> bool` qui :  
        * renvoie `True` si le triangle est plat (les trois points `p1`, `p2` et `p3` sont alignés)
        * et `False` sinon  

    3. Écrire une méthode `estIsocele(self) -> bool` qui :  
        * renvoie `True` le triangle est Isocèle,
        * `False` sinon.

        Rappel : un triangle $ABC$ est isocèle si $AB = AC$ ou $AB = BC$ ou $BC = AC$
        Remarque : On pourra utiliser une classe `Racine` déjà développée, ou commencer par la créer
    4. Écrire une méthode `estEquilateral(self)->bool` qui renvoie :  
        * `True` si le triangle est équilatéral
        * `False` sinon
    5. Écrire une méthode `estRectangle(self)->bool` qui renvoie :  
        * `True` si le triangle est un triangle Rectangle (Dans ce cas, on précisera en quel point)
        * `False` sinon
    6. Dans un fichier `main.py`, tester toutes les classes et méthodes que vous avez implémentées

!!! ex "Classe Rectangle"
    1. Écrire une classe `Rectangle` en langage Python, permettant de construire un rectangle dotée d'**attributs** `longueur` (par défaut `longeur=10`) et `largeur` (par défaut `largeur=5`).  
    On pourra utiliser un ***constructeur*** `__init__()`  
    2. Créer les **méthodes** suivantes:  

        * une **méthode** `Perimetre()` permettant de calculer le périmètre du rectangle, et  
        * une **méthode** `Aire()` permettant de calculer l'aire du rectangle  
        Rappel: <center><enc>$Aire_{Rectangle}=largeur\times longueur$</enc></center>  
    3. **a.** Créer les getters `getLongueur()` et `getLargeur()` qui permettent de lire (/qui renvoient) la `longueur` et la `largeur` depuis l'extérieur de la classe.  
      **b.** Créer les setters `setLongueur(L:float)` et `setLargeur(l:float)` qui permettent de modifier la `longueur` et la `largeur` depuis l'extérieur de la classe.  
    4. **a.** Créer une classe fille **Parallelogramme** héritant de la classe **Rectangle** telle que :  

        * la classe `Parallelogramme` soit dotée en plus d'un *attribut* **`hauteur`** (par défaut `hauteur=4`)  
        * et d'une autre *méthode* `Aire()` permettant de calculer le volume du Parallélogramme.  
        Rappel : <center><enc>$Aire_{Parallelogramme} = largeur\times hauteur$</enc></center>  

        **b.** Créer une classe fille **Carre** héritant de la classe **Rectangle** telle que :  

          * la classe `Carre` soit dotée en plus d'un *attribut* **`cote`** (par défaut `cote=5`)
          * et d'une autre *méthode* `Aire()` permettant de calculer le volume du Carré.  
          Rappel : <center><enc>$Aire_{Carré} = cote\times cote$</enc></center>  

          * le constructeur `__init__()` fixe par défaut la valeur des attributs :
              * longueur = cote
              * largeur = cote

!!! ex "Classe Cercle"
    1. Définir une classe `Cercle` permettant de créer un cercle $C(O,r)$ de **centre** `O(a,b)` et de **rayon** `r` à l'aide du constructeur `__init__()`
    2. Définir une méthode `Surface()` de la classe qui permet de calculer la surface du cercle
    3. Définir une méthode `Perimetre()` de la classe qui permet de calculer le périmètre du cercle
    4. Définir une méthode `testAppartenance()` de la classe qui permet de tester si un point `A(x,y)` appartient ou non au cercle `C(O,r)`

