from math import sqrt

class Point:
    def __init__(self, x:float=0, y:float=0):
        self.__x = x
        self.__y = y
    def getX(self):
        return self.__x
    def setX(self, valeur):
        self.__x = valeur
        return self.__x
    def getY(self):
        return self.__y
    def setY(self, valeur):
        self.__y = valeur
        return self.__y
    def distance(self,p)->float:
        xA = self.__x
        yA = self.__y
        xB = p.__x
        yB = p.__y
        return sqrt((xB-xA)**2+(yB-yA)**2)
    def milieu(self, p):
        return Point((self.__x+p.__x)/2,(self.__y+p.__y)/2)
    def sym_par_rapport_a(self, p):
        xB = 2*p.__x - self.__x
        yB = 2*p.__y - self.__y
        return Point(xB, yB)
    def __repr__(self):
        return f"({self.__x},{self.__y})"

class Droite:
    def __init__(self, p1 = Point(1,0), p2 = Point(2,0)):
        self.p1 = p1
        self.p2 = p2
    def coeff_dir(self):
        a = ( self.p2.getY() - self.p1.getY() )/( self.p2.getX() - self.p1.getX() )
        return a
    def parallele(self, d2):
        a_d1 = self.coeff_dir()
        a_d2 = d2.coeff_dir()
        if a_d1 == a_d2:
            return True
        else:
            return False
    def secantes(self, d2):
        a_d1 = self.coeff_dir()
        a_d2 = d2.coeff_dir()
        if a_d1 != a_d2:
            return True
        else:
            return False
    def equation_reduite(self)->tuple:
        xA = self.p1.getX()
        yA = self.p1.getY()
        xB = self.p2.getX()
        yB = self.p2.getY()
        a = self.coeff_dir()
        b = ( xB*yA - xA*yB ) /(xB-xA)
        return a,b
    def affiche_equation_reduite(self)->None:
        a,b = self.equation_reduite()
        print(f"y={a}x+{b}")
    def passePar(self, p)->bool:
        a,b = self.equation_reduite()
        xP = p.getX()
        yP = p.getY()
        # if yP == a*xP + b:
        #     return True
        # else:
        #     return False
        return yP == a*xP + b

if __name__ == "__main__":
    P1 = Point(1,1)
    P2 = Point(2,2)
    P3 = Point(3,4)
    P4 = Point(4,6)
    d = Droite(P1,P2)
    rep1 = d.passePar(P1)
    rep2 = d.passePar(P3)
    print("rep1=", rep1)
    print("rep2=", rep2)



