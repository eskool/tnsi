# TNSI : TD Classe Fraction

On se donne la classe `Frac` suivante (à compléter) :

```python
class Frac:
  def __init__(self, ...etc...):
    # ... à compléter ...
```

1. Compléter le constructeur `__init__()` de sorte qu'il reçoive en entrée deux arguments (en plus de `self`) : le numérateur `num` (par défaut `num=1`) et le dénominateur `denom` (par défaut `denom=1`)  
2. Créer une méthode magique `__repr__()` qui représente la fraction de l'instance courante `self` dans un Terminal, sous la forme `num/denom`
3. Créer une méthode `pgcd()` **récursive** qui renvoie le **pgcd** (plus grand commun diviseur) du numérateur `num` et du dénominateur `denom`  
    <bd @gris>Rappel</bd> Si $a$ et $b$ sont deux entiers tels que $a\gt b$. Alors :  

    * `pgcd(a,b) = pgcd(b, a-b)`
    * `pgcd(a,0) = a`

4. En déduire une méthode `simp()` qui simplifie la fraction de l'instance courante `self`
5. Créer une méthode `est_entier()` qui renvoie un booléen : 

    *  `True`, si la fraction de l'instance courante `self` *tombe juste* (c'est un nombre entier, par exemple $\dfrac 62=3$)
    *  `False`, sinon

6. Créer une méthode magique `__eq__()` qui teste l'égalité de deux fractions.  
    Cette méthode renvoie donc un booléen:

    * `True` lorsque les deux fractions sont égales
    * `False` sinon : lorsque les deux fractions NE sont PAS égales
    
    <bd @gris>Remarque</bd> Les deux syntaxes des méthodes magiques doivent donc fonctionner pour cette méthode précise, lorsque `fractionGauche` et `fractionDroite` sont deux instances de la classe `Fraction` :

    * ou bien `fractionGauche.__eq__(fractionDroite)`
    * ou bien `fractionGauche == fractionDroite`

7. Créer une méthode magique `__ge__()` qui teste si la fractionGauche est **supérieure ou égale** ( $\ge$ ) à fractionDroite.  
    Cette méthode renvoie donc un booléen:

    * `True` lorsque fractionGauche $\ge$ fractionDroite
    * `False` sinon

    <bd @gris>Remarque</bd> Les deux syntaxes des méthodes magiques doivent donc fonctionner pour cette méthode précise, lorsque `fractionGauche` et `fractionDroite` sont deux instances de la classe `Fraction` :

    * ou bien `fractionGauche.__ge__(fractionDroite)`
    * ou bien `fractionGauche >= fractionDroite`

8. Créer une méthode magique `__gt__()` qui teste si la fractionGauche est **strictement supérieure** ( $\gt$ ) à la fractionDroite.  
    Cette méthode renvoie donc un booléen:

    * `True` lorsque fractionGauche $\gt$ fractionDroite
    * `False` sinon

    <bd @gris>Remarque</bd> Les deux syntaxes des méthodes magiques doivent donc fonctionner pour cette méthode précise, lorsque `fractionGauche` et `fractionDroite` sont deux instances de la classe `Fraction` :

    * ou bien `fractionGauche.__gt__(fractionDroite)`
    * ou bien `fractionGauche > fractionDroite`

9. Créer une méthode magique `__le__()` qui teste si la fractionGauche est **inférieure ou égale** ( $\le$ ) à la fractionDroite.  
    Cette méthode renvoie donc un booléen:

    * `True` lorsque fractionGauche $\leq$ fractionDroite
    * `False` sinon

    <bd @gris>Remarque</bd> Les deux syntaxes des méthodes magiques doivent donc fonctionner pour cette méthode précise, lorsque `fractionGauche` et `fractionDroite` sont deux instances de la classe `Fraction` :

    * ou bien `fractionGauche.__le__(fractionDroite)`
    * ou bien `fractionGauche <= fractionDroite`

10. Créer une méthode magique `__lt__()` qui teste si la fractionGauche est **strictement inférieure** ( $\le$ ) à la fractionDroite.  
    Cette méthode renvoie donc un booléen:

    * `True` lorsque fractionGauche $\lt$ fractionDroite
    * `False` sinon

    <bd @gris>Remarque</bd> Les deux syntaxes des méthodes magiques doivent donc fonctionner pour cette méthode précise, lorsque `fractionGauche` et `fractionDroite` sont deux instances de la classe `Fraction` :

    * ou bien `fractionGauche.__lt__(fractionDroite)`
    * ou bien `fractionGauche < fractionDroite`

11. Créer une méthode magique `__add__()` qui ajoute deux fractions. **Cette méthode magique renvoie donc un object, préalablement simplifié, de classe Fraction**
12. Créer une méthode magique `__sub__()` qui soustrait deux fractions. **Cette méthode magique renvoie donc un object, préalablement simplifié, de classe Fraction**.
13. Créer une méthode magique `__mul__()` qui multiplie deux fractions. **Cette méthode magique renvoie donc un object, préalablement simplifié, de classe Fraction**.
14. Créer une méthode magique `__truediv__()` qui divise deux fractions. **Cette méthode magique renvoie donc un object, préalablement simplifié, de classe Fraction**.

