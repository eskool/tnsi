class Frac:
    def __init__(self, num, denom):
        self.num = num
        self.denom = denom
    def pgcd(self)->int:
        a = self.num
        b = self.denom
        # print("a=",a)
        # print("b=",b)
        a=abs(a)
        b=abs(b)
        if a<b:
            a,b = b,a
        # ici, forcément :  a>=b
        if b == 0:
            return a
        else:
            return Frac(b,a-b).pgcd()
    def simp(self):
        d = self.pgcd()
        self = Frac(int(self.num/d),int(self.denom/d))
        return self
    def est_entier(self)->bool:
        return self.simp().denom == 1

    def __eq__(self, f2)->bool:
        return self.num * f2.denom == self.denom * f2.num

    def __ge__(self, f2)->bool:
        return self.num / self.denom >= f2.num / f2.denom

    def __gt__(self, f2)->bool:
        return self.num / self.denom > f2.num / f2.denom

    def __le__(self, f2)->bool:
        return self.num / self.denom <= f2.num / f2.denom

    def __lt__(self, f2)->bool:
        return self.num / self.denom < f2.num / f2.denom

    def __add__(self, f2):
        return Frac(self.num*f2.denom+f2.num*self.denom,self.denom*f2.denom).simp()

    def __sub__(self, f2):
        return Frac(self.num*f2.denom-f2.num*self.denom,self.denom*f2.denom).simp()

    def __repr__(self):
        self = self.simp()
        if self.simp().denom == 1:
            return f"{self.simp().num}"
        else:
            return f"{self.num}/{self.denom}"

if __name__ == "__main__":
    f23 = Frac(2,3)
    f79 = Frac(7,9)
    print(f23.__sub__(f79))
    print(f23 - f79)
    if f23 + f79 > Frac(4,5):
        print("GAGNE")
    else:
        print("PERDU")