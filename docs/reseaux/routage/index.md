# TNSI : Routeurs & Routage

## Routeurs / Relais

### Qu'est-ce qu'un Routeur / Relais ?

!!! def "Routeurs"
    Un <bred>Routeur</bred> / <bred>Relais</bred> :fr: / <bred>Router</bred> / <bred>Relay</bred> :gb: est un matériel réseau (une sorte d'ordinateur), qui a vocation a **relayer** / **router** les paquets d'information reçus en entrée, vers leur prochaine destination (le prochain routeur), voire leur destination finale (lorsqu'ils sont arrivés). 
    Un routeur dispose de (au moins) $2$ <bred>ports</bred> / <bred>Interfaces</bred> (réseau) / <bred>NIC - Network Interface Card</bred> / <bred>Cartes Réseau</bred> :

    * Chaque interface réseau (NIC) d'un routeur dispose d'une adresse IP distincte
    * Chaque interface réseau (NIC) d'un routeur définit un réseau différent

### A quoi sert un Routeur / Relais ?

!!! pte "A quoi sert un Routeur ?"
    * Un routeur sert d'**intermédiaire** dans la transmission d'un message.  
    * Plus précisément, son rôle est de relayer / router des informations entre des **hôtes** distincts **situés dans des réseaux différents** (càd dans un inter-réseau). 
    * Chaque routeur reçoit des données et c'est lui qui décide/calcule à qui les transmettre, en déterminant la meilleure route, grâce à des **protocoles/algorithmes de routage** qu'il contient.
    * Lorsqu'il joue le rôle de passerelle (c'est le cas usuellement au sens réseau), un routeur détermine les limites d'un réseau.

## Qu'est-ce que le Routage?

!!! def "Routage"
    Le <bred>Routage</bred> :fr: / <bred>Routing</bred> :gb: est le mécanisme général qui implémente la communication (par commutation de paquets) dans un inter-réseau, entre un **hôte source** (émetteur, dans un réseau) et un (ou plusieurs) **hôtes de destination** (récepteur, dans un autre réseau). Le routage est basé sur:

    * un ensemble de **matériels** : les routeurs, qui correspondent aux noeuds/nodes de l'inter-réseau. (Les commutateurs/switchs sont utilisés dans un même réseau local). 
    * un ensemble de **logiciels** : les protocoles/algorithmes (de routage), contenus dans chaque routeur

C'est cet ensemble **matériel** et **logiciel** qui permet d'acheminer / relayer / router les paquets de données depuis l'hôte source (expéditeur) vers le ou les hôtes de destination.

<center>
<img src="./../img/commutation-paquets.gif">
:sk-copyright: CC-BY-SA 3.0 [Wikipedia](https://commons.wikimedia.org/wiki/File:Packet_Switching.gif)
</center>

## Exemples de Routeurs

### Votre Box : un routeur domestique

Votre box domestique est également un routeur qui possède plusieurs interfaces réseau :

* une interface est connectée au réseau de votre opérateur / Fournisseur d'Accès (FAI) (ici WAN - Wide Area Network: en rouge)
* une ou plusieurs interfaces filaires (ethernet) connectées à votre réseau local
* une interface Wifi

!!! col __40 center
    ![Bbox Fibre](./../img/bbox-fibre.png)
    <figcaption>
    Bbox Fibre
    </figcaption>

!!! col __60 center clear
    ![Bbox Fibre Arrière](./../img/bbox-fibre-arriere.png)
    <figcaption>
    Bbox Fibre - Arrière
    </figcaption>

En fait, une box familiale joue plusieurs rôles:

* Le rôle de **routeur** pour déterminer les meilleurs routes par lesquelles acheminer/router les paquets de données
* Le rôle de **passerelle** / **gateway** pour sortir du réseau local (ici : l'interface filaire ethernet rouge)
* le rôle de **commutateur/switch** pour permettre la communication entre eux des hôtes d'un même réseau (ici: les interfaces filaires ethernet jaunes, et l'interface wifi)

### Routeurs Professionels

Un routeur sur internet est un peu plus sophistiqué, possède souvent plus de ports et ressemble d'extérieur à un switch. Il dispose d'un logiciel interne bien plus sophistiqué afin de lui permettre de communiquer avec ses routeurs voisins pour l'aider à déterminer les meilleures routes à emprunter pour acheminer ses paquets.

!!! col _2 center
    ![Routeur Cisco](./../img/routeur-cisco.png)
    <figcaption>Routeur Cisco - WS-C2960X-48FPD-L - Catalyst 2960-X</figcaption>

!!! col _2 center clear
    ![Routeur Cisco](./../img/routeur-cisco-arriere.png)
    <figcaption>Routeur Cisco - Arrière - WS-C2960X-48FPD-L - Catalyst 2960-X</figcaption>

## Rôle d'un Routeur

Essentiellement, le rôle d'un routeur est de:

### Relayer / Router les paquets de données

Chaque routeur reçoit des données en entrée, sous forme de paquets, et doit décider à qui les <bred>relayer</bred> / rediriger / <bred>router</bred> :

* vers le **prochain routeur** / <bred>passerelle</bred> / <bred>gateway</bred>, également surnommé le <bred>saut suivant</bred> :fr: / <bred>next hop</bred> :gb:. En pratique, de manière équivalente, le routeur doit déterminer vers laquelle de ses propres <bred>interfaces de sortie</bred> / <bred>ports</bred> il doit relayer / router / rediriger le paquet.  
Pour prendre une telle décision, le routeur dispose de :
    * **tables de routage** qui sont des tableaux d'informations stockés sur chaque routeur, composés de plusieurs lignes / <bred>entrées</bred>, comprenant classiquement, a minima:
        * les adresses IP des réseaux connus (les destinations possibles),
        * les adresses IP des passerelles / gateways correspondant au saut suivant (instructions pour rejoindre ces destinations)
        * le Port / Interface de sortie (qui mène vers le saut suivant)
        * quelquefois d'autres indications (variables selon les protocoles implémentés par le routeur: distance des prochains sauts, coût de la route, etc.)
    * des **protocoles de routage**, qui sont des algorithmes d'aide à la décision : en charge de déterminer le saut suivant sur chaque routeur
* vers la **machine de destination**, si le paquet est arrivé au routeur final, directement connecté réseau de destination

### Maintenir à jour ses Tables de Routage

Les réseaux sont soumis à des évolutions constantes, prévues ou pas, souhaitées ou pas, ce qui oblige la mise à jour/le maintien réguliers des tables de routage dans les routeurs:

* ajout de nouveaux matériels
* suppression de matériels
* remplacement de matériel (vieillissant, défectueux, etc.)
* pannes imprévues
* etc.

Plusieurs méthodes pour maintenir à jour les tables de routage existent, et dépendent des protocoles de routage choisis. On distignue deux grandes familles de protocoles de routage:

* **statiques** ou **centralisés** (par un humain): on parle dans ce cas de (protocoles de) <bred>routage statique</bred>
* **dynamiques** (par un programme): on parle dans ce cas de (protocoles de) <bred>routage dynamique</bred>

## Routeurs et Modèle OSI

### Modèle OSI vs Modèle TCP/IP

La communication entre deux hôtes, qu'elle se fasse par connexion directe, ou bien à distance via un réseau, est soumise aux modèles de communications par couches. Il en existe principalement deux :

* Le <bred>modèle OSI - Open Systems Interconnection</bred>, est une **norme ISO - International Standards Organisation** ou Organisation Internationale de Normalisation. C'est un **modèle Académique et Abstrait** (sans implémentation pratique), mais qui admet néanmoins une utilité réelle. **Le modèle OSI contient $7$ couches**.
* Le <bred>modèle TCP/IP</bred> est une **implémentation pratique**, et un standard qui s'est imposé de fait. c'est ce modèle qui est utilisé en pratique. **Le modèle TCP/IP contient $4$ couches**.

<center>
<img src="./../img/OSI-vs-TCP-IP.svg">
<figcaption>Modèle OSI vs Modèle TCP/IP
</figcaption>
</center>

<env>Aller Plus Loin</env> : Relire le [Cours de 1ère NSI - Réseaux](https://eskool.gitlab.io/1nsi/reseaux/)

### Communication par Couches, via un inter-Réseau

!!! pte "Routeurs & Modèle OSI"
    Les <bred>routeurs</bred> sont des matériels de **niveau 3 du Modèle OSI**.  
    En particulier:  

    * Les routeurs manipulent uniquement des identifiants de la couche $3$ (des adresses IP),
    * Les routeurs ne manipulent jamais aucun identifiant de la couche $2$ (PAS d'adresse MAC)

!!! info "Switchs & Modèle OSI"
    Les <bred>switchs</bred>, par contre, sont des matériels de niveau $2$ du Modèle OSI (à l'exception notable des **switchs** dits **administrables** qui, par défnition, sont de niveau 3). Les switchs sont donc utilisés dans le cadre d'un même réseau local (LAN).

<center>
<img src="./../img/osi-switch-routeur.svg" style="width:80%;">
<figcaption>
SW1=Switch Numéro 1, SW2 = Switch Numéro 2<br/>
R1 = Routeur Numéro 1
</figcaption>
</center>

### Traversée des Couches OSI dans un Routeur

!!! mth "Traversée des couches d'un Routeur"

    * Une trame arrive sur la Couche $1$ du routeur
    * elle est traduite logiquement sous la forme d'une trame de Couche $2$ :

    | MAC<br/>dest | MAC<br/>source | . | IP<br/>source | IP<br/>dest | Données | . |
    |:--- |:---|:---|:---:|:---|:---|:---:|

    * Le routeur lit l'adresse MAC du destinataire : 

        * Si ce n'est pas la sienne, il rejette la trame,
        * sinon, la trame lui est destinée, donc il envoie le paquet IP à sa couche $3$. Le protocole IP lit alors l'adresse IP du destinataire :
            * Si elle est dans son réseau local, il l'envoie à la machine concernée,
            * sinon, il regarde dans sa table de routage à  quel nouveau routeur il va l'adresser (passerelle - *gateway* en anglais).

## Notes et Références

[^1]: [Zeste de Savoir, Les réseaux de Zéro, Dans les basses couches du modèle OSI: la couche 3 - Le Réseau](https://zestedesavoir.com/tutoriels/2789/les-reseaux-de-zero/dans-les-basses-couches-du-modele-osi/la-couche-3-le-reseau/)

