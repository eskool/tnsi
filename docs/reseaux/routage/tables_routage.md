# TNSI : Tables de Routage

En pratique, pour déterminer le prochain routeur / saut suivant / next hop, et donc pour déterminer la passerelle de sortie / gateway, chaque routeur dispose de 

* Tables de Routage
* Protocoles/Algorithmes de Routage

## Tables de routage

### Définition

!!! def "Tables de Routage"
    Les <bred>Tables de Routage</bred> :fr: /<bred> Routing Tables</bred> :gb: sont des tableaux d'informations stockés sur chaque routeur, composés de plusieurs lignes / <bred>entrées</bred>, comprenant classiquement, a minima:

    * les adresses IP des routes / réseaux connus, càd les <bred>Destinations</bred> possibles:
        * ou bien NON directement connectés : on ajoute alors les adresses IP des <bred>passerelles</bred> / <bred>gateways</bred> / sauts suivants (comprendre les instructions/chemins intermédiaires) pour rejoindre ces destinations finales
        * ou bien directement connectés, notés par convention :
        ```console linenums="0"
        Passerelle / Gateway
        0.0.0.0
        ```
    * le Port / Interface de sortie (et/ou leurs adresses IP) du routeur à emprunter (qui mène vers le saut suivant)
    * quelquefois d'autres indications (qui sont variables selon les protocoles implémentés par le routeur: distance, délai, coût de la route, qui mesurent l'*efficacité* de la route, etc..)
    * Enfin, (en général) Une **route par défaut**, notée par convention :
    ```console linenums="0"
    Destination
    0.0.0.0/0
    ```

!!! info "Destination = Réseau de Destination"
    En raison de certaines limitations des routeurs (en mémoire et/ou en ressources), la **destination** désigne le **réseau de destination**, et non pas/plus l'hôte de destination.

!!! info "Efficatité ? Coût?"
    L'**efficacité/coût d'une route**, peut modéliser/mesurer plusieurs concepts, qui sont variables, selon le protocole de routage utilisé:
    
    * Distance (nombre de sauts)
    * Délai
    * Saturation/Congestion, etc..
    
    Cette efficacité sera exploitée et détaillée par la suite, dans les algorithmes de routage (RIP et OSPF) pour déterminer les meilleures routes en tenant compte de ces coûts.

### Réseaux Directement connectés = Passerelle `0.0.0.0`

Remarquer que d'une manière générale, au sens IPv4, l'adresse `0.0.0.0` désigne une **adresse invalide, inconnue ou une cible non-applicable**. Néanmoins,

!!! def "Passerelle/Gateway `0.0.0.0` en IPv4 (ou `::` en IPv6) [^1]"
    Dans le contexte plus précis des tables de routage, par convention [^1]:  
    Une **Passerelle/Gateway** `0.0.0.0` signifie que la passerelle pour joindre le réseau de destination n'est pas spécifiée/définie. Cela signifie usuellement que **le réseau de destination est directement connecté au routeur** (donc aucun besoin de définir le saut suivant). 

### Exemples de Tables de Routage

<env>Classless / VLSM (Notation CIDR)</env>
```console linenums="0"
Destination        Passerelle (Gateway)  Port/Interface   Cost
192.168.0.0/25     192.168.0.1           v10              1
192.168.1.0/25     192.168.1.1           v11              2
192.168.2.0/25     0.0.0.0               v12              2
0.0.0.0/0          192.168.1.3           v11              1
100.10.42.0/25     192.168.1.2           v10              2
```
On pourrait se demander quel est l'intérêt de préciser encore une entrée/ligne après avoir donné la route par défaut? La réponse est que cette dernière entrée est d'une plus grande spécificité : l'algorithme de routage prend sa décision après avoir lu TOUTE la table de routage.  
<env>Masques de sous-réseau</env>
```console linenums="0"
Destination        Masque Sous-Réseau    Passerelle (Gateway)  Port/Interface   Cost
192.168.0.0        255.255.255.128       192.168.0.1           v10              1
192.168.1.0        255.255.255.128       192.168.1.1           v11              2
192.168.2.0        255.255.255.128       0.0.0.0               v12              2
0.0.0.0            0.0.0.0               192.168.1.3           v11              1
100.10.42.0        255.255.255.128       192.168.1.2           v10              2
```

## Routage Statique vs Routage Dynamique

Les tables de routage peuvent être construites et maintenues :

* soit **statiquement**, par un humain : on parle de (protocoles de) <bred>routage statique</bred>
* soit **dynamiquement**, par un programme/algorithme/protocole de routage: on parle de (protocoles de) <bred>routage dynamique</bred>

!!! info
    Certains routeurs (e.g. Cisco, Oracle Solaris [^2], etc..) autorisent la configuration des deux routages (statique et dynamique) simultanément dans une même table de routage.

## Route / Passerelle par défaut

### Définition

!!! def "Route / Passerelle par défaut"
    * Pour résoudre le cas où aucune entrée de la table de routage ne correspond explicitement au réseau de destination demandé, on définit une <bred>route par défaut</bred> :fr: / <bred>default route</bred> :gb:.  
    Les paquets dont le réseau de destination n'est pas connu seront alors renvoyés vers la <bred>passerelle par défaut</bred> / <bred>passerelle de dernier recours</bred> :fr: / <bred>gateway of last resort</bred> :gb:, qui est la passerelle définie pour l'entrée correspondante de la route par défaut.
    * Lorsqu'aucune route par défaut n'a été définie, et que le réseau destination n'est pas explicitement défini dans la table de routage, alors le routeur éliminera le paquet en question.

### Notation `0.0.0.0/0`

Remarquer que d'une manière générale, au sens IPv4, l'adresse `0.0.0.0` désigne une **adresse invalide, inconnue ou une cible non-applicable**. Néanmoins,

!!! note "Notation `0.0.0.0/0` en IPv4 (ou `::/0` en IPv6) [^1]"
    Dans le contexte plus précis des tables de routages, par convention [^1]:

    Un réseau de **Destination** `0.0.0.0/0` en IPv4, ce qui est équivalent à IP `0.0.0.0` et masque de sous-réseau `0.0.0.0` (ou bien `::/0` en IPv6) désigne la **passerelle/route par défaut** : **TOUT sous-réseau** n'apparaissant pas explicitement dans la table de routage suivra ces indications.
    
## Utilisation d'une Table de Routage

!!! mth "Utilisation d'une Table de Routage"
    Le routeur décide de quelle route emprunter en suivant la procédure suivante, qui dépend du nombre d'entrées existant dans la table de routage pour le réseau de destination demandé:

    * Si une seule route existe dans la table de routage, alors elle est utilisée
    * Lorsque plusieurs routes existent dans la table de routage, la route **la plus spécifique** sera utilisée, c'est-à-dire **celle qui aura le préfixe CIDR le plus long**.
    * En cas d'égalité (càd que plusieurs routes avec le même préfixe CIDR existent), le routeur peut utiliser une métrique appelée **Distance Administrative (AD)** pour (tenter de) les départager, qui dépend du type de route/protocole :
        * les routes **directement connectées** sont prioritaires par rapport aux autres (cf Distance Administrative), 
        * les routes **statiques** et **dynamiques** sont aussi départagées par une **Distance Administrative**
        * Si tous les paramètres sont égaux, le routeur pourra au choix:
            * **distribuer le trafic** entre toutes ces routes, ou bien, 
            * n'en utiliser qu'une seule
    * Si aucune route n'existe explicitement dans la table de routage pour le réseau de destination, MAIS qu'il existe une route par défaut, alors le routeur utilise cette route par défaut
    * Si aucune route par défaut n'est configurée dans la table, ET qu'aucune route explicite n'existe pour le réseau de destination demandé, alors le routeur éliminera le paquet dont la destination n'est pas connue. 

```console linenums="0"
Total number of IP routes: 687
Destination     NetMask           Gateway           Port    Cost
137.194.2.0     255.255.254.0     137.194.4.254     v10     2
137.194.4.0     255.255.255.248   137.194.4.253     v10     2
137.194.4.8     255.255.255.248   137.194.4.251     v10     11
137.194.4.192   255.255.255.192   0.0.0.0           v10     1
137.194.6.0     255.255.254.0     137.194.4.254     v10     2
137.194.8.0     255.255.248.0     137.194.4.251     v10     20
137.194.16.0    255.255.255.128   137.194.160.230   v160    11
137.194.16.128  255.255.255.128   137.194.192.102   v192    11
137.194.16.144  255.255.255.240   137.194.192.102   v192    11
137.194.16.176  255.255.255.240   137.194.192.103   v192    20
137.194.17.0    255.255.255.0     137.194.192.103   v192    2
```

Le routeur sait ainsi que pour atteindre la machine `137.194.2.21`, il doit s'adresser au réseau `137.194.2.0/23` en relayant/redirigeant le paquet au routeur `137.194.4.254` qui est joignable sur l'interface `v10`.

## Distance Administrative

Dans la vraie vie, un routeur peut disposer simultanément de plusieurs protocoles de routage distincts. Dans le cas où un routeur dispose d'informations de routage vers la même destination pour plusieurs protocoles, la première chose qu'il doit faire c'est déterminer quel est le **protocole le plus fiable** parmi tous ceux à sa disposition.   
Pour cela, il utilise une métrique appelée **Distance Administrative** (AD) censée mesurer la fiabilité de chaque protocole. Chaque protocole dispose de **Distances Administratives par défaut**, mais ces valeurs sont paramétrables (donc personnalisables). En outre, cette valeur n'a de signification que locale (elle n'est pas annoncée dans les mises à jour de routage).

!!! def "Distance Administrative"
    * La <bred>Distance Administrative (AD)</bred>  est une métrique qui mesure la **fiabilité de chaque protocole**.
    * Chaque protocole admet une <bred>Distance Administrative par défaut</bred>

Pour information, voici quelques valeurs usuelles de distances administratives par défaut, par protocole (supportés par les routeurs Cisco [^3]):

<center>

| Protocole/Route | Valeurs de <br/>Distance Administrative<br/>par défaut |
|:-:|:-:|
| Interface Connectée | 0 |
| Route Statique | 1 |
| BGP externe | 20 |
| IGRP | 100 |
| OSPF | 110 |
| RIP | 120 |
| BGP interne | 200 |
| Inconnu | 255 |

</center>

!!! pte
    Plus la Distance Administrative est petite, et plus le protocole/la route est fiable, donc mieux c'est.

!!! exp "OSPF vs RIP"
    On voit que le protocole OSPF est préféré au protocole RIP (dans les routeurs Cisco)

## Références & Notes

### Références

* [Implémentation des Tables de Routage, sur CISCO IOS, Goffinet](https://cisco.goffinet.org/ccna/routage/table-routage-cisco-ios/)

### Notes

[^1]: [Page '0.0.0.0' sur Wikipedia](https://en.wikipedia.org/wiki/0.0.0.0)
[^2]: [Docs Oracle Solaris](https://docs.oracle.com/cd/E19957-01/820-2982/gdyen/index.html)
[^3]: [Distances Administratives, Cisco](https://www.cisco.com/c/fr_ca/support/docs/ip/border-gateway-protocol-bgp/15986-admin-distance.pdf)
