# TNSI : Passerelles / Gateway

## Qu'est-ce qu'une Passerelle/Gateway?

!!! def "Passerelle /Gateway"
    Une <bred>Passerelle</bred> :fr: / <bred>Gateway</bred> :gb: est un composant réseau, matériel ou virtuel, disposant de plusieurs interfaces réseau (NIC) et qui:

    * permet de relier des réseaux de types différents (e.g. un réseau local LAN et le réseau internet)
    * définit les limites d'un réseau, comme :
        * <bred>Point d'accès</bred> (entrée) du réseau
        * <bred>Point d'extrémité</bred> (sortie) du réseau
    * permet la **conversion de protocoles** (contrairement aux routeurs)

## À quoi sert une Passerelle?

!!! pte "A quoi sert une Passerelle ?"
    En plus des utilisations de la définition ci-dessus, une passerelle peut être utilisée (souvent dans le monde professionel) en tant que :

    * Parefeu
    * Proxy
    * Qualité de Service (QoS) : Contrôle de Trafic et Performance
    * etc..

## Passerelles vs Routeurs

### Ressemblances

Les deux types de périphériques sont utilisés pour réguler le trafic réseau entre plusieurs réseaux distincts. La ligne qui les distingue est mince, d'ailleurs les deux matériels disposent de certaines fonctionnalités communes, et sont souvent confondus.

De plus, en pratique, au sens réseau, il arrive fréquemment que les routeurs et les passerelles se trouvent sur la même machine:

* Les routeurs sont aussi, fréquemment mais pas toujours, des passerelles
* Une passerelle est toujours obligatoirement un routeur

C'est pourquoi, on retient souvent la simplification suivante (malgré les différences décrites dans le § ci-après):

!!! warning "Au sens Réseau : Passerelle = Routeur"
    Au sens *réseau* du terme (par opposition au sens Télécom): **une passerelle est un routeur.**  

### Différences

Les routeurs et les passerelles sont cependant très différents dans leur principe de fonctionnement [^1] [^2]:

| Différences | Routeur | Passerelle |
|:-:|:-:|:-:|
| Fonction<br/>Principale| S'assurer que les paquets<br/> de données sont envoyés à<br/>la bonne adresse par la meilleure route. | Convertir un protocole en un autre<br/> pour la communication<br/> entre deux réseaux |
| Réseaux | Il achemine les paquets de données <br/>via des réseaux similaires. | Il relie deux réseaux différents. |
| Prise en charge du<br/> Routage Dynamique | Oui | Non |
| Couche OSI | Couches $3$ (et $4$) | Couche $5$ |
| Fonctions<br/> supplémentaires | Réseau sans fil, Routage Statique, NAT,<br/> serveur DHCP, etc. | Contrôle d'accès au réseau (Sécurité),<br/> IPSec, VPN, AntiDDoS, Parefeu,<br/>URL Filtering<br/>Conversion de protocole, NAT, etc. |
| Hébergé sur | Appareil dédié (matériel de routeur) | Appareil dédié / virtuel<br/>ou serveur physique |

### Conclusion

Puisque les passerelles et les routeurs ont des utilités différentes, les deux sont importants pour les réseaux. Une passerelle sert de Point d'accès (/extrémité) unique entre des réseaux ayant des protocoles différents, elle délimite le réseau, tandis que les routeurs sont en charge de déterminer les meilleures routes pour les paquets de données de l'hôte/réseau source à l'hôte/réseau destination. [^3]

Néanmoins, au sens réseau, les deux se retrouvent fréquemment sur la même machine, c'est pourquoi on retient la simplification suivante:

!!! warning "Au sens Réseau : Passerelle = Routeur"
    Au sens réseau du terme (par opposition au sens Télécom): **une passerelle est un routeur.**  
    (ce que nous supposerons toujours par la suite, malgré les différences décrites dans le § ci-dessus)

## Notes Références

[^1]: [Geeksforgeeks.com, Router vs Gateway](https://www.geeksforgeeks.org/difference-between-router-and-gateway/)
[^2]: [community.fs.com, router-vs-gateway](https://community.fs.com/blog/router-vs-gateway-what-is-the-similarity-and-difference.html)
[^3]: [Javatpoint, router-vs-gateway](https://www.javatpoint.com/router-vs-gateway)
