# TNSI : Corrigé Métropole 2021, Sujet 1

(Cryptographie)

<env>Exercice du Sujet</env> Partie C de l'Exercice 2

1. 
    a. `01100011 = 63` donc `c`  
    `01000110 = 46` donc `F`  
    b. `c = 0b 1000 1101 1011 0110`  
2.  
    a.  

    <center>

    | a | b | a XOR b | (a XOR b) XOR b |
    |:-:|:-:|:-:|:-:|
    | $0$ | $0$ | $0$ | $0$ |
    | $0$ | $1$ | $1$ | $0$ |
    | $1$ | $0$ | $1$ | $1$ |
    | $1$ | $1$ | $0$ | $1$ |

    </center>

    b. Il doit faire `c XOR k` car il va alors retrouver `m`

