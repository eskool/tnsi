# TNSI - Annales Cryptographie


## Métropole 2021, Sujet 1

<env>Exercice du Sujet</env>Partie C de l'Exercice 2

Dans cette partie, pour une meilleure lisibilité, des espaces sont placées dans les écritures
binaires des nombres. Il ne faut pas les prendre en compte dans les calculs.
Pour chiffrer un message, une méthode, dite du masque jetable, consiste à le combiner avec une
chaîne de caractères de longueur comparable.
Une implémentation possible utilise l’opérateur XOR (ou exclusif) dont voici la table de vérité  :

<center>

| a | b | a XOR b |
|:-:|:-:|:-:|
| $0$ | $0$ | $0$ |
| $0$ | $1$ | $1$ |
| $1$ | $0$ | $1$ |
| $1$ | $1$ | $0$ |

</center>

Dans la suite, les nombres écrits en binaire seront précédés du préfixe `0b`.

1. Pour chiffrer un message, on convertit chacun de ses caractères en binaire (à l’aide du format Unicode), et on réalise l’opération `XOR` bit à bit avec la clé.
Après conversion en binaire, et avant que l’opération `XOR` bit à bit avec la clé n’ait été effectuée, Alice obtient le message suivant :  
    
    <center>
    
    `m = 0b 0110 0011 0100 0110`
    
    </center>

    a. Le message `m` correspond à deux caractères codés chacun sur $8$ bits : déterminer quels sont ces caractères. On fournit pour cela la table ci-dessous qui associe à l’écriture hexadécimale d’un octet le caractère correspondant (figure ci-dessous). 

    Exemple de lecture : le caractère correspondant à l’octet codé `4A` en hexadécimal est la lettre `J`.

    ![UTF8 en Hexa](./img/utf8-hexa.png)

    b. Pour chiffrer le message d’Alice, on réalise l’opération XOR bit à bit avec la clé suivante :  

    <center>

    `k = 0b 1110 1110 1111 0000`
    
    </center>

    Donner l’écriture binaire du message obtenu.  
2.
    a. Dresser la table de vérité de l’expression booléenne suivante :
    `(a XOR b) XOR b`  
    b. Bob connaît la chaîne de caractères utilisée par Alice pour chiffrer le message. Quelle opération doit-il réaliser pour déchiffrer son message ?