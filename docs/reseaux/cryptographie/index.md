# TNSI : Cryptographie - Sécurisation des Communications

<center>

| Contenus | Capacités Attendues | Commentaires |
|:-:|:-:|:-:|
| Sécurisation<br/>des Communications | Décrire les principes de<br/>chiffrement symétrique (clef partagée) et<br/> asymétrique (avec clef privée/clef publique).<br/>Décrire l’échange d’une clef symétrique<br/>en utilisant un protocole asymétrique<br/>pour sécuriser une communication HTTPS. | Les protocoles symétriques et<br/>asymétriques peuvent être<br/>illustrés en mode débranché,<br/>éventuellement avec<br/>description d’un chiffrement<br/>particulier.<br/>La négociation de la méthode<br/>de chiffrement du protocole SSL<br/>(Secure Sockets Layer) n’est<br/>pas abordée. |

</center>

## Introduction

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/7W7WPMX7arI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Chiffrement Symétrique, Chiffrement Asymétrique, 
Attaque de l'Homme du milieu, & Protocole HTTPS

</center>

## Chiffrement Symétrique

### Activité du masque jetable

!!! ex
    === "Énoncé"
        On considère la variable suivante :

        `python
        masque = "CETTEPHRASEESTVRAIMENTTRESTRESLONGUEMAISCESTFAITEXPRES"
        `

        * Créer une fonction `chiffre(message, masque)` qui chiffre `message ` en le XORant avec `masque`.
        * Cette fonction doit pouvoir **aussi** servir à déchiffrer le message chiffré.

    === "Aide"
        * Le `XOR` (voir [ici](https://glassus.github.io/premiere_nsi/T2_Representation_des_donnees/2.5_Booleens/cours/#disjonction-exclusive-xor){target = "_blank"}) est une opération symétrique :  
        ```python
        >>> 34 ^ 23
        53
        >>> 53 ^ 23
        34
        ```
        * La fonction `ord` permet de renvoyer le code ASCII d'un caractère. La fonction `chr` fait l'opération inverse.  
        ```python
        >>> ord('A')
        65
        >>> chr(65)
        'A'
        ```

    === "Correction"
        TO DO

### Principe du Chiffrement Symétrique

!!! info
    Dans un chiffrement symétrique, c'est **la même clé** qui va servir au chiffrement et au déchiffrement.

![image](./img/sym.png){ .center }


#### Qu'appelle-t-on une Clé ?

La clé est un renseignement permettant simultanément de **chiffrer** et de **déchiffrer** un message. Cela peut être :

* un **nombre** (dans un simple décalage des lettres de l'alphabet, comme [le chiffre de César](https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage))
* une **phrase** (dans la méthode du [masque jetable](https://fr.wikipedia.org/wiki/Masque_jetable))
* une **image** (imaginez un chiffrement où on effectue un XOR par les pixels d'une image, comme dans [cette énigme](https://github.com/glassus/nsi/blob/master/Premiere/DM/DM1/enonce.md))

#### Chiffrement Symétrique

!!! def
    Un chiffrement est dit <rb>Symétrique</rb> lorsque l'on utilise la **même clé partagée** pour **chiffrer** et **déchiffrer**.
 
<env>Exemple</env>  
Alice chiffre son message en décalant les lettres de $3$ rangs vers la droite dans l'alphabet, Bob saura qu'il doit les décaler de $3$ rangs vers la gauche pour retrouver le message initial.

### Avantages d'un Chiffrement Symétrique ?

Souvent, les chiffrements Symétriques :

* sont rapides,
* consomment peu de ressources, et donc 
* sont adaptés au chiffrement de flux important d'informations (donc à des applications concrètes de la vraie vie)

Comme nous le verrons, la sécurisation des données transitant par le protocole `https` est basée (dans une deuxième partie) sur un chiffrement symétrique.

### Inconvénients d'un Chiffrement Symétrique ?

Le gros point faible : c'est l'**échange de la clé partagée** et donc l'**initialisation de la communication** ! En effet : Si Alice et Bob ont besoin d'utiliser un chiffrement pour se parler, comment peuvent-ils échanger leur clé partagée puisque leur canal de transmission n'est pas sûr ?

Le chiffrement symétrique impose en effet qu'Alice et Bob :

* aient pu se rencontrer **physiquement** au préalable pour convenir d'une clé secrète, ou bien 
* qu'ils aient déjà réussi à **établir une connexion sécurisée au préalable** (et indépendamment) pour ensuite s'échanger cette clé symétrique partagée sur un canal déjà sécurisé (ce sera la fonction du chiffrement asymétrique, et de la première partie du chiffrement SSL/TLS, qui est utilisé pour HTTPS). **Le chiffrement symétrique ne permet donc pas d'initialiser une communication entre deux interlocuteurs**

### L'Attaque de l'Homme du Milieu sur un Chiffrement Symétrique

Ce paragraphe s'intéresse à l'éventualité qu'un Homme, que nous nommerons <rb>Malotru</rb> [^1], se place au milieu (entre) Alice et Bob et tente de récupérer leurs communications dans le but de :

* les lire
* éventuellement (s'il le souhaite) les modifier
* usurper leurs identités respectives

!!! def "Attaque de l'Homme du Milieu (Malotru)"
    Cette attaque est connue sous le nom d'[<rb>Attaque du l'Homme du Milieu</rb>](https://fr.wikipedia.org/wiki/Attaque_de_l%27homme_du_milieu) :fr: / [<rb>MITM - Man In the Middle</rb>](https://en.wikipedia.org/wiki/Man-in-the-middle_attack) :gb:.

Les chiffrements symétriques :

* **sont sujets à l'Attaque de l'Homme du Milieu**, lorsque ni Alice ni Bob ne disposent déjà de leur clé partagée secrète (et qu'ils doivent donc commencer par se l'envoyer l'un l'autre sur un canal non sécurisé)

```mermaid
sequenceDiagram
    actor Alice
    actor Malotru
    actor Bob
    Alice ->> Malotru: Salut Bob, Je te propose cette clé partagée 🔑 <br/>(Malotru enregistre la clé partagée)
    Malotru ->> Bob: Salut Bob, Je te propose cette clé partagée 🔑 <br/>(modifié ou pas: par discrétion)
    Bob ->> Malotru: Salut Alice, OK je la note 🔑
    Malotru ->> Alice: Salut Alice, OK je la note 🔑 (idem)
    Alice ->> Malotru: message chiffré avec la clé partagée 🔑
    Malotru ->> Bob: message chiffré avec la clé partagée 🔑 <br/>(Malotru peut le lire et le modifier s'il le souhaite)
    Bob ->> Malotru: Réponse au message chiffré avec la clé partagée 🔑
    Malotru ->> Alice: Réponse au message chiffré avec la clé partagée 🔑 <br/>(Malotru peut le lire et le modifier s'il le souhaite)
```

* **ne sont pas sujets à l'Attaque de l'Homme du Milieu** lorsqu'Alice et Bob sont chacun, déjà en possesion de la même clé.


### Un Chiffrement Symétrique est-il un chiffrement de mauvaise qualité ?

Pas du tout ! S'il est associé naturellement à des chiffrements simples et faibles (comme le décalage de César), un chiffrement symétrique peut être très robuste... voire inviolable.

!!! exp "du Chiffrement du masque jetable"
    C'est le cas du chiffrement par <rb>masque jetable</rb>. Si le masque avec lequel on effectue le XOR sur le message est aussi long que le message, alors il est **impossible** de retrouver le message initial. Pourquoi ?  
    Imaginons qu'Alice veuille transmettre le message clair "LUNDI".  
    Elle le chiffre avec un masque jetable (que connait aussi Bob), et Bob reçoit donc "KHZOK". 
    Si Malotru a intercepté le message "KHZOK", **même s'il sait que la méthode de chiffrement utilisée est celle du masque jetable** (principe de Kerckhoffs), il n'a pas d'autre choix que de tester tous les masques de 5 lettres possibles.
    Ce qui lui donne $26^5$ possibilités (plus de $11$ millions) pour le masque, et par conséquent (propriété de bijectivité du XOR) $26^5$ possibilités pour le message «déchiffré»...

    Cela signifie que Malotru verra apparaître, dans sa tentative de déchiffrage, les mots "MARDI", "JEUDI", "JOUDI", "STYLO", "FSDJK", "LUNDI, "LUNDA"... 
    Il n'a aucune possibilité de savoir où est le bon message original parmi toutes les propositions (on parle de *sécurité sémantique*).

!!! pte "Principe de Kerckhoffs"
    La sécurité d'un système de chiffrement ne doit reposer que sur la sécurité de la clé, et non pas sur la connaissance de l'algorithme de chiffrement. Cet algorithme peut même être public (ce qui est pratiquement toujours le cas).

### AES : Un exemple de Chiffrement Symétrique moderne

!!! col __40 right center
    ![Joan Daeman & Vincent Rijmen](./img/rijmen-daemen.webp){ .center style="width:100%; max-width:435px;"}
    Joan Daeman & Vincent Rijmen

L'algorithme de chiffrement symétrique le plus utilisé actuellement est le chiffrement [<rb>AES</rb>](https://fr.wikipedia.org/wiki/Advanced_Encryption_Standard) - <rb>Advanced Encryption Standard</rb> :gb:. Le chiffrement AES est quelquefois connu sous le nom <rb>Rijndael</rb>, qui est un mélange entre les deux noms de ses inventeurs belges :be: Vincent **Rijmen** et Joan **Daemen**. En fait le chiffrement Rinjdael est, en théorie, un peu plus général qu'AES : Rinjdael autorise des clés variables, alors qu'AES utilise des clés de longueur fixe. En octobre $2000$, c'est l'algorithme Rijndael (donc AES) qui a finalement remporté un concours international lancé en $1997$ par le **NIST - National Institute of Standards and Technology** (une agence du département du Commerce des USA), parmi un total initial de $15$ algorithmes candidats, dont $5$ furent conservés en $1999$ pour une évaluation plus poussée : **MARS**, **RC6**, **Rijndael**, **Serpent**, et **Twofish**. Le chiffrement AES remplace le **DES** (choisis comme standard dans les années $1970's$) et le **3DES** (triple DES).  

<clear></clear>

Le chiffrement AES est caractérisé par :  

!!! col __40 center right
    ![AES](./img/AES.svg){ .center style="width:100%; max-width:400px;"}
    Matrice carrée $4\times 4$

* chiffrement par bloc de $128$ bits, répartis dans une matrice carrée de $16=4\times 4$ octets (chaque cellule fait $1$ octet).
* ces $128$ bits sont transformés par des rotations, multiplications, transpositions, [...] de la matrice initiale, en faisant intervenir dans ces transformations une clé de $128$, $192$ ou $256$ bits (seulement ces $3$ longueurs sont possibles pour AES)
* pour l'AES-$256$ (avec une clé de $256$ bits), l'attaque <rb>par force brute</rb> (en testant aveuglément toutes les clés possibles, sans stratégie particulière, et ce jusqu'à trouver la bonne clé) nécessiterait $2^{256}$ opérations, soit un nombre à $78$ chiffres... 

<clear></clear>

* Recommandations de la NSA : En Juin $2003$, une analyse de la NSA affirme à propos de l'algorithme AES:
    * Toutes les longueurs d'AES 128, 192 et 256 bits sont suffisantes pour protéger des documents jusqu'au niveau SECRET
    * Le niveau TOP SECRET nécessite des clés 192 ou 256
* il n'existe pas d'attaque connue efficace à ce jour. Les seules attaques sont des attaques sur des faiblesses d'implémentation, ou par [canal auxiliaire](https://fr.wikipedia.org/wiki/Attaque_par_canal_auxiliaire).

<clear></clear>

## Chiffrement Asymétrique

Inventé par Whitfield **Diffie** et Martin **Hellman** en $1976$, le chiffrement asymétrique vient résoudre l'inconvénient essentiel du chiffrement symétrique : le nécessaire partage d'un secret (la clé) **AVANT** l'établissement de la communication sécurisée.

### Principe du Chiffrement Asymétrique

Le principe de base est l'existence d'un **couple de clés**, créées ensemble initialement pour chaque utilisateur souhaitant communiquer, et ayant des relations mathématiques intrinsèques entre elles :

* une **clé publique**, :lock-open: qui a vocation à être distribuée largement, et publiquement
* une **clé privée** :private-key:, qui ne quitte jamais son propriétaire

![image](./img/asym.png){ .center }

### Le rôle interchangeable des Clés Publiques et Privées

L'illustration précédente associe :

* une image de cadenas à la **clé publique** :lock-open: (car on s'en sert pour chiffrer les messages)
* une image de clé à la **clé privée** :private-key: (car on s'en sert pour déchiffrer les messages)

Concrètement, (nous le verrons dans l'application par le chiffrement RSA), la **clé privée** :private-key: et la **clé publique** :lock-open: sont **deux nombres** aux rôles identiques. Appelons-les A et B :

* il est impossible de trouver A en fonction de B. Réciproquement, si on connaît A, il est impossible d'en déduire B.
* si on chiffre un message avec A, on peut le déchiffrer avec B. Réciproquement, si on chiffre avec B, on peut déchiffrer le message grâce à A.
* on peut donc chiffrer avec une **clé publique** :lock-open: et déchiffrer avec la **clé privée** :private-key: associée (c'est ce qui est fait dans l'exemple précédent, et de manière classique). Mais on peut aussi chiffrer avec la clé privée, et déchiffrer avec la clé publique associée.

A et B ont donc des rôles interchangeables (chacun peut être un cadenas, chacun peut être une clé), et ce n'est qu'en connaissant A **et** B qu'on peut déchiffrer le message.

Nous allons donc maintenant adopter une nouvelle convention infographique :

* Considérons ce message :

<center>

![image](./img/a1.png)

</center>

* Si ce message est chiffré avec la **clé publique** :lock-open: d'Alice, le message sera :
![image](./img/a2.png){ .center }

* Si on déchiffre ce message avec la **clé privée** :private-key: d'Alice, il deviendra
![image](./img/a3.png){ .center }
et donc
![image](./img/a1.png){ .center }

puisque l'application de la **clé privée** :private-key: sur la **clé publique** :lock-open:, ou bien de la **clé publique** :lock-open: sur la **clé privée** :private-key:, permet de retrouver le message en clair.

De manière graphique, la connaissance des deux moitiés du disque qui s'assemblent permet de les faire disparaitre, peu importe qu'on ait commencé par chiffrer avec la **clé publique** :lock-open: ou avec la **clé privée** :private-key:.

![image](./img/gif_auth.webp){ .center }


### L'Attaque de l'Homme du Milieu sur un Chiffrement Asymétrique

#### Le Principe de l'Attaque sur un Chiffrement Asymétrique

Dans le cas d'un chiffrement asymétrique :

```mermaid
sequenceDiagram
    actor Alice
    actor Malotru
    actor Bob
    Alice ->> Malotru: Salut Bob, Voici ma clé publique 🔓1
    Bob ->> Malotru: Salut Alice, Voici ma clé publique 🔓2
    Malotru ->> Alice: Salut Alice, C'est Bob! Merci! Et voici la mienne <br/>(en fait celle de Malotru..🔓3)<br/>Chiffré avec la clé publique d'Alice 🔓1
    Malotru ->> Bob: Salut Bob, C'est Alice! Merci! Et voici la mienne <br/>(en fait celle de Malotru..🔓3)<br/>Chiffré avec la clé publique de Bob 🔓2
    Alice ->> Malotru: Salut Bob : Je t'ai bien déchiffré avec ma clé privée 🔑
    Bob ->> Malotru: Salut Alice : Je t'ai bien déchiffrée avec ma clé privée 🔑
```

* Supposons qu'une personne malveillante (encore appelé <rb>Malotru</rb>[^1]) se place entre <rb>Alice</rb> et <rb>Bob</rb>
* Malotru se fait passer pour l'un d'eux, et donne sa propre clé publique à la place. Par exemple :
* Il envoie sa clé publique à Bob, en se faisant passer pour Alice, et sa clé publique à Alice en se faisant passer pour Bob.
* Bob croie alors que c'est la clé publique d'Alice, et inversement Alice croie que c'est la clé publique de Bob. Mais en réalité, Alice et Bob disposent tous les deux de la clé publique de Malotru, en pensant qu'il s'agit de la clé publique de leur interlocuteur de confiance (Alice ou Bob).
* Lorsqu'Alice envoie un message chiffré à Bob, elle utilise la clé publique qu'elle croie être de Bob (en réalité c'est celle de Malotru)
* Malotru intercepte le message chiffré par Bob (avec la clé publique de Malotru), peut le déchiffrer avec sa clé privée. Il peut ainsi (le lire.. pour commencer.. et) le modifier à loisir, puis :
    * Renvoyer à Bob un message, modifié (ou pas..), prétendûment la réponse d'Alice, en le chiffrant avec la clé publique d'Alice (pour ne pas éveiller les soupçons)
    * Transmettre à Alice un message, modifié (ou pas..), prétendûment un message de Bob, en le chiffrant avec la clé publique de Bob (pour ne pas éveiller les soupçons)
* Bob et Alice pourront ainsi déchiffrer les messages reçus :
    * avec leur propre clé privée
    * en pensant à tout instant qu'il s'agit de leur inlerlocuteur privilégié
* Malotru est donc capable de lire (/modifier) les informations entre les deux et (surtout?) récupérer les informations importantes

Pour éviter ce type d'attaques, il faudrait pouvoir **certifier/authentifier** le porteur de la clé/l'émetteur.

### Communication Authentifiée

Dans la siuation du paragraphe précédent (Chiffrement Asymétrique), Alice (qui a distribué largement sa **clé publique** :lock-open:) ne peut pas s'assurer que le message vient bien de Bob. Il peut avoir été créé par un Homme du Milieu ***Malotru***, qui signe faussement «Bob», en usurpant ainsi son identité. 

Le protocole que nous allons décrire ci-dessous permet de :

* <env>Chiffrer un message</env> càd empêcher qu'un message intercepté soit déchiffré (ce qui était déjà le cas dans le paragraphe - Chiffrement Asymétrique), mais aussi de
* <env>Authentifier l'émetteur</env> càd s'assurer que chaque personne est bien celle qu'elle prétend être.

![image](./img/total_auth.png){ .center }

**En résumé :**

* Alice est sûre que seul Bob pourra déchiffrer le message qu'elle envoie.
* Bob est sûr que le message qu'il reçoit vient bien d'Alice.


### Comparaison Symétrique vs Asymétrique

* Le <rb>Chiffrement Symétrique</rb> :
    * **ne permet pas d'échanger initialement des clés à distance**. En effet :
        * Il est vulnérable à l'Attaque de l'Homme du Milieu, lorsqu'Alice et Bob ne sont pas déjà échangé/mis d'accord sur leur clé partagée (c'est bien cette situation qui est envisagée dans un échange initial de clés)
        * (Néanmoins) il n'est pas vulnérable à l'Attaque de <rb>Homme du Milieu</rb> :fr: / <rb>MITM - Man In The Middle</rb> lorsque Alice et Bob disposent déjà chacun, séparément et préalablement, de leur clé partagée :gb: (mais ce n'est pas cette situation qui nous intéresse ici)
    * n'est **PAS très gourmand en ressources**, donc il est adapté à une implémentation pratique (de ce point de vue là)
* Par opposition, Le <rb>Chiffrement Asymétrique</rb> **permet l'échange initial de clés**, mais attention, **uniquement** dans le cas d'une **communication avec authentification** :
    * **il est vulnérable à l'Attaque de l'Homme du milieu dans le cas où les clés publiques respectives sont envoyées sans authentification de l'émetteur**,
    * Néanmoins, **il n'est pas vulnérable à l'Attaque de l'Homme du Milieu, avec authentification de l'émetteur**
    * est **très gourmand en ressources**, donc il n'est PAS très adapté à une implémentation pratique (de ce point de vue là). Cela justifie que le protocole HTTPS n'utilise un chiffrement asymétrique QUE pour l'échange initial d'une clé symétrique.


### RSA : Un exemple de Chiffrement Asymétrique

#### Histoire

!!! col __30 right center
    ![Diffie-Hellman](./img/difie-hellman.jpg){ .center style="width:100%; max-width:300px;" }
    Martin Hellman & Whitfield "Whit" Diffie

Lorsqu'en $1976$ <rb>Diffie</rb> et <rb>Hellman</rb> (chercheurs à Stanford) présentent le concept de chiffrement asymétrique (souvent appelé *cryptographie à clés publiques*), ils en proposent uniquement un modèle théorique, n'ayant pas trouvé une réelle implémentation de leur protocole.

<clear></clear>

!!! col __50 right center
    ![image](./img/pic_RSA.jpeg){ .center style="width:100%; max-width:600px;"}
    Rivest - Shamir - Adleman

Trois chercheurs du **MIT - Massachussets Institute of Technology (Boston)**, Ron **Rivest**, Adi **Shamir** et Len **Adleman** se penchent alors sur ce protocole, convaincus qu'il est en effet impossible d'en trouver une implémentation pratique. En $1977$, au cours de leurs recherches, ils démontrent en fait l'inverse de ce qu'ils cherchaient : ils créent le premier protocole concret de chiffrement asymétrique : le chiffrement **RSA**.

<clear></clear>

!!! col __30 right center
    ![image](./img/clifford.jpg){ .center style="width:100%; max-width:600px;" }
    Clifford Cocks

Au même moment à Londres, [Clifford <rb>Cocks</rb>](https://fr.wikipedia.org/wiki/Clifford_Cocks), (chercheur au très secret [GCHQ](https://fr.wikipedia.org/wiki/Government_Communications_Headquarters){. target="_blank"}) apprend que Rivest Shamir et Adleman viennent de découvrir ce que lui-même a découvert **$3$ ans plus tôt** (en $1974$) mais qui est resté classé Secret Défense.

Il est le véritable inventeur du RSA... mais le reste du monde ne l'apprendra qu'en $1997$ au moment de la déclassification de cette information. 

#### Description

Le chiffrement RSA est basé sur *l'arithmétique modulaire*. Faire des calculs *modulo* un entier $n$, c'est ne garder que le reste de la division euclidienne par $n$.

Le fait que $15$ soit égal à $1$ modulo $7$ (car $15=2 \times 7+1$) s'écrira $15 \equiv 1 [7]$.

De même, $10 \equiv  3 [7]$, $25 \equiv 4 [7]$, $32 \equiv 2 [10]$, etc.

#### Étape 1

Alice choisit $2$ grands nombres premiers $p$ et $q$. Dans la réalité ces nombres seront vraiment très grands (plus de $100$ chiffres). Dans notre exemple, nous prendrons $p = 3$ et $q = 11$.

#### Étape 2

Alice multiplie ces deux nombres $p$ et $q$ et obtient ainsi un nombre $n$.  
Dans notre cas $n=p\times q=3\times 11=33$

!!! note
    Il est très facile pour Alice de calculer $n$ en connaissant $p$ et $q$, mais il  extrêmement difficile pour Malotru de faire le travail inverse : trouver $p$ et $q$ en connaissant $n$ prend un temps exponentiel avec la taille de $n$.

C'est sur cette difficulté (appelée difficulté de *factorisation*) que repose la robustesse du système RSA.

#### Étape 3

Alice choisit un nombre $e$ qui doit être premier avec $(p-1)(q-1)$.  
On note $\phi(n)$ le nombre $(p-1)(q-1)$.

Dans notre exemple, $\phi(33)=(p-1)(q-1) = 2\times 10 = 20$, Alice choisit donc (par exemple)  $e = 3$. (mais elle aurait pu tout aussi bien choisir $7$, $9$, $13$...).

Le couple $(e, n)$ sera **la clé publique** :lock-open: d'Alice. Elle la diffuse à qui veut lui écrire.

Dans notre exemple, la clé publique d'Alice est $(3, 33)$.

#### Étape 4

Alice calcule maintenant sa **clé privée** :private-key: : elle doit trouver un nombre $d$ qui vérifie l'égalité $e d \equiv 1 [\phi(n)]$.

Dans notre exemple, comme $7 \times 3  \equiv 1 [20]$, donc on peut choisir ce nombre $d$ comme égal à $7$.

En pratique, il existe un algorithme simple (algorithme d'[Euclide étendu](https://fr.wikipedia.org/wiki/Algorithme_d%27Euclide_%C3%A9tendu)) pour trouver cette valeur $d$, appelée *inverse de* $e$.

Le couple $(d, n)$ sera **la clé privée** :private-key: d'Alice. Elle ne la diffuse à personne.

Dans notre exemple, la **clé privée** :private-key: d'Alice est $(7, 33)$.

#### Étape 5

Supposons que Bob veuille écrire à Alice pour lui envoyer le nombre $4$. 
Il possède la clé publique d'Alice, qui est $(3, 33)$.

Il calcule donc $4^3$ modulo $33$, qui vaut $31$. C'est cette valeur $31$ qu'il transmet à Alice.

$$4^3 \equiv 31 [33]$$

!!! note
    Si Malotru intercepte cette valeur $31$, même en connaissant la **clé publique** :lock-open: d'Alice $(3,33)$, il ne peut pas résoudre l'équation $x^3 \equiv 31 [33]$ de manière efficace.

#### Étape 6

Alice reçoit la valeur $31$.  
Il lui suffit alors d'élever $31$ à la puissance $7 $(sa **clé privée** :private-key:), et de calculer le reste modulo $33$ :

$31^7 = 27512614111$

$27512614111 \equiv 4 [33]$

Elle récupère la valeur $4$, qui est bien le message original de Bob.

**Comment ça marche ?** : Grâce au [Petit Théorème de Fermat](https://fr.wikipedia.org/wiki/Petit_th%C3%A9or%C3%A8me_de_Fermat), on démontre (voir [ici](https://fr.wikipedia.org/wiki/Chiffrement_RSA)) assez facilement que <enc>$M^{ed} \equiv M [n]$</enc>.

Il faut remarquer que $M^{ed} = M^{de}$. On voit que les rôles de la clé publique et de la clé privée sont **symétriques** : un message chiffré avec la clé publique se déchiffrera en le chiffrant avec la clé privée, tout comme un message chiffré avec la clé privée se déchiffrera en le chiffrant avec la clé publique.

**Animation interactive**
voir [https://animations.interstices.info/interstices-rsa/rsa.html](https://animations.interstices.info/interstices-rsa/rsa.html)


#### RSA, un système inviolable ?

Le chiffrement RSA a des défauts (notamment une grande consommation des ressources, due à la manipulation de très grands nombres).
Mais le choix d'une clé publique de grande taille (actuellement $1024$ ou $2048$ bits) le rend pour l'instant inviolable. 

Actuellement, il n'existe pas d'algorithme efficace pour factoriser un nombre ayant plusieurs centaines de chiffres.

Deux évènements pourraient faire s'écrouler la sécurité du RSA :

* la découverte d'un algorithme efficace de factorisation, capable de tourner sur les ordinateurs actuels. Cette annonce est régulièrement faite, et tout aussi régulièrement contredite par la communauté scientifique. (voir, le $05$/$03$/$2021$,  [https://www.schneier.com/blog/archives/2021/03/no-rsa-is-not-broken.html](https://www.schneier.com/blog/archives/2021/03/no-rsa-is-not-broken.html))
* l'avènement d'[ordinateurs quantiques](https://fr.wikipedia.org/wiki/Calculateur_quantique), dont la vitesse d'exécution permettrait une factorisation rapide. Il est à noter que l'algorithme de factorisation destiné à tourner sur un ordinateur quantique existe déjà : [l'algorithme de Schor](https://fr.wikipedia.org/wiki/Algorithme_de_Shor).

## Chaînes d'Authentifications

En pratique, dans la vraie vie sur Internet, la **confiance** que l'on accorde à une **entité** (personne ou entité abstraite, e.g. client/serveur, etc..) est un peu plus compliquée que l'exemple précédent de Communication Authentifée. 

Elle est basée sur des modèles de confiance centralisés appelés des <rb>Infrastructures à Clés Publiques (ICP</rb> :fr: / <rb>Public Key Infrastructure (PKI)</rb> :gb:

* une **Chaîne (de Confiance) de l'Authentification** d'une entité :
    * Tout en haut de cette chaîne de Confiance : une <rb>Autorité de Certification (AC)</rb> :fr: / <rb>Certificate Authority (CA)</rb> :gb:
    *  Des entités de Certification Intermédiaires
* la notion de <rb>Certificat (Numérique)</rb> **signé** par l'Autorité de Certification

### Autorité de Certification

Une <rb>Autorité de Certification</rb> est un tiers de confiance permettant d'authentifier l'identité des correspondants. Une autorité de certification est l'autorité/institution chargée de délivrer des Certificats décrivant des identités numériques et met à disposition les moyens de vérifier la validité des certificats qu'elle a fournis. Son rôle est de garantir l'**intégrité** (non altération) des informations du Certificat. 


Les navigateurs web modernes intègrent nativement une liste de certificats provenant de différentes Autorités de Certification choisies selon des règles internes définies par les développeurs du navigateur. 

### Certificats

Un Certificat est un fichier avec un ensemble de données contenant au minimum:

* Une Clé Publique (au moins)
* Des Informations permettant d'identifier une entité. Pou rune personne par ex.
    * ses noms, prénoms
    * email
    * adresse, etc..
* Des Informations sur le Certificat (date de validité, chiffrement utilisé, etc..)
* Une Signature électronique d'une Autorité de Certification, 

!!! exp "Certificat pour Alice"
    Si Alice veut obtenir un Certificat, il faut qu'elle fournisse à l'Autorité de Certification :

    * sa clé publique
    * des informations sur elle (en les prouvant par des documents qu'on lui demandera)
    * de l'argent (ce n'est, en général pas gratuit)

    Ensuite, L'Autorité de Certification crée un Certificat pour Alice, qu'elle signe numériquement avec sa clé privée d'Autorité de Certication. Le Certificat atteste donc que la clé publique de ce certificat apartient bien à la personne désignée  dans le Certificat.


### Signature Electronique

Une <rb>Signature Electronique</rb> est la preuve que :

* le Certificat a bien été vérifié par l'Autorité de Certification (AC)
* elle garantit l'**intégrité** (pas d'altération) des informations du Certificat
* C'est la combinaison entre les informations du Certificat et la Clé Publique, le tout chiffré par la clé privée  :private-key: de l'Autorité de Certification (AC)


## HTTPS - HyperTexte Transfer Protocole Secure

Aujourd'hui, plus de $90\%$ du trafic sur internet est chiffré : les données ne transitent plus en clair (protocole `http`) mais de manière chiffrée (protocole `https`), ce qui assure la **confidentialité** (données chiffrées) des paquets éventuellements interceptés.

Le protocole <rb>HTTPS - HTTP Secure</rb> :gb: est en fait la réunion de deux protocoles :

* le protocole <rb>TLS - Transport Layer Security</rb> :gb: succède au protocole <rb>SSL</rb> - <rb>Secure Sockets Layer</rb> :gb: initialement développé par <rb>Netscape Communications Corporation</rb> en $1995$. Le protocole TLS utilise deux systèmes de chiffrements :
    * un premier **chiffrement asymétrique** (RSA, ECHDE, ..) utilisé (uniquement) pour générer et surtout partager une (même) **clé partagée** chez le client et chez le serveur.
    * un deuxième **chiffrement symétrique** (actuellement AES) utilisant la clé partagée pour chiffrer en pratique les données
* le protocole **HTTP** (qui est non non sécurisé) est implémenté **au-dessus** du protocole SSL/TLS (donc après), sans profonde modification. Les données chiffrées peuvent toujours être interceptées, mais elles sont illisibles.

![image](./img/https.png){ .center }

**Pourquoi ne pas utiliser que le chiffrement asymétrique, RSA par exemple ?**
Car il est très gourmand en ressources. Le chiffrement/déchiffrement doit être rapide pour ne pas ralentir les communications ou l'exploitation des données, c'est pourquoi on utilise :

* Le chiffrement asymétrique exclusivement pour l'échange de clés (au début de la communication).
* Le chiffrement symétrique, bien plus rapide, prend ensuite le relais pour l'ensemble de la communication.

## :fa-book: (HORS-PROGRAMME) Principe de Fonctionnement du TLS

### Introduction Vidéo

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/_UpuZ0Y3k-c" id="two" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Le Protocole SSL/TLS, GrafikArt

</center>

### Contexte

Le protocole TLS fonctionne suivant un mode client-serveur : On se place dans le contexte où un (ordinateur) client demande à communiquer de manière sécurisée (en HTTPS, càd en TLS) avec un site web (par exemple celui sa banque) hébergé sur un (ordinateur) serveur hébergeant un site sécurisé .

### La Négociation/Handshake TLS

Les messages transmis par l’intermédiaire du protocole TLS sont appelés des <rb>records</rb> :gb: / *enregistrements*, qui sont généralement encapsulés dans des segments TCP. Il existe $4$ grands types de *records* :

<center>

|Types de Records|
|:-:|
| `handshake` |
| `change_cypher_spec` |
| `application_data` |
| `alert` |

</center>

Négociation des Paramètres TLS dans un cas générique entre le client et le serveur :

![Handshake TLS](./img/handshake-TLS.png)

#### Étape 1

Le client initie une requête en envoyant au serveur du site un message de type <rb>ClientHello</rb>, contenant notamment :

* la version de TLS supportée par le client
* un ensemble de <rb>suites de chiffrement</rb> (asymétriques) supportées par le client, ainsi que les <rb>Hashs</rb> (fonctions de Hashage) supportés par le client, par exemple :

<center>

| Type <br/>de Clé | Algorithme <br/>de Chiffrement | Hash |
|:-:|:-:|:-:|
| RSA | RC4 | HMAC-MD5 |
| DH-RSA | AES | HMAC-SHA1 |
| PSK | 3DES EDE CBC | ACBA |

</center>

Un ClientHello peut par exemple contenir des informations additionnelles relatives aux courbes elliptiques prises en charge par le client pour effectuer des calculs ECC

#### Étape 2

Le serveur répond au client par un <rb>ServerHello</rb> qui contient la suite retenue, en particulier :

* l'Algorithme de Chiffrement et le Hash (càd la fonction de Hashage) retenus, parmi ceux compatibles avec ceux du client
* le type de Clé Asymétrique, en fonction de ses propres préférences

#### Étape 3

Le serveur envoie un message **Certificate**, contenant le **Certificat SSL/TLS** du (serveur du) site web contenant :

* la **Clé Publique** $p$ du site web
* des informations sur le site (Domaine, Nom, Email, etc..), et

Ce Certificat SSL/TLS aura (préalablement et indépendamment) été **Authentifié** par (la clé privée d') une **Autorité de Certification (AC)**

!!! note "Remarque"
    * Un certificat joue le même rôle qu'une pièce d'identité dans la vie réelle. 
    * La présentation du certificat à l'Autorité de Certification (AC) peut se représenter comme le scan d'une pièce d'identité dans un aéroport. 
    * L'autorité de certification est alors l'État (dont la base de données est interrogée par un logiciel) qui valide que la pièce d'identité est bien un document officiel.

#### Étape 4

Le serveur transmet dans un <rb>ServerKeyExchange</rb> une *valeur éphémère* qu’il **signe à l’aide de la Clé Privée du serveur**

#### Étape 5

Le serveur manifeste sa mise en attente avec un **ServerHelloDone**

#### Étape 6

* Le client vérifie l'identité du serveur en validant le **Certificat SSL/TLS** du site web en vérifiant la signature du serveur web : Pour cela, le client utilise les **clés publiques** des **Autorités de Certification (AC)** que contient tout navigateur web. 
* Après l'authentification du serveur et la vérification que son certificat est valide, le client poursuit l’échange de clés en choisissant à son tour une *valeur éphémère* et en la transmettant dans un <rb>ClientKeyExchange</rb>. 
* Néanmoins, le client doit encore vérifier la clé privée du serveur (Authentification). Pour cela le client, génère (grâce aux éléments contenus dans le *ServerKeyExchange* et le *ClientKeyExchange*) et chiffre avec la **Clé Publique du serveur** un (même) <rb>premaster secret</rb> (une clé secrète partagée). 
* Optionnellement (si requis), le serveur peut authentifier le certificat du client à cette étape
* Le serveur pourra alors déchiffrer ce *premaster-secret* (grâce à sa clé privée). 
* Le client et le serveur générent alors une <rb>master key</rb> (en prenant en compte les aléas contenus dans le **ClientHello** et le **ServerHello**).
* Le **master-secret** (partagé) est enfin utilisé pour générer la **clé secrète partagée de chiffrement symétrique** retenue des deux côtés (avec le protocole AES en pratique) qui permet de protéger les données applicatives en confidentialité et en intégrité, avant de les encapsuler dans des messages de type **application_data**.

#### Étape 7

Le client signale l’adoption de la suite de chiffrement symétrique négociée avec un **ChangeCipherSpec**
Les deux parties ont maintenant la garantie de savoir à qui elles parlent.

#### Étape 8

Le client envoie un **Finished**, qui est un message de test, chiffré avec la clé symétrique partagée **master key** calculée précédemment : c'est le premier message protégé (du client vers le serveur) selon la suite de chiffrement (symétrique) retenue (AES en pratique). Si le serveur parvient à déchiffrer correctement le message, c'est que la communication est sûre.

#### Étape 9

Le serveur signale l’adoption de la même suite avec un **ChangeCipherSpec**

#### Étape 10

Le serveur envoie à son tour un **Finished** : son premier message sécurisé (vers le client), qui confirme le bon décodage du Finished envoyé par le client, et que la communication est sûre et fonctionnelle.

#### Résumé en 4 étapes de la négociation/handshake TLS

On peut résumer la négociation TLS décrite ci-dessus, en la figure suivante contenant seulement $4$ étapes :

![image](./img/tls.png){ .center }


## Historique des Versions SSL/TLS

<center>

| Protocole | Date |
| :-: | :-: |
| TLS $1.3$ | Encore Draft.. |
| TLS $1.2$ | $2008$ |
| TLS $1.1$ | $2006$ |
| TLS $1.0$ | $1999$ |
| SSL $3.0$ | $1996$ |
| SSL $2.0$ | $1995$ |
| SSL $1.0$ | $\approx 1994$ |

</center>

## Références et Notes

### Références

* [Synetis, SSL/TLS, une explication?](https://www.synetis.com/ssl-tls-explication/)
* [Gilles Lassus, Cryptographie](https://github.com/glassus/terminale_nsi/blob/main/docs/T5_Architecture_materielle/5.4_Cryptographie/cours.md)
* Numérique et Sciences Informatiques, Terminale, T. BALABONSKI, S. CONCHON, J.-C. FILLIATRE, K. NGUYEN, éditions ELLIPSES.
* Prépabac NSI, Terminale, G. CONNAN, V. PETROV, G. ROZSAVOLGYI, L. SIGNAC, éditions HATIER.
* https://www.cloudflare.com/fr-fr/learning/ssl/what-happens-in-a-tls-handshake/

### Notes

[^1]: **Malotru** est le nom d'un personnage de fiction dans la (célèbre!) série française [Le Bureau des Légendes](https://fr.wikipedia.org/wiki/Le_Bureau_des_l%C3%A9gendes)
[^2]: [Recommandations de la NSA, FACT SHEET](https://web.archive.org/web/20070927035010/http://www.cnss.gov/Assets/pdf/cnssp_15_fs.pdf)
[^3]: [Que se passe-t-il lors d'une négociation TLS ?, Cloudshare](https://www.cloudflare.com/fr-fr/learning/ssl/what-happens-in-a-tls-handshake/)
[^4]: [Recommandations TLS , guide ANSSI](https://www.ssi.gouv.fr/uploads/2016/09/guide_tls_v1.1.pdf)
[^5]: [Digicert, How does SSL Handshake work?](https://www.websecurity.digicert.com/fr/fr/security-topics/how-does-ssl-handshake-work)
[^6]: [Présentation de SSL Handshake](https://www.ssl.com/fr/article/Pr%C3%A9sentation-de-SSL-Handshake/)
[^7]: [Cheap is Security](https://cheapsslsecurity.com/blog/what-is-ssl-tls-handshake-understand-the-process-in-just-3-minutes/)