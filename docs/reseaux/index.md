# TNSI : Rappels Réseaux

## MOOC SNT

<iframe width="100%" height="600" src="https://www.youtube.com/embed/s18KtOLpCg4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Hôtes d'un réseau

!!! def "Hôtes"
    Les <bred>Hôtes</bred> :fr: / <bred>Hosts</bred> :gb: émettent (expéditeur) et/ou reçoivent (récepteur) des messages

## Commutation de Paquets

Lors d'une transmission de données dans un réseau, les données à transmettre sont découpées en <bred>paquets</bred> : on parle de **Segmentation** ou de **Fragmentation**.  

!!! def "Commutation de paquets"
    La <bred>Transmission par Commutation de paquets</bred>, ou la <bred>Commutation de paquets</bred>, ou <bred>Commutation par paquets</bred>, ou <bred>Transmission par paquets</bred> :fr: ou *le* <bred>packet switching</bred> :gb:, est une méthode d'envoi et de regroupement de données via un réseau numérique, dont le principe général consiste en :

    * une <bred>Segmentation / Fragmentation</bred> des données : On découpe les données initiales à envoyer en un grand nombre de (plus petits) <bred>paquets</bred>, de taille fixée par des protocoles
    * Chaque paquet est transmis sur le réseau numérique, indépendamment des autres
    * Chaque paquet peut emprunter des routes (à priori) différentes (selon l'encombrement du réseau par exemple)
    * Chaque paquet est composé d'un **en-tête** et d'une **charge utile** (les "*vraies*" données brutes, à proprement parler)

<center>
<img src="./img/commutation-paquets.gif">
:sk-copyright: CC-BY-SA 3.0 [Wikipedia](https://commons.wikimedia.org/wiki/File:Packet_Switching.gif)
</center>

## Rappels : Protocole Internet (IP), Transmission de Paquets (TCP) & Adresses IP
