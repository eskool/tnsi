# TNSI : Systèmes Autonomes

## Systèmes Autonomes

!!! def "Système Autonome"
    Un <bred>Système Autonome</bred> [^1] [^2] :fr: ou <bred>AS - Autonomous System</bred> est un regroupement de réseaux informatiques intégrés à internet, offrant une **même politique cohérente** de routage (vis à vis de l'ensemble d'internet) :
    
    * Des **Protocoles de Routage Interne** :fr: / **IGP - Interior Gateway Protocols** :gb: communs et cohérents, spécifiques au Système Autonome, usuellement:
        * Une même gestion technique au sein de l'AS : par ex. définition des routes à choisir en priorité, filtrage des *annonces* (=envoi d'infos) des routeurs, etc..
        * Même métrique de routage interne dans l'AS
    * Un Même **Protocole de Routage Externe** :fr: / **EGP - Exterior Gateway Protocol** :gb: pour la communication avec les autres Systèmes Autonomes : Historiquement le protocole **EGP - Exterior Gateway Protocol** [^2] (de mi-$1980$'s à mi-$1990$'s), aujourd'hui **BGP - Border Gateway Protocol** [^5]

!!! def "Numéro d'un Système Autonome"
    Chaque Système Autonome dispose d'un <bred>Numéro (de Système Autonome)</bred> [^3] :fr: / <bred>ASN - Autonomous System Number</bred> :gb: qui le caractérise. C'est l'[IANA](https://www.iana.org/) [^6] qui gère ces numéros ASN mais pas de manière directe: L'IANA en délègue la gestion *régionale* aux organisations *régionale* qui allouent les adresses IP: les **Registres Internet Régionaux (RIR)**. En Europe, c'est le **[RIPE-NCC](https://www.ripe.net/)** [^7] qui assume cette charge, par délégation de l'IANA.

## Exemples de Systèmes Autonomes

!!! exp "de Systèmes Autonomes"
    Un Système Autonome est généralement sous le contrôle d'une **entité** ou **organisation unique**, typiquement:
    
    * un **FAI - Fournisseur d'Accès à Internet** :fr: / **ISP - Internet Service Provider** :gb:
    * Une très large entreprise ayant des connections indépendantes à de multiples réseaux
    * de nos jours, cette définition a été un peu élargie

## Quelques Représentations Graphiques

<center>
<img src="../../img/as_internet.svg"/>

<figcaption>
Évolution d'un nombre de Systèmes Autonomes, 1997 - 2014
</figcaption>
</center>

<center>
<img src="../../img/as-2020-ipv4.png"/>
</center>

<center>
graphe des relations entre les AS, en IPv4, 2020, (source caida.org [^4])
</center>



## Notes & Références

[^1]: [Systèmes Autonomes, wikipedia](https://fr.wikipedia.org/wiki/Autonomous_System)
[^2]: [EGP - Exterior Gateway Protocol, RFC 827, 1982](https://datatracker.ietf.org/doc/html/rfc827). Cette RFC détaille aussi, pour la première fois, la notion de Système Autonome (début de la page 2)
[^3]: [IANA, Autonomous Systems Numbers](https://www.iana.org/assignments/as-numbers/as-numbers.xhtml#as-numbers-2)
[^4]: [caida.org cartography, AS Core 2020](https://www.caida.org/projects/cartography/as-core/2020/)
[^5]: [BGP - Border Gateway Protocol, RFC 1771](https://datatracker.ietf.org/doc/html/rfc1771)
[^6]: [IANA - Internet Assigned Numbers Authority](https://www.iana.org/)
[^7]: [RIPE - NCC Network Coordination Centre]([RIPE-NCC](https://www.ripe.net/))

* INVOCOM: Cours [Routage](http://www.invocom.et.put.poznan.pl/~invocom/C/INT/tcpip/fr/ch5.html)