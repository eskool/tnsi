# TNSI : Routage Interne vs Routage Externe

## Routage Interne

!!! def "Routage Interne - IGP"
    Les protocoles de <bred>Routage Interne</bred>, ou simplement le <bred>routage interne</bred> :fr: / <bred>[IGP - Interior Gateway Protocols](https://fr.wikipedia.org/wiki/Interior_gateway_protocol)</bred> [^1] :gb:, sont les protocoles utilisés par/à l'intérieur d'un même Système Autonome.

!!! exp "Protocoles de Routage Interne / protocoles IGP"
    Il existe plusieurs types de Protocoles de Routage Interne / Protocoles IGP. On les divise usuellement de la manière suivante:
    
    * Les protocoles de Routage <bred>à Vecteur de Distance</bred> :fr: / <bred>Distance Vector Protocols</bred> :gb:, dans lesquels **chaque routeur a une vision partielle du réseau**, et qui utilisent l'**Algorithme de Bellman-Ford** pour le calcul des meilleures routes:  
    <env>Exemples pour IPv4</env> 
        * **RIP = Routing Information Protocol (RIP v2 est au Programme de TNSI, mais PAS RIPv1)**
        * IGRP = Interior Gateway Routing Protocol est un protocole de routage propriétaire (Cisco), Classful (avec des Classes: obsolète) (Hors Programme de TNSI) 
    * Les protocoles de routage <bred>à état de liens</bred> :fr: / <bred>link state protocols</bred> :gb:, dans lesquels **chaque routeur a une vision complète du réseau**, et qui établissent des tables de voisinage et utilisent l'**Algorithme de Dijkstra** pour calculer les meilleures routes.  
    <env>Exemples pour IPv4</env>
        * **OSPF = Open Shortest Path First (au Programme de TNSI)**
        * IS- IS = Intermediate System to Intermediate System (Hors Programme de TNSI) 
    * Hybride: Un mélange des deux précédents.  
    <env>Exemples pour IPv4</env>
        * EIGRP = Enhanced Interior Gateway Routing Protocol, le successeur de IGRP, est un protocole de routage propriétaire (Cisco), mais partiellement ouvert en $2013$. Il est Classless / VLSM (sans Classes: càd à Taille Variable de Masques de Sous-réseau) (Cf. [RFC 7868](https://tools.ietf.org/html/rfc7868), Hors Programme de TNSI) 

<enc>Résumé</enc>

<center>

| Classes | Vecteur de Distance | État de Lien | Hybride |
| :-: | :-: | :-: | :-: |
| Classless / VLSM | <bblue>RIPv2<bblue> | <bblue>OSPF</bblue>, IS-IS | EIGRP |
| Classfull | IGRP, RIPv1 | |  |

<figcaption><bblue>Au Programme de TNSI</bblue>
</figcaption>

</center>

!!! info "Au Programme de TNSI = 'Uniquement' le Routage interne (RIPv2 et OSPF)"
    Seuls les protocoles de Routage Interne **RIPv2** et **OSPF** sont au Programme de TNSI. 

## Routage Externe (Hors-programme)

!!! def "Routage Externe - EGP"
    Les protocoles de <bred>Routage Externe</bred>, ou simplement le <bred>routage externe</bred> :fr: / <bred>[EGP - Exterior Routing Protocols](https://fr.wikipedia.org/wiki/Exterior_gateway_protocol)</bred> [^2] :gb:, quant à eux, sont les protocoles de routage **ENTRE les Systèmes Autonomes**.

!!! exp "Protocoles de Routage Externe : Protocoles EGP"
    Historiquement [^6], EGP (RFC [^3] [^4]) était aussi le nom d'un protocole externe (un EGP) spécifique.
    De nos jours , le protocole EGP a été remplacé par le protocole <bred>[BGP - Border Gateway Protocol](https://fr.wikipedia.org/wiki/Border_Gateway_Protocol)</bred> [^5] (RFC [^7]), qui est maintenant l'unique protocole de routage externe.

!!! info "Routage Externe = Hors-Programme de TNSI"
    * Les protocoles de routage externes sont **Hors-Programmes de TNSI**.
    * Certains protocoles de routage externe sont quelquefois propriétaires.

## Résumé

<center>
<img src="../../img/igp_vs_bgp.png"/>

<figcaption>
Routage Interne (IGP) vs Routage Externe (EGP/BGP)
</figcaption>
</center>

## Notes & Références

[^1]: [IGP - Interior Gateway Protocol, wikipedia](https://fr.wikipedia.org/wiki/Interior_gateway_protocol)
[^2]: [EGP - Exterior Gateway Protocols](https://fr.wikipedia.org/wiki/Exterior_gateway_protocol)
[^3]: [EGP - Exterior Gateway Protocol, RFC 827, 1982](https://datatracker.ietf.org/doc/html/rfc827). Cette RFC détaille aussi, pour la première fois, la notion de Système Autonome (début de la page 2)
[^4]: [EGP Formal Specification, RFC 904, 1984](https://datatracker.ietf.org/doc/html/rfc904)
[^5]: [BGP wikipedia](https://fr.wikipedia.org/wiki/Border_Gateway_Protocol)
[^6]: [GGP, EGP and BGP: 25 years of History, routerfreak.com](https://www.routerfreak.com/ggp-egp-and-25-years-of-bgp-a-brief-history-of-internet-routing/#elementor-toc__heading-anchor-0) : pour une Chronologie des protocoles de routage externe
[^7]: [BGP - Border Gateway Protocol, RFC 1771](https://datatracker.ietf.org/doc/html/rfc1771)

* INVOCOM: Cours [Routage](http://www.invocom.et.put.poznan.pl/~invocom/C/INT/tcpip/fr/ch5.html)

* Pour des documents techniques sur BGP, Cf les RFC [^2], [^3] et [^4]
