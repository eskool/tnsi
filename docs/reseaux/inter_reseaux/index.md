# TNSI : Inter-Réseaux

## Inter-réseaux / Réseaux de Réseaux

Internet est un <bred>inter-réseau</bred>, càd un réseau de réseaux reliés au moyen de routeurs, qui est rapidement devenu complexe, par sa taille, mais aussi par ses interconnections :

<center>
<img src="../img/opteProject.gif">
</center>

<center>
[Opte Project](https://www.opte.org/) : [the Internet $1997$ - $2021$](https://www.opte.org/the-internet)  
(gif animé, accéléré $\approx 8$x, $\approx 17$ sec)
</center>

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/-L1Zs_1VPXA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Opte Project](https://www.opte.org/) : la vidéo complète ($2$ min $18$: [the Internet $1997$ - $2021$](https://www.opte.org/the-internet)
</center>

Des regroupements *cohérents* entre certains réseaux, que l'on peut envisager comme des *réseaux de réseaux*, ont donc été mis en place, pour en faciliter la gestion technique et leur donner du sens.

