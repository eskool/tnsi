# TNSI : Routage Statique

## Routage Statique

Le <bred>routage statique</bred> est configuré manuellement par un humain : l'**administrateur réseau**. En pratique, cela revient souvent à déléguer la gestion technique manuelle de chaque routeur à un **centre de contrôle centralisé**, qui décide de tout :

* calculer les bonnes routes
* transmettre manuellement à chaque routeur les mises à jour des tables de routage

C'est pourquoi un *routage statique* s'appelle aussi quelquefois un <bred>routage centralisé</bred>.

### Avantages

Le centre de contrôle a une vision complète et plus fine du réseau, il est au courant des nouvelles modifications (pannes, nouvelles machines, etc..) : il est donc capable de prendre les bonnes décisions et d'éviter de faire confiance à une boîte noire qu'il ne contrôle pas.

Certains chemins privilégiés peuvent aussi être définis de cette façon (intranet, routes privées), par exemple pour adresser directement le destinataire par un réseau longue distance (des sortes de *raccourcis* ou "shortcut routing").  

### Inconvénients

En cas de panne du centre de contrôle, ou s'il est coupé d'une partie du réseau, le réseau peut être paralysé ou du moins incapable de réagir aux événements.

Le routage statique/centralisé n'est cependant intéressant/viable que pour des **réseaux de petite taille**: il est en effet impossible de gérer toutes les dizaines de milliards de routeurs d'internet, ou même de très grosses entités (entreprises, etc..)

C'est pourquoi on préfère souvent laisser des algorithmes distribués (chaque routeur est indépendant et prend ses propres décisions) et itératifs (l'algorithme qui gère les mises à jour est périodique et fréquent: il ne s'arrête jamais) gérer les mises à jour des tables de routage.