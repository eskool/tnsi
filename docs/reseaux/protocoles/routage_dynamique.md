# TNSI : Routage Dynamique

## Principe

Il est impossible au niveau d'internet, ou même de gros réseaux d'entreprise de définir **statiquement**, càd manuellement, les **tables de routages** de milliers, voire de milliards, de routeurs. Des algorithmes/protocoles dits de **routage dynamique** ont été inventés pour automatiser cette tâche. Nous allons en étudier plus en détail deux d'entre eux, dans le cas de réseaux de taille moyenne, qui sont spécifiquement au programme de TNSI:

* Le Protocole <bred>RIP - Routing Information Procotol</bred>
* Le Protocole <bred>OSPF - Open Shortest Path First</bred>

Ces deux protocoles utilisent chacun des paramètres supplémentaires (pas les mêmes) appelés des **métriques**.

## Métrique

### Définition

!!! def "Métrique (de Routage)"
    Une <bred>métrique</bred> ou <bred>métrique de routage</bred> est un paramètre supplémentaire (numérique) stocké dans les tables de routages, qui **mesure un certain coût** de la route:

    * les distances entre un routeur et un sous-réseau de destination
    * la bande-passante
    * la charge du tronçon, 
    etc..

Cette métrique dépend du protocole utilisé, et est en pratique utilisée par chaque routeur pour déterminer les (meilleures) routes de sortie des paquets qu'il reçoit en entrée.  

### Un Exemple: La Distance Manhattan

On parle de **métrique**, car ce paramètre vérifie les mêmes propriétés que la distance classique entre deux points $A$ et $B$, appelée la **distance euclidienne**, représentée en <bgreen>vert</bgreen> sur l'image qui suit.

La quartier de **Manhattan**, à New York, est connu pour ses rues à angles droits (rectangulaires). 

<center>

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d52723.42188169624!2d-74.00180099927931!3d40.768086490233415!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2588f046ee661%3A0xa0b3281fcecc08c!2sManhattan%2C%20New%20York%2C%20%C3%89tat%20de%20New%20York%2C%20%C3%89tats-Unis!5e0!3m2!1sfr!2sfr!4v1643561799967!5m2!1sfr!2sfr" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

</center>

Dans l'image ci-dessous, on voit que les 3 distances <bred>rouge</bred>, <bblue>bleue</bblue>, et <byellow>jaune</byellow> parcourues par un Taxi à Manhattan sont toutes égales, car toutes les largeurs totales sont égales et toutes les hauteurs totales aussi: la hauteur totale et la largeur totale sont d'ailleurs les seuls critères qui comptent. Pour mieux traduire ce constat par un/des nombres, on mesure la **distance Manhattan** (à parcourir par le taxi) entre un point $A$ et un point $B$ par la formule :

<center>

<enc>
$d=\lvert x_B - x_A \rvert + \lvert y_B - y_A\rvert=Largeur_{Totale}+Hauteur_{Totale}$
</enc>

</center>

(ATTENTION, ce n'est PAS la formule classique de la distance mathématique vue au Lycée: il manque les carrés..)

![Distance Manhattan](../img/distance_manhattan.svg){.center}
