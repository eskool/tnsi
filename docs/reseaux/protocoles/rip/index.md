# TNSI : Le Protocole RIP <br/>& Protocoles à Vecteurs de Distance

## Repères Historiques

Le <bred>protocole RIP - Routing Information Protocol</bred> :gb: fait partie de ces protocoles qui ont été d'abord largement implémentés avant d'être finalement standardisés par une RFC: c'est un standard de fait. Dès les années $1970$'s [^4], dans son centre de recherche **PARC - Palo Alto Research Center**, **XEROX** implémente son prédécésseur (GWINFO), qui sera renommé XNS RIP lorsqu'utilisé dans son architecture réseau expérimentale **XNS - Xerox Network System**. Cette implémentation sera reprise et deviendra la base des premiers protocoles de routage **IPX RIP** de **Novell**, **Routing Table Maintenance Protocol (RTMP)** d'**AppleTalk**, et IP RIP. En $1982$, la version $4.2$ Unix BSD (Berkeley Software Distribution) se distingue en incluant le protocole RIP dans son démon *routed*. Cette version populaire d'Unix influencera notablement le développement de RIP. Finalement, RIP aura été largement déployé avant de devenir la [RFC 1058](https://datatracker.ietf.org/doc/html/rfc1058) [^1] pour RIPv1 écrite par Charles Hedrick en $1988$. La [RFC 2453](https://datatracker.ietf.org/doc/html/rfc2453) pour RIPv2 [^3] date de 1998.

## Introduction Vidéo

Le <bred>protocole RIP - Routing Information Protocol</bred> :gb: rentre dans la catégorie des protocoles <bred>à Vecteur de Distance</bred>.

<center>

<iframe width="560" height="315" src="https://www.youtube.com/embed/kzablGaqUXM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Claude Chaudet, Maître de Conférences IMT - Institut Mines-Télécom  
Protocoles à Vecteurs de Distance
</center>

## Algorithmes à Vecteurs de Distance

### Métrique / Distance

!!! def "Métrique / Distance"
    Au sens du protocole RIP, La <bred>métrique</bred> ou <bred>distance</bred> d'un routeur à un réseau de destination est le nombre de <bred>sauts</bred> :fr: (de routeurs) ou <bred>hops</bred> qui le séparent de ce réseau, càd le nombre de routeurs devant être traversés/sautés pour parvenir au réseau.

!!! pte "Valeurs Possibles/Remarquables de la Distance dans RIP"
    Dans le protocole RIP, la distance est un nombre entier:
    
    * compris entre $1$ et $15$ inclus
    * Une distance égale à $1$ signifie que le réseau de destination est **directement connecté**
    * Une distance égale à $16$ représente l'**infini**, donc que le réseau de destination n'est pas atteignable

### Vecteur de Distance

!!! def "Vecteur de Distance"
    Un <bred>Vecteur de Distance</bred> est la donnée d'un couple **Vecteur = (Direction, Distance)** (comme en mathématiques..), où :

    * La **Direction** = (L'adresse IP du) Saut suivant: Vers où faut-il aller pour le prochain saut/hop? Quelle est la nouvelle *direction* à prendre?
    * la **Distance** = le nombre de sauts/hops (de routeurs) pour rejoindre le réseau

### Protocoles à Vecteurs de Distance

Le principe général résumé des <bred>Protocoles (de Routage) à Vecteur de Distance</bred> :fr: / <bred>Vector Distance Protocols</bred> :gb: est le suivant:

* Chaque routeur transmet à ses voisins directs (uniquement) une copie de sa table de routage complète. 
* Les tables de routage se modifient au fur et à mesure de leur propagation, car chaque route est associée à une métrique qui croît par défaut d'une unité au passage de chaque routeur. 
* Pour chaque réseau de destination dont il entend parler, chaque routeur détermine la meilleure route (celle qui est à la distance minimale) menant vers ce réseau, grâce à l'<bred>Algorithme de Bellman-Ford</bred> ($1950's$). Seule la meilleure route est propagée, les autres sont oubliées.

Ainsi:

!!! def "Topologie Locale vs Topologie Globale"
    * **Chaque** routeur n'a qu'une connaissance partielle du réseau : On dit que chaque routeur a une connaissance (uniquement) de la **Topologie Locale** du réseau
    * **Aucun** routeur n'a une connaissance complète du réseau: On dit qu'aucun routeur n'a connaissance de la **Topologie Globale** du réseau

### Exemples de Protocoles à Vecteurs de Distance

Des exemples classiques de protocoles à Vecteur de Distance sont :

* :warning: <bred>RIP - Routing Information Protocol</bred>:warning: <env><bred>Au programme de TNSI</bred></env>
* <bred>IGRP - Interior Gateway Routing Protocol</bred> <env>PAS au programme de TNSI</env> : Protocole **Obsolète** et Propriétaire ([Cisco (Systems)](https://fr.wikipedia.org/wiki/Cisco_Systems) :fa-trademark:) dont la métrique améliorée (par rapport à RIP) tient compte également des bandes passantes, de la charge, des délais, de la fiabilité, etc... Utilise également l'Algorithme de Bellman-Ford
* <bred>EIGRP- Enhanced Interior Gateway Routing Protocol</bred> ([RFC 7868](https://datatracker.ietf.org/doc/html/rfc7868)) <env>PAS au programme de TNSI</env> : Protocole Propriétaire ([Cisco (Systems)](https://fr.wikipedia.org/wiki/Cisco_Systems) :fa-trademark:), Amélioré et Successeur d'IGRP. Partiellement ouvert depuis $2013$. Sa métrique améliorée tient compte de la bande passante, de la charge, des délais, de la fiabilité, etc.. Utilise également l'Algorithme de Bellman-Ford

## Le Protocole RIP - Routing Information Protocol

Le <bred>protocole RIP - Routing Information Protocol</bred> :gb: rentre dans la catégorie des protocoles <bred>à Vecteur de Distance</bred>.

### Exécution et Convergence

L'exécution se fait en plusieurs étapes, qui se répètent, jusqu'à convergence (stabilité) du réseau :

* <enc>Étape :zero: </enc> CHAQUE ROUTEUR est d'abord **initialisé** avec une table de routage contenant ses réseaux **directement accessibles/connectés**, càd sans passer par aucun autre routeur, donc ceux à une distance $1$.

* <enc>Étape :one: </enc> Ensuite, **PÉRIODIQUEMENT**:
    * <envpink>Toutes les $30$s environ</envpink> CHAQUE ROUTEUR reçoit périodiquement des <bred>annonces</bred>, qui sont des **groupes de** <bred>messages</bred> (paquets/datagrammes) RIP, qui contiennent les mises à jour des tables de routage **entières** depuis ses voisins directs/passerelles/gateways (et vers eux).  
    CHAQUE ROUTEUR reçoit donc périodiquement des mises à jour des *vecteurs de distances* de chacun de ses voisins directs vers tous les réseaux que ceux-ci connaissent.  
    De cette manière, CHAQUE ROUTEUR met à jour périodiquement, dans sa propre table de routage:
        * Tous les **réseaux de destination possibles**
        * leurs **distances** (minimales) correspondantes. Seule la distance minimale vers un même réseau de destination est conservée: Elle est calculée par l'**Algorithme de Bellman-Ford**
    * <envpink>Toutes les $3$ min = $180$s environ</envpink> : **Suppression dans les tables d'un réseau** lorsqu'il n'apparaît plus dans les annonces au bout d'un certain temps ($3$ minutes). C'est une Détection de Panne : ou bien la passerelle a crashé, ou bien la ligne/connexion est indisponible. (Des messages sont occasionnellement perdus par les réseaux. C'est pourquoi, il n’est probablement pas souhaitable d’invalider une route sur base d’un seul message manqué.)
* <enc>Convergence</enc> Après plusieurs étapes (répétitions de l'étape :one: précédente), (on peut montrer que) les tables se stabilisent et le routage est pleinement opérationnel: on parle alors de <bred>Convergence</bred> du réseau. **Le temps nécessaire à la stabilisation des tables est proportionnel au diamètre du graphe** modélisant le réseau (le diamètre d'un graphe est le nombre maximal de sauts nécessaires pour relier deux points quelconques du réseau, en tenant compte de la pondération-distance de chaque arête). Pour de grands réseaux, la convergence sera donc lente..

<env>Quels changements de topologie du réseau sont-ils détectés ?</env>

* Lorsque de nouvelles destinations apparaissent
* Lorsque des chemins plus courts sont rendus possibles
* En cas de panne (et/ou de maintenance si le routeur est down)

### Format d'un Message/Datagramme RIP

Les annonces des routeurs sont des <bred>messages RIP</bred> envoyés par **multicast** (multidiffusion) à l'adresse `224.0.0.9` sur le port UDP réservé $520$ (pour RIPv1 et RIPv2) (resp. à l'adresse `FF02::9` sur le port $521$ pour RIPng), sous forme d'un **Datagramme UDP - User Datagram Protocol** (couche Transport $=$ Couche $4$ du modèle OSI). Un format de message RIP ressemble à :

<center>

```console linenums="0"
0               1               2               3                
0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7 0 1 2 3 4 5 6 7  
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|   commande    |   version     |      domaine de routage       |
+---------------+---------------+---------------+---------------+
| identifieur de famille d'@    |    tag/marqueur de route      |
+-------------------------------+-------------------------------+
|                         adresse IP                            |
+---------------------------------------------------------------+
|                     masque de sous réseau                     |
+---------------------------------------------------------------+
|                 saut suivant / passerelle                     |
+---------------------------------------------------------------+
|                          métrique                             |
+---------------------------------------------------------------+
| identifieur de famille d'@    |    tag/marqueur de route      |
+-------------------------------+-------------------------------+
|                         adresse IP                            |
+---------------------------------------------------------------+
|                     masque de sous réseau                     |
+---------------------------------------------------------------+
|                 saut suivant / passerelle                     |
+---------------------------------------------------------------+
|                         métrique                              |
+---------------------------------------------------------------+
| identifieur de famille d'@    |    tag/marqueur de route      |
+-------------------------------+-------------------------------+
|                       etc..                                   |
+-------------------------------+-------------------------------+
+----------------------                                          
```

</center>

<env>Exemple</env> [Capture d'un Datagramme RIP sur Cloudshare](https://www.cloudshark.org/captures/0d0fb368dfb9)


avec les précisions supplémentaires suivantes:

* un **En-Tête / Header** (Taille = $4$ octets) composé de :
    * **Commande** qui peut prendre les valeurs suivantes, au choix: 
        * `request`, une **requête (RIP request)** : c'est un message envoyé d'un routeur à un autre, lui demandant de lui renvoyer tout, ou partie, de sa table de routage
        * `response`, une **réponse (RIP Response)** : c'est un message envoyé par un routeur contenant tout, ou partie, de sa table de routage (pas obligatoirement en réponse à une réquête)  
        Les `request` et/ou les `response` se font usuellement par multidiffusion (multicast) pour RIPv2, à tous ses voisins sur l'adresse `224.0.0.9` (`FF02::9` pour IPv6) [ou bien, par diffusion / broadcast `255.255.255.255` pour RIPv1]. La diffusion unicast (vers un seul hôte/réseau) reste possible pour certains types de réseaux (réseaux [NBMA](https://en.wikipedia.org/wiki/Non-broadcast_multiple-access_network)).
    * **Version** : indique la version du protocole utilisé: $1$ ou $2$.
    * Numéro de **Domaine de Routage** / **Routing Domain** (RD) Number [^2] : `0` **par défaut**. C'est le numéro du processus de routage auquel appartient cette mise à jour. Ce champ est utilisé pour associé une mise à jour à un processus de routage spécifique sur le routeur qui réceptionne. Cela autorise l'existence de plusieurs "clouds" implémentant RIP indépendants sur le même réseau physique (câblage). Les administrateurs réseaux peuvent ainsi lancer, éventuellement en parallèle,  plusieurs instances RIP, de sorte à implémenter une même politique. Un routeur appartenant un même Domaine de Routage, ou ensemble de Domaines de Routages, sont censés ignorer les paquets RIP appartenant à un autre Domaine de Routage.
* Un ou Plusieurs enregistrements d'**entrées de routes** ou **Route Entries (RTE)** (entre $1$ à $25$ enregistrements de routes. Au maximum $24$ seulement, si un message d'authentification est requis) (Chaque entrée de la table de routage a une taille de $5 \times 4$ octets $= 20$ octets)
    * **Identifieur de Famille d'Adresses (@)** est un identifiant de *types d'adresses IP autorisées* pour le socket : Pour RIPv1 et RIPv2 cet identifiant correspond par défaut à `AF_INET` (Adresses IPv4), sauf exception (`0xFFFF`: en cas d'utilisation de l'Authentification dans RIPv2).
    * **Tag/marqueur de Route** (RT) : Tag/marqueur dont le but est de distinguer les routes apprises "*en interne*" par le protocole RIP, des routes apprises par d’autres protocoles que RIP:
        * protocoles de routage internes (IGP), mais autres que RIP: par exemple OSPF, ou bien,
        * protocoles de routage externes (EGP/BGP)
    * **Adresse IP** du réseau de destination
    * **Masques de sous-réseau** du réseau de destination
    * **Saut suivant / Passerelle**
    * **Métrique** :
        * Distance de la route **compris entre 1 et 15**
        * Une distance égale à $1$ signifie que le réseau de destination est **directement connecté**
        * Une Distance égale à **16 signifie l'infini**: càd que le réseau de destination est non atteignable

<env>Taille Maximale d'un Datagramme UDP RIP</env>
$4+25 \times 20=504$ octets


### Propriétés de RIP

!!! def "Algorithme Distribué"
    On dit que RIP est un algorithme <bred>distribué</bred>:
    
    * **Aucun** routeur ne connaît la Totalité du Réseau (**Topologie Globale**)
    * **Chaque** routeur ne connaît que l'état de ses voisins directs (**Topologie Locale**). On parle de <bred>routing by rumor</bred>
    En particulier, il revient à chaque routeur de :
    
    * faire ses propres calculs
    * prendre ses propres décisions quant au relayage de chaque paquet IP

!!! def "Algorithme Itératif"
    On dit que RIP est un algorithme <bred>itératif</bred>, car il se répète périodiquement, en particulier il ne s'arrête jamais.

### Détection et Prévention des Boucles

Une <bred>boucle de routage</bred> :fr: / <bred>routing loop</bred> :gb:est une route diffusée pour des paquets qui n’atteignent jamais leur destination : ils passent de façon répétée par la même série de noeuds du réseau. Ce phénomène est dû à une convergence lente des informations de routage. Un routeur éloigné fait croire à des routeurs (bien informés d’une route modifiée) qu’il dispose d’une nouvelle route (à coût plus élevé) vers ce réseau.

Par exemple une route du type `R2 -> R3 -> R5 -> R2` est une boucle: ce n'est évidemement pas souhaitable, car cela provoque des routes infinies.
RIP implémente certains mécanismes pour empêcher que se forment des boucles de routage. Parmi ces mécanismes, on peut citer :

#### Route Poisoning

Lorsqu’une route vers un réseau tombe, le réseau est immédiatement averti d’une métrique de distance infinie ($16$): plus aucune incrémentation n’est possible.
On parle de <bred>Route Poisoning</bred> :gb: / <bred>Empoisonnement de Route</bred> :fr:

#### TTL - Time to Live

Afin d'éviter à un paquet/datagramme RIP de tourner en rond infiniment, le protocole RIP définit un nombre maximal de sauts de routeurs autorisés, une sorte de **Durée de Vie** :fr: / <bred>TTL - Time To Live</bred> :gb: pour le datagramme:

* $15$ sauts au maximum
* $16$ désignant l'infini

#### Split horizon ou Horizon Partagé

Le **Split Horizon** existe en fait en deux versions:

* <bred>Simple Split Horizon</bred> :gb: / <bred>Horizon Partagé Simple</bred>: Le mécanisme d’« horizon partagé simple » n'ajoute pas les routes apprises depuis un voisin dans les mises à jour envoyées à ce voisin.
* <bred>Split Horizon with poisoned reverse</bred> :gb: / <bred>Horizon partagé avec empoisonnement</bred> :fr: : L’« horizon partagé avec empoisonnement » inclut de telles routes dans les mises à jour, mais fixe leur métrique à l’infini ($16$). Cette astuce empêche un schéma de *mutuelle déception* à 2 routeurs (seulement). (TODO)

De manière générale le Split Horizon est utile car:

* sinon, l'algorithme peut créer des boucles
* De plus, ce principe revient à ne pas envoyer d'infos inutiles à ses voisins, ce qui diminue la bande passante.

#### Trigerred Updates / Mises à jour Déclenchées

L’horizon partagé avec empoisonnement empêchera toute boucle de routage n’impliquant que **deux passerelles**. Néanmoins, il est toujours possible d’arriver à des situations où trois passerelles sont engagées dans une partie de *mutuelle déception*. Par exemple, A peut croire qu’il a une route vers B, B vers C, C vers A. L’horizon partagé ne peut arrêter une telle boucle. La boucle ne sera résolue que lorsque la métrique atteindra l’infini, et le réseau impliqué sera ensuite déclaré injoignable.

Les <bred>Triggered updates</bred> :gb: / <bred>mises à jour déclenchées</bred> :fr: constituent une tentative d’accélérer cette convergence. Pour utiliser des mises à jour déclenchées, on ajoute simplement la règle suivante:

!!! pte "Triggered Updates / Mises à Jour Déclenchées"
    Chaque fois qu’une passerelle change la métrique d’une route, elle doit envoyer des messages de mise à jour presque immédiatement, même si ce n’est pas encore le moment d’envoi du message de mise à jour régulier.

### Détection et Gestion des Pannes

Le protocole RIP est en mesure de détecter des pannes : Si un routeur ne reçoit pas d'information de la part d'un de ses voisins (ou bien le routeur a crashé, ou bien la liaison/ligne est indisponible) au bout d'un temps de l'ordre de $3$ minutes (configurable) il va considérer que ce lien est mort et en informer ses voisins en indiquant un nombre de sauts égal à la distance infinie $16$ (**Route poisoning**).
De cette manière, les voisins vont pouvoir recalculer leurs routes en conséquence en évitant le lien qui est tombé.

## Avantages / Désavantages

### Avantages

* RIP est facile à configurer et à maintenir (peu de paramètres par rapport à d'autres protocoles)
* RIP est facile à implémenter
* RIP implémente des mécanismes pour éviter des Boucles

### Désavantages / Limitations

* RIP est distribué: chaque routeur n'a de renseignement que sur ses voisins directs : PAS de vision globale du réseau (**routing by rumor**)
* La métrique/distance utilisée par RIP ne tient compte que du nombre de sauts de routeurs, mais elle ne tient PAS compte:
    * ni de l'**état de la liaison/lien** : par exemple pour choisir la meilleure bande passante possible). Exemple: Si l'on considère un réseau composé de trois routeurs $A$, $B$ et $C$, reliés en triangle, RIP préférera passer par la liaison directe $A-B$ même si la bande passante n'est que de $56$ kbit/s alors qu'elle est de $10$ Gbit/s entre $A$ et $C$ et $C$ et $B$.
    * ni de leur charge, etc..
* Pour éviter les boucles de routage, il y a une **distance maximum permise de $15$ sauts**. Une distance égale à $16$ représente l'infini: C'est une limitation claire sur la taille des réseaux.
* **les tables de routage ont 25 entrées au maximum**
* La **Convergence est Lente** (pour des réseaux moyens ou grands)
* **Assez gourmand en bande passante,** car il nécessite l'échange d'un volume de données assez important (envoie de la totalité des tables de routage)
* RIPv2 ne fonctionne que pour IPv4. Pour IPv6, il faut utiliser un autre procotole RIPng
* Authentification/Sécurité: Bien que cela puisse sembler aberrant, certains vieux routeurs utilisent encore (en $2015-2021$) l'ancien protocole RIPv1.. qui ne permet pas l'Authentification.. et a donné lieu à des amplifications d'attaques DDoS (Déni de Service). Une simple mise à jour de RIP en v2 permet l'Authentification :
    * avec mot de passe en clair, [RFC 1723](https://datatracker.ietf.org/doc/html/rfc1723#page-4) $1994$ (déconseillé..)
    * avec Hashage MD5 (dépassé): cf [RFC 2082](https://datatracker.ietf.org/doc/html/rfc2082) $1997$, 
    * avec Hashage SHA - [RFC 4822](https://datatracker.ietf.org/doc/html/rfc4822) $2007$

## Comparaison de RIP

### RIPv2 vs RIPv1 (Hors-Programme)

#### RIPv1

La première version de RIP (RIPv1) [^1] :

* ne prend pas en compte la notion de masque de sous-réseau à longueur variable (VLSM): on dit que **RIPv1 est Classfull**
* ne prend pas en compte l'authentification des routeurs (par mot de passe en clair, ni MD5, ni SHA, etc..)
* RIPv1 diffuse en broadcast (ce qui alourdit les charges)

Cette version RIPv1 n'est néanmoins PAS au programme de TNSI. 

#### RIPv2

RIPv2 [^3] (la seule version de RIP au programme de TNSI), développée en $1993$, apporte les améliorations suivantes par rapport à RIP v1 :

* la gestion des Masques de Sous-réseaux à Longueur Variable (**VLSM - Variable Length Subnet Mask**), avec la notation CIDR. **RIPv2 est Classless**.
* l’authentification des routeurs (mots de passes, MD5, SHA, etc..)
* les updates de routage multicast,

Néanmoins, RIPv2 souffre encore des limitations du métrique par nombre de sauts et d’une convergence lente qui sont essentielles dans les réseaux actuels.

### RIP vs OSPF

Les trois caractéristiques principales qui le distingueront de OSPF sont :

* la distance de RIP est mesurée en nombre de sauts
* chaque routeur n'a de renseignement que sur ses voisins directs, donc n'a pas de vision globale du réseau (**routing by rumor**)
* il y a une **distance maximum permise de 15 sauts** ($16$ représente l'infini) et
 **les tables ont 25 entrées au maximum.**
* RIP génère un traffic non négligeable, et ne peut s'appliquer qu'à de petits réseaux. Pour de plus grands réseaux, on lui préfèrera OSPF.

## Un Exemple Expliqué

Considérons le réseau suivant qui relie deux réseaux d'une entreprise :

* le réseau $1$ contient des postes de travail dans un bureau.
* le réseau $2$ contient un serveur dans un centre de données.

Les routeurs *R1* et *R6* permettent d'accéder au réseau de l'entreprise, *R2*, *R3*, *R4* et *R5*, des routeurs internes au réseau. 

![Réseau 2](../../img/reseau2.svg){.center}

Nous allons nous intéresser à l'évolution des tables de routage des routeurs *R1* et *R3* sur lesquels on a activé le protocole RIP.

### Étape 0

Au démarrage, les routeurs *R1* et *R3* ne connaissent que leurs voisins proches. Leurs tables peuvent donc ressembler à ceci :

<center>

| Destination |	Passerelle | Interface | Nb sauts | Remarques |
| :-: |	:-: | :-: | :-: | :-: |
| <pre>192.168.1.0</pre> | | wifi0  |   $1$  | ==> vers les postes de travail |
| <pre>172.16.0.0</pre> | | eth0   |   $1$  | ==> vers *R3* |

</center>

Au départ, *R1* ne peut atteindre que ses voisins immédiats (nb Sauts vaut $1$). Aucune passerelle n'est nécessaire puisque la communication est directe. Chaque sous réseau utilise une interface spécifique. Le réseau local 1 contenant les postes de travail est accessible en wifi.

En ce qui concerne le routeur $3$, celui-ci possède $4$ interfaces réseau filaires, que nous nommerons `eth0-3` qui permettent d'atteindre les routeurs immédiats (*R1*, *R2*, *R4* et *R5*). Voici à quoi peut ressembler sa table de routage au démarrage :

<center>

| Destination |	Passerelle | Interface | Nb sauts | Remarques |
| :-: |	:-: | :-: | :-: | :-: |
| <pre>172.16.0.0</pre> | |	eth$0$ | $1$ | ==> vers *R1* |
| <pre>172.16.1.0</pre> | |	eth$1$ | $1$ | ==> vers *R2* |
| <pre>172.16.6.0</pre> | |	eth$2$ | $1$ | ==> vers *R5* |
| <pre>172.16.3.0</pre> | |	eth$3$ | $1$ | ==> vers *R4* |

</center>

### Étape 1

Au bout de $30$ secondes, un premier échange intervient avec les voisins immédiats de chacun des routeurs.

!!! mth "Principe de l'Algorithme RIP"
    Lorsqu'un routeur reçoit une nouvelle route de la part d'un voisin, $4$ cas sont envisageables :

    * Il découvre une route vers un nouveau **réseau inconnu** 
    ==> Il l'ajoute à sa table.
    * Il découvre une route vers un réseau **connu**, **plus courte** que celle qu'il possède dans sa table 
    ==> Il actualise sa table
    * Il découvre une route vers un réseau **connu**, **plus longue** que celle qu'il possède dans sa table
    ==> Il ignore cette route.
    * Il reçoit une route vers un réseau **connu** en provenance d'un routeur **déjà existant dans sa table** 
    ==> Il met à jour sa table car la topologie du réseau a été modifiée.

En appliquant ces règles, voici la table de routage de *R1* après une étape :

<center>

| Destination | Passerelle | Interface |	Nb sauts | Remarques |
| :-: |	:-: | :-: | :-: | :-: |
| 192.168.1.0 |      | wifi0   |   $1$  | ==> vers les postes de travail |
| 172.16.0.0 |      |	eth0   |   $1$  | ==> vers *R3* |
| 172.16.1.0 | 172.16.0.3 | 	eth0   |   $2$  | Ces 3 routes |
| 172.16.6.0 | 172.16.0.3 | 	eth0   |   $2$  | proviennent |
| 172.16.3.0 | 172.16.0.3 | 	eth0   |   $2$  | de *R3* |

</center>

`172.16.0.3` est l'adresse IP du routeur *R3*. On ajoute à la table précédente les réseaux atteignables par *R3*. On pense cependant à ajouter $1$ au nombre de sauts ! Si *R1* veut atteindre le réseau `172.16.3.0`, il s'adressera à *R3* et atteindra le réseau cible en $2$ sauts.

Voici la table de *R3* qui s'enrichit des informations envoyées par *R1* afin d'atteindre le réseau local, mais aussi des informations en provenance de *R2*, *R4* et *R5*. Il découvre ainsi $4$ nouveaux réseaux.

<center>

| destination |	passerelle | interface | Nb sauts | remarques |
| :-: |	:-: | :-: | :-: | :-: |
| 172.16.0.0 	|      | eth$0$   | $1$ | |
| 172.16.1.0 	|      | eth$1$   | $1$ | |
| 172.16.6.0 	|      | eth$2$   | $1$ | |
| 172.16.3.0 	|      | eth$3$   | $1$ | |
| 192.168.1.0 | 172.16.0.1 | eth$0$   | $2$ | reçu de *R1* |
| 172.16.2.0 | 172.16.1.2 | eth$1$   | $2$ | reçu de *R2* |
| 172.16.5.0 | 172.16.6.5 | eth$2$   | $2$ | reçu de *R5* |
| 172.16.4.0 | 172.16.3.4 | eth$3$   | $2$ | reçu de *R4* |

</center>

### Étape 3

Comme vous le voyez, les tables deviennent vite longues et énumérer dans le détail chacune d'elle est trop long. On va donc passer directement à l'étape finale : l'étape $3$. Voici ce que contient la table de routage de *R1* :

<center>

| Destination | Passerelle | Interface | Nb sauts | Remarques |
| :-: |	:-: | :-: | :-: | :-: |
| 192.168.1.0 |      | wifi0 | 1 | ==> vers les postes de travail |
| 172.16.0.0 |      | eth0 | 1 | ==> vers R3 |
| 172.16.1.0 | 172.16.0.3 | eth0 | 2 | |
| 172.16.6.0 | 172.16.0.3 | eth0 | 2 | |
| 172.16.3.0 | 172.16.0.3 | eth0 | 2 | |
| 172.16.5.0 | 172.16.0.3 | eth0 | 3 | obtenu à l'étape 2 |
| 192.168.2.0 | 172.16.0.3 | eth0 | 4 | obtenu à l'étape 3 |

</center>

Comme vous le voyez, le routeur *R1* est à présent en capacité d'acheminer un paquet du poste de travail du réseau 1 vers le serveur se trouvant dans le réseau 2.

## A vous de jouer

### Scénario 1

Élaborez au fil du temps la table de routage du routeur R4 de manière similaire à ce que l'on vient de faire.

### Scénario 2 : Travail en groupe

* En constituant $6$ groupes dans la classe, vous élaborerez en suivant le protocole RIP votre table de routeur et l'échangerez au fur à mesure avec vos voisins afin de finaliser la table de routage du réseau.

* Vous simulerez la suppression du lien entre R3 et R5 et continuerez l’algorithme afin d'observer la mise à jour des tables de routage.

## RFCs, Références et Notes

### RFCs

Les spécifications du protocole RIP sont détaillées dans les RFC suivantes (en :gb:) :

<center>

| RFC | qui détaille .. | Date |
|:-:|:-:|:-:|
| [RFC 1058](https://datatracker.ietf.org/doc/html/rfc1058) En :fr: [ici](http://abcdrfc.free.fr/rfc-vf/pdf/rfc1058.pdf) | Routing Information Protocol | Juin 1988 |
| [RFC 1388](https://datatracker.ietf.org/doc/html/rfc1388) | RIP Version 2 Carrying<br/>Additional Information | Janvier 1993<br/>remplacé par<br/>RFC 1723|
| [RFC 1723](https://datatracker.ietf.org/doc/html/rfc1723) En :fr: [ici](http://abcdrfc.free.fr/rfc-vf/pdf/rfc2453.pdf) | RIP Version 2 - Carrying<br/>Additional Information | Novembre 1994<br/> remplacé par<br/> RFC 2453 |
| [RFC 2453](https://datatracker.ietf.org/doc/html/rfc2453) | RIP Version 2 | Novembre 1998<br/>mis à jour par<br/>RFC 4822 |
| [RFC 2082](https://datatracker.ietf.org/doc/html/rfc2082) | RIP-2 MD5 Authentication, IPSec | Janvier 1997<br/>remplacé par<br/> RFC 4822 |
| [RFC 4822](https://datatracker.ietf.org/doc/html/rfc4822) En :fr: [ici](http://abcdrfc.free.fr/rfc-vf/pdf/rfc4822.pdf) | RIPv2 Cryptographic<br/> Authentication | Février 2007<br/>remplace RFC2082<br/>met à jour RFC 2453 |

</center>

### Références

* [Algorithmes Distribués de plus court chemin, RIP & OSPF](https://www.lri.fr/~jcohen/documents/enseignement/CoursRoutage.pdf), Johanne Cohen, LORIA/CNRS, Nancy, France
* [Traduction de la RFC 1058, pdf, par Frédéric Delanoy](http://abcdrfc.free.fr/rfc-vf/pdf/rfc1058.pdf)
* [Portail Francophone de Traduction des RFC](http://abcdrfc.free.fr/)
* [Routage Dynamique RIPv2, Goffinet, CCNA](https://cisco.goffinet.org/ccna/rip/routage-dynamique-ripv2/)

### Notes

[^1]: RIP v1 (Hors-Programme, ATTENTION): [RFC 1058](https://datatracker.ietf.org/doc/html/rfc1058), 1988
[^2]: RIP v2 : [RFC 1388, Carrying Additional Information](https://datatracker.ietf.org/doc/html/rfc1388), 1993
[^3]: RIP v2 : [RFC 2453](https://datatracker.ietf.org/doc/html/rfc2453), 1998
[^4]: [RIP History, wikipedia](https://en.wikipedia.org/wiki/Routing_Information_Protocol)
[^5]: [Cours Introduction à TCP/IP, developpez.com, avec RIP et OSPF](https://laissus.developpez.com/tutoriels/cours-introduction-tcp-ip/?page=page_8)
