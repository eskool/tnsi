# TNSI - Exercices Protocole RIP & OSPF

## Exercice 1

Créer une fonction ```meme_sous_reseau(ip_a, ip_b, masque)``` qui renvoie un booléen indiquant si A et B sont partie du même sous-réseau.

Exercice à réaliser en Test Driven Developpement à partir du squelette de code ci-dessous, en testant chaque fonction après sa réalisation, jusqu'à la fonction finale.

```python
def convert_ip_to_list(ip):
    """
    entrée : ip (string) 
    sortie : liste d'entiers
    """
    # à vous

def test_convert_ip_to_list():
    assert convert_ip_to_list('192.168.0.1') == [192, 168, 0, 1]
    

def nb_to_binary_word(masque):
    """
    entrée : masque (int)
    sortie : string
    """
    # à vous
    
def test_nb_convert_to_binary_word():
    assert nb_to_binary_word(24) == '11111111111111111111111100000000'


def binary_word_to_list(word):
    """
    entrée : word (string de 32 caractères)
    sortie : liste de 4 entiers
    """
    # à vous


def test_binary_word_to_list():
    assert binary_word_to_list('11111111111111111111111100000000') == [255, 255, 255, 0]



def meme_sous_reseau(ip_a, ip_b, masque):
    """
    ip_a:  string contenant une IP (ex "192.168.0.1")
    ip_b : string contenant une IP
    masque : entier du masque en notation CIDR (ex : 24)
    renvoie un booléen indiquant si ip_a et ip_b sont dans
    le même sous-réseau
    """
    # à vous
    

def test_meme_sous_reseau():
    assert meme_sous_reseau("192.168.0.1", "192.168.1.3", 24) == False
    assert meme_sous_reseau("192.168.0.1", "192.168.1.3", 20) == True
    assert meme_sous_reseau("192.168.0.1", "192.168.0.3", 30) == True
```

??? tip "Correction"
    Exercice difficile, il n'est pas à savoir faire mais c'est bien de le comprendre !
    ```python
    def convert_ip_to_list(ip):
        """
        entrée : ip (string) 
        sortie : liste d'entiers
        """
        return [int(k) for k in ip.split(".")]

    def test_convert_ip_to_list():
        assert convert_ip_to_list('192.168.0.1') == [192, 168, 0, 1]

    def nb_to_binary_word(masque):
        """
        entrée : masque (int)
        sortie : string
        """
        return '1'*masque + '0'*(32-masque)

    def test_nb_convert_to_binary_word():
        assert nb_to_binary_word(24) == '11111111111111111111111100000000'

    def binary_word_to_list(word):
        """
        entrée : word (string de 32 caractères)
        sortie : liste de 4 entiers
        """
        decoupe = [word[8*i:8*(i+1)] for i in range(4)]
        return [int(k,2) for k in decoupe]

    def test_binary_word_to_list():
        assert binary_word_to_list('11111111111111111111111100000000') == [255, 255, 255, 0]

    def meme_sous_reseau(ip_a, ip_b, masque):
        lstA = convert_ip_to_list(ip_a)
        lstB = convert_ip_to_list(ip_b)
        mask = binary_word_to_list(nb_to_binary_word(masque))
        resA = [lstA[i] & mask[i] for i in range(4)]
        resB = [lstB[i] & mask[i] for i in range(4)]
        return resA == resB

    def test_meme_sous_reseau():
        assert meme_sous_reseau("192.168.0.1", "192.168.1.3", 24) == False
        assert meme_sous_reseau("192.168.0.1", "192.168.1.3", 20) == True
        assert meme_sous_reseau("192.168.0.1", "192.168.0.3", 30) == True
    ```

## Exercice 2 

<env>2020, sujet 0</env>

On considère un réseau composé de plusieurs routeurs reliés de la façon suivante :

<center>

```mermaid
flowchart LR
    A --- B
    A --- D
    B --- D
    A --- C
    C --- F
    C --- E
    D --- E
    F --- G
    E --- G
```

</center>

Le protocole RIP permet de construire les tables de routage des différents routeurs, en indiquant pour chaque routeur la distance, en nombre de sauts, qui le sépare d’un autre routeur. Pour le réseau ci-dessus, on dispose des tables de routage suivantes :

!!! col __50 center
    | Table<br/>Routeur A |||
    |:-:|:-:|:-:|
    | Destination | Routeur Suivant | Distance |
    | B | B | 1 |
    | C | C | 1 |
    | D | D | 1 |
    | E | C | 2 |
    | F | C | 2 |
    | G | C | 3 |

    <br/>

    | Table<br/>Routeur C |||
    |:-:|:-:|:-:|
    | Destination | Routeur Suivant | Distance |
    | A | A | 1 |
    | B | A | 2 |
    | D | E | 2 |
    | E | E | 1 |
    | F | F | 1 |
    | G | F | 2 |

    <br/>

    | Table<br/>Routeur E |||
    |:-:|:-:|:-:|
    | Destination | Routeur Suivant | Distance |
    | A | C | 2 |
    | B | D | 2 |
    | C | C | 1 |
    | D | D | 1 |
    | F | G | 2 |
    | G | G | 1 |

!!! col __50 center
    | Table<br/>Routeur B |||
    |:-:|:-:|:-:|
    | Destination | Routeur Suivant | Distance |
    | A | A | 1 |
    | C | A | 2 |
    | D | D | 1 |
    | E | D | 2 |
    | F | A | 3 |
    | G | D | 3 |

    <br/>

    | Table<br/>Routeur D |||
    |:-:|:-:|:-:|
    | Destination | Routeur Suivant | Distance |
    | A | A | 1 |
    | B | B | 1 |
    | C | E | 2 |
    | E | E | 1 |
    | F | A | 3 |
    | G | E | 2 |

    <br/>

    | Table<br/>Routeur F |||
    |:-:|:-:|:-:|
    | Destination | Routeur Suivant | Distance |
    | A | C | 2 |
    | B | C | 3 |
    | C | C | 1 |
    | D | C | 3 |
    | E | G | 2 |
    | G | G | 1 |

**Question 1**

1.a°) Le routeur A doit transmettre un message au routeur G, en effectuant un nombre minimal de
sauts. Déterminer le trajet parcouru.  
1.b°) Déterminer une table de routage possible pour le routeur G obtenu à l’aide du protocole RIP.

**Question 2**

Le routeur C tombe en panne. Reconstruire la table de routage du routeur A en suivant le
protocole RIP.


??? tip "Correction"
    **Q1.1.** Le trajet parcouru de A à G est A-C-F-G  
    **Q1.2.** 
    Table de routage de G :  

    <center>

    | Destination | Routeur suivant | Distance |
    |:--:|:--:|:--:|
    |A|F|3|    
    |B|E|3|
    |C|E|2|
    |D|E|2|
    |E|E|1|
    |F|F|1|
    
    </center>

    **Q2**  
    Nouvelle table de routage de A :  

    <center>
    
    | Destination | Routeur suivant | Distance |
    |:--:|:--:|:--:|
    |B|B|1|
    |D|D|1|
    |E|D|2|
    |G|D|3|
    |F|D|4|
    
    </center>

## Exercice 3

<env>2021, sujet Métropole 1</env>

On représente ci-dessous un réseau dans lequel R1, R2, R3, R4, R5 et R6 sont des routeurs. Le réseau local L1 est relié au routeur R1 et le réseau local L2 au routeur R6.

```mermaid
flowchart LR
    L1-- 192.168.1.0/24 --- R1
    R1-- 86.154.10.0/24 --- R2
    R1-- 112.44.65.0/24 --- R3
    R3-- 62.34.2.0/24 --- R4
    R2-- 212.194.171.0/24 --- R4
    R2-- 176.139.8.0/24 --- R3
    R2-- 10.94.75.0/24 --- R5
    R2-- 37.49.236.0/24 --- R6
    R4-- 87.3.5.0/24 --- R5
    R4-- 94.23.122.0/24 --- R6
    R5-- 218.32.15.0/24 --- R6
    R6-- 54.37.122.0/24 --- L2
```

![image](./img/bac1.png){: .center width=70%}


Dans cet exercice, les adresses IP sont composées de 4 octets, soit 32 bits. Elles sont notées X1.X2.X3.X4, où X1, X2, X3 et X4 sont les valeurs des 4 octets, convertis en notation décimale.
La notation X1.X2.X3.X4/n signifie que les n premiers bits de poids forts de l’adresse IP représentent la partie « réseau », les bits suivants représentent la partie « hôte ».
Toutes les adresses des hôtes connectés à un réseau local ont la même partie réseau et peuvent donc communiquer directement. L’adresse IP dont tous les bits de la partie « hôte » sont à 0 est appelée « adresse du réseau ».

On donne également des extraits de la table de routage des routeurs R1 à R5 dans le tableau suivant :

![image](./img/bac2.png){: .center width=70%}

**Question 1**

Un paquet part du réseau local L1 à destination du réseau local L2.

1.a. En utilisant l’extrait de la table de routage de R1, vers quel routeur R1 envoie-t-il ce paquet : R2 ou R3 ? Justifier.

1.b. A l’aide des extraits de tables de routage ci-dessus, nommer les routeurs traversés par ce paquet, lorsqu’il va du réseau L1 au réseau L2.

**Question 2**

La liaison entre R1 et R2 est rompue.

2.a. Sachant que ce réseau utilise le protocole RIP (distance en nombre de sauts), donner l’un des deux chemins possibles que pourra suivre un paquet allant de L1 vers L2.

2.b. Dans les extraits de tables de routage ci-dessus, pour le chemin de la question 2.a, quelle(s) ligne(s) sera (seront) modifiée(s) ?

**Question 3**

On a rétabli la liaison entre R1 et R2.
Par ailleurs, pour tenir compte du débit des liaisons, on décide d’utiliser le
protocole OSPF (distance liée au coût minimal des liaisons) pour effectuer le
routage. Le coût des liaisons entre les routeurs est donné par le tableau suivant :

<center>

|Liaison|R1-R2|R1-R3|R2-R3|R2-R4|R2-R5|R2-R6|R3-R4|R4-R5|R4-R6|R5-R6|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|Coût|100|100|?|1|10|10|10|1|10|1|

</center>

![image](./img/bac3.png){: .center width=90%}

3.a. Le coût _C_ d’une liaison est donné ici par la formule
$C = \dfrac{10^9}{BP}$

où $BP$ est la bande passante de la connexion en bps (bits par seconde).
Sachant que la bande passante de la liaison R2-R3 est de 10 Mbps, calculer le coût correspondant.


3.b. Déterminer le chemin parcouru par un paquet partant du réseau L1 et arrivant au réseau L2, en utilisant le protocole OSPF.

3.c. Indiquer pour quel(s) routeur(s) l’extrait de la table de routage sera modifié pour un paquet à destination de L2, avec la métrique OSPF.

??? tip "Correction"
    1.a. D'après la table, R1 doit passer par la passerelle 86.154.10.1 qui correspond au routeur R2.  
    1.b. Le paquet va traverser R1, R2, R6 avant d'arriver à L2.  
    2.a. RIP doit minimiser le nombre de sauts, donc les deux chemins minimaux possibles sont R1-R3-R4-R6 et R1-R3-R2-R6.  
    2.b. La ligne R1 sera modifiée, il faudra partir vers R3 (et son réseau 112.44.65.0/24). Les autres lignes n'ont pas à être modifiées puisque R3 amène en R4 qui amène en R6.  
    3.a $\dfrac{10^9}{10 \times 10^6}=100$ donc le coût R2-R3 est 100.  
    3.b. Avec OSPF, le chemin qui minimise le coût est le chemin R1-R2-R4-R5-R6 (coût 103) :
    ![image](./img/bac1_corr.png){: .center width=50%}
    3.c. Dans la table de routage initiale, il faut modifier R2 pour qu'elle envoie sur R4 (et non sur R6), mais aussi R4 pour qu'elle envoie sur R5 (et non sur R6).

## Exercice 4

<env>2021, Sujet Métropole 2</env>

![](./img/ex3_1.png){.center}
<center>
Figure 1
</center>

La figure 1 ci-dessus représente le schéma d’un réseau d’entreprise. Il y figure deux réseaux locaux L1 et L2. Ces deux réseaux locaux sont interconnectés par les routeurs R2, R3, R4 et R5. Le réseau local L1 est constitué des PC portables P1 et P2 connectés à la passerelle R1 par le switch Sw1. Les serveurs S1 et S2 sont connectés à la passerelle R6 par le switch Sw2.

Le tableau 1 suivant indique les adresses IPv4 des machines constituants le réseau de l’entreprise.

!!! note ":warning: À lire impérativement :warning:"
    J'ai eu la confirmation que cet exercice proposait à tort une adresse de passerelle y compris lorsque le routeur accédait directement au réseau.  
    Les tableaux ci-dessous ont donc été **modifiés** par rapport à ce qui a été fait en classe.  
    Les adresses de passerelles n'apparaissent maintenant que quand elles sont réellement nécessaires. 

    Je rappelle la définition d'une adresse de passerelle :  
    :heart: C'est une adresse vers laquelle un ordinateur (ou un routeur) va envoyer un paquet **dont il ne sait pas quoi faire** parce que son adresse de destination ne fait pas partie de son sous-réseau. On dit qu'on «confie» le paquet à un autre routeur afin que celui-ci l'achemine vers la bonne destination. :heart:

<center>

|Nom|Type|Adresse IPv4|
|:-:|:-:|:-:|
|R1|Routeur|Interface 1: 192.168.1.1/24<br/>Interface 2: 10.1.1.2/24|
|R2|Routeur|Interface 1: 10.1.1.1/24<br/>Interface 2: 10.1.2.1/24<br/>Interface 3: 10.1.3.1/24|
|R3|Routeur|Interface 1: 10.1.2.2/24<br/>Interface 2: 10.1.4.2/24<br/>Interface 3: 10.1.5.2/24|
|R4|Routeur|Interface 1: 10.1.5.1/24<br/>Interface 2: 10.1.6.1/24|
|R5|Routeur|Interface 1: 10.1.3.2/24<br/>Interface 2: 10.1.4.1/24<br/>Interface 3: 10.1.6.2/24<br/>Interface 4: 10.1.7.1/24|
|R6|Routeur|Interface 1: 172.16.0.1/16<br/>Interface 2: 10.1.7.2/24|
|P1|Ordinateur<br/>Portable|192.168.1.40/24|
|P2|Ordinateur<br/>Portable|192.168.1.46/24|
|S1|Serveur|172.16.8.10/16|
|S2|Serveur|172.16.9.12/16|

Tableau 1 : adresses IPv4 des machines
</center>

**Rappels et notations**

Rappelons qu’une adresse IP est composée de 4 octets, soit 32 bits. Elle est notée
X1.X2.X3.X4, où X1, X2, X3 et X4 sont les valeurs des 4 octets. Dans le tableau 1, les valeurs des 4 octets ont été converties en notation décimale.

La notation X1.X2.X3.X4/n signifie que les n premiers bits de poids forts de l’adresse IP représentent la partie « réseau », les bits suivants de poids faibles représentent la partie « machine ».

Toutes les adresses des machines connectées à un réseau local ont la même partie réseau.
L’adresse IP dont tous les bits de la partie « machine » sont à 0 est appelée « adresse du réseau ».
L’adresse IP dont tous les bits de la partie « machine » sont à 1 est appelée « adresse de diffusion ».

**Question 1**

1.a. Quelles sont les adresses des réseaux locaux L1 et L2 ?

1.b. Donner la plus petite et la plus grande adresse IP valides pouvant être attribuées à un ordinateur portable ou un serveur sur chacun des réseaux L1 et L2 sachant que l’adresse du réseau et l’adresse de diffusion ne peuvent pas être attribuées à une machine.

1.c. Combien de machines peut-on connecter au maximum à chacun des réseaux locaux L1
et L2 ? 

**Question 2**

2.a. Expliquer l’utilité d’avoir plusieurs chemins possibles reliant les réseaux L1 et L2.

2.b. Quel est le chemin le plus court en nombre de sauts pour relier R1 et R6 ? Donner le nombre de sauts de ce chemin et préciser les routeurs utilisés.

2.c. La bande passante d’une liaison Ether (quantité d’information qui peut être transmise en bits/s) est de $10^7$ bits/s et celle d’une liaison FastEther est de $10^8$ bits/s. Le coût d’une liaison est défini par $\frac{10^8}{d}$ , où $d$ est sa bande passante en bits/s.

| Liaison | R1-R2 | R2-R5 | R5-R6 | R2-R3 | R3-R4 | R4-R5 | R3-R5 |
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| Type | Ether | Ether | Ether | FastEther | FastEther | FastEther | Ether |

<center>
Tableau 2 : type des liaisons entre les routeurs
</center>

Quel est le chemin reliant R1 et R6 qui a le plus petit coût ? Donner le coût de ce chemin et préciser les routeurs utilisés.

**Question 3**

Dans l’annexe A ci-dessous figurent les tables de routages des routeurs R1, R2, R5 et R6 au
démarrage du réseau. Indiquer sur votre copie ce qui doit figurer dans les lignes laissées vides des tables de routage des routeurs R5 et R6 pour que les échanges entre les ordinateurs des réseaux L1 et L2 se fassent en empruntant le chemin le plus court en nombre de sauts.

<env>Annexe A: Tables de Routage du réseau de la figure 2</env>

<env>R1</env>

<center>

|IP Réseau<br/>de Destination|Passerelle|Interface|
|:-:|:-:|:-:|
|192.168.1.0/24| - |Interface 1|
|10.1.1.0/24| - |Interface 2|

</center>

<env>R2</env>

<center>

|IP Réseau<br/>de Destination|Passerelle|Interface|
|:-:|:-:|:-:|
|10.1.1.0/24| - |Interface 1|
|10.1.2.0/24| - |Interface 2|
|10.1.3.0/24| - |Interface 3|
|192.168.1.0/24| 10.1.1.2 |Interface 1|
|172.16.0.0/24| 10.1.3.2 |Interface 3|

</center>

<env>R5</env>

<center>

|IP Réseau<br/>de Destination|Passerelle|Interface|
|:-:|:-:|:-:|
|10.1.3.0/24| - |Interface 1|
|10.1.4.0/24| - |Interface 2|
|10.1.6.0/24| - |Interface 3|
|10.1.7.0/24| - |Interface 4|

</center>

<env>R6</env>

<center>

|IP Réseau<br/>de Destination|Passerelle|Interface|
|:-:|:-:|:-:|
|172.16.0.0/16| - |Interface 1|

</center>

??? tip "Correction"
    1.a L'adresse du réseau L1 est 192.168.1.0/24. L'adresse de L2 est 175.6.0.0/16.   
    1.b Pour le réseau L1 (192.168.1.0/24), l'adresse min est 192.168.1.1/24, l'adresse max est 192.168.1.254/24.  
    Pour le réseau L2 (175.6.0.0/16), l'adresse min est 175.6.0.1/16 et l'adresse max est 175.6.255.254/16  
    1.c. Pour le réseau L1, il y a donc 254 adresses (256 moins les deux interdites)  
    Pour le réseau L2, il y en a $256^2-2$, soit 65534.

    2.a Il est utile d'avoir plusieurs chemins en cas de panne d'un routeur.  
    2.b En nombres de sauts (protocole RIP), le chemin le plus court est R1-R2-R5-R6, qui contient 3 sauts.  
    2.c Les liaisons Ether ont un coût de 10, les liaisons FastEther ont un coût de 1. Ce qui donne :
    ![image](./img/ex3_1_corr.png){: .center width=50%}
    Le chemin le plus court est donc R1-R2-R3-R4-R5-R6, avec un coût total de 23.

    3. On veut que le chemin soit le plus court en nombre de sauts, donc il faut que le chemin soit R1-R2-R5-R6.

    ![image](./img/ex3_1_corr3.png){: .center width=70%}
    Dans la table R5, il manque les lignes

    <center>

    | IP destination | Passerelle | Interface|
    |:--:|:--:|:--:|
    |192.168.1.0/24|10.1.3.1|Interface 1|
    |172.16.0.0/16|10.1.7.2|Interface 4|  
    
    </center>

     Dans la table R6, on peut compléter comme ceci (il faudrait des lignes supplémentaires pour y inscrire tous les réseaux)

    <center>

    | IP destination | Passerelle | Interface|
    |:--:|:--:|:--:|
    |10.1.7.0/24| |Interface 2|
    |192.168.1.0/24|10.1.7.1|Interface 2|

    </center>

## Exercice 5

<env>2021, Sujet Amérique du Nord</env>

Un constructeur automobile possède six sites de production qui échangent des documents entre eux. Les sites de production sont reliés entre eux par six routeurs A, B, C, D, E et F.  
On donne ci-dessous les tables de routage des routeurs A à F obtenues avec le protocole RIP.

![image](./img/tabAN.png){: .center}

1. Déterminer à l'aide de ces tables le chemin emprunté par un paquet de données envoyé du routeur A vers le routeur F.
2. On veut représenter schématiquement le réseau de routeur à partir des tables de routage. 
Recopier sur la copie le schéma ci-dessous : 

![image](./img/graphAN.png){: .center}

En s'appuyant sur les tables de routage, tracer les liaisons entre les routeurs.

??? tip "Correction"
    1. A-B-E-F  
    2.
    ![image](./img/graphAN_corr.png){: .center}
    

