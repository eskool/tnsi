# TNSI : Intro aux Protocoles de Routage (Interne)

## Introduction

Commençons par une petite vidéo introductive expliquant pourquoi et comment les routeurs communiquent entre eux afin de déterminer le meilleur itinéraire à faire emprunter aux paquets dont ils ont la charge: 

<center>
<iframe width="560" height="315" src="https://www.youtube.com/embed/sT9-IcbjqzI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Claude Chaudet, Maître de Conférence IMT (Institut Mines-Télécom), ($6$ min $30$)
</center>

## Je retiens

Un **paquet de données** qui traverse un réseau n'a **à priori** aucune idée du chemin qu'il va devoir suivre. Il fait entièrement confiance aux indications fournies par les routeurs, espérant que celles-ci soient les plus fiables possibles.

Il revient au routeur, et plus précisément à ses **protocoles de routage**, de:

* Déterminer les **meilleurs chemins/routes** dans le réseau (en particulier le saut suivant/ next hop), depuis l'hôte source jusqu'au réseau/hôte destination. 
    * Le protocole de routage doit prendre une décision en ne connaissant que:
        * l'IP de destination du paquet
        * les informations situées dans la table de routage
    * Des protocoles de routage différents peuvent utiliser des algorithmes différents pour déterminer le meilleur chemin (les réponses peuvent donc varier)
    * Choisir quels paramètres sont intéressants/retenus, ou pas, pour déterminer les meilleurs chemins: des protocoles de routage différents utilisent des critères différents (à priori)
* Éviter les boucles et trous noirs
* Stocker et Maintenir les Routes ouvertes
* Stocker et Maintenir les Routes fermées (en maintenance, etc..)
* Stocker et Maintenir les Pannes
* Stocker et Maintenir les Nouvelles Liaisons
