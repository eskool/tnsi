# TNSI : Comparaison RIP vs OSPF

Voici une Comparaison de RIP avec OSPF (référence [^1]) :

## Résumé RIP

La rapide croissance et l’expansion des réseau aujourd’hui a poussé RIP vers ses limites. RIP a des limitations qui peuvent poser des problèmes dans les réseaux étendus.

* RIP est limité à 15 sauts. Un réseau RIP dont la distance dépasse 15 sauts (15 routeurs) est considéré comme non joignable (16 désigne l'infini)
* RIP est simple à configurer, mais gère uniquement les petits réseaux (15 routeurs)
* RIP n'a qu'une vision locale, partielle du réseau
* RIP utilise l'Algorithme de **Bellman-Ford**
* RIP v2 gère les masques de sous-réseaux de longueur variable (VSLM). Mais pas RIP v1. Permettant la réduction des adresses IP et la flexibilité, les VSLM permettent d’assigner les adresses IP plus efficacement, ce qui est considéré comme un défaut majeur de RIP v1
* Les broadcasts périodiques de la totalité des tables de routage consomment une grande partie de la bande passante. C’est un problème important dans les réseaux étendus, spécialement sur les liaisons de faibles débits et dans les WANS,
* RIP converge plus lentement qu’OSPF. Dans les réseaux étendus, le temps de convergences peut atteindre plusieurs minutes ce qui incompatible dans les environnements étendus et pourrait provoquer des routages erronés,
* RIP n’a pas de concept de **coût de liaison** (par ex. le débit de chaque ligne). Les décisions sont basées sur le nombre de sauts uniquement. Le chemin avec le nombre de sauts le plus faible vers la destination est toujours préféré même si le chemin le plus long a une meilleure bande passante.
* RIP ne gère pas les concepts d’aire et de frontière. Avec l’introduction du routage sans classe et l’utilisation de l’agrégation et de la summarization, les réseaux RIP sont dépassés.

## Résumé OSPF

OSPF, par contre, résout la plupart des limitations de RIP :

* Avec OSPF, il n’y pas de limitations du nombre de sauts.
* OSPF est utilisé pour gérer les grands réseaux
* OSPF a une vision globale de la totalité du réseau
* OSPF utilise l'Algorithme de **Dijkstra**
* L’utilisation des VLSM est très utile dans l’attribution des adresses IP,
* OSPF utilise les adresses de multicast pour envoyer les updates de link-state. Cela permet diminuer les traitements sur les routeurs qui n’écoute pas les paquets OSPF. De plus, les updates sont envoyés que suite à des changements, au lieu d’être envoyés périodiquement. Cela permet une meilleure utilisation de la bande passante,
* OSPF a une meilleure convergence que RIP. Cela est dû au fait que les changements sont propagés instantanément et non périodiquement,
* OSPF permet une meilleure gestion du partage de charge,
* OSPF permet une définition logique des réseaux dans laquelle les routeurs peuvent être divisés en **aires**. Cela limite l’explosion des mises à jour de l'état des liens sur la totalité du réseau. Cela permet aussi un mécanisme d’agrégation des routes et supprimer la propagation d’information sur les sous réseaux,
* OSPF permet d’authentifier le routage en utilisant différentes méthodes d’authentification par mot de passe,
* OSPF permet le transfert et le marquage des routes externes injectées dans un système autonome. Cela permet de garder la trace des routes externes apprises par des protocoles de routage externes tels que BGP,

## Tableau de Comparaison

<center>

| Vecteur de Distance (RIP) |	État de Lien (OSPF) |
| :-: |	:-: |
| Algorithme **Bellman-Ford** | Algorithme **Dijkstra** |
| Facile à configurer | Compétences requises|
| Partage des Tables de Routage | Partage des Liaisons |
| Réseaux plats | Réseaux conçus (design) <br/>organisés en areas/zones |
| Convergence plus lente | Convergence rapide, répartition de charge |
| Topologies limitées | Topologies complexes et larges |
| Gourmand en bande passante | Relativement discret |
| Peu consommateur en RAM et CPU | Gourmand en RAM et CPU |
| Mises à jour régulière en Broadcast/Multicast | Mises à jour immédiate |
| Pas de signalisation | Signalisation fiable et en mode connecté |
| RIPv1 - UDP 520 - 255.255.255.255,<br/>RIPv2 - UDP 520 - 224.0.0.9,<br/>EIGRP - Cisco Systems (DUAL) <br/>- 224.0.0.10 - FF02::A |	OSPFv2 (resp. v3) - IP89 - 224.0.0.5,<br/> 224.0.0.6, (resp. FF02::5, FF02::6) IS-IS |

</center>

## Références & Notes

[^1]: Cette page est inspirée de [wapiti.enic.fr](http://wapiti.enic.fr/commun/ens/peda/options/ST/RIO/pub/exposes/exposesrio2000/LheliasBouvier/igp.htm)