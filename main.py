from os import listdir
from os.path import dirname, isfile, join, isdir
import datetime

YEAR = datetime.date.today().year

def define_env(env):
    "Hook function"

    @env.macro
    def basthon(exo: str, hauteur: int) -> str:
        "Renvoie du HTML pour embarquer un fichier `exo` dans Basthon"
        return f"""<iframe src="https://console.basthon.fr/?from={env.variables.io_url}{env.variables.page.url}../{exo}" width="100%;" height="{hauteur};"></iframe>

[Lien dans une autre page](https://console.basthon.fr/?from={env.variables.io_url}{env.variables.page.url}../{exo})
"""

    @env.macro
    def affiche_script(lang: str, nom: str) -> str:
        "Renvoie le script (chemin absolu) dans une balise bloc avec langage spécifié"
        chemin = f"""```{lang}
--8<---  "docs/{nom}"
```"""
        return chemin

    @env.macro
    def affichePDF(nom: str) -> str:
        """Renvoie le pdf dans une balise embed"""
        return f"""<embed src="{nom[:-4]}.pdf" width="100%" height="800" type="application/pdf">"""

    @env.macro
    def epreuves_ecrites(dirpath:str="epreuves_ecrites", pdfHeight:str="800px",pdfWidth:str="100%") -> str:
        """Renvoie le script dans une balise bloc avec langage spécifié
        Ajouter des dossiers PAR ANNÉE dans le dossier relatif 'dirpath' : 2021, 2022, ..
        """
        # GET LIST OF YEARS
        dirpath0 = f"docs/ressources/annales/{dirpath}"
        listOfYears = [f for f in listdir(dirpath0) if isdir(join(dirpath0, f))]
        listOfYears.sort()
        listOfYears.reverse()

        # BUILD RETURN STRING
        chaine = ""
        for annee in listOfYears:
            chaine += f"""## {annee}
  
"""
            dirpath1 = f"docs/ressources/annales/{dirpath}/{annee}"
            # les liens PDFs sont relatifs
            lstrip_dirpath1 = f"../{dirpath}/{annee}" # only 'annales/epreuves_ecrites/annee/'
            # because os.listdir() lists all Files AND Directories
            listOfFilesPDF = [f for f in listdir(dirpath1) if ((isfile(join(dirpath1, f))) and (f[-4:] == ".pdf"))]
            listOfFilesPDF.sort()

            for nomFichier in listOfFilesPDF:
                chaine += f"""### {nomFichier[:-4]}
  
<embed src="{lstrip_dirpath1}/{nomFichier[:-4]}.pdf" width="{pdfWidth}" height="{pdfHeight}" type="application/pdf">
"""
        return chaine

    @env.macro
    def epreuves_pratiques(dirpath:str="epreuves_pratiques", pdfHeight:str="800px",pdfWidth:str="100%") -> str:
        """Renvoie le script dans une balise bloc avec langage spécifié
        Ajouter des dossiers PAR ANNÉE dans le dossier relatif 'dirpath' : 2021, 2022, ..
        """
        # GET LIST OF YEARS
        dirpath0 = f"docs/ressources/annales/{dirpath}"
        listOfYears = [f for f in listdir(dirpath0) if isdir(join(dirpath0, f))]
        listOfYears.sort()
        listOfYears.reverse()

        # BUILD RETURN STRING
        chaine = ""
        for annee in listOfYears:
            dirpath1 = f"docs/ressources/annales/{dirpath}/{annee}"            
            chaine += f"""## {annee}
  
Télécharger Toute l'année {annee}: <a href="../{dirpath}/{annee}/{annee}_epreuves_pratiques_nsi.zip">{annee}_epreuves_pratiques_nsi.zip</a>
  
"""
            # because os.listdir() lists all Files AND Directories
            listOfFilesPDF = [f for f in listdir(dirpath1) if ((isfile(join(dirpath1, f))) and (f[-4:] == ".pdf"))]
            listOfFilesPDF.sort()

            for nomFichier in listOfFilesPDF:
                chaine += f"""### {nomFichier[:-4]}
  
<embed src="{annee}/{nomFichier[:-4]}.pdf" width="{pdfWidth}" height="{pdfHeight}" type="application/pdf">
  
```python
--8<---  "{dirpath1}/{nomFichier[:-4]}.py"
```  
  
"""
        # print("chaine=", chaine)
        return chaine


    # @env.macro
    # def py(nom: str) -> str:
    #     "macro python rapide"
    #     return script('python', "scripts/" + nom + ".py")

    @env.macro
    def html_fig(num: int) -> str:
        "Renvoie le code HTML de la figure n° `num`"
        return f'--8<-- "docs/' + os.path.dirname(env.variables.page.url.rstrip('/')) + f'/figures/fig_{num}.html"'
