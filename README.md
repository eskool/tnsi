# Bienvenue sur le Projet eSkool ! Le Libre pour l'Éducation!

Vous cherchez la page d'accueil du site ? Elle est disponible sur [cette page](https://eskool.gitlab.io/tnsi/)

## Vous aimez ce projet? Vous croyez en une Éducation Libre ?

### &rarr; Soutenez-nous :

* en ajoutant des étoiles :star: :star: :star: à [CE dépôt GitLab](https://gitlab.com/eskool/accueil/) : çà motive! :smile: **je follow les starrers**
* Follow me ([@rod2ik](https://gitlab.com/rod2ik) ou [@rodrigo.schwencke](https://gitlab.com/rodrigo.schwencke)) sur GitLab! **Je follow back!**
* Ajoutez des étoiles à chaque dépôt GitLab, correspondant à chaque discipline, par niveau! ça motive aussi ! :smile:
    * NSI :
        * Terminale : https://gitlab.com/eskool/tnsi
        * 1ère: https://gitlab.com/eskool/1nsi
    * Histoire:
        * Terminale : https://gitlab.com/eskool/thistoire
        * 1ère: https://gitlab.com/eskool/1histoire
        * 2nde: https://gitlab.com/eskool/2histoire
    * Géographie:
        * Terminale : https://gitlab.com/eskool/tgeographie
        * 1ère: https://gitlab.com/eskool/1geographie
        * 2nde: https://gitlab.com/eskool/2geographie

* Faites nous connaître auprès de vos proches

## Vous souhaitez nous aider? 

En acceptant de publier des contenus pour votre discipline, sous [Licence CC-BY-NC-SA (CC - Creative Commons, BY- Citation de l'auteur, NC - Non Commercial, SA - Share Alike - Partage dans les Mêmes Conditions)](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr)?

<center><a href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr"><img src="https://upload.wikimedia.org/wikipedia/commons/c/ce/Cc-by-nc-sa_euro_icon.svg" style="width:100px;height:auto;"/></a></center>

### &rarr; Contactez-nous (&#x1F4E7; email : contact.eskool@gmail.com)! 

Nous serons heureux de travailler avec vous, et tâcherons de développer des fonctionnalités adaptées à votre discipline !
